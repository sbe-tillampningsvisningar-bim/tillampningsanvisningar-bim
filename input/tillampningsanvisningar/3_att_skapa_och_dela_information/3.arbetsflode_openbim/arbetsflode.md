
# Process för informationsleveranser med IFC

Här beskrivs både det övergripande arbetsflödet i openBIM-processen enligt buildingSMART, och en detaljerad beskrivning av arbetsflödet vid informationsleveranser med IFC.

## Det övergripande arbetsflödet i openBIM-processen

buildingSMART beskriver arbetsflödet i openBIM med hjälp av figuren nedan.

![Arbetsflöde openBIM](../3.arbetsflode_openbim/media/openbim_swe.jpg)

#### 1. Behov (Need)

Informationsbehovet inom organisationen identifieras.

buildingSMART har tagit fram exempel på ett antal användarfall. Dessa presenteras på deras hemsida under ”Use Case Management” – UCM (se länk)

#### 2. Kravställa (Require)

Utifrån organisationens och tillgångsförvaltningens informationsbehov definierar uppdragsgivaren kraven för projektet. Vid beskrivning av informationsbehovet och kraven är det viktigt att använda samma språk, samma definitioner och samma processer. Som stöd för detta används ”buildingSMART Data Dictionary” (bSDD),se länk.


IDS är det verktyg som ska användas för att beskriva och definiera kraven på den data som hanteras via IFC. Med hjälp av IDS kan informationskraven både beskrivas och verifieras (se länk till IDS hos buildingSMART).



#### 3. Skapa (Produce)

I tredje steget ska data skapas och bearbetas, Programvarorna som används för detta ska kunna hantera openBIM-standarder. Även här är bSDD ett stöd i hanteringen för att få en enhetlig och gemensam begreppsvärld.

Information som skapas i modeller exporteras till IFC för att data ska kunna hanteras på samma sätt oavsett vilket modelleringsverktyg som använts.


Informationen som skapas ska kontrolleras och valideras innan den delas med andra. IFC-filen valideras mot kravställningen, IDSen. bildingSMART tillhandahåller ett verktyg för validering av data ”IFC Validation Service”.


#### 4. Säkra (Check)

Efter att egenkontrollen gjorts och informationen validerats ska den granskas av mottagande part. Resultatet av granskningen kommuniceras via BCF (BIM Collaboration Format - se länk till buildingSMART).


#### 5. Dela (Deliver)

Efter godkänd granskning kan informationen delas med övriga i projektet. Genom att lägga informationen i en gemensam datamiljö (CDE) kan var och en hämta information när den behövs.openCDE är en del i openBIM:s workflow för delad information och enkelt samarbete. Inom buildingSMART arbetar man på att ta fram en sådan miljö.

## Detaljerad beskrivning av arbetsflödet i openBIM

Figuren nedan beskriver en mer detaljerad bild över arbetsflödet för informationsleveranser baserade på en rad openBIM-standarder.

![Informationsleverans](../3.arbetsflode_openbim/media/informationsleverans.png)

Processen för en informationsleverans utgår från att det finns en uppdragsgivare som skapar en kravställning och en uppdragstagare som skapar och levererar informationen. I arbetsflödet ingår både granskning och validering av informationen. I processen finns flera olika roller med ansvar för att informationen och informationsflödet följer kravställningen.

### Uppdragsgivare och uppdragstagare

För varje leverans finns en uppdragsgivare som kravställer vad leveransen ska innehålla. Uppdragsgivaren är också informationsmottagare av leveransen. Kravställningen kan göras på flera sätt, men genom att använda IDS, kan kontroll och verifiering av mottagen information lättare göras av både uppdragsgivaren och uppdragstagaren.

Uppdragsgivaren är informationsmottagare av slutleveransen. Det är framför allt här som IDS:en har en viktig funktion. Den information som levereras efter ett genomfört projekt måste kunna kontrolleras och verifieras utifrån den information som kravställts.

Men under projektets gång görs även flera ”mindre” leveranser där arbetsflödet kan tillämpas. Ett exempel är om arkitekten i ett skede under projekteringen är uppdragsgivare av information som berör innertaket. Vad arkitekten vill göra är att se till att innertaket blir så funktionellt och samtidigt utseendemässigt bra som möjligt. Alla aktörer som har objekt placerade så att innertaket påverkas är då uppdragstagare. El, ventilation och sprinkler är troliga uppdragstagare, som ska skicka information till arkitekten. Även konstruktören kan vara det. Informationen som arkitekten mottar kommer troligen att bland annat innehålla brandsensorer, tilluftsdon, sprinklerhuvuden och eventuellt någon balk eller pelare. I denna typ av delleveranser mellan olika aktörer inom projektet är inte IDS det primära formatet för kravställning, men IDS:en kan innehålla delar som kan användas vid mindre leveranser inom projektet.

## Beskrivning av arbetsflöde openBIM

#### 1. Skapa kravställning [Uppdragsgivaren]

Uppdragsgivaren formulerar krav utifrån organisationens informationskrav (OIR) och tillgångsinformationskrav (AIR). Dessa utgör grund för projektets informationskrav (PIR). OIR, AIR och PIR beskrivs mer ingående i informationen om ISO 19650. Vilken information som sedan ska levereras vid varje informationsutbyte fastställs i informationsutbyteskrav (EIR).

Kraven beskrivs, utifrån tre aspekter:

-   geometrisk information
-   alfanumerisk information
-   dokumentation.

Dessa tre aspekter baseras på SS-EN ISO 7817-1:2024 *Byggnadsinformationsmodellering – Nivåer för informationsbehov – Del 1: Begrepp och principer*,

Den geometriska och alfanumeriska informationen kan beskrivas via IDS. Övrig dokumentation kan bestå av produktdatablad, instruktioner för drift och underhåll och mycket annat.

I detta moment ska alltså en IDS skapas.

#### 2. Leverera kravställning [Uppdragsgivaren]

Kravställningen levereras och görs tillgänglig för uppdragstagarna genom att den sparas i en gemensam datamiljö (CDE), där både uppdragsgivare och uppdragstagare kan hämta och lämna information. IDS:en får status ”Publicerad” då den från och med nu kan användas i olika sammanhang. I svensk kontext rekommenderas dock att använda statusbeteckningar enligt SS 32209, där “publicerad” motsvaras av “Preliminär”.

#### 3. Mottagning och analys av kravställning [Uppdragstagaren]

Uppdragstagaren startar sin process genom att gå igenom informationsutbyteskraven (EIR).

-   Läs igenom dokumentationen.
-   Gå igenom IDS.
-   Gör bedömning om tänkt programvara för modellering kan användas för att tillgodose kraven.

 Specifikationen för informationsleveransen, som skapas enligt IDS-formatet, innehåller en del som är mänskligt tolkningsbar. Det innebär att utföraren av den kommande informationsleveransen kan läsa och förstå kraven för att omsätta dessa till information i en 3D CAD-modell. IDS:en innehåller även en del som kan tolkas av datorer, vilket gör att hanteringen vid validering av informationen i IFC-filen kan automatiseras.

Utbytesformatet förutsätts vara IFC, då detta ger en större frihet vid val av modelleringsprogram. Val av program och verktyg kan dock kravställas av uppdragsgivaren. Sådana krav görs enligt dokumentet ”3.14 BIM-föreskrifter för projekt”.

Om sådan krav inte ställs ska val av program och verktyg styras upp gemensamt inom projektet och dokumenteras i ”Genomförandeplan för BIM”. Denna uppstyrning innebär inte att alla måste använda samma programvara. Det handlar istället om att samordna utbytet av information.

Uppdragstagaren ska kontrollera att valt program har funktioner för att exportera och importera korrekt information till IFC, eller att kunna länka IFC-filer. Kontrollera även att kravställd information lagras på rätt plats i IFC enligt kravställningen.

#### 4. Skapa BIM-modell [Uppdragstagaren]

När kraven är fastställda och ett lämpligt modelleringsprogram har valts skapar uppdragstagaren sin CAD-modell.

#### 5. Bearbeta CAD-modell [Uppdragstagaren]

Modellen byggs successivt upp och fylls på med information under projekteringen. Information som läggs in struktureras enligt kravställningen.

#### 6. Kontrollera CAD-modell [Uppdragstagaren]

Precis som med allt material som levereras ska egenkontroll göras innan leverans av IFC-modell. Bland annat ska ”Header” i IFC-filen kontrolleras så att den innehåller den information som beskriver IFC-filens innehåll på rätt sätt.

#### 7. Skapa IFC-modell [Uppdragstagaren]

IFC-modellen skapas via en funktion i modelleringsprogrammet. Välj rätt version för IFC vid exporten.

#### 8. Validera/kontrollera IFC-modell [Uppdragstagaren]

Uppdragstagaren kontrollerar IFC-modellen innan den levereras för granskning. Genom att använda IDS:en säkerställs att kravställd information har validerats. Felaktigheter kan antingen finnas i CAD-modellen eller göras om inställningarna vid exporten inte är korrekta.

##### *8.1 OK? Nej*

Gå tillbaka och kontrollera CAD-modellen. Justera och exportera till IFC. Kontrollera IFC-modellen mot IDS:en igen. Om det fortfarande finns felaktigheter kan inställningarna vid IFC-exporten granskas.

##### *8.2 OK? Ja*

Genomför leverans.

#### 9. Levererans av IFC-modell för granskning [Uppdragstagaren]

IFC-modellen levereras till CDE och ges status ”Delad”, som i svensk kontext bör vara “För granskning”. Det innebär att informationen görs tillgänglig för andra aktörer, men att den fortfarande inte är godkänd.

#### 10. Granska leverans [Uppdragsgivaren]

Uppdragsgivaren granskar den delade IFC-modellen med hjälp av IDS:en.

##### *10.1 OK? Nej*

Kommunicera resultatet. Kommentarer och synpunkter ges via BCF och delas i CDE av granskande part. BCF-hanteringen görs också via CDE. Det kan vara via BCF-filer eller med en BCF-server i bakgrunden beroende på vilket system eller vilken programvara som används.

Uppdragstagaren justerar enligt kommentarerna. När CAD-modellen är uppdaterad görs processen om från punkt 7.

##### *10.2 OK? Ja*

Genomför och publicera leveransen. Godkänd leverans ges status ”Publicerad” i CDE, eller “Godkänd” i svensk kontext