# openBIM

## Vad är openBIM?

openBIM handlar om att öppna standarder tillämpas och neutrala format används vid hantering och överföring av data. Öppna standarder möjliggör att olika aktörer kan dela data genom arbetsflöden och via IFC-certifierade programvaror. I samarbetsprocessen är detta en viktig del för delning av data och information.

openBIM innebär att data lagras i format som kan hanteras av olika programvaror, till skillnad från “closed BIM” där proprietära format används, alltså programspecifika format som bara kan läsas av en specifik programvara.

För att tydliggöra skillnaden mellan programprecifika och neutrala format lanserade buildingSMART begreppet openBIM. Det beskrivs på följande sida  https://www.buildingsmart.org/about/openbim/

openBIM som begrepp har inte använts så mycket i Sverige, men kommer förhoppningsvis användas mer framöver för att tydliggöra ett data- och informationsflöde som är oberoende av specifika programvaror och som möjliggör ett bredare samarbete i branschen.

Eftersom BIM per definition handlar om digitalt samarbete är användningen av format som stödjer openBIM central. Det krävs en samarbetsprocess som är leverantörsneutral och som stödjer ett sömlöst samarbete för alla deltagare inom ett projekt. I openBIM används öppna standarder med syfte att underlätta digital samverkan (“interoperabilitet”) till nytta för projekt och tillgångar under hela deras livscykel.

openBIM tillåter digitala arbetsflöden baserade på leverantörsneutrala format som IFC, MVD, BCF, COBie, CityGML och gbXML.

Principerna för openBIM är:

-   digital samverkan är nyckeln till digital transformation i branschen för byggda tillgångar
-   **öppna** och neutrala standarder bör utvecklas för att underlätta interoperabilitet
-   **tillförlitligt** datautbyte kräver oberoende kvalitetsriktmärken
-   arbetsflöden för **samarbete** förbättras av öppna och agila dataformat
-   **flexibla** teknologival skapar större värde för alla intressenter
-   **hållbarheten** skyddas genom långsiktiga, kompatibla datastandarder.

Fördelarna som detta ger för branschen är att openBIM:

-   förbättrar samarbetet för projektleverans
-   möjliggör bättre kapitalförvaltning
-   tillgängliggör data som skapats under projekteringen under hela livscykeln för den byggda tillgången.
-   utökar bredden och djupet av BIM-leverabler genom att skapa gemensam anpassning och språk genom att följa internationella standarder och gemensamt definierade arbetsprocesser
-   möjliggör en gemensam datamiljö som ger möjligheter för användare att utveckla nya arbetsflöden, mjukvaruapplikationer och teknikautomation
-   möjliggör en tillgänglig digital tvilling som utgör grunden till en långsiktig datastrategi för byggda tillgångar.
