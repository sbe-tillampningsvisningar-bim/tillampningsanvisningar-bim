# buildingSMART International (bSI)

## Vad är buildingSMART? 

buildingSMART International är en neutral, internationell organisation med syfte att initiera, skapa och anta öppna digitala standarder och neutrala format för BIM-processer. Mest känt är överföringsformatet IFC.

I arbetet ingår grundläggande funktioner, begrepp och metoder som alla har till syfte att kunna användas inom projektering, byggande och förvaltning av byggd miljö. Organisationen tar även fram och visar på goda exempel från verkligheten, lyfter upp aktuella frågeställningar och organiserar informationstillfällen för medlemmarna.

## Vilka standarder har buildingSMART tagit fram?   

buildingSMART har för närvarande tagit fram följande öppna BIM-standarder:

-   **IFC** (*Industry Foundation Classes*), som är en begreppsmodell och ett digitalt filformat  
     för strukturerad beskrivning av CAD-modeller
-   **MVD** (*Model View Definition*), som är ett utsnitt eller en delmängd av IFC-schemat för ettspecifikt syfte
-   **IDS** (*Information Delivery Specification*), som är en strukturerad textfil som beskriver  
     vad en specifik IFC-fil ska innehålla
-   **BCF** (*BIM Collaboration Format*), som är ett filformat (bcfxml) för att kommunicera  
     objekt- och modellbaserade ärenden mellan olika program och aktörer
-   **bSDD**(*buildingSMART Data Dictionary*), som är ett online-bibliotek för klasser,  
     egenskaper, relationer och enheter.

buildingSMART standardiserar sina specifikationer genom ISO/TC 59 *Buildings and civil engineering works*.

## Hur drivs utvecklingen av IFC-formatet?

Utvecklingen av IFC drivs i arbetsgrupper med medlemmar från hela världen. Samverkansträffar inom organisationen och de årliga fysiska träffarna (*BIM Summits*) bidrar till att sprida information och få in synpunkter på pågående arbeten.

bSI främjar internationell konsensus bland intressenter om specifika standarder för att påskynda implementering och användning.

## Vad innebär certifiering av programvaror?

Vid publicering av en ny version eller delversion av IFC kan programvaruleverantörer ansöka hos buildingSMART om att bli certifierade för den versionen. Godkända programleverantörer förtecknas i Software Implementations, där det framgår för vilka versioner programvaran är certifierad. Här framgår även certifiering för BCF och bsDD.

## Vilka är medlemmarna i buildingSMART? 

buildingSMART baserar hela sin verksamhet på engagemang från branschen. Medlemmarna deltar genom olika typer av engagemang i form av arbetsgrupper och samarbeten, eller som ekonomiskt stöttande parter. Engagemanget kan vara i olika former: som medlemmar, chapters, partners och sponsorer.

**Medlemmar**

Medlemskapet är öppet för företag, statliga organ och institutioner från hela världen. Som medlem stöttar man buildingSMART ekonomiskt på olika sätt, och kan delta i olika typer av samarbeten och utvecklingsprojekt.

**Chapters**

Ett *chapter* är en lokal gruppering av ett eller flera länder, som delar samma vision och mål som buildingSMART och verkar för detta inom sitt geografiska område. Den svenska gruppen utgörs av buildingSMART Sweden, och har formerats inom BIM Alliance där det har sin bas.

**Partners**

Partners är andra organisationer som också arbetar för ett standardiserade arbetssätt och neutrala format, till exempel CEN, ISO och GS1.

**Sponsorer**

Sponsorer är medlemsföretag som går in stöttar olika delar inom buildingSMART ekonomiskt eller genom obetalt arbete. Här finns många olika typer av företag, till exempel programvaruleverantörer, beställarorganisationer och konsulter.
