# BIM Collaboration Format (BCF)

## Vad är BCF?

BIM Collaboration Format (BCF) är ett neutralt och öppet filformat (bcfxml) som gör det möjligt att kommunicera objekt- och modellbaserade ärenden mellan olika program och aktörer.

## Vad används BCF till?

BCF används vid till exempel samordning och ärendehantering där informationen visas i modellfiler. Formatet gör det möjligt att visuellt visa både vad ärendet gäller och var i modellen det är.

## Var finns funktioner för BCF?

BCF används genom programvaror som har implementerat formatet. Certifierade programvaror förtecknas hos buidlingSMART (se länk under "Relaterat"). Överst på websidan hos buildingSMART finns möjlighet att filtrera informationen i listan.

En programvara som har BCF-loggan är certifierad för formatet.

Läs mer om BCF formatet i länkarna till höger under "Relaterat".

