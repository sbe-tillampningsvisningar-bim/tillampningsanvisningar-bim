# Model View Definition (MVD) och utbyteskrav (Exchange Requirements)

## Vad?  

En *Model View Definition* (**MVD**) är en specifikation för en delmängd av det övergripande IFC-schemat. Man kan likna det vid en fördefinierad ”vy”, som specificerar kraven för användning av IFC i en viss kontext eller för ett specifikt ändamål. Med MVD:n anges vilka objekt, egenskaper och relationer som är relevanta för en viss tillämpning, till exempel för att skapa ritningar, utföra energiberäkningar, eller för att hantera underhållsinformation.

En MVD beskriver alltså enbart en del av innehållet i en specifik version av IFC, så som figuren nedan illustrerar.

![IFC och MVD](../1.introduktion_openbim/media/ifc_mvd.jpg)

*Bild 5: Schematisk bild över skillnaden mellan IFC (vänster) och MVD (höger).  [Källa: Mark Baldwin, The BIM Manager, 2019]*

MVD är viktigt för byggbranschen eftersom det möjliggör digital samverkan – “interoperabilitet” – mellan programvaror och domäner. IFC är ett brett format där man till exempel kan lagra information om en vägg på olika sätt. Olika arbetsflöden och informationsutbyten kräver sina data. För att stödja vissa arbetsflöden behöver det finnas en överenskommelse om vad som skall tas med och hur det ska kodas vid exporten. Detta görs via en MVD.

## Hur? 
MVD:er nyttjas främst av programvaruleverantörer för att beskriva hur deras programvaror tar emot och skickar information via IFC, det vill säga vilka delar av IFC-standarden som nyttjas. I praktiken är det dessa MVD:er, som finns fördefinierade i programmen, som används vid exporten.

I programvaror som stödjer informationsutbyten med flera parter har ofta flera valbara MVD:er. En viss MVDär grundinställning i en viss programvara. Det finns också program där val av MVD inte görs då endast ett informationsutbyte är underförstått.

För att minimera antalet MVD:er, och därmed underlätta kommunikation mellan olika programvaror, har buildingSMART fastställt ett antal MVD:er. Det är mot dessa specifika MVD:er som programföretag certifieras för IFC, det vill säga en programvara blir certifierad för en viss MVD och IFC-version.

De två mest använda är:

-   *Coordination View* för IFC2x3 TC1
-   *Reference View* för IFC4 ADD2 TC1.

Båda dessa används för samordning mellan tekniska discipliner. *Reference View* visar mer förenklad geometri och relationer jämfört med *Coordination View*

Utöver dessa två finns ett par till som utgör ”(bas) MVD:er” för implementering av IFC:

-   *Alignment based Reference View* (IFC 4.3), som är en bas för att skapa MVD:er för infrastruktur (vägar, järnvägar, broar)
-   *Design Transfer View* (IFC 2x3, IFC 4.\*), som är en ännu ej fastställd vy för avancerad överföring av geometrier och relationer.

Många MVD:er är, som sagt, specifikt anpassade för ett visst ändamål. Ett exempel är *IFC4Precast*, som används för att skicka geometrisk information från CAD-programvaror till MES-system för maskintillverkning av prefabricerade betongkomponenter.

För en översikt över MVD:er och dess status, se buildingSMART MVD Database.

## Exchange Requirements 

Specifika utbyteskrav kan definieras ovanpå (bas) MVD. Dessa utbyteskrav (*Exchange Requirements*) kan vara specifika för olika domäner som *Architectural*, *Structural*, *MEP*, *Infra* och *Rail*. Mjukvaror kan bli certifierade av buildingSMART för sådana utbyteskrav.

Det finns flera sätt att definiera utbyteskraven och buildingSMART rekommenderar att använda IDS-standarden för kravställning.

<table>
    <tbody>
        <tr>
            <td colspan="2"><b>TÄNK PÅ</b></td>
        </tr>
        <tr>
            <td colspan="2">
                <p>Vid utbyte av information via IFC är det viktigt att säkerställa vilken MVD som ska nyttjas till överenskommen version av IFC.</p>
                <p><em>Den aktör som beställer en informationsleverans via IFC måste alltid ange både gällande IFC-version och tillhörande MVD.</em></p>
            </td>
        </tr>
    </tbody>
</table>
