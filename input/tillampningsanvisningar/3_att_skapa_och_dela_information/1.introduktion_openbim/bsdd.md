# buildingSMART Data Dictionary (bSDD)

## Vad är bSDD? 
BuildingSMART Data Dictionary (bSDD) är en webbaserat lexikon som tillhandahålls av buildingSMART för att hantera, strukturera och dela begrepp som rör byggd miljö. Det går att beskriva som en ontologi: en systematisk beskrivning av verkligheten och dess beståndsdelar.

bSDD fungerar som en central databas för att definiera och standardisera terminologi, egenskaper och klassifikationer som används i bygg- och fastighetssektorn. Det är ett viktigt verktyg för att skapa en gemensam förståelse och digital samverkan (“interoperabilitet”) mellan programvaror och intressenter i byggnadsinformationsmodellering (BIM).

## Vad används bSDD till? 
1.  **Standardisering av begrepp och egenskaper**

    bSDD tillhandahåller en standardiserad och strukturerad databas med begrepp och egenskaper som kan användas i BIM-projekt. Detta hjälper till att säkerställa att alla använder samma språk och definitioner, vilket minskar risken för missförstånd.

<br>

2.  **Klassifikation och metadata**

    bSDD innehåller klassifikationer och metadata för byggnadselement, vilket gör det möjligt att tilldela specifika egenskaper till objekten i en byggnadsmodell. Detta gör det lättare att dela och förstå information mellan olika system.

<br>

3.  **Integration med BIM-verktyg**

    bSDD kan integreras med BIM-verktyg och programvaror, vilket underlättar skapandet av BIM-modeller som följer internationella standarder. Detta möjliggör bättre datautbyte och samarbete mellan olika parter i ett projekt.

<br>

4.  **Användning av API**

    bSDD erbjuder ett API som gör det möjligt för utvecklare att integrera och använda data från bSDD i sina applikationer. Detta kan inkludera att hämta klassifikationer eller att koppla specifika egenskaper till objekt i en modell.

<br>

5.  **Anpassning och lokal anpassning**

    bSDD stödjer flera språk och kan anpassas för att inkludera nationella eller organisationsspecifika klassifikationer och standarder, vilket gör det flexibelt och användbart i olika länder och projekt.
<br>

## Varför ska bSDD användas? 

Genom att använda bSDD kan projekt säkerställa att de arbetar med korrekt och standardiserad struktur för data och använder rätt benämningar. Det förbättrar kvaliteten och effektiviteten i design-, bygg- och förvaltningsprocesser och ger ett hållbart sätt att hantera information från projekt till projekt.


