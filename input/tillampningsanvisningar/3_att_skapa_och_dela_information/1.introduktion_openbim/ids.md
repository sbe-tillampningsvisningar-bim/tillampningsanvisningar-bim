# Information Delivery Specification (IDS)

## Vad är IDS? 

IDS är en datafil som används för att kravställa och kontrollera IFC-filer innan och efter de levereras. En IDS är direkt kopplad till IFC genom att använda IFC-schemats klasser, och är en syntes av kravställning, leveranstillfälle och IFC-schema. Det finns många applikationer på marknaden som klarar att skapa och redigera IDS-filer.

I ISO 19650 beskrivs EIR, som kan ses som en kravställning för projekt. IDS är ett verktyg i detta arbete. 

Tekniskt sett är IDS en strukturerad textfil i form av ett xml-protokoll. Formatet är läsbart både för människa och maskin. I en applikation kan den läsas som ett vanligt textdokument, medan en dator ser filen som ett facit som antingen godkänner eller underkänner en levererad IFC.

## Vad används IDS till? 

IDS använder IFC-schemat för att precisera hur en objektorienterad modell ska vara strukturerad när den levereras som en IFC-fil. Den styr vilka klasser av objekt som ska ingå, vilka egenskaper som ska användas för att beskriva dem, och hur det ska finnas värden för dessa egenskaper vid det aktuella leveranstillfället.

## Vad innehåller IDS?

En IDS består av tre delar:

-   Beskrivning (*Description*). Här formuleras krav i klartext, till exempel “Bärande väggar ska vara brandklassade”.
-   Tillämpning (*Applicability*), som anger vilka objekt som kravet gäller, baserat på typ eller någon annan egenskap, till exempel klassifikation.
-   Krav (*Requirements*), som visar vilka specifika krav som ska var uppfyllda, till exempel på vilken nivå av detaljering som modellen ska vara.

*Applicability* och *Requirements* kan brytas ner och specificeras genom sex aspekter:

-   Entitet (*Entity*), alltså vilka objekt som ska ingå.
-   Attribut (*Attribute*), som handlar om de grundläggande beskrivningarna av objektet.
-   Klassifikation (*Classification*), som visar objektet klass utöver den som finns i IFC.
-   Egenskap (*Property*), som i detalj beskriver objektet. Här kan man till exempel ange att enbart bärande väggar ska ingå.
-   Material (*Material*), alltså vad objekten är gjorda av.
-   Del av (*Part of* ), som visar om IFC-filen ingår i en samling av leveranser.

För varje aspekt anges om den är obligatorisk (*Required*), valfri (*Optional*) eller förbjuden (*Prohibited*), det vill säga hur den specifika kravställningen ska hanteras för en specifik leverans. Exempel: om *Classifikation* ska göras med CoClass och detta sätts till *Required* innebär att det inte får finnas delar som inte är klassifierade enligt CoClass.

När en IFC-fil levereras kan man låta IDS-applikationen kontrollera att den följer IDS:en.
