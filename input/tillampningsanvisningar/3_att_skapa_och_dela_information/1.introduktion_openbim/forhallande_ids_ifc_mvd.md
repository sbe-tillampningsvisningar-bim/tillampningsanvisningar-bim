# IDS, IFC och MVD: hur förhåller de sig till varandra?

## Information ska kunna delas och förstås av alla parter 

För att få en så användbar och korrekt leverans som möjligt som kan hanteras av alla berörda parter, behövs en tydlig kravställning, en bra anpassning i använda programvaror och filformat som kan läsas av alla. I dag finns förutsättningarna för detta genom att använda öppna, neutrala BIM-standarder.

## Användning av öppna, neutrala standarder

Den information som ska levereras vid ett visst tillfälle kravställs i en IDS. Vid export till IFC från ett CAD-program görs urvalet av informationen med hjälp av en MVD, och mottagande parter erhåller strukturerad användbar information i IFC för definierat syfte.

Visst låter det bra, men vad står de olika förkortningarna för och hur förhåller de sig till varandra?

**IFC** (*Industry Foundation Classes*) är en öppen och neutral standard för informationsmodellering. Det är en begreppsmodell och ett digitalt filformat för strukturerad beskrivning av CAD-modeller. Formatet är neutralt, icke-proprietärt och används därför vid export från olika CAD-program. Vid export skapas en IFC-fil som innehåller objektinformation lagrad enligt IFC-schemat. En IFC-fil är alltså en digital modell av en byggnad eller anläggning där informationen är strukturerad enligt vissa regler.

Syftet med IFC-filen är att tillgängliggöra informationen i modellen för alla intressenter, inte bara de som använder sig av en viss programvara. IFC-filen kan öppnas i program som kan läsa IFC-formatet, eller importeras till andra program. Detta är en förutsättning för utbyte av modellinformation.

**MVD** (*Model View Definition*) är ett utsnitt eller en delmängd av IFC-schemat. En MVD innehåller en beskrivning av den geometri och objektdata som behövs för ett specifikt ändamål.

Vid export till IFC används alltid en MVD som är fördefinierad i programvaran. Det kan finnas flera MVD:er som kan väljas av användaren. Vissa programvaror har dock endast en MVD, och då framgår det inte alltid för användaren att en MVD väljs vid exporten.

Ett exempel på MVD är CV 2.0 (*Coordination View 2.0*), som ofta används vid samordning av 3D-modeller. Denna är fastställd av buildingSMART, medan andra MVD:er kan vara bransch- eller projektspecifika (mer om det nedan).

**IDS** (*Information Delivery Specification*) kan beskrivas som utbyteskrav. Den definierar vilken alfanumerisk information som ska finnas i IFC-modellen vid en specifik leverans. Uppbyggnaden av IDS följer en bestämd struktur som är tolkningsbar för både människor och datorer, och därmed hanterbar digitalt.

![Schematisk bild MVD, krav IDS](../1.introduktion_openbim/media/schematiskbild_mvd_kravids.jpg)

*Figur 1: Schematiskt bild på hur en delmängd av IFC-schemat definieras med hjälp av en MVD. Kraven från IDS:en visas i blått*



## Vem skapar vad? 

IFC-filer skapas av projektörer eller andra parter, vid leverans av information genom en exportfunktion i programvaran som används. En arkitekt-fil och en VVS-fil innehåller olika typer av information, men informationen är i båda fallen strukturerad enligt IFC-schemat.

MVD:er definieras oftast av buildingSMART, men själva MVD:n implementeras av programvarutillverkare och finns med i deras programvara. MVD:er kan även definieras av branschorganisationer för ett specifikt syfte, eller i ett visst projekt.

IDS:er skapas av kravställare och delges den som ska leverera information. I IDS:en framgår vilka egenskaper som ska finnas på olika objekt vid leverans. IDS nyttjas av både den som ska leverera och den som ska granska informationsleveransen.

![Informationsleverans](../1.introduktion_openbim/media/inform_leverans1.jpg)

## Sammanfattning 
-   Den information som ska levereras vid ett visst tillfälle finns kravställd i en IDS.
-   Vid export till IFC från ett CAD-program används IDS för urvalet av information.
-   MVD definierar vad som kommer med vid exporten.

<br>

![Informationsleverans 2](../1.introduktion_openbim/media/inform_leverans2.jpg)
