

# System (*IfcSystem*)

**Säkerställ att alla objekt i system är placerade i korrekt system med korrekt identifikation.**

## Vad?

I IFC används system (*IfcSystem*) främst för att hantera tekniska installationssystem, men också system för zonindelning, och konstruktioner som används för till exempel analyser. Dessa relaterar till *IfcBuilding* genom relationen *IfcRelServicesBuildings*.

På installationssidan finns system som tillför funktioner som elkraft, luft, värme, kyla och vatten till byggnadsverk, och system för styrning och övervakning.

## Hur?

**Kontinuerliga system**

När objekt i installationssystemen modelleras är det viktigt att de objekt som tillhör ett och samma system sitter ihop. Delar i ett system ska inte brytas itu, så att de CAD-mässigt tillhör olika system. Då blir exporten till IFC fel.

**Anslutningspunkter**

Ett objekt som tillhör ett installationssystem ansluts till sitt system via en anslutningspunkt, i IFC benämnd *IfcDistributionPort*. (I CoClass klassificeras sådana komponenter som primär klass X, benämnd Gränssnittsobjekt.)

En anslutningspunkt har ett inlopp eller utlopp för ett flöde av fasta, flytande eller gasämnen, för elkraft eller signaler för datakommunikation.

Anslutningspunkter definieras av systemtyp och flödesriktning. Det innebär att om två punkter ska kunna anslutas mot varandra måste de tillhöra samma systemtyp och ha motsatta flödesriktningar: en sida är inlopp (*Sink*) och den andra är utlopp (*Source*). Det finns också dubbelriktade anslutningspunkter, benämnda *SinkAndSource*.

Anslutningspunkter har inte någon synlig geometri i sig, men de har placering som indikerar dess position och orientering.

En anslutningspunkt tillhör ett system för att ange dess funktion i ett visst system, till exempel kallvattenutlopp.

**Struktur och attribut i IFC-filen**

*IfcDistributionPort* beskrivs med attributen enligt följande tabell:

| **Attribut**          | **Exempel (värde)**  | **Förklaring**                                                                                                                                                                                                                                                     |   |
|-----------------------|----------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---|
| **FILE_DESCRIPTION**  |                      |                                                                                                                                                                                                                                                                    |   |
| *FlowDirection<sup>2</sup>*      |                      | Egenskap som visar om det är avlopp (*Sink*) eller ett tillopp (*Source*) eller både och (*SinkAndSource*).                                                                                                                                                        |   |
| *PredefinedType<sup>2</sup>*     |                      | En lista över typer för att ytterligare identifiera objektet. Vissa egenskapsuppsättningar kan vara specifikt tillämpliga på en av dessa typer.  Notera: Om objektet har ett associerat *IfcTypeObject* med en *PredefinedType*, ska detta attribut inte användas  |   |
| *SystemType<sup>2</sup>*         |                      | Egenskap som identifierar systemtypen. Om en systemtyp är definierad får porten endast anslutas till andra portar med samma systemtyp                                                                                                                              |   |

<sup>1</sup> Obligatorisk (värde läggs in av programmet)<br> <sup>2</sup> Valfri (värde kan anges av användaren)

*Tabell: Egenskaper för IfCDistributionPort*