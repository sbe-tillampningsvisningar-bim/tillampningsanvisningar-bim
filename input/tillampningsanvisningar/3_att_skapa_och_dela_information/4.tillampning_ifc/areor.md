
# Areor (*IfcElementQuantity*)

**Areor som kan användas inom IFC.**

## Vad?

För byggnader finns behov av areahantering utifrån ekonomiska kalkyler och myndighetskrav. IFC har goda förutsättningar för att hantera olika typer av areor, men i nationella standarder finns ofta fler typer av areor definierade än vad IFC har definierat i dagsläget.

I svensk standard SS 21054:2020 beskrivs definitionen för olika typer av areor. I IFC finns inte lika många typer definierade, men tre av dem har en motsvarighet i standarden:

Area Bet IFC

Bruttoarea BTA GrossFloorArea

Nettoarea NTA NetFloorArea

Bruksarea BRA UsableFloorArea

Inom infrastruktur används också areor som underlag för olika typer av ekonomiska beräkningar, men det beskrivs inte här.

## Varför?

För byggnader är dessa tre areor grundläggande för bland annat kalkylering, redovisning av byggkostnader, energianvändning och hyresintäkter.

Vid behov av hantering av fler areatyper rekommenderas att hantera dessa utfrån egenskaper definierade enlig CoClass eller som egendefinierade. Läs mer om egenskaper och egenskapsuppsättningar i separat kapitel.
