
# Utrymme (*IfcSpace*)

**Säkerställ att utrymmesgränser är definierade.**

## Vad?

Utrymmesgränser (*IfcRelSpaceBoundary*) definierar gränser mellan utrymmen och dess omgivande byggdelar. Det finns två nivåer av utrymmesgränser:

-   Nivå 1 Ingen hänsyn tas till om det finns ett utrymme eller byggdel på andra sidan utrymmesgränsen.
-   Nivå 2 Hänsyn tas till om det finns ett utrymme eller byggdel på andra sidan utrymmesgränsen. På denna nivå finns två typer
    -   Typ A et är ett utrymme på andra sidan utrymmesgränsen.
    -   Typ B et är en byggdel på andra sidan utrymmesgränsen.

## Varför?

Vid export av modeller som innehåller utrymmen är det viktigt att definiera vilken nivå på utrymmesgränser som ska exporteras. Det i sig beror av det aktuella syftet. Dessa utrymmesgränser är viktiga exempelvis vid energianalyser och mängdberäkningar.

Förutom de två beskrivna nivåerna brukar även programmen ha en möjligt att helt exkludera utrymmesgränser från exporten.

## Hur?

För att kunna skapa utrymmesgränser behövs antingen objekt som begränsar utrymmet, till exempel väggar, eller en fiktiv begränsning som programmet kan skapa för utrymmet, eller både oc. Oftast finns det funktioner för detta i modelleringsprogrammen.
