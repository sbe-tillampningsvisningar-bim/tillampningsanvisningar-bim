
# Referensbeteckning

**Identifiera entiteter med en unik sträng, referensbeteckning.**

## Vad?

En referensbeteckning är ett sätt att identifiera ett specifikt objekt. Den utgörs av en textsträng, som struktureras på olika sätt beroende på beställarens behov. Referensbeteckningar sätts endast på de objekt som är av intresse att följa, till exempel för produktionsplanering, produktion, drift och underhåll.

Referensbeteckningen kan innehålla flera delar. Till exempel kan typ av objekt, dess funktion och dess rumsliga placering ingå som delar i en referensbeteckning.

En referensbeteckning kan struktureras enligt tillämpningar av standarden SS-EN 81346-1 *Struktureringsprinciper och referensbeteckningar - Del 1: Grundläggande regler*. Konkret kan det göras med i kombination med CoClass. Detta beskrivs mer i texterna om referensbeteckningar med CoClass.

Om det finns krav på att referensbeteckningar ska finnas på definierade objekt och entiteter är det viktigt att den placeras på rätt plats i IFC.

## Varför?

Genom att sätta referensbeteckningar på entiteter enligt en fördefinierad struktur underlättas identifieringen av varje enskild entitet. Detta gör hanteringen av entiteterna och objekten blir enklare under dess livscykel.

## Hur?

Vid kravställning av informationsleveranser med IFC-formatet bör det framgå vilken identifierande information som kravställs och om den ska beskrivas via attribut eller som egenskaper i egenskapsuppsättningar.

Om referensbeteckningar ska placeras på attribut är det viktigt att säkerställa att de programvaror som används vid export till IFC-formatet kan leverera detta. Det bygger på att användaren har möjlighet att påverka innehållet i attributen.

Om referensbeteckningar ska placeras på ett attribut rekommenderas att nyttja attributet *Tag* enligt tabellen nedan.

*Tabell: Referensbeteckning som identifierande attribut.*

| **Egenskap**       | **Beskrivning**      | **Källa**                                         | **Attribut** |
|--------------------|----------------------|---------------------------------------------------|--------------|
| Referensbeteckning | Unik identifikation. | Enligt uppdragsgivarens referensbeteckningssystem | *Tag*        |

IFC-standarden har även möjlighet att redovisa referensbeteckningar på egenskapen *AssetIdentifier* i egenskapsuppsättningen *Pset_ConstructionOccurence* enligt tabellen nedan. (Termen *occurence* betyder ungefär ”förekomst”, och betecknar alltså ett specifikt objekt.)

*Tabell: Referensbeteckning som egenskap AssetIdentifier, som krav på identifierande egenskaper (IFC-standarden).*

| **Egenskap**      | **Beskrivning**      | **Källa**                                         | **Egenskapsuppsättning**      |
|-------------------|----------------------|---------------------------------------------------|-------------------------------|
| *AssetIdentifier* | Unik identifikation. | Enligt uppdragsgivarens referensbeteckningssystem | *Pset_ConstructionOccurrence* |

CoClass innehåller även egenskaper för referensbeteckningar (se tabell nedan). Dessa egenskaper läggs i egenskapsuppsättningen *SE-CC_Properties*, alternativt *SE-CC_AdministrativeProperties*. Lär mer i avsnittet om egenskaper och egenskapsuppsättningar.

*Tabell: Exempel på krav på identifierande egenskaper enligt CoClass.*

| **Egenskap**                 | **Beskrivning**                         | **Källa**                                          | **Egenskapsuppsättning**                                   |
|------------------------------|-----------------------------------------|----------------------------------------------------|------------------------------------------------------------|
| *ccNameReferenceDesignation* | Referensbeteckning                      | Enligt uppdragsgivarens referensbetecknings-system | *SE-CC_Properties,* alt*. SE-CC_AdministrativeProperties*  |
| *ccNameRdFunction*           | Referensbeteckning funktion, objekt     | Enligt uppdragsgivarens referensbetecknings-system | *SE-CC_Properties,* alt*. SE-CC_AdministrativeProperties*  |
| *ccNameRdAssembly*           | Referensbeteckning, komposition, objekt | Enligt uppdragsgivarens referensbetecknings-system | *SE-CC_Properties,* alt*. SE-CC_AdministrativeProperties*  |
| *ccNameRdLocation*           | Referensbeteckning lokalisering, objekt | Enligt uppdragsgivarens referensbetecknings-system | *SE-CC_Properties,* alt.* SE-CC_AdministrativeProperties*  |
| *ccNameRdPosition*           | Referensbeteckning position, objket     | Enligt uppdragsgivarens referensbetecknings-system | *SE-CC_Properties*, alt.* SE-CC_AdministrativeProperties*  |

Ett sista alternativ är att skapa egna – användardefinierade – egenskaper och egenskapsuppsättningar för att redovisa referensbeteckningar.