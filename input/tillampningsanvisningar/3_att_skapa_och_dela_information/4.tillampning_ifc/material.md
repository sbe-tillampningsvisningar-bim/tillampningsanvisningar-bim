# Material (*IfcMaterial*)

**Bestäm metod för att ange byggdelars material.**

## Vad?

I IFC-formatet finns möjlighet att lagra information om vilket material ett objekt består av.

I en IDS finns plats för kravställning av material. Kravställningen kan läggas på två ställen, dels som ett eget attribut, dels som en del i en egenskapsuppsättning. Läs mer om detta under IDS.

## Varför?

Vilket material en entitet har är en egenskap som är viktig att få med för vissa typer av objekt. Material används till exempel av konstruktörer för att beräkna vikten av ett objekt genom att använda densiteten för materialet och volymen av objektet.

Att ange material är viktigt eftersom informationen kan användas för många syften. Hur ett material beskrivs varierar därför, bland annat beroende på:

* disciplin
* olika aktörers behov
* skede i projektet.

## Hur?

Material beskrivs med *IfcMaterial*, som kan hålla information om materialet för ett objekt genom framför allt tre attribut.

| **Attribut (3)**        | **Exempel**       | **Förklaring**                                   |
| ----------------------- | ----------------- | ------------------------------------------------ |
| Name<sup>2</sup>        | S355J2            | Namnet på materialet.                            |
| Description<sup>2</sup> | Konstruktionsstål | Beskrivning av materialet (klartext)             |
| Category<sup>2</sup>    | Steel             | En vidare beskrivning av materialet, materialtyp |

<sup>1</sup> Obligatorisk (värde läggs in av programmet)

<sup>2</sup> Valfri (värde kan anges av användaren)

*Tabell: Attribut för IfcMaterial*

Namn (*Name*) och kategori (*Category*) bör i praktiken betraktas som obligatoriska. Beskrivningen (*Description*) underlättar för den som inte kan tolka en kod för namnet, och utgör också en slags kvalitetssäkring.

Kategori kan användas till att filtrera fram alla förekomster av ett visst material, oavsett kvalitet eller sort. buildingSMART har en lista med förslag på namn som kan användas. Några exempel: *concrete, steel, aluminium, block, brick, stone, wood, glass, gypsum, plastic, earth*.

Till *IfcMaterial* kan olika egenskapsuppsättningar knytas. Dessa kan innehålla olika materialdata för till exempel CO2-beräkning, eller kemiska egenskaper för till exempel bedömning av lämplighet med avseende på hälsa. På buildingSMART:s sida för *IfcMaterial* finns en lista över egenskapsuppsättningar som kan läggas till.

#### Beskrivning av material

Hur ett material beskrivs eller namnges kan variera mellan olika skeden i ett projekt samt med vilken disciplin som behöver informationen. I ett tidigt skede kan en arkitekt beskriva att en vägg ska utföras med en viss pigmentering av betongen. I ett senare skede behöver konstruktören beskriva exakt vilken kvalitet betongen ska ha med avseende på hållfasthet, och för beställning från leverantören, inklusive koder för pigmentering.

Material kan beskrivas som enskilt eller som sammansatt. För sammansatta material finns tre varianter.

-   Enskilt material beskriv bara med *IfcMaterial*.
-   För skiktade material (till exempel en sandwichvägg) beskrivs varje skikt med *IfcMaterialLayer*.
-   Sammansatta profiler (till exempel svetsade balkar) beskrivs med *IfcMaterialProfile*.
-   För sammansatta objekt (till exempel fönster med ruta och karm) beskrivs varje beståndsdel med *IfcMaterialConstituent*.

Hur materialet beskrivs kan också skilja sig mellan olika aktörers behov. En arkitekt modellerar till exempel en sandwichvägg som ett enda objekt, men med material med flera skikt. Konstruktören behöver beskriva varje skikt (innerskiva, isolering, ytterskiva) som separata objekt med endast ett material per objekt.

#### Sammansatta skiktade material (IfcMaterialLayerSet)

För sammansatta objekt kan de olika skikten beskrivas med hjälp av *IfcMaterialLayerSet*. Egenskapen för dessa skiktade material är att de har en utbredningsriktning, att inga mellanrum finns mellan skikten samt att de ligger i en viss ordning.

Varje skikt i ett *IfcMaterialLayerSet* kan beskrivas med *IfcMaterialLayer*. Attribut för varje skikt är bland annat material (beskrivet med *IfcMaterial*); tjocklek; huruvida skiktet är ventilerat (typ luftspalt); namn; beskrivning i fritext; kategori (till exempel lastbärande, isolerande, innerfinish och ytterfinish). Det finns även en prioritet man kan sätta som beskriver hur skikt ska anpassa sig till varandra i till exempel hörn.

I regelväggar kan inte skiktet som utgör väggkärna beskrivas med sina ingående komponenter som reglar och isolering. Denna typ av vägg kan namnges som regelvägg i fältet *Description.*

Till varje material kan också data för hur ett objekts yta och synlighet lagras. Man kan till exempel ange en viss procentuell genomskinlighet och kulör. Denna information gör att den som visar IFC-filen får korrekt visuellt intryck av objektet, till exempel att glas är genomskinligt, men ändå går att se. Detta beskrivs på sidan Wiki IFC materials.

Om det finns behov av att sätta kulör och transparens på ett objekt utifrån materialet bör en specifik egenskap för detta användas. I IFC finns vissa möjligheter för detta. Läs mer i avsnittet ”kulörer på objekt”.

<table>
    <tbody>
        <tr>
            <td><b>&#9432;</b></td>
            <td><b>TÄNK PÅ</b></td>
        </tr>
        <tr>
            <td></td>
            <td>
                <p>Attributet “Name” kan ha olika betydelse i olika tillämpningar, till exempel kvalitet/hållfasthet för konstruktörstillämpningar i vissa program eller visuellt intryck för arkitektoniska tillämpningar.</p>
                <ul>
                    <li>I CoClass finns en tabell med typer av material som kan användas. Denna är baserad på förteckningar från IVL, och som används av Boverket för klimatdeklarationer.</li>
                    <li>I ett tidigt skede kan det räcka med att en vägg modelleras som ett enda objekt, men med de olika skikten beskrivna med *IfcMaterialLayerSet*.</li>
                    <li>Hur väggar med olika skikt modelleras beror på förutsättningarna i modelleringsprogrammet och hur informationen ska levereras. I vissa program måste skikten i väggen modelleras var för sig, medan det i andra program finns funktioner för att definiera skikt i objektet vägg.</li>
                </ul>
                <p>Inom vissa tillämpningar som tillverkning av prefabricerade element behöver man beskriva varje skikt mer detaljerat. Olika skikt kan också behöva olika attribut. I de fall där stor detaljnoggrannhet behövs, modelleras varje skikt för sig. En sandwichvägg kommer till exempel att bestå av tre separata objekt: innerskiva, isolering och ytterskiva.</p>
            </td>
        </tr>
    </tbody>
</table>
