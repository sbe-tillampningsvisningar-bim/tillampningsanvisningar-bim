

# Mängder och mängduppsättningar (*Quantity och QuantitySet*)

**Hantera och ta fram mängder från IFC-filer.**

## Vad?

att få fram mängder från en IFC-fil.

-   inbakade i IFC-filen vid export
-   beräknade värden ur modellen efter export
-   av användaren valda mängder bifogade i egenskapsuppsättning vid export

Från en modell kan man få ut brutto- eller nettovärde av en storhet. Man kan även välja olika projiceringsriktningar för till exempel areor, fotavtryck area. Vid dessa tillfällen kan det vara bra att själv ta med de mängder man behöver i en egen mängduppsättning.

öjlighete att få fram rätt variant av en viss storhet modeller.

## Hur?

**Inbakade i IFC-filen vid export**

IFC-filer finns vissa mängder inkluderade som egenskaper. IFC-taggen *IfcElementQuantity* innehåller referenser till de storheter som finns lagrade för en viss typ av objekt. Olika objekt kan ha olika uppsättningar.

*IfcElementQuantity* kan ha följande typer av mängder:

-   antal
-   vikt
-   längd
-   area
-   volym
-   tid.

uppsättningkallas *Base quantities*) definitioner som är oberoende av en specifik mätmetod och därför internationellt tillämpliga. som brutto- och nettovärden och beräknas genom korrekt geometrisk formrepresentation av elementet.

**Beräknade värden ur modellen efter export**

De andra är att program man IFC-modellen beräknar vissa mängder utifrån data i IFC-filen. Vad som visas och hur det beräknas ärmjukvar.

**Av användaren valda mängder bifogade i egenskapsuppsättning vid export**

De tredje att inkludera mängder specificera i en egen egenskapsuppsättning.

Olika aktörer har olika krav på vilket data som behöver redovisas. Från ett objekt kan man härleda olika värden för en viss storhet, till exempel arean ett objekt? Vid anbudsberäkning behövs en översiktlig beräkning av till exempel volym betong, men vid gjutningen på bygget vill man ha en mer precis för att slippa hantera spill.

