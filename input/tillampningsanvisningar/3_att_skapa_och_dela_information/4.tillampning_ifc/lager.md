# Lager (*IfcPresentationLayerAssignment*)

**Bestäm om eventuell lagerhantering även ska redovisas i IFC-modellen.**

## Vad?

*IfcPresentationLayerAssignment* kan jämföras med CAD-lager. Det är ett attribut som används för en enklare gruppering av objekt i syfte att styra synlighet och presentationsstil (färg, linjetyp och linjebredd).

## Varför?

Attributet lager kan användas för att få objektinformation från modelleringsprogram som inte hanterar objekt, utan som är lagerbaserade (till exempel AutoCAD®).

## Hur?

Följande attribut finns tillgängliga för lager i IFC:

| **Attribut**                | **Exempel (värde)**     | **Förklaring**                                                                             |
| --------------------------- | ----------------------- | ------------------------------------------------------------------------------------------ |
| *Name<sup>1</sup>*          | A-43CB- -E-             | Namnet på lagret.                                                                          |
| *Description<sup>2</sup>*   | Innerväggar (ej stomme) | Ytterligare beskrivning av lagret förutom namnet                                           |
| *AssignedItems<sup>2</sup>* |                         | Gruppering av objekt som ingår i det här lagret.                                           |
| *Identifier<sup>2</sup>*    |                         | En (intern) identifierare tilldelad till lagret. En alfanumerisk sträng, maskinellt läsbar |

<sup>1</sup> Obligatorisk (värde läggs in av programmet)

<sup>2</sup> Valfri (värde kan anges av användaren)

*Tabell: Attribut för lagerhantering i en IFC-fil*

Med hjälp av *IfcPresentationLayerWithStyle* utökas möjligheten att styra presentationen av lagren. Synligheten kan styras utifrån på/av (*LayerOn*) och fryst/tinat (*LayerFrozen*). Det går också göra så att lagret inte kan ändras genom att vara låst/inte låst (*LayerBlocked*).

Det går också att ge objekt i lagret en viss ”stil” för presentation med hjälp av attributet *LayerStyles.* Vissa objektstilar har dock företräde och påverkas inte av lagerstilen. Exempel på stilar som råder över lagret är *IfcCurveStyle*, *IfcFillAreaStyle*, *IfcTextStyle* och *IfcSurfaceStyle*.

| **\#** | **Attribut**               | **Exempel (värde)** | **Förklaring**                                                                                                 |
| ------ | -------------------------- | ------------------- | -------------------------------------------------------------------------------------------------------------- |
| 5      | *LayerOn<sup>2</sup>*      | TRUE                | Lagret är på (*On*) och är synligt. Övriga tillåtna värden är *FALSE* och *UNKNOWN*                            |
| 6      | *LayerFrozen<sup>2</sup>*  | FALSE               | Lagret är tinat (*Not Frozen*) och är synligt. Övriga tillåtna värden är *TRUE* och *UNKNOWN*                  |
| 7      | *LayerBlocked<sup>2</sup>* | TRUE                | Lagret är låst (*Blocked*) och kan inte redigeras i IFC-filen. Övriga tillåtna värden är *FALSE* och *UNKNOWN* |
| 8      | *LayerStyles<sup>2</sup>*  |                     | En (intern) identifierare tilldelad till lagret.                                                               |

<sup>1</sup> Obligatorisk (värde läggs in av programmet)

<sup>2</sup> Valfri (värde kan anges av användaren)

*Tabell: Attribut för styrning av lagers synlighet i en IFC-fil*

<table>
    <tbody>
        <tr>
            <td><b>&#9432;</b></td>
            <td><b>TÄNK PÅ</b></td>
        </tr>
        <tr>
            <td></td>
            <td>I Sverige har CAD-lager använts länge för att koda geometrisk information, främst i AutoCAD-baserade program. Om lagerfunktionen i IFC används så tänk på att inte ange lager på samma sätt som i en CAD-fil samtidigt som samma information finns på objekten med sina olika attribut. Risken finns att informationen blir motstridig.</td>
        </tr>
    </tbody>
</table>
