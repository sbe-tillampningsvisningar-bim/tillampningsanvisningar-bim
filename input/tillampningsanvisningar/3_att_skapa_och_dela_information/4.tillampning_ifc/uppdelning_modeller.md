
# Uppdelning av modeller

**Dela upp innehållet i modellerna så att det blir praktiskt hanterbart.**

## Vad?

När informationsleveranser via IFC genomförs bör det finnas principer för hur informationen ska delas upp i olika filer. En förutsättning för detta är att filerna kan särskiljas via en bestämd namngivning.

## Varför?

Namngivningen tydliggör innehållet ur olika aspekter.

## Hur?

Uppdelningen av en modell i flera filer bör alltså utgå från strukturen för gällande filbenämning enligt leveranskraven. Vanligen delas filer upp per ansvarig part, innehåll, och geografisk utbredning. Nedan följer rekommendationer för dessa principer.

#### Byggnad

Vanligtvis delas innehållet i filer upp enligt följande princip:

1.  **Ansvarig part**: enligt SS 32202:2011.
2.  **Geografisk utbredning**: per byggnad. Geometrier som inte ingår i en byggnad, exempelvis markytor, gator, belysning, och installationer i mark, delas vanligtvis upp per fastighet.
3.  **Innehåll**: enligt SS 32271:2016 eller ett klassifikationssystem, enligt praxis BSAB96.

Ofta sammanfaller ansvarig part och innehåll, men de anges på olika sätt och plats i filnamnet. Exempelvis betecknar E, i positionen för ansvarig part, el-projektör, medan siffran 60 i positionen för innehåll betecknar innehållet ”Sammansatt redovisning av el- och telesystem” enligt BSAB 96.

Det förekommer även att innehållet måste delas upp i flera modeller. Anledningen kan vara att renodla varje modell utifrån innehåll, eller att minska storlek på modellfilerna.

Ett exempel för el blir då att 60-modellen delas upp i modeller för 61 Kanalisation, 63 Elkraftsystem, 64 Telesystem och 66 System för spänningsutjämning och elektrisk separation.

Ett annat exempel är att arkitekten redovisar 46 Rumskompletteringar i en separat A-modell.

Inom många industrier har uppdragsgivaren ofta redan en struktur för uppdelning av modeller som skiljer sig från det som beskrivs ovan. Det viktiga är att tidigt i projekten strukturera uppdelning av modeller.

#### Infrastruktur

I infrastrukturprojekt kan även modeller struktureras utifrån entreprenaduppdelningen. Detta är rimligt att göra om det handlar om en geografisk uppdelning eller någon form av tydlighet i ansvarsfördelningen för att kunna dela upp modellerna enligt entreprenadansvar.