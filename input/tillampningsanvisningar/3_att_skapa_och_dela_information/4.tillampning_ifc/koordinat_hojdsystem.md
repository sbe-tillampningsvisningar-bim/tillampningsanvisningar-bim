# Koordinat-och höjdsystem (IfcCoordinateReferenceSystem)

**Redovisa aktuellt koordinat- och höjdsystem samt eventuell insättningspunkt .**

## Vad?

För att kunna beskriva ett byggnadsverks placering används koordinatsystem, eller CoordinateReferenceSystem (CRS), som det heter inom IFC.

Ett koordinatsystem är ett referenssystem med definierad utbredning vars syfte är att kunna beskriva var en punkt befinner sig i för hållande till andra punkter.

![Koordinataxlar](../4.tillampning_ifc/media/koordinataxlar.png)

Vid modellering av ett byggnadsverk behöver man ta ställning till var och hur byggnadsverket ska placeras, dels i koordinatsystemet i 3D-modelleringsprogrammet, dels i verkligheten (i förhållande till jordklotet). Dessa två koordinatsystem behöver relateras till varandra.

I infraprojekt används i princip alltid det verkliga koordinatsystemet vid modellering, men för husprojekt bör ett lokalt koordinatsystem användas där byggnadsmodellen är placerad nära origo i modellen. Denna nollpunkt (origo) ska dock vara relaterad till en punkt och en vridning i det verkliga koordinatsystemet. Vid export till IFC rekommenderas att använda verklighetens koordinatsystem, alternativt det koordinatsystem som kravställts i projektet.

#### Koordinatsystem i 3D-modelleringsprogram

Alla 3D-modelleringsprogram har ett internt koordinatsystem. Placeringen av modellen i detta koordinatsystem ska styras upp i projektet. I mark- och infraprojekt används regelmässigt den lokala versionen av det nationella systemet SWEREF 99. Här ligger origo på ekvatorn och 15 grader öst. Koordinaterna blir därmed stora; för Stockholm ungefär 674000,6580000 för öst-väst respektive nord-syd, med enhet i meter.

Om dessa koordinater skulle användas med millimeter som enhet blir värdena för stora för CAD-programmen att hantera, och hopplösa för mänsklig läsning. I husprojekt, som modelleras i millimeter bör i stället origo placeras nära, men utanför byggnaden på ett sådant sätt att byggnaden placerats i positiva koordinater. Om koordinatsystemet dessutom roteras ortogonalt efter byggnadens huvudriktning blir koordinaterna små och följer de system- och stomlinjer som skapas.

Om en sådan husmodell ska visas i det nationella systemet används inställningarna för konvertering som finns i IFC-filen. Modellen krymps med en faktor 1/1000, roteras och flyttas till rätt läge i världen.

#### Koordinatsystem i verkligheten

För den verkliga världen finns det flera koordinatsystem. Jordens yta kan beskrivas med ett koordinatsystem beståenda av longitud och latitud. Detta görs till exempel i WGS84, som är ett globalt **geodetiskt referenssystem** som är vanligt i GPS-mottagare. Sådana system tar hänsyn till jordens krökning. Detta görs inte i **projicerade koordinatsystem**, där approximationer har gjorts för att räta ut axlarna i ett lokalt, platt koordinatsystem. Exempel på ett projicerat koordinatsystem är SWEREF 99, som är ett nationellt system för Sverige.

I IFC motsvaras geografiskt koordinatsystem av *IfcGeographicCRS* och projecerat koordinatsystem av *IfcProjectedCRS*.

![Geografiskt och projicerat koordinatsystem](../4.tillampning_ifc/media/geografiskt.png)

*Figur 1: geografiskt och projicerat koordinatsystem (IfcGeographicCRS och IfcProjectedCRS)*

Inom samhällsbyggnadssektorn där inte så stora områden hanteras används projicerade koordinatsystem. Det är viktigt att ange vilket system som ska användas, både vad avser i plan och i höjdled, då utgångsläget kan skilja sig åt mellan olika system.

För byggnadsverk i Sverige är det vanligast att använda SWEREF 99, som är indelat i tolv regionala kartprojektioner. Som exempel ligger Stockholm i SWEREF 99 18 00.

Dessutom anges en EPSG-kod som korrigerar för det fel som blir när den sfäriska ytan plattas ut. För placering i höjdled används RH-systemet (RH 2000), som är Sveriges nationella höjdsystem.

I SWEREF-systemet är orienteringen i plan. X-axeln är positiv i östlig riktning och Y-axeln är positiv i nordlig riktning.

![Väderstreck](../4.tillampning_ifc/media/vaderstreck_liten.png)

## Varför?

En modell ska fungera i olika sammanhang och i andra sammanhang än det som det skapades för, till exempel i GIS.

Koordinatsystem är avgörande för byggprojekt eftersom de tillhandahåller ett standardiserat sätt att lokalisera och positionera objekt inom projektområdet. De säkerställer att placeringen av olika element som vägar, broar, installationer och byggnader är exakt synkroniserade med varandra och med objekten i projektet. Att fastställa koordinatsystemet möjliggör en konsekvent och standardiserad metod för datautbyte.

Utan ett definierat koordinatsystem finns det en risk för felaktig placering, vilket kan leda till problem som ineffektivitet, säkerhetsrisker och projektförseningar.

Möjligheten att hantera koordinatsystem implementerades redan i IFC 4. Då introducerades nya attribut för objektplacering. Dessa attribut är *IfcCoordinateReferenceSystem*, *IfcProjectedCRS* och *IfcMapConversion*. I IFC 4x3 introducerades även *IfcGeographicCRS* med stöd för geografiska koordinater. IfcMapConversion hanterar inte geodetiska koordinater.

När en karta tas fram måste en approximation göras för att ta hänsyn till jordens sfäriska form.

I bilden nedan visas hur koordinattransformationen görs i steg allt eftersom inzomningen ökar. Med *IfcProjectedCRS* projiceras jordens sfär till en platt karta. Ju större område som visas desto större blir felet.

Med hjälp av *IfcMapConversion* kan koordinatsystemet flyttas mellan olika inzomade områden och behålla tillräckligt bra noggrannhet för koordinaterna i området.

Med *IfcLocalPlacement* anges respektive objekts koordinater

![CRS](../4.tillampning_ifc/media/crs.png)

*IfcProjectedCRS (Källa: BIM Corner (bimcorner.com)*

## Hur?

Vid export till IFC görs en transformering, så att byggnaden i IFC-filen hamnar rätt i globalt koordinatsystem, till exempel SWEREF och RH 2000. I IFC-modellen är objekten placerade i relation till ett koordinatsystem och alla positioner anges i ett lokalt koordinatsystem. I IFC-filen anges sedan en förskjutning till önskat koordinatsystem. Koordinatsystemet i IFC-filen anges i alla tre dimensioner (X, Y, Z).

Med *IfcMapConversion* appliceras en gemensam skafaktor för alla tre koordinataxlarna. Detta används om olika enheter används lokalt och globalt. Med *IfcMapConversionScaled*, kan olika skalfaktorer appliceras per koordinataxel.

Koordinater läses alltid (X,Y,Z), men vilket koordinatsystem som används måste också anges. I IFC-filen lagras det som metadata i *IfcProjectedCRS*.

Det finns stöd för att ange koordinat- och höjdsystem i IFC 4x3, men om programvarorna saknar stöd för detta rekommenderas att använda en egenskapsuppsättning (ett *IfcPropertySet*) på Site-nivå där information om vilket koordinatsystem som använts kan läggas in.

När olika discipliner och teknikområden modellerar sina delar ska det gå att sätta ihop dessa till en helhet som kan betraktas i sitt verkliga läge. Olika program har olika förutsättningar för koordinathanteringen därför är det viktigt att exporten till IFC görs på ett sådant sätt att delarna relateras till det verkliga koordinatsystemet.

Viktigt att styra upp med avseende på koordinatsystem är:

-   **Placering i plan:** nollpunkten (insättningspunkten) eller en känd punkt i byggnaden ska ligga i en punkt som är väl definierad (bra koordinater och inga decimaler) och känd i det verkliga koordinatsystemet.
-   **Vridning:** rotationen av koordinatsystemet i 3D-modelleringsprogrammet i förhållande till det verkliga koordinatsystemet ska vara känd. Rotationen kommer av att det lokala koordinatsystemet bör orienteras efter byggnaden .
-   **Höjdplacering:** nollpunkten (insättningspunkten) eller en känd punkt i byggnaden ska vara känd i det verkliga höjdsystemet. Det är vanlig att byggnaden placeras i Z=0 i det lokala höjdsystemet, men kan **också**  placeras rätt i förhållande till det verkliga höjdsystemet.
-   När det gäller infra ska byggnadsverket ligga i verkligt system både i plan och höjd.

I IFC-filen framgår valt koordinatsystem och höjdsystem genom den EPSG-kod som angivts. EPSG är en internationell databas för geodetiska parametrar, där samtliga svenska system finns registrerade.

I EPSG finns det ofta kombinerade koordinatsystem (plan + höjd), till exempel https://epsg.io/5850. Det finns även möjlighet i *IfcProjectedCRS* att separat specificera *VerticalDatum*. Där EPSG-kod saknas finns möjlighet att använda *IfcWellKnownText* för att specificera koordinatsystem enligt OGC-standarden.

#### Byggnad

-   Modellering bör alltid göras i ett koordinatsystem där modellen placeras nära nollpunkten (origo) i positiva koordinater.
-   Vid export till IFC ska det koordinatsystem som är kravställt väljas. Lämpligt är att välja SWEREF 99.

#### Infrastruktur

Vid export av IFC 4.3 för leverans bör korrekt koordinatsystem anges med en EPSG-kod, till exempel anges EPSG-kod 3011 för SWEREF 99 18 00. Modellen får inte roteras i förhållande till georeferensens koordinatsystem. Rotation mellan karta och modell ska alltså vara noll (0) grader. Detta gäller oberoende av utförarens modelleringspraxis.

| **Attribut** | **Exempel (värde)**       | **Förklaring**                                                                                      |
|--------------|---------------------------|-----------------------------------------------------------------------------------------------------|
| Name2        | EPSG:5850                 | EPSG-kod. **Endast en kod!** Om mer information behöver läggas till ska *IfcWellKnownText* användas |
| Description2 | SWEREF 99 18 00 + RH 2000 | Information om vilket koordinatsystem som använts.                                                  |

1 Obligatorisk (värde läggs in av programmet) 2 Valfri (värde kan anges av användaren)

*Tabell: Attribut för IfcCoordinateReferenceSystem*

| **Attribut**   | **Exempel (värde)** | **Förklaring**                                                                                                                                                                                                              |
|----------------|---------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|                |                     |                                                                                                                                                                                                                             |
| VerticalDatum<sup>2</sup> | RH 2000             | Här anges vilket höjdreferenssystem som använts. Denna kan användas om motsvarande information inte framgår avattributet *Name*. Ange endast en kod. Om mer information behöver läggas till ska *IfcWellKnownText* användas |
| MapProjection<sup>2</sup> |                     | Information om vilket projicerat koordinatsystem som använts.                                                                                                                                                               |
| MapZone<sup>2</sup>       |                     | Namnet på den zon, i relation till MapProjection, som använts.                                                                                                                                                              |
| MapUnit<sup>2</sup>       |                     | Enhet på axlar i aktuellt koordinatsystem.                                                                                                                                                                                  |

<sup>1</sup> Obligatorisk (värde läggs in av programmet)

<sup>2</sup> Valfri (värde kan anges av användaren)

*Tabell: Attribut för IfcProjectedCRS*

#### GIS och kart-modellering

Beroende på modelleringsprogram används ”Bounding Box” i GIS-värden för att hålla sig inom definierade områden vid modelleringen.

#### Kravställning och leverans

I kravställningen ska det framgå vilket koordinatsystem som ska användas vid leverans av IFC-modeller.

## Tänk på

-   Axlarnas riktningar är inget som alltid blir rätt automatiskt. Projektör och modellhanterare måste vara medvetna om detta föra att placera byggnaden rätt i ett rätt definierat koordinatsystem i projektet.   
    ![Väderstreck](../4.tillampning_ifc/media/vaderstreck_liten.png)
-   Koordinatsystem hanteras olika i IFC2x3 och IFC4.3. I IFC2x3 används *IFCLOCALPLACEMENT* och i IFC4.3 används *IFCMAPCONVERSION*. Alla programtillverkare har inte implementerat detta korrekt i IFC4 och 4x3 vilket gör att koordinaterna i IFC-filen ej tas hänsyn till när den öppnas i en programvara med felaktig inställning.  
      
    För att veta om din IFC-fil exporterats korrekt enligt version 4.3 ska den innehålla raden *IFCMAPCONVERSION*:

#### Exempel av IFC4.3 fil:

![Exempel](../4.tillampning_ifc/media/exempel_koordinater.png)

<table>
    <tbody>
        <tr>
            <td><b>&#9432;</b></td>
            <td><b>TÄNK PÅ</b></td>
        </tr>
        <tr>
            <td></td>
            <td>
                <p>Många 3D-modelleringsprogram är beroende av grafikkort för både input och output av positioner i modellen. Grafikkorten har lägre noggrannhet än vad programmen räknar med internt. Detta gör att stora koordinater inte kan hanteras så bra och att decimaler riskerar att klippas bort. Detta problem undviks genom att placera byggnaden nära origo.</p>
                <p>Tänk på att hålla modellen ren från skräp så att det inte ligger objekt både vid origo och i byggnadens verkliga läge. Det kommer resultera i en IFC-fil med objekt med stora avstånd vilket riskerar ge grafikfel.</p>
                <p>Det koordinatsystem som kravställs vid leverans, är nödvändigtvis inte samma som används i modelleringsprogrammet under projekteringen. Under projekteringen kan tänkas att ett internt koordinatsystem används vid utbyte av data i programmets egna format. Definitionen av det interna koordinatsystem ska styras upp i början av projektet och relateras till det koordinatsystem som ska användas vid leveranserna.</p>
            </td>
        </tr>
    </tbody>
</table>
