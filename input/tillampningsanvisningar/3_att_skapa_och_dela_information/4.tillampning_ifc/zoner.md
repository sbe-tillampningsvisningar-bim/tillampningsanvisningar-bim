# Zoner (*IfcZone*)

**Gruppera utrymmen för att hantera utrymmen med likartade funktioner.**

## Vad?

En **zon** (*IfcZone*) är i IFC en rumslig funktion som skapas genom att gruppera utrymmen (*IfcSpace*). En zon kan alltså inte skapas självständigt. Ett utrymme kan ingå i flera zoner.

## Varför?

Genom att använda zoner på ett systematiskt sätt kan det användas som filter för flera olika ändamål. Det är ett bra verktyg för gruppering och filtrering av olika områden, som kan överlappa varandra i en byggnad.

Exempel på användning av zoner är att skapa en zon för varje typ av lägenhet i ett bostadshus och även skapa zoner för varje brandcell i samma hus. Ett utrymme, som ett kök, kommer då att dels tillhöra en zon för en lägenhetstyp, dels en zon för en viss brandcell.

## Hur?

Utrymmen grupperas till zoner genom att använda den objektifierade relationen *IfcRelAssignsToGroup* som har supertypen *IfcGroup*.

Varje zon har ett namn (*Name*) och en tillhörande typ (*ObjectType*). Med detta attribut kan zonens namn kompletteras för att tydliggöra funktion eller typ. Ett exempel är en zon som får *Name* A och *ObjectType* 3-rumslägenhet.

Vissa zoner kan det finnas många av, till exempel en typ av lägenhet i ett bostadshus. Andra zoner är unika, till exempel en byggnad i ett område med flera byggnader där varje byggnad kan vara en zon.

#### Zonnamn och zonsnummer

Nedan visas en tabell med de attribut som används för zoner. Fyra informationssträngar för zoner är listade nedan med de attribut som ska användas.

-   Zonnummer *Name* (Synligt på ritning/i modell för rumsidentifikation)
-   Zonnamn *Long name* (Synligt på ritning/i modell för rumsidentifikation)
-   Zonbeskrivning *Description*
-   Zonfunktion *Object type*

Notera att attributet ”*PredefinedType*” måste vara satt till *USERDEFINED för att* kunna använda attributet *ObjectType.*

| **Attribut**              | **Exempel (värde)**    | **Förklaring**                                                                                                  |
| ------------------------- | ---------------------- | --------------------------------------------------------------------------------------------------------------- |
| **IfcRoot**               |                        |                                                                                                                 |
| Name<sup>2</sup>          | 1103                   | Zonnummer. Unikt nummer för zonen, till exempel ett lägenhetsnummer, som är unikt(om varje lägenhet är en zon). |
| Description<sup>2</sup>   | Större trerumslägenhet | Förtydligande beskrivning av zonen, till exempel för kravställning eller förvaltning.                           |
| **IfcObject (5)**         | **Exempel**            | **Förklaring**                                                                                                  |
| ObjectType<sup>2</sup>    | Trerumslägenhet typ 2  | Typ av zon. Definition enligt uppdragsgivare eller projekt. *PredefinedType*=*USERDEFINED*                      |
| **IfcSpatialElement (6)** | **Exempel**            | **Förklaring**                                                                                                  |
| LongName<sup>2</sup>      | 3 R.O.K                | Zonnamn. Namn på zonen anges i relation till vad som anges under *Name* (som är ett nummer).                    |
| PredefinedType            | USERDEFINED            | För att kunna använda ”*ObjectType*” måste *PredefinedType* vara satt till ”*USERDEFINED*”                      |

<sup>1</sup> Obligatorisk (värde läggs in av programmet)<br> <sup>2</sup> Valfri (värde kan anges av användaren)

*Tabell: Attribut för zoner*

## Exempel

I figuren nedan visas två utrymmen i form av en stor och en liten volym. Det kan symbolisera en enrumslägenhet med badrum. Två zoner har skapats för lägenheten. Båda zonerna innehåller båda utrymmena. Den ena zonen har markerats med en rödstreckad linje och den andra zonen med en blåprickad linje.

![Lägenhetszon](../4.tillampning_ifc/media/lagenhetszon.png)

**Lägenhet – röd streckad linje**

Den röda streckade linjen är lägenhetsgräns och indikerar att båda utrymmena tillhör en och samma lägenhet. Zonen (lägenheten) har attributen:

*Name*: 1003 (lägenhetsnummer enligt Lantmäteriet)  
*LongName*: 1 R.O.K, plan 1  
*Description*: Enrumslägenhet med badrum, plan 1, Storgatan 8  
*ObjectType*: Enrumslägenhet typ 2  
*PredefinedType*: USERDEFINED

**Brandcell – blå prickad linje**

Den blå punktade linjen är brandcellsgräns och indikerar att de två utrymmena tillhör en och samma brandcell. Nedan visas två exempel med zoners attribut. Ex 1 kan användas i ett tidigt skede, när endast angivelse av brandcell är intressant. Ex 2 kan användas i ett senare skede när brandcellerna har fått definitioner och namn.

Ex 1:

*Name*: (ingen numrering av zonen har gjorts)  
*LongName*: (ingen namngivning av zonen har gjorts)  
*Description*: Brandcell (allmän beskrivning)  
*ObjectType*: (ingen typangivelse av zonen har gjorts).  
*PredefinedType*: (ingen då inte *ObjectType* används)

Ex 2:

*Name*: BC12 (Brandcell nr 2 på plan 1)  
*LongName*: Brandcell 2, plan 1   
*Description*: (ingen beskrivning av zonen har gjorts)  
*ObjectType*: (ingen typangivelse av zonen har gjorts).  
*PredefinedType*: (ingen då inte *ObjectType* används)

För brandzoner finns en egenskapsuppsättningen *Pset_SpaceFireSafetyRequirements*, som bör användas. Den innehåller relevanta brandegenskaper som kan användas för både utrymmen och zoner. Se vidare i avsnittet Egenskaper och Egenskapsuppsättningar.

Ett annat exempel på användning av zoner är en lägenhetsbyggnad där det finns tio lägenheter med tre lägenhetstyper: två ”fyra rum och kök”, fem ”tre rum och kök” och tre ”två rum och kök”.Varje lägenhetstyp är helt identisk och zoner kan skapas för varje lägenhetstyp, med *LongName* ”4 R.O.K”, ”3 R.O.K” och ”2 R.O.K”.

Låt säga att det finns tre likadana hus på tomten. Då kan ytterligare en zon för varje hus skapas. Ytterligare en zon skapas för hela huset, eftersom det finns tre likadana hus på tomten. Genom att ge de tre zonerna ett unikt nummer (*Name*) och ett namn (*LongName*) blir det möjligt att filtrera ut både lägenhetstyperna (via *LongName*) och varje enskild lägenhet (via *Name*) i respektive hus eller i alla tre husen, samtidigt. Samma gäller för utrymmena i zonerna.

<table>
    <tbody>
        <tr>
            <td><b>&#9432;</b></td>
            <td><b>TÄNK PÅ</b></td>
        </tr>
        <tr>
            <td></td>
            <td><b>Skapa zoner med eftertanke. Nyttan med zoner är att kunna hantera information som är gemensam för flera utrymmen och därmed få en enklare hantering av gemensam information.</b></td>
        </tr>
    </tbody>
</table>
