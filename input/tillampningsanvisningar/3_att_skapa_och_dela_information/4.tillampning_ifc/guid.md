# GUID (*IfcGloballyUniqueId*)

**Korrekt hantering av GUID är viktigt för spårbarhet.**

## Vad?

GUID står för *Global Unique Identifier*, och utgörs vanligen en sträng med 32 tecken. Varje objekt i en objektsmodell tilldelas ett unikt GUID. Ett GUID är statiskt, och ändras alltså inte under objektets livstid.

GUID används av program för att kunna skilja mellan olika objekt. Även om de är exakt lika i övrigt, kommer GUID vara olika. Tack vare detta går det att avgöra vilket objekt som avses vid till exempel ändringar.

Med hjälp av speciella programvaror kan objekt i två versioner av en modell jämföras. Jämförelsen görs med avseende på geometri, men även med avseende på attribut och egenskaper. Eftersom GUID är unikt kan programmet avgöra om ett objekt har ändrats, raderats eller tillkommit sedan förra versionen.

GUID används även av BCF-formatet för att kunna knyta kommentarer till bestämda objekt. Vid uppdatering i modellen kommer kommentaren fortfarande vara knuten till samma objekt.

## Varför?

GUID kan alltså användas för att spåra ändringar. Därför är det viktigt att modelleringen görs på rätt sätt. Om man raderar ett objekt och ersätter det med ett exakt likadant, så kommer det nya ha ett annat GUID. Det innebär att programmet kommer inse att det gamla har raderats och att det sedan har tillkommit ett nytt. För användaren ser det dock ut som att inget har hänt.

Om det är viktigt i projektet att bevara GUID, till exempel för att kunna spåra ändringar, så bör man anpassa sin modellering till det. Det är då bättre att justera ett objekt i stället för att ersätta det med ett annat som har önskade egenskaper.

GUID kan också användas till att dokumentera ändringar på ett automatiserat sätt. Det blir lätt att göra en lista över vilka objekt som påverkas av en ändring. Detta kan man sedan använda som dokumentation på hur en ändring från en part påverkar arbetet för en annan part. Om till exempel arkitekten flyttar en dörr i en betongvägg kommer ändringen att påverka konstruktören.

## Hur?

Vid projektstart är det viktigt att bestämma hur GUID ska hanteras. Till exempel kan krav ställas på att från och med en viss tidpunkt ska GUID användas för att identifiera förändringar av unika objekt. Från den tidpunkten får objekt som behöver ändras inte raderas och ersättas med ett nytt.

<table>
    <tbody>
        <tr>
            <td><b>&#9432;</b></td>
            <td><b>TÄNK PÅ</b></td>
        </tr>
        <tr>
            <td></td>
            <td>
                <p>GUID:en är tänkt att användas av datorprogram och inte av människor. Det utgör ett 128-bitars tal som kan skapas med olika bas i olika program. Söker man då efter en specifik GUID-sträng i en IFC-fil är det alltså inte säkert att man hittar det. Det finns webbaserade verktyg där man kan konvertera mellan varianter av GUID, baserade på olika baser.</p>
                <p>Höjden på ett utrymme ska definieras så att alla objekt som ska ha en rumstillhörighet ligger inom inom denna höjd.</p>
                <p>Varje objekt ska ha en ansvarig part. Den som äger objektet ansvarar också för dess GUID. Det finns exempel på objekt som ägs av en part, men där en annan part måste göra en förändring. Vissa förändringar kan då påverka GUID. Nedan listas några exempel:</p>
                <ul>
                    <li>Rum måste hanteras konsekvent med avseende på GUID. Det gäller alla objekt som andra är beroende av.</li>
                    <li>Håltagning i stomobjekt och förfrågan om håltagning (*Provision for voids*):</li>
                    <li>Stomobjektet ägs av K, medan håltagningens placering och storlek definieras av till exempel installationsprojektören. Själva håltagningen bör göras av K utifrån installationsprojektörens förfrågan.</li>
                    <li>Utrymme (*Space*): Detta är ett objekt som många andra aktörer är beroende av. Utrymmen definieras oftast av A. Efter att det skapats och delats med andra får det inte raderas och skapas på nytt utan överenskommelse med alla berörda.</li>
                    <li>Våningsplan (*Storey*):<br> Även detta är ett objekt som många andra aktörer är beroende av. Våningsplan definieras oftast av A. Var definitionen för respektive våningsplan läggs är ett projektgemensamt beslut eller ett beställarspecifikt krav. Rekommendationen är att plushöjden för varje våningsplan läggs på färdigt golv. Efter att våningsplanen skapats och delats med andra får de inte ändras utan ett projektgemensamt beslut.</li>
                    <li>Ägandeskap av objekt:
                    <br>
                    Om någon i en annan disciplin importerar objekt till sitt program skapas en kopia med nytt GUID. Olika aktörer som till exempel förvaltare och tillverkande industrier kan ha krav på egna GUID eller andra typer av identifierande beteckningar för objekt i modellen. Det gör att det kan finnas flera GUID/beteckningar i en modell. Dessa behöver lagras separat i en egenskapsuppsättning med ett namn som förklarar vad det har för syfte, till exempel förvaltnings-GUID/beteckning.</li>
                </ul>
            </td>
        </tr>
    </tbody>
</table>
