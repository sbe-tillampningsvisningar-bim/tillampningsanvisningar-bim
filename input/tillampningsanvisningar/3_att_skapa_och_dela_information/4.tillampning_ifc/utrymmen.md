# Utrymmen (IfcSpace)

**Säkerställ att utrymmen och zoner nyttjas korrekt för valda tillämpningar.**

## Vad?

Utrymmen i IFC (*IfcSpace*) är avgränsade rum och områden i byggnader (*IfcBuilding*). Det finns även exteriöra utrymmen. Ett utrymme är:

-   en sluten area eller volym.
-   fysiskt eller abstakt
-   tillför specifika funktioner
-   kan grupperas i zoner (*IfcZone*).

Ett utrymme i en byggnad är knutet till ett våningsplan (*IfcBuildingStorey*). Byggnad, våning och utrymme är hierarkiskt knutna till varandra.

![Utrymme byggnad](../4.tillampning_ifc/media/utrymme_byggnad.png)

Ett exteriört utrymme är knutet till en byggplats (*IfcSite*) i stället för till byggnad eftersom våningsplan inte finns för exteriöra utrymmen.

![Utrymme exterior](../4.tillampning_ifc/media/utrymme_exterior.png)

## Varför?

Utrymmen och zoner är viktiga informationsbaser för andra delar i byggnaden. Många objekt och andra delar knyts till utrymmet de tillhör, och informationen kan användas i många sammanhang av olika parter.

Utrymmen används inte alltså enbart för att definiera ”rum” utan också för att fastställa till exempel den plats som krävs för en installation i ett större vindsutrymme.

Därför är det viktigt att även definiera höjden på utrymmet på ett sådant sätt att det är användbart för alla parter. Höjden bör läggas till underkant bjälklag. Om höjden bara definieras till undertak utesluts alla objekt som ligger ovanför undertak.

## Hur?

I ett projekt bör endast en eller ett fåtal definiera och skapa utrymmen. Det är desto fler som är beroende av och hämtar information om utrymmen för egna behov.

Utrymmen definieras vanligen av arkitekten. Omslutande väggar definieras av arkitekten eller konstruktören, beroende på vad det är för typ av vägg.

I IFC-standarden kan ett utrymme definieras på tre sätt. Förutom att ett utrymme kan vara singulärt (“ett rum”) kan ett utrymme bestå av flera, eller vara uppdelat i delar. I IFC-filen framgår det genom definitionen

-   COMPLEX = flera utrymmen i en grupp
-   ELEMENT = ett enskild utrymme
-   PARTIAL = en del av ett utrymme.

Enligt CoClass finns det två typer av utrymmen:

-   **byggt utrymme** (ett ”rum”), som avgränsas av golv, tak och väggar, alternativt en viss markyta utomhus
-   **aktivitetsutrymme** som kan utgöras av ett helt eller en del av ett byggt utrymme.

Ytterligare egenskaper för funktion, som till exempel brandklass, inbrottsskydd och fuktskydd kan läggas på antingen själva utrymmet eller på omslutande väggar, dörrar och fönster. Det senare rekommenderas, eftersom det tydliggör kraven på hur rummets avgränsning ska konstrueras och vilka produkter som ska installeras.

Andra krav, till exempel behovet av luftomsättning i ett rum, kan ställas av uppdragsgivaren, läggas in av arkitekten och därefter användas vid dimensionering luftkanaler av ventilationsprojektören.

Under *IfcZone* beskrivs hur utrymmen kan grupperas genom *IfcZone*.

Areor kan inte skapas utifrån ett IfcSpace. För att kunna beräkna areor måste olika typer av utrymmesobjekt skapas. Detta beskrivs mer under ”Areor”.

#### Rumsnamn och rumsnummer

Nedan visas en tabell med de attribut som används för utrymmen. Fyra viktiga informationssträngar för rum är listade nedan med de attribut som ska användas.

<table>
   <tbody>
      <tr>
         <td>Rumsnummer</td>
         <td><em>Name</em></td>
         <td>(synligt på ritning/i modell för rumsidentifikation)</td>
      </tr>
      <tr>
         <td>Rumsnamn</td>
         <td><em>Long name</em></td>
         <td>(synligt på ritning/i modell för rumsidentifikation)</td>
      </tr>
      <tr>
         <td>Rumsbeskrivning</td>
         <td><em>Description</em></td>
         <td></td>
      </tr>
      <tr>
         <td>Rumstyp/funktion</td>
         <td><em>Object type</em></td>
         <td></td>
      </tr>
   </tbody>
</table>

Notera att attributet ”*PredefinedType*” måste vara satt till *USERDEFINED* för att kunna använda attributet *ObjectType.*

| **Attribut**                      | **Exempel (värde)**                 | **Förklaring**                                                                                                                          |
| --------------------------------- | ----------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------- |
| **IfcRoot**                       |                                     |                                                                                                                                         |
| Name<sup>2</sup>                  | 1004                                | Rumsnummer. Unikt nummer för utrymmet. (Görs synligt på ritning/i modell för rumsidentifikation).                                       | Större handikapptoalett             | Förtydligande beskrivning av utrymmet. Valfri information om utrymmet, till exempel för kravställning, förvaltning eller annat ändamål. 
| **IfcObject (5)**                 | **Exempel**                         | **Förklaring**                                                                                                                          |
| ObjectType<sup>2</sup>            | Handikapptoalett typ 2, eller 38462 | Rumstyp. Definition i text eller kod enligt uppdragsgivare eller projekt. *PredefinedType*=*USERDEFINED*                                |
| **IfcSpatialElement (6)**         | **Exempel**                         | **Förklaring**                                                                                                                          |
| LongName<sup>2</sup>              | RWC                                 | Rumsnamn. Namn på utrymmet anges i relation till vad som anges under *Name*. (Görs synligt på ritning/i modell för rumsidentifikation). |
| **IfcSpace (4)**                  | **Exempel**                         | **Förklaring**                                                                                                                          |
| PredefinedType                    | USERDEFINED                         | För att kunna använda ”*ObjectType*” måste *PredefinedType* vara satt till ”*USERDEFINED*”                                              |
| ElevationWithFlooring<sup>2</sup> | +93,14                              | Golvnivå i detta utrymme (överkant färdigt golv). Om golvet är sluttande väljs medelvärdet.                                             |

<sup>1</sup> Obligatorisk (värde läggs in av programmet)

<sup>2</sup> Valfri (värde kan anges av användaren)

*Tabell: Attribut för utrymmen*

Exempel: i en IFC-fil visar följande rad ett utrymme med GUID [0wG_lZQs1C8fiDGL4d4Aqo]. Det är en handikapptoalett [RWC] med rumsnummer [1004]:

![kodstrang](../4.tillampning_ifc/media/kodstrang.png)

Delar som oftast inte kopplas till utrymmen är konstruktionens bärande delar, administrativa stödfunktioner, som till exempel stomlinjer, och de rumsavgränsande objekten, som väggar. Däremot är det viktigt att veta vilka väggar och objekt som skapar ett utrymme och vilka egenskaper dessa objekt har (exempelvis ljudklass och brandskydd). Denna typ av egenskaper bör även läggas på nivån för zon.

Ett utrymme placeras rätt i IFC-filen utifrån *IfcPlacement*, som är en generell inställning i IFC-filen. *IfcPlacement* kan ställas in på tre sätt:

-   **absolut** i det koordinatsystem som används i projektet
-   **relativt** ett annat objekt
-   **relativt sekundärnätet**, som kan vara byggnadens systemlinjer och sekundärlinjer.

## Exempel

I figuren nedan visas två utrymmen i form av en stor och en liten volym. Det kan symbolisera en enrumslägenhet där den stora volymen är ett vardagsrum med en köksdel och den lilla volymen är ett badrum.

![Volym lagenhetszon](../4.tillampning_ifc/media/volym_lagenhetszon.png)

För den stora volymen har utrymmet attributen:

*Name*: 201 (unikt rumsnummer enligt projektet)  
*LongName*: V.rum m kök  
*Description*: Vardagsrum med köksenhet.  
*ObjectType*: Vardagsrum typ 2.  
*PredefinedType*: USERDEFINED

För den lilla volymen har utrymmet attributen:

*Name*: 202 (unikt rumsnummer enligt projektet)  
*LongName*: WC/Bad  
*Description*: Badrum med dusch och WC.  
*ObjectType*: Badrum typ 1.  
*PredefinedType*: USERDEFINED

Varje utrymme kan ha egenskaper kopplade till sig från *PsetSpaceCommon*. Några vanliga egenskaperna är vägg-, golv- och takbeläggning (*WallCovering, FloorCovering* och *CeilingCovering*). Ytterligare egenskaper kan vara tillgängligheten, såsom rullstolsanpassad och/eller allmänt tillgänglig läggas till.

<table>
    <tbody>
        <tr>
            <td><b>&#9432;</b></td>
            <td><b>TÄNK PÅ</b></td>
        </tr>
        <tr>
            <td></td>
            <td>
               <p>Utrymmen är objekt som många aktörer är beroende av. Det är en fördel om endast en aktör är ansvarig för definitionerna och skapandet av utrymmen. Vanligen har arkitekten den uppgiften. De måste skapas på ett för projektet funktionellt sätt.</p>
               <p>Höjden på ett utrymme ska definieras så att alla objekt som ska ha en rumstillhörighet ligger inom inom denna höjd.</p>
               <p>Kom ihåg att</p>
               <ul>
                  <li>ett utrymme inte behöver vara avgränsat av väggar (gränsen kan vara fiktiv)</li>
                  <li>ett byggt utrymme kan därmed innehålla flera utrymmen, typ ”allrum” med ”pentrydel”.</li>
               </ul>
               <p>Egenskaper såsom brandklass, ljudklass och inbrottsskydd rekommenderas att läggs på objekten som om sluter et utrymm, alltså på väggar, dörrar och fönster, medan kravställningen ofta görs på själva utrymmet. Det är då viktigt att ha en rutin för att kontrollera de egenskaper som är satta på omslutande objekt.</p>
               <p>Alla utrymmen där objekt finns ska representeras av ett <em>IfcSpace</em>. Detta gäller till exempel vindsutrymmen där ventilationsaggregat eller annan vital installation finns.</p>
            </td>
        </tr>
    </tbody>
</table>
