


# Linjeföring (IfcAlignment)

**Hantera objekt som definierar långsträckta objekt.**

## Vad?

Objektet **linjeföring** (*IfcAlignment*) är specifikt framtaget för tillämpning inom infrastruktur för att kunna placera och utforma **långsträckta objekt** i en modell och sedan relatera till verkligheten.

Linjeföring är ett abstrakt objekt i IFC och det har tre syften:

-   att definiera ett referenssystem för linjär positionering
-   att styra geometrisk utformning av långsträckta objekt, såsom vägar och järnvägar
-   för skydd och optimering av fordons rörelser längs en sträcka 

Den sista punkten ligger utanför ämnet för detta dokument.

*IfcAlignment* bygger på ISO 19148:*2021Geografisk information – Linjära referenssystem*.

## Varför?

Genom att hantera långsträckta objekt, såsom vägar och spår, på ett definierat sätt kan de presenteras med hjälp av IFC. Objektet linjeföring möjliggör exakt referens till positioner längs en linje eller kurva i x-,y- och z-led.

## Hur?

Vid modellering av långsträckta objekt behöver modelleringsprogrammen information om vad det är för objekttyp och vad objektet har för anläggningstillhörighet. Egenskaper som finns högre upp i den rumsliga hierarkin ärvs ner, men ytterligare egenskaper kan läggas på.

| **Attribut**    | **Exempel** | **Förklaring**                                                                                                                                                                                                                                                                                                                     |
|-----------------|-------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| PredefinedType<sup>2</sup> |             | En lista med typer för att kunna identifiera objektet ytterligare. Eventuellt kan någon egenskapsuppsättning appliceras till en av dessa typer. Det här attributet ska inte användas om PredefinedType är satt till ” *USERDEFINED*”. (För att kunna använda ”*ObjectType*” måste *PredefinedType* vara satt till ”*USERDEFINED*”) |

<sup>1</sup> Obligatorisk (värde läggs in av programmet) <br><sup>2</sup> Valfri (värde kan anges av användaren)

*Tabell: Attribut för IfcAlignment*

#### Hierarki av placeringsobjekt

IFC har en hierarki som används för att beskriva var ett objekt är placerat. För linjeobjekt gäller att:

-   ett **linjärt placeringsobjekt** (*IfcLinearPositioningElement*) är ett abstrakt objekt som beskriver positionering längs en kurva.
-   **linjeföring** (*IfcAlignment*) är en undertyp av IfcLinearPositioningElement och används för att beskriva en beräknad linje, exempelvis en väglinje. Det används också för att definiera referens och placering av element längs en beräknad linje.

#### Skillnad mellan verksamhetsaspekter och geometrisk definition


Konceptet för linjeföring är organiserat i två delar. Båda delarna kan samverkan, men kan också användas oberoende av varandra.

-	Verksamhetsaspekter av linjeföring.<br>
Med IFC-schemat kan terminologi och begrepp som ligger nära det verksamhetsmässiga användas. Det gör det möjligt att beskriva till exempel horisontell och vertikal rälsföring, rälsens struktur samt attribut. Med verksamhetsaspekterna kan även domänspecifika egenskaper som dimensionerande hastighet eller rälsförhöjning användas.
-	Geometrisk definition av linjeföring<br>
Med IFC-schemat kan IFC-geometriska element användas. 

#### Layouter för linjeföring
I IFC beskrivs en linjeföring utifrån en eller flera av följande layouter
-	horisontell geometri (IfcAlignmentHorizontal), som visar linjeföringen projicerad horisontellt (x- och y-värde).
-	vertikal geometri (IfcAlignmentVertical), som visar linjeföringen projicerad vertikalt (z-värde) längs den horisontella geometrin.
-	sidolutning (IfcAlignmentCant), som visar lutningen vinkelrätt mot linjen (typiskt i kurvor).


#### Exempel

![Exempel IfcAlignment](../4.tillampning_ifc/media/exempel_ifcalignment.png)

*Figur: IfcAlignment för Railway i den Rumsliga hierarkin för infrastruktur i IFC.*
