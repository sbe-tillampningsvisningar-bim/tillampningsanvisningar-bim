
# Byggnad (IfcBuilding)

**Definiera fastighetens byggnader**

## Vad?

Byggnad definieras i IFC som en nivå i den rumsliga hierarkin. Den relaterar till andra IFC-klasser på så sätt att en byggnad tillhör en specifik fastighet (IfcSite) och kan bestå av våningsplan (IfcBuildingStorey) och utrymmen (IfcSpace)

![Hierarki Byggnad](../4.tillampning_ifc/media/hierarki_byggnad.png)

I IFC-standarden kan en byggnad (*IfcBuilding*) definieras på tre olika sätt. Förutom att en byggnad kan varasingulär “ett hus” kan en byggnad bestå av flera byggnader (som sitter ihop eller är fristående från varandra) eller vara uppdelad i delar. I IFC-filen anges detta med attributet *IfcSpatialStructureElement* genom någon av de tre definitionerna

-   COMPLEX = flera byggnader i en grupp
-   ELEMENT = en enskild byggnad
-   PARTIAL = en del av en byggnad

Rekommendationen är att en modell endast ska innehålla en byggnad och då ska attributet tilldelas värdet ”ELEMENT”. Om ingen specifikation görs tilldelas värdet ELEMENT automatiskt

## Varför?

I och med att en byggnadn ingår i den hierarkiska strukturen ärver den sin identifikation via attributet Name. På det sättet går det att härleda i vilken byggnad ett objekt är placerat bara genom att titta på objektets egenskaper. Detta är till exempel användbart för att skapa referensbeteckningar utifrån objektens placeringar.

Det är även viktigt att kunna definiera varje byggnads geometriska omfattning eller utbredning samt gränsdragning mot andra byggnader. Detta underlättar vid till exempel uppdelning av modeller, som oftast delas upp per byggnad.

”Byggnad” behövs som en del i hierarkin för den rumsliga ordningen. Den hierarkiska ordningen är till för att kunna relatera objekts tillhörighet och beroende på ett strukturerat sätt. Detta beskrivs mer under ”Rumslig Hierarki”.

## Hur?

Varje IFC-klass beskrivs med hjälp av attribut. för att beskriva byggnader används följande attribut.

*Tabell: Attribut för IfcBuilding*

| **Attribut**         | **Beskrivning**                                                                                                                                                                                                                     |
|----------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 1. GlobalId<sup>1</sup>         | Global unik identifikation som automatiskt skapas i programvaran under modelleringen. Den används för samverkan mellan olika programvaror och är inte gjord för mänsklig tolkning.                                                  |
| 2. OwnerHistory<sup>2</sup>     | Information om ägaren.                                                                                                                                                                                                              |
| 3. Name<sup>2</sup>             | Byggnadens beteckning, oftast ett byggnadsnummer, ex. 5186.                                                                                                                                                                         |
| 4. Description<sup>2</sup>      | En förtydligande beskrivning av byggnaden. Detta attribut kompletterar Name och LongName.                                                                                                                                           |
| 5. ObjectType<sup>2</sup>       | Byggnadstyp. Detta attribut får enbart användas om attributet *PredefinedType* har värdet USERDEFINED. (Se vidare i avsnittet om Entiteter och Typer.)                                                                              |
| 6. ObjectPlacement<sup>2</sup> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;   | Byggnadens läge i gällande koordinatsystem.                                                                                                                                                                                         |
| 7. Representation<sup>2</sup>   | Objektets visuella representation                                                                                                                                                                                                   |
| 8. LongName<sup>2</sup>        | Byggnadens populärnamn, ex. HUS 1. Detta attribut kompletterar Name.                                                                                                                                                                |
| 9. CompositionType<sup>2</sup> | Möjlighet att ange om byggnaden är ett byggnadsverkskomplex bestående av flera byggnader (COMPLEX), en enskild byggnad (ELEMENT), eller en del av en byggnad (PARTIAL). Om fältet lämnas tomt tilldelas värdet ELEMENT automatiskt. |

<sup>1</sup> Obligatorisk (värde läggs in av programmet)<br><sup> 2</sup> Valfri (värde kan anges av användaren)

Attributen Name och LongName bör kravställas vid informationsleveranser med IFC-formatet. I vissa fall kan det även vara bra att kravställa attributet Description.

## Egenskaper och egenskapsuppsättningar

Det finns ett antal egenskapsuppsättningar för byggnad. I Pset_BuildingCommon finns allmänna egenskaper, såsom till exempel byggnadstyp (vad byggnaden används till). I Pset_SpaceCommon finns allmänna egenskaper för utrymmena i byggnaden, såsom till exempel om ett utrymme är handikappanpassat.

![Propertysets](../4.tillampning_ifc/media/propertysets.png)

## Mängder och kvantitet

Mängder och kvantiteter som behöver tas fram i en byggnad relateras till byggnaden även om de görs för våningsplans- eller utrymmesnivå.

Mängder, till exempel areor, relateras till en byggnad genom *IfcElementQuantity*. I tabellen nedan visas mängder som kan tas fram. Det är höjder, areor och volymer. Denna typ av mängder ska beräknas enligt nationell standard och redovias som egenskaper (Property) i en mängduppsättning (Quantity Set).

*Tabell: Egenskaper (Properties) för Areor och Volymer för hel byggnad enligt mängduppsättningen Qto_BuildingBaseQuantities*

| **Namn**       | **Värdetyp**  | **Förklaring**                                                                                                                                                                                                                                                                          |
|----------------|---------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Height         | 3000          | Standardvåningshöjden ÖK bjälklag till ÖK bjälklag. Används bara om höjden är samma för alla våningsplan. Anges i millimeter.                                                                                                                                                           |
| EavesHeight    | 2750          | Standardvåningshöjden mellanbjälklagen från ÖK bjälklag till UK bjälklag våningen över. Används bara om höjden är samma för alla våningsplan. Anges i millimeter.                                                                                                                       |
| FootPrintArea  | 5500          | Byggnadsarea [BYA]. Byggnadens fotavtryck (”kontur”) på marken enligt nationell standard. Anges i m2.                                                                                                                                                                                   |
| GrossFloorArea | 27500         | Bruttoarea [BTA] Totala summan av alla areor i byggnaden, ofta inklusive stomme. Definitionen kan anpassas till nationell standard eller beräkningsmetod. Anges i m2.                                                                                                                   |
| NetFloorArea   |  24000        | Nettoarea [BTA]  Totala summan av alla areor i byggnaden, ofta exklusive stomme. Definitionen kan anpassas till nationell standard eller beräkningsmetod. Anges i m2.                                                                                                                   |
| GrossVolume    |  82500        | Bruttovolym [BTV]. Totala summan av alla volymer i byggnaden, ofta inklusive stomme. Definitionen kan anpassas till nationell standard eller beräkningsmetod. BTV enligt svensk standard utgörs av en byggnadsvolym begränsad av omslutande byggnadsdelars utsida. Anges i m3.          |
| NetVolume      |  72000        | Nettovolym [NTV]. Totala summan av alla volymer i byggnaden, ofta exklusive stomme. Definitionen kan anpassas till nationell standard eller beräkningsmetod. . NTV enligt svensk standard utgörs av en eller flera utrymmen begränsade av omslutande byggnadsdelars insida. Anges i m3. |


