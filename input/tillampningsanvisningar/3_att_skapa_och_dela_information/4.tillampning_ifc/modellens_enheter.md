
# Modellens enheter (IfcUnitAssignment)

**IFC-formatet hanterar eventuella skillnader mellan SI-enheter i olika modeller.**

## Vad?

Vilka enheter som används för att ange storheter i IFC-filer hanteras av IFC-standarden. Den som ska modellera i CAD väljer lämplig enhet; i Sverige självklart millimeter eller meter. Filer som modellerats i olika enheter och som exporterats till IFC kan sedan sättas ihop. Om exempelvis en arkitektmodell är modellerad i millimeter och en markmodell i meter kan exporterade IFC-modeller sammanfogas så att de får samma enhet.

Programmet som utför exporten väljer enhet som skrivs in i IFC-filen. När en IFC-modell lyfts in i ett program väljer användaren vilken enhet som skall visas.

## Varför?

Enheten som är inställd i modellen påverkar de aktörer som vill kunna länka in modellen i sina modeller av olika anledningar. I IFC-filer hanteras enheten på ett litet annorlunda sätt än i flera CAD-program.

## Hur?

Vill man kontrollera vilken enhet som gäller i en IFC-fil kan man öppna den i en textredigerare och läsa rader som inleds med *IFCSIUNIT*. I exemplet nedan framgår att enheten för längd är millimeter och enheten för area är kvadratmeter (m2) (rader med röd text).

-   Rad 6 (\#6) enhet för längd:  
    längdenhet [LENGTHUNIT], prefix är milli [MILLI], enheten är meter [METRE]
-   Rad10 (\#10) enhet för area:  
    Enhet för area [AREAUNIT], prefix är valfritt [\$], enheten är m2 [SQUARE_METRE]

#### Exempel

![Modellens enheter](../4.tillampning_ifc/media/modellens_enheter_exempel.png)

IFC-formatet fungerar så här för enheter:

*IFCUNITASSIGNMENT* bestämmer vilken enhet som alla värden i filen har. I exemplet framgår att informationen plockas från flera *IFCSIUNIT*-rader som till exempel längd, area, volym och tid (\#27 i exemplet). Dessa definitioner av enheter påverkar inte egna attribut som man själv har definierat i en egen egenskapsuppsättning.

I *IFCUNITASSIGNMENT* finns en lista på referenser till rader där storheterna definieras. I detta exempel innehåller rad \#6 informationen om vilken längdenhet (LENGTHUNIT) som används i IFC -filen. Där står "METRE" vilket betyder att enheten meter används. I exemplet står även "MILLI", vilket är prefixet till enheten, det vill säga millimeter används.

Information inkluderas om hur alternativa måttenheter ska användas, till exempel brittiska.

Detta styrs av entiteterna *IFCCONVERSIONBASEDUNIT* som redovisar vilken enhet man kan konvertera till, i detta fall "FOOT" för längdmått. *IFCMEASUREWITHUNIT* redovisar konverteringsfaktorn, i detta fall 304,8. Värden för dessa entiteter styrs av ISO 10303-41 och ISO/CD 10303-41:1992. De två sista entiteterna är frivillig information mottagande program kan använda om enhet ska bytas.

<table>
    <tbody>
        <tr>
            <td><b>&#9432;</b></td>
            <td><b>TÄNK PÅ</b></td>
        </tr>
        <tr>
            <td></td>
            <td>
                <p>Det är viktigt att kontrollera att man själv använder rätt enhet. Använder du meter eller tum?</p>
                <p>Tar man fel mall i modelleringsprogrammet så att man har en mall för tum, men modellerar som om det vore millimeter kommer det vara mycket tidskrävande att göra om modellen i rätt enhet. Enheten spelar sedan ingen roll när man exporterar till IFC.</p>
            </td>
        </tr>
    </tbody>
</table>
