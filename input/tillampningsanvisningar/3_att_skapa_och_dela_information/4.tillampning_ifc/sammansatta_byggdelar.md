# Sammansatta byggdelar (*IfcElementAssembly*)

**Bestäm vad som ska hanteras som enskilda delar respektive montagedelar.**

## Vad?

En sammansatt byggdel (*IfcElementAssembly*) är en entitet som grupperar en eller flera delar, men som inte har någon egen geometrisk avbildning. En sammansatt byggdel kan bära egna attribut, egenskaper, och egenskapsuppsättningar. De riktar sig främst till tillverkare och byggare samt de som tar fram modeller för dessa syften.

## Varför?

Det engelska begreppet “assembly” avser en sammansatt byggdel – ett ”byggelement” – bestående av en eller flera delar som är sammanfogade med hjälp av fästelement, så som skruv, spik, och svets, eller som del i ett gjutet objekt, till exempel armering eller gods i ett betongelement.

Byggelement kan också avse samlade betongelement (*cast unit*) som bildar en gjutomgång (*pour*), till exempel kantbalkar, voter, fundament och platta. Dessa gjuts i ett stycke men kanske har modellerats som flera enskilda delar. Vid redovisning bör dessa grupperas som ett objekt inklusive armering, gods, isolering med mera, det vill säga precis så som det ska utföras i verkligheten.

Andra exempel kan vara en stålpelare med fotplåt, knap, livplåtar och infästningar. Alla dessa delar sammanfogas på stålverkstaden och den färdiga pelaren levereras ut till bygget som ett stycke. Detta vill man kunna redovisa i modellen, det vill säga redovisa vilka delar som hör ihop i en montagedel. Samma gäller prefabricerad betong. Där vill man redovisa en sandwichvägg som ett stycke, bestående av innerskiva, ytterskiva, hyllor, isolering, armering, stegar, el, gods med mera.

## Hur?

Vid export till IFC kan man inkludera sammansatta byggdelar, och då lagras alla ingående delar som ett objekt (*IfcElementAssembly*). De enskilda delarna kommer också att läggas med i filen. När man ska ange information i en egenskapsuppsättning kan man välja att lägga det på enskild del eller på den sammansatta byggdelen. Exempel på data man lägger på enskild del kan vara material. Exempel på data man lägger på montagedelen kan vara montagedatum på byggplats.

För varje typ av egenskap och egenskapsuppsättningar bör man ange på vilken nivå informationen ska läggas, det vill säga på enskild del eller på sammansatt byggdel.

<table>
    <tbody>
        <tr>
            <td><b>&#9432;</b></td>
            <td><b>TÄNK PÅ</b></td>
        </tr>
        <tr>
            <td></td>
            <td>
                <p>Kontrollera att informationen i egenskapsuppsättningar söks på rätt nivå. Hur man gör det ser olika ut i olika program.</p>
                <p>Vid export till IFC där informationen om sammansatta byggdelar tas med så kommer alla objekt tillhörande den sammansatta byggdelen att ingå, även om man valt att ta med endast en delmängd av dem.</p>
            </td>
        </tr>
    </tbody>
</table>
