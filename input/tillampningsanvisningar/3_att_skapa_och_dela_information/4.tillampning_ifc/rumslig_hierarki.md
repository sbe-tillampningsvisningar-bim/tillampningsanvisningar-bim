
# Rumslig hierarki (IfcSpatialStructureElement)

**Anpassa modellens rumsliga hierarki efter typ av byggnadsverk.**

## Vad?

IFC använder sig av en rumslig hierarki för att organisera och lokalisera objekt i ett byggnads- eller infrastrukturprojekt. Nedbrytningen i nivåer åskådliggörs i figur nedan. Varje del betraktas som ett *IfcSpatialStructureElement* inom IFC.

![Rumslig hierarki](../4.tillampning_ifc/media/rumslig_hierarki.png)

*Figur: Rumslig hierarki enligt IFC. Den engelska termen "Facility" motsvarar den svenska termen "byggnadsverk" enligt ISO 12006-2, och omfattar byggnader och anläggningar.*

Med svenska termer kan hierarkin beskrivas som att ett projekt kan innehålla en eller flera byggplatser, som kan innehålla ett eller flera byggnadsverk, som innehåller byggnads- eller anläggningsdelar.

Egenskaper som läggs på objekt på respektive nivå ärvs sedan ner till alla underliggande objekt.

## Varför?

Genom att dela upp projektet utifrån en fysisk hierarki tydliggörs sammanhanget för de olika delarna och hanteringen underlättas.

## Hur?

De två översta nivåerna är samma för infrastruktur och byggnad.

-   **IfcProject** är projektet och anges med en benämning i klartext.
-   **IfcSite** är platsen för projektet och anges med en benämning i klartext.

Nivåerna därunder skiljer sig något beroende på att infraprojekt har en ofta lång utbredning, medan byggnader är mer avgränsade. Principen är dock densamma.

#### Byggnad

För byggnad ser hierarkin ut enligt figuren nedan. Nedbrytningen under ”Site” görs i tre nivåer. I tabellen beskrivs de olika delarna.

![Rumslig hierarki byggnad](../4.tillampning_ifc/media/hierarki_byggnad.png)

*Figur: Rumslig hierarki av byggnader i IFC.*

| **Nivå**                            | **Beskrivning**                                                                                                                                                                                                                                                                                                                                               |
| ----------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **IfcProject**                      | Benämning av projektet. Anges med en benämning i text.                                                                                                                                                                                                                                                                                                        |
| &nbsp; **IfcSite**                  | Den fysiska platsen där projektet genomförs. Fastighet eller byggplats där byggnaden är placerad. Geometrier som inte ingår i en byggnad, exempelvis markytor, gator, belysning och installationer under markytor, ska placeras inom *IfcSite*. De ska alltså varken vara placerade i *IfcBuilding* eller *IfcBuildingStorey*. Anges med en benämning i text. |
| &nbsp; &nbsp; **IfcBuilding**       | För byggnader representeras *IfcFacility* av entiteten *IfcBuilding*, det vill säga byggnad. Anges med byggnadsnummer eller byggnadsnamn.                                                                                                                                                                                                                     |
| &nbsp; &nbsp; **IfcBuildingStorey** | För byggnader representeras *IfcFacilityPart* av entiteten *IfcBuildingStorey*, det vill säga våningsplan. Våningsplanen är alltså en viktig del i den strukturella nedbrytningen av en byggnad.                                                                                                                                                              |
| &nbsp; &nbsp; &nbsp; **IfcSpace**   | Utrymme i en byggnad. Byggt utrymme eller aktivitetsutrymme.                                                                                                                                                                                                                                                                                                  |

Önskas ytterligare nedbrytning, till exempel i trapphus, lägenhet och avdelning används *IfcZone*, där utrymmen kan kombineras ihop i zoner.

#### Infrastruktur

För infrastruktur ser hierarkin ut enligt följande figur. Nedbrytningen efter ”Site” görs i två nivåer, men inom varje nivå beskrivs olika infrastrukturobjekt enligt sin tillhörighet (väg, bro, järnväg och hamn). I tabellen beskrivs de olika delarna.

![Rumslig hierarki infra](../4.tillampning_ifc/media/hierarki_infra.png)

*Figur: Rumslig hierarki för infrastruktur i IFC.*

*Tabell: Rumslig hierarki för infrastruktur i IFC.*

| **Nivå**                                                                                                                       | **Beskrivning**                                                                                                                                                                                                                               |
| ------------------------------------------------------------------------------------------------------------------------------ | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **IfcProject**                                                                                                                 | Benämning av projektet. En geografiskt avgränsad del. För spårbunden trafik är det ett stråk som har en sträckning mellan två definierade platser. Ett stråk utgörs av en eller flera sammanhängande bandelar. Anges med en benämning i text. |
| &nbsp; **IfcSite**                                                                                                             | Den fysiska platsen där projektet genomförs, en byggplats. Kan även vara en delmängd av ett projekt, exempelvis en entreprenad. Anges med en benämning i text.                                                                                |
| &ensp; **IfcBridge/** <br> &ensp;  **IfcMarineFacility/** <br> &ensp; **IfcRailway/** <br> &ensp; **IfcRoad**                  | För infrastruktur representeras *IfcFacility* av följande entiteteter: *IfcBridge* (bro), *IfcMarineFacility* (marin anläggning), *IfcRailway* (spåranläggning), och *IfcRoad* (väg och gata)                                                 |
| &emsp; **IfcBridgePart/** <br> &emsp; **IfcMarineFacilityPart/** <br> &emsp; **IfcRailwayPart**    <br> &emsp; **IfcRoadPart** | För infrastruktur representeras *IfcFacilityPart* som en ytterligare uppdelning av ovan nämnda entiteter.                                                                                                                                     |

#### Exempel

![Rumslig hierarki infra rail](../4.tillampning_ifc/media/hierarki_infra_rail.png)

*Figur: Rumslig hierarki för infrastruktur ”IfcRailway” i IFC, så som den presenteras i en BIM-viewer.*

#### Komplexitet

I den rumsliga hierarkin kan varje entitet (*IfcSpatialStructureElement*) ses på tre sätt. Förutom att representera sig själv som en singulär, fristående entitet (*Element*) kan den vara en av flera entiteter i en gruppering (*Complex*) eller bara en del av en större entitet (*Part*). I IFC-filen framgår detta genom attributet *CompositionType*. Standardvärdet är ”ELEMENT” om inte attributet används.

-   COMPLEX = en av flera entiteter i en grupp
-   ELEMENT = en enskild entitet
-   PARTIAL = en del av en större entitet

| **Attribut**                | **Exempel** | **Förklaring**                                                                               |
| --------------------------- | ----------- | -------------------------------------------------------------------------------------------- |
| CompositionType<sup>2</sup> |             | Anger entitetens komplexitet. Om inte CompositionType anges gäller standardvärdet ”ELEMENT”. |

<sup>1</sup> Obligatorisk (värde läggs in av programmet)

<sup>2</sup> Valfri (värde kan anges av användaren)

*Tabell: Attribut för IfcSpatialStructureElement*