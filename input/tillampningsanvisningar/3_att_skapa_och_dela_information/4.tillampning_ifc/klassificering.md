
# Klassificering (IfcClassificationReference och IfcClassification)

**Bestäm metod för klassificering.**

## Vad?

I IFC skiljer sig klassificering från övrig information om objekt som redovisas via attribut eller via egenskaper i olika egenskapsuppsättningar. I stället används två specifika entiteter:

-   *IfcClassificationReference*, som anger klassificeringen av aktuellt objekt
-   *IfcClassification*, som anger aktuellt klassifikationssystem.

Attributnamnen kan tyckas vara missvisande, men ”referensen” i den första entiteten är alltså objektets referens till en viss klass. Den andra entiteten ger detaljer om det klassifikationssystem som används.

Det är heller inget krav på att använda båda. Även *IfcClassificationReference* visar var det använda klassifikationssystem finns. Om denna uppgift räcker för alla som använder klassifikationen behöver man alltså inte med *IfcClassification* visa detaljerna om det. Rekommendationen är dock att använda båda.

#### Klassificering – *IfcClassificationReference*

Nedan redovisas attributen för IfcClassificationReference.

*Tabell: Attribut för IfcClassificationReference*

| **\#** | **Attribut**        | **Exempel (värde)**            | **Förklaring**                                                                          |
|--------|---------------------|--------------------------------|-----------------------------------------------------------------------------------------|
| 1      | *Location<sup>2</sup>*         | https://coclass.byggtjanst.se/ | Adress till aktuellt klassifikationssystem.                                             |
| 2      | *Identification<sup>2</sup>*   | RNB31                          | Klassifikationskod.                                                                     |
| 3      | *Name<sup>2</sup>*             | Tilluftsdon                    | Tillhörande benämning till aktuell klassifikationskod.                                  |
| 4      | *ReferencedSource<sup>2</sup>* | CoClass_KO                     | Hänvisning till aktuellt klassifikationssystem och tabell.                                         |
| 5      | *Description<sup>2</sup>*      |                                | Kompletterande beskrivning av aktuell kod och benämning.                                |
| 6      | *Sort<sup>2</sup>*             |                                | Möjlighet att ange en kompletterande identifierare för att kunna sortera informationen. |

<sup>1</sup> Obligatorisk (värde läggs in av programmet)<br> <sup>2</sup> Valfri (värde kan anges av användaren)

Det är möjligt att använda flera *IfcClassificationReference* för att beskriva ett objekt, till exempel både CoClass och BSAB 96. Det går också att kombinera en formell klassmed en objektindelning som används i ett befintligt system för fastighetsunderhåll. Attributet *Sort* används då för att visa vilken av systemen som har företräde. I en IFC-modell kan man använda båda klassifikationerna för att hitta objekt i en modell.

#### Klassifikationssystem – *IfcClassification*

*IfcClassification* redovisar information om vilken tabell i ett klassifikationssystem som används, till exempel tabellen för komponenter enligt CoClass eller tabellen för byggdelar enligt BSAB 96.

För att redovisa flera tabeller används flera entiteter av *IfcClassification*, en för varje tabell. För att redovisa både tabellen för komponenter och konstruktiva system enligt CoClass behövs alltså en entitet av *IfcClassification* där *Name* är ”CoClass_KO” (tabellen Komponenter), och en entitet där *Name* är ”CoClass_KS” (tabellen Konstruktiva system).

Nedan redovisas attributen för *IfcClassification*.

*Tabell: Attribut för IfcClassification*

| **\#** | **Attribut**       | **Exempel (värde)**                    | **Förklaring**                                                                                                                                                        |
|--------|--------------------|----------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 1      | *Source<sup>2</sup>*          | Svensk Byggtjänst                      | Utgivare av klassifikationssystemet.                                                                                                                                  |
| 2      | *Edition<sup>2</sup>*         | 3.5.0                                  | Version av aktuell tabell.                                                                                                                                            |
| 3      | *EditionDate<sup>2</sup>*     | 2023-04-19                             | Datum när aktuell version publicerades.                                                                                                                               |
| 4      | *Name<sup>1</sup>*            | CoClass_KO                             | Namn på aktuellt klassifikationssystem samt eventuellt   tabell.                                                                                                                                               |
| 5      | *Description<sup>2</sup>*     | CoClass tabell för konstruktiva system | Kompletterande beskrivning av klassificeringssystem och tabell.                                                                                                                         |
| 6      | *Specification<sup>2</sup>*   | https://coclass.byggtjanst.se/         | Adress till aktuellt klassifikationssystem.                                                                                                                           |
| 7      | *ReferenceTokens<sup>2</sup>* | [‘.’, ‘/’]                             | Möjlighet att ange gällande skiljetecken och antal nivåer i klassificeringssystemet. (Exemplet till vänster visar skiljetecknen för BSAB96 Byggdelar, t.ex. 27.D/11.) |

<sup>1</sup> Obligatorisk (värde läggs in av programmet)<br> <sup>2</sup> Valfri (värde kan anges av användaren)

#### Exempel

Figuren nedan visar ett exempel på en klassificering av ett tilluftsdon. Där framgår exempelvis att klass- och typkoden (RNB31) och benämningen (Tilluftsdon) av tilluftsdonet redovisas med hjälp av *IfcClassificationReference* (”Reference” i trädstrukturen i figuren nedan) som i sin tur relaterar till en *IfcClassification* (nivån över ”Reference” där det står CoClass_KO).

![Exempel Classification](../4.tillampning_ifc/media/exempel_classification.png)

Vad gäller referensbeteckningar, som kan vara baserade på klassifikationssystem, ska dessa inte redovisas via *IfcClassification.* För referensbeteckningar rekommenderas i stället att nyttja egenskapen *AssetIdentifier* i egenskapsuppsättningen *Pset_ConstructionOccurence*.

Både geometriska och abstrakta objekt bör klassificeras. Nedanstående abstrakta objekt bör klassificeras:

-   *IfcSite*
-   *IfcBuilding*
-   *IfcBuildingStorey*
-   *IfcSpace*

*IfcSite:*<br>
![IfcSite](../4.tillampning_ifc/media/ifcsite.png)<br>

*IfcBuilding:*<br>
![IfcBuilding](../4.tillampning_ifc/media/ifcbuilding.png)<br>

*IfcBuildingStorey:*<br>
![IfcBuildingStorey](../4.tillampning_ifc/media/ifcbuildingstorey.png)<br>


## Varför?

Att klassificera objekt är nödvändigt för att kunna hantera information strukturerat. Vilket eller vilka klassifikationssystem som väljs skiljer sig från projekt till projekt och mellan olika organisationer och länder. Dessutom utvecklas klassifikationssystem över tid, vilket gör det viktigt att dokumentera vilken version av ett klassifikationssystem som nyttjades när informationen om respektive objekt skapades.

## Hur?

Vid kravställning av informationsleveranser via IFC-formatet är det viktigt att ange vilket klassifikationssystem som ska användas och vilken klassifikation (vilken/vilka tabeller) som ska redovisas per objekt samt hur informationen ska redovisas.

Rekommendationen är att ange klassifikationssystem via *IfcClassification* och klassificeringen (värdet) på respektive objekt via *IfcClassificationReference e*ftersom information om klassifikationssystem och version inte är möjlig att få via IFC-standardens attribut och egenskaper Använd inte användardefinierade egenskaper och egenskapsuppsättningar för klassificering.

#### Användning av flera klassifikationstabeller

Eftersom det finns många olika klassifikationssystem är det svårt att inom IFC-standarden skapa egenskaper för varje klassifikationssystem. Om det gjordes skulle det exempelvis kunna resultera i att IFC-standarden innehåller egenskaper för MasterFormat, Uniclass och Omniclass men inte för CoClass och BSAB 96, vilket skulle orsaka problem för oss i Sverige, eller tvärt om. Genom att däremot möjliggöra användning av obegränsat antal av *IfcClassification* kan användarna själva skapa platshållare för olika typer av klassificering av objekt.

#### Namnkonvention för klassifikationssystem

Vid informationshantering behövs två delar tydliggöras för klassificering, vilket klassifikationssystem samt vilken tabell i klassifikationssystemet som används. Genom att följa en namnkonvention för vad som används underlättas informationshanteringen. Benämning av använt klassifikationssystem bör följa namnkonventionen nedan:

[Klassifikationssystem]_[ev. tabellkod]

-   [Klassifikationssystem] – Benämning av aktuellt klassifikationssystem.
-   [Ev. tabellkod] – Kod som specificerar vilken tabell av klassifikationssystemet som avses.

Tabellen nedan redovisas några exempel på klassifikationssystem och hur de bör benämnas:

*Tabell: Exempel på klassifikationssystem och hur de bör benämnas*

| **Benämning** | **Klassifikationssystem**          | **Tabell**              |
|---------------|------------------------------------|-------------------------|
| BSAB83_PT1    | BSAB 83                            | Produkttabell 1         |
| BSAB83_PT2    | BSAB 83                            | Produkttabell 2         |
| BSAB96_BD     | BSAB 96                            | Byggdelar/Byggdelstyper |
| BSAB96_BV     | BSAB 96                            | Byggnadsverk            |
| BSAB96_PR     | BSAB 96                            | Produktionsresultat     |
| BSAB96_UT     | BSAB 96                            | Utrymmen                |
| CoClass_BV    | CoClass                            | Byggnadsverk            |
| CoClass_BX    | CoClass                            | Byggnadsverkskomplex    |
| CoClass_FS    | CoClass                            | Funktionella system     |
| CoClass_KO    | CoClass                            | Komponenter             |
| CoClass_KS    | CoClass                            | Konstruktiva system     |
| CoClass_PR    | CoClass                            | Produktionsresultat     |
| CoClass_UT    | CoClass                            | Utrymmen                |
| MASTERFORMAT  | MasterFormat                       |                         |
| OmniClass     | OmniClass                          |                         |
| SBEF          | Svenska Byggentreprenörsföreningen |                         |
| Uniclass_En   | Uniclass                           | Entities                |
| Uniclass_Ss   | Uniclass                           | Systems                 |
| UNIFORMAT II  | UniFormat                          |                         |


