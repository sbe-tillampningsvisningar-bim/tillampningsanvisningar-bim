# Byggplats, anläggningens fysiska placering (IfcSite)
**Anläggningens fysiska placering, en del i den rumsliga hierarkin.**

## Vad?
Den fysiska placeringen av en byggnad eller ett infrastrukturprojekt kallas IfcSite inom IFC. På svenska skulle man kunna säga byggarbetsplats, då det relateras till ett projekt.
IfcSite är också själva byggnaden eller anläggningens fysiska placering i förvaltningen.

För spårbunden trafik är det en bandel, som börjar och slutar i anslutning till en driftplats, driftplatsdel eller linjeplats. (Bandel definierar banans ekonomiska indelning enligt Trafikverket).

För en byggnad kan det vara fastigheten. Platsen anges med en benämning i klartext. Den kan vara fastighetsbenämningen enligt Lantmäteriet, till exempel ”1:234 Kv Huset”.

Läs mer om IfcSite i kapitlet om rumslig hierarki.
