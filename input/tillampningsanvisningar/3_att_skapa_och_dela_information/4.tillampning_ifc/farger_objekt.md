


# Färger på objekt

**Färg kan användas för att tydliggöra olika delari en IFC- eller mellan IFC-filer.**

## Vad?

Objekt i en IFC-modell har en färg och en transparensnivå. Genom att välja färg och transparensnivå kan man ge mottagaren en mer lättorienterad och lättläst modell.

*IfcSurfaceStyleRendering* är den parameter som styr ett objekts utseende. Den hämtar information från *IfcColourRgb* för att sätta färgen. Färgen beskrivs med tre decimaltal för RGB och varierar mellan 0 och 1, till exempel 0.5, 0.25, 0.25. Färgen kan också ges ett namn i klartext.

#### Material och ljus

Det finns även möjlighet att inkludera beskrivningar hur objekt ska se ut med hänsyn till material och ljus. Få programvaror har idag stöd för detta.

## Varför?

Genom att sätta färger på objekt på ett strukturerat sätt erhålls en mer lättläst modell. Det kan handla om att tydliggöra olika discipliners objekt och ansvar, att inom en disciplin tydliggöra valda delar eller att färga objekt utifrån en viss aktivitet, till exempel montagestatus. Färger kan även användas för att tydliggöra olika områden för resultat av en gjord analys inom . Färg kan alltså ändras beroende på vad syftet med en IFC-export är under projektets gång.

## Hur?

Färg väljs ofta vid export till IFC och kan vara baserat på attribut i modellen. I en IDS kan man ställa krav på vilken färg olika objekt ska ha lagrad IFC-filen.

Använd fördefinierade egenskaper på objekten för de olika syftena för färgningen. Med fördefinierade egenskaper avses egenskaper som fastställts inom projektet och som alla använder sig av vid IFC-export.

<table>
    <tbody>
    <tr>
        <td><b>&#9432;</b></td>
        <td><b>TÄNK PÅ</b></td>
    </tr>
    <tr>
        <td></td>
        <td>
            <p>Färgen som visas på objekt i en modell är inte statisk och kan sättas i en vy efter olika attribut för att åskådliggöra en viss egenskap. Färger i en vy kan vara statiskt tilldelade, till exempel för att färga delar efter etapper, eller dynamiskt tilldelade, till exempel för att färga objekt efter vilken montagestatus de har.</p>
            <p>Förutsätt aldrig att en viss färg på en viss del betyder något. Kontrollera vad färgen i aktuell vy betyder.</p>
        </td>
    </tr>
    </tbody>
</table>
