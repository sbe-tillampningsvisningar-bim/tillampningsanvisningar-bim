# Validering av IFC-modellen

**Kontrollera innehållet i IFC-modellen.**

## Vad?

Att validera IFC-modellen är viktigt för att få bekräftat att innehållet är strukturerat på rätt sätt. Validering kan utföras på många olika sätt.

buildingSMART har tagit fram en valideringsfunktion för att kunna kontrollera IFC-modeller. Bland annat kontrollerar funktionen att egenskapslistor och attribut som används har rätt benämning. Till exempel får egenskapade egenskapsuppsättningar (Property sets) inte benämnas pset.  

Hos buildingSMART International finns information om valideringsfunktionen (se länk).

För att kunna använda tjänsten behöver användaren ett användarkonto, som kan skapas av användaren själv. Själva hanteringen bygger på att IFC-filer dras in i det webbaserade verktyget.
Tänk på att tjänsten är en molntjänst, vilket potentiellt är en säkerhetsrisk. 
