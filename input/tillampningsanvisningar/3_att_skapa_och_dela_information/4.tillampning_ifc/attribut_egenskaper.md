


# Attribut och egenskaper (*Attribute* och *Property*)

**Bestäm vilken information som ska ligga på attribut respektive egenskaper.**

## Vad?

Information i en IFC-modell redovisas via **attribut** (*Attribute*) och **egenskaper** (*Property*), som grupperas i olika egenskapsuppsättningar (*PropertySet*). Attribut är en form av egenskap, men det är definierat   och strukturerat på ett visst sätt i IFC-modellen. Strukturen följer en hierarkisk ordning, där nivåerna ovanför bestämmer vad som kan ärvas nedåt. Detta blir tydligast när man använder en BIM-viewer och tittar på en entitet i en IFC-modell. Det är därför bra att känna till vilka de övre nivåerna för attributen är.

Följande attribut gäller för alla fysiskt existerande objekt (*IfcElement*) och ska således alltid finnas med i strukturen för en entitet.


*Tabell: IFC:s attribut överst i hierarkin .*

| **Attribut**        | **Beskrivning**                                                                                                                                                                    |
|---------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 1. GlobalId<sup>1</sup> &ensp; &ensp; &ensp; &ensp; &ensp; &ensp; &ensp; &ensp; &ensp; &ensp; &ensp; &ensp; &ensp; &ensp; &ensp; &ensp; &ensp;        | Global unik identifikation som automatiskt skapas i programvaran under modelleringen. Den används för samverkan mellan olika programvaror och är inte gjord för mänsklig tolkning. |
| 2. OwnerHistory<sup>2</sup>    | Information om ägaren till respektive entitet.                                                                                                                                     |
| 3. Name<sup>2</sup>            | Benämning av objektet.                                                                                                                                                             |
| 4. Description<sup>2</sup>     | Beskrivning av objektet, utöver *Name*.                                                                                                                                            |
| 5. ObjectType<sup>2</sup>      | Objekttyp. Detta attribut får enbart användas om attributet *PredefinedType* har värdet USERDEFINED. (Se vidare i avsnittet om Entiteter och Typer.)                               |
| 6. ObjectPlacement<sup>2</sup> | Objektets läge i gällande koordinatsystem.                                                                                                                                         |
| 7. Representation<sup>2</sup>  | Objektets visuella representation. Hanteras av programvaran och kan ej styras av användaren.                                                                                                                                                 |
| 8. Tag<sup>2</sup>             | Identifikation av respektive förekomst, till exempel ”Fönster 1”. Denna kan till skillnad från *GlobalId* vara mänskligt tolkbar. Denna kan exempelvis användas för referensbeteckningar.                                                 |
| X. PredefinedType<sup>2</sup>  | Objekttyp enligt fast värdelista inom IFC-formatet. (se vidare i avsnittet om Entiteter och Typer).                                                                                |

<sup>1</sup> Obligatorisk (värde läggs in av programmet) <br><sup>2</sup> Valfri (värde kan anges av användaren)

Av dessa är enbart *GlobalId* obligatorisk. Övriga attribut är valfria, men de flesta programvaruleverantörer väljer ändå att exportera information till dessa attribut. Denna ”mappning” mellan programvaran och IFC-formatet går oftast inte att ändra eller styra av användaren. Vissa programvaror har dock infört en möjlighet att skapa parametrar som överskrider denna mappning med så kallade *override parameters*.

Av de valfria attributen ovan är följande identifierande information:

-   Name
-   Description
-   ObjectType (eller PredefinedType)
-   Tag
-   PredefinedType.

De egenskaper (*Properties*) som redovisas i IFC-formatets egenskapsuppsättningar (*PropertySets*) innehåller däremot inte denna typ av identifierande egenskaper. Det vill säga, om dessa egenskaper ska anges via ”egenskaper”, alltså inte via attribut, måste de skapas som användardefinierade egenskaper i en användardefinierad egenskapsuppsättning. Detta beskrivs vidare i kapitlet ”Egenskaper och egenskapsuppsättningar”.

## Varför?

För att kunna sortera objekt i en IFC-modell är den identifierande informationen för varje entitet viktig att få med. Informationen som sådan kan läggas i attributen eller i användardefinierade egenskaper, som kan läggas i en användardefinierad egenskapsuppsättning. De två alternativen utesluter inte varandra. Det handlar om hur projektet vill tydliggöra viss typ av information.

## Hur?

Vid kravställning av informationsleveranser med IFC-formatet bör det framgå vilken identifierande information som kravställs och om denna ska beskrivas via attribut eller som egenskaper i en användardefinierad egenskapsuppsättning.

För att kunna kravställa information på dessa attribut är det viktigt att säkerställa att de originalprogramvaror som används vid export till IFC-formatet kan leverera detta, det vill säga att användaren har möjlighet att påverka innehållet i dessa attribut.

Nedan följer ett exempel på hur attributen kan kravställas:

*Tabell: Exempel på krav på identifierande attribut.*

| **Identifierande information** | **Beskrivning**                  | **Källa**                                                   | **Attribut**   |
|--------------------------------|----------------------------------|-------------------------------------------------------------|----------------|
| Benämning                      | Benämning i fritext.             | Användardefinierad                                          | Name           |
| Beskrivning                    | Beskrivning.                     | [www.bipkoder.se/\#/beteckningar](www.bipkoder.se/\#/beteckningar)<br> Kolumn: Underkategori       | Description    |
| Beteckning                     | Beteckning på typnivå (littera). | [www.bipkoder.se/\#/beteckningar](www.bipkoder.se/\#/beteckningar)<br>  Kolumn: Beteckning (TypeID) | ObjectType     |
| Referensbeteckning             | Unik identifikation.             | Enligt uppdragsgivarens referensbeteckningssystem.          | Tag            |
| Objekttyp                      |                                  | IFC-standarden. Ange alltid värdet USERDEFINED              | PredefinedType |

Nedan följer samma exempel hur den identifierande informationen kan kravställas som egenskaper i en egenskapsuppsättning.

*Tabell: Exempel på krav på identifierande egenskaper.*

| **Identifierande information** | **Beskrivning**                                    | **Källa**                                                   | **Egenskap**    | **Egenskaps- uppsättning** |
|--------------------------------|----------------------------------------------------|-------------------------------------------------------------|-----------------|----------------------------|
| Benämning                      | Benämning i fritext.                               | Användardefinierad                                          | TypeName        | SE-BIP                     |
| Beskrivning                    | Beskrivning.                                       | [www.bipkoder.se/\#/beteckningar](www.bipkoder.se/\#/beteckningar)<br> Kolumn: Underkategori       | TypeDescription | SE-BIP                     |
| Beteckning                     | Beteckning på typnivå (littera).                   | [www.bipkoder.se/\#/beteckningar](www.bipkoder.se/\#/beteckningar)<br> Kolumn: Beteckning (TypeID) | TypeID          | SE-BIP                     |
| Referensbeteckning             | Unik identifikation.                               | Enligt beställarens referensbeteckningssystem.              | ObjectID        | SE-BIP                     |
| Objekttyp                      | Används ej vid nyttjande av egenskapsuppsättningar |                                                             |                 |                            |

