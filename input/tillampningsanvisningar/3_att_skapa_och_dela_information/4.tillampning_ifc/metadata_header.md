# Metadata för IFC (HEADER)

**Säkerställer att korrekt metadata inkluderas när IFC levereras.**

## Vad?

Metadata är information om en datamängd, det vill säga ”data om data”. Varje IFC-modell inleds med ett avsnitt med metadata för själva IFC-modellen. Avsnittet rubriceras ”HEADER”. Därefter kommer modellinformationen, som rubriceras “DATA”. Informationen i en IFC-leverans består alltså av följande två delar:

1.  **HEADER** med information om IFC-filen. Här finns information om vem som skapat filen, vilken IFC- och MVD-version som använts vid export med mera.
2.  **DATA** är innehållet i IFC-filen i form av IFC-entiteter med tillhörande attribut och egenskaper, samt deras förhållanden till varandra.

![Exempel_HEADER_DATA](../4.tillampning_ifc/media/exempel_header_data.png)

*Figur: Exempel på HEADER och DATA i en IFC-fil. (Filen är öppnad i Windows Anteckningar)*

#### Struktur och attribut i IFC HEADER

HEADER innehåller ett antal fördefinierade metadatafält, som kommer med vid IFC-exporten.Dessa beskrivs i tabellen nedan.

*Tabell: Attribut i HEADER för IFC*

| **Attribut**                      | **Exempel (värde)**                             | **Förklaring**                                                                                                                                                                                                      |
| --------------------------------- | ----------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **FILE_DESCRIPTION**              |                                                 |                                                                                                                                                                                                                     |
| Description <sup>1</sup>          | ViewDefinition (Coordination View)              | Formell definition av den model view definition (MVD) som använts vid export. Denna information kan kompletteras med ytterligare valfri information om exporten, t.ex. “RevitIdentifiers” som i figuren i exemplet. |
| Implementation Level <sup>1</sup> | 2:1                                             | Efterlevnadsklass enligt ISO 10303–21 (STEP). För IFC-formatet är detta värde alltid 2:1.                                                                                                                           |
| **FILE_NAME**                     | **Exempel**                                     | **Förklaring**                                                                                                                                                                                                      |
| Name<sup>1</sup>                  | C:\\V-57-V-000.ifc                              | Lokal sökväg och namn för aktuell fil.                                                                                                                                                                              |
| Time Stamp<sup>1</sup>            | 2024–01-15T21:55:31+01:00                       | Datum och tidpunkt enligt ISO 8601 för när filen skapades.                                                                                                                                                          |
| Author<sup>2</sup>                | Skopis Skopis@byggarebob.se                     | Namn och e-postadress till den person som skapat filen.                                                                                                                                                             |
| Organization<sup>2</sup>          | Byggare Bob AB                                  | Namnet på det företag där personen som skapat filen arbetar.                                                                                                                                                        |
| Preprocessor version<sup>1</sup>  | ODA SDAI 23.12                                  | Namn och version för det verktyg som använts för att skapa filen, inte originalprogramvaran som skapat informationsinnehållet.                                                                                      |
| Originating System<sup>1</sup>    | Autodesk Revit 24.0.5.432 (ENU) - IFC 24.2.0.49 | Namn, version, och kompileringsnummer för originalprogramvaran.                                                                                                                                                     |
| Authorization<sup>2</sup>         | Bandis Bandis@byggarebob.se                     | Namn och e-postadress till den person som godkänt att filen levereras, t.ex. uppdragsansvarig.                                                                                                                      |
| **FILE_SCHEMA**                   | **Exempel**                                     | **Förklaring**                                                                                                                                                                                                      |
| Schema Identifiers<sup>1</sup>    | IFC4X3                                          | Namn på aktuell IFC-version.                                                                                                                                                                                        |

<sup>1</sup> Obligatorisk (värde läggs in av programmet)

<sup>2</sup> Valfri (värde kan anges av användaren)

## Varför?

Metadata behövs för att kunna identifiera ursprunget på den data som exporterats till IFC. Den metadata som redovisas i filens HEADER är viktig bland annat för att kunna kontrollera följande:

-   Är modellen levererad i rätt IFC-version med tillhörande MVD?
-   När skapades filen?
-   Vem är kontaktperson, om innehållet i modellen behöver diskuteras?
-   Vem har skapat filen och vem är uppdragsansvarig?
-   Vilken programvara och version har använts för att skapa modellen?
-   Vilken export-programvara och version har använts vid IFC-exporten?

Allt detta kan vara bra att veta om till exempel felsökning behöver göras i ursprungsmodellen.

## Hur?

Hur metadata genereras till IFC-filens HEADER beror på vilken programvara som används vid exporten. Hur fälten för METADATA fylls i varierar enligt följande två metoder:

1.  Metadata genereras automatiskt vid export till IFC-formatet baserat på programvaruleverantörens inställningar.
2.  Användaren väljer vilken data som skrivs till metadatafälten. Hur detta går till varierar beroende på vilken programvara som används. I vissa programvaror finns specifika fält där användaren anger metadata, exempelvis i Autodesk Revit. I andra programvaror hämtas informationen från fördefinierade fält där innehållet inte kan ändras av användaren. I det senare fallet är det viktigt att veta från vilka fält metadata hämtas så att rätt data kommer med vid exporten.

#### Ansvar för korrekt METADATA

Ansvaret för att metadata genereras på ett korrekt sätt ligger hos den som skapar IFC-modellen, alltså leverantören av informationen. Det kan vara modellansvarig hos respektive disciplin. I vissa fall exporteras metadata på ett felaktigt sätt. Vid dessa tillfällen bör denna person åtgärda felaktigheten i den exporterade filen via ett textredigeringsprogram, exempelvis Windows Anteckningar.

Metadata redovisas tydligast i programvaror som läser IFC-filer (en “BIM-viewer”). Nedan presenteras samma exempel med metadata i programmet Solibri . Informationen i HEADER redovisas på nivån *IfcProject*. För att granska metadata måste toppnoden i IFC-modellen markeras.

![Redovisning HEADER](../4.tillampning_ifc/media/redovisning_header.png)

*Figur: Exempel på hur HEADER kan redovisas  i en programvara som kan läsa IFC*

## Kravställning och leverans

För att säkerställa att metadata för IFC-modellen levereras på ett korrekt sätt ska mottagaren av leveransen ställa krav på metadata i HEADER. Det görs förslagsvis via en leveransspecifikation för objekt, men i nuvarande version av IDS (vers 1.0) går det inte att kontrollera krav på metadata via IDS-formatet. Kraven ska vara en del av projektets informationsutbyteskrav (EIR) i enlighet med anvisningarna i ISO 19650.
