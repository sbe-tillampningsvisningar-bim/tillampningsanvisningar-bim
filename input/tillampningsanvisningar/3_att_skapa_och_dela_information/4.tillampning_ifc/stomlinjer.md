# Stomlinjer (IfcGrid)

**Säkerställ att stomlinjer är definierade för valda tillämpningar.**

## Vad?

Stomlinjer beskrivs med IfcGrid. En instans av IfcGrid kan rymma flera stomlinjer i ett tvådimensionellt rutnät eller radiellt nät. Stomnätet kan ligga i alla riktningar (x, y, z). 

## Varför?

Stomlinjer används för att skapa en struktur i en byggnad så att byggnadsstommen med tillhörande objekt kan lokaliseras i ett system.

## Hur?

Varje instans av IfcGrid har eget GUID för identifikation. Namn på stomlinjer väljs av projektet. Namnet kan även innehålla mellanslag.

<table>
    <tbody>
        <tr>
            <td><b>&#9432;</b></td>
            <td><b>TÄNK PÅ</b></td>
        </tr>
        <tr>
            <td></td>
            <td><b>Använd rätt metod för att skapa stomlinjer i använt program så att informationen läggs i IfcGrid.</b></td>
        </tr>
    </tbody>
</table>
