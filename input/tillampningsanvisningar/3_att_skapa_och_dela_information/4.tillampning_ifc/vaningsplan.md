# Våningsplan (IfcBuildingStorey)

**Samordna våningsplanens benämningar, numreringar och höjder.**

## Vad?

Begreppet våningsplan är kopplat till en byggnad och definieras enligt ISO 6706-1 som ”ett utrymme mellan två på varandra följande våningar eller mellan ett golv och ett tak”.

Inom IFC kan ett våningsplan dock spänna över flera sammankopplade våningar. Det kan också vara en del av ett större våningsplan. Det finns alltså möjlighet att hantera byggnader med komplexa våningsstrukturer. Uppdelningen görs på samma sätt som för utrymmen.

-   COMPLEX = flera våningar i en grupp
-   ELEMENT = en enskild våning
-   PARTIAL = en del av en våning

Vid projektering av byggnader definieras nivån för varje våningsplan, alltså våningsplanets plushöjd. Dessa nivåer är gemensamma för alla i projektet och väsentliga ur flera aspekter. Därför får inte dessa nivåer ändras utan gemensamt godkännande i projektet. Man skulle kunna kalla dem huvudnivåer, för att kunna skilja dem från andra definierade nivåer.

## Varför?

Våningsplan är en viktig del i projekteringen av byggnader. Definitionen av dem och deras placering i höjdled fungerar som en basstruktur för hela byggnaden. Placeringen av många objekt är relaterade till våningsplanen. Flera aktörer är därför beroende av placeringen i höjdled och deras arbete påverkas om nivåer och avstånd mellan våningsplanen ändras.

## Hur?

Genom att definiera våningsplan med namn (*Name*) och höjd (*Elevation*) i det interna koordinatsystemet erhålls den verkliga plushöjden av att byggnadens placering i höjdsystemet finns angivet i *IfcBuilding*. Är det rätt gjort erhålls rätt värde vid export till IFC.  
En beskrivning till varje våningsplan (*Description*) ska också anges. De tre attributen bör fyllas i på ett projektgemensamt sätt. ”Name” kan vara svårt att styra i vissa program.

| **Attribut (3)**        | **Exempel** | **Förklaring**                                                                                                                                                              |
| ----------------------- | ----------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Name<sup>2</sup>        | Level 2     | Namn som ges av använt program.                                                                                                                                             |
| Description<sup>2</sup> | Våning 2    | Beskrivning av våningsplanet.                                                                                                                                               |
| Elevation<sup>2</sup>   | Steel       | Plushöjden för våningsplanet i det interna nollplanet. Den relateras till *ElevationOfRefHeight* som anges för *Ifc Building* och är den höjden i det använda höjdsystemet. |

<sup>1</sup> Obligatorisk (värde läggs in av programmet) <br> <sup>2</sup> Valfri (värde kan anges av användaren)

*Tabell: Attribut för våningsplan*

Våningsplan definieras oftast av A, men var plushöjden för respektive våningsplan läggs är ett projektgemensamt beslut eller ett beställarspecifikt krav. Efter att definitionen av våningsplanens plushöjder skapats får de inte ändras utan ett projektgemensamt beslut.

Våningsplan bör läggas systematiskt och konsekvent genom hela byggnaden i prioriteringsordning enligt ett av följande alternativ:

1.  färdigt golv på varje våning
2.  överkant bjälklag för varje bjälklag

Olika projektörer kan ha olika behov av var nivåerna läggs. Därför är det viktigt att tydliggöra vilka som är huvudnivåerna.

Arkitekten definierar våningsplanen vid färdigt golv och installatörerna använder samma våningsplan. Dessa våningsplan betraktas som ”huvudnivåer”, alltså nivåer som flera aktörer är beroende av och som inte får ändras utan vidare. Konstruktören kan behöva definiera egna våningsplan vid överkant bjälklag, men måste relatera dessa till arkitektens definierade våningsplan.

I projekt kan enskilda aktörer skapa ”hjälpnivåer” vid behov, men till dessa bör inte placeringar av objekt låsas.

![Våningsplan IFC](../4.tillampning_ifc/media/vaningsplan_ifc.png)

*Figur 3: Hantering av nivåer för våningsplan enligt IFC-standarden.*

Enligt IFC finns följande höjder att beakta för utrymmen:

* Ett utrymmes totala höjd definieras av de utrymmesbegränsande bjälklagen, från överkant av golvbjälklag *(IfcBuildingStorey.Elevation)* till underkant bjälklaget ovanför. Värdet ligger i BaseQuantity med benämningen "Height".*
* Höjden på ett utrymmes golvkonstruktion definieras från överkant bjälklag till överkant färdigt golv *(IfcSpace.ElevationWithFlooring)*. Värdet ligger i  BaseQuantity med benämningen "FinishFloorHeight".
* Ett utrymmes nettohöjd definieras från överkant färdigt golv till underkant undertak. Värdet ligger i BaseQuantity med benämningen "FinishCeilingHeight".


De två nivåerna ”överkant bjälklag” (*IfcBuildingStorey.Elevation*)och ”överkant färdigt golv” (*IfcSpace.ElevationWithFlooring*) relateras till noll-nivån i aktuellt höjdsystem (*IfcBuilding.ElevationOfRefHeight*). På så sätt placeras byggnaden rätt i höjdled.

<table>
    <tbody>
        <tr>
            <td><b>&#9432;</b></td>
            <td><b>TÄNK PÅ</b></td>
        </tr>
        <tr>
            <td></td>
            <td><b>Våningsplanens placering i höjdled får inte ändras hur som helst. Många objekt placeras utifrån våningsplanen och påverkas om höjden ändras. Detta gäller objekt som många andra aktörer ansvarar för.<br><br> Våningsplan definieras oftast av A, men var höjden för respektive våningsplan läggs är ett projektgemensamt beslut eller ett beställarspecifikt krav. Efter att våningsplanen definierats och skapats får de inte ändras utan ett projektgemensamt beslut.</b></td>
        </tr>
    </tbody>
</table>
