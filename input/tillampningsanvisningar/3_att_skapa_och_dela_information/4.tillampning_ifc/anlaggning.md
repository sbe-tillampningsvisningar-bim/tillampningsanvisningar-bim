


# Anläggning och anläggningsdel (*IfcFacility* och *IfcFacilityPart*)

**Bestäm anläggningsområde och anläggningsdel för redovisning.**

## Vad?

I den rumsliga hierarkin finns flera nivåer. Detta beskrivs i kapitlet rumslig hierarki. Inom infrastruktur finns begreppet **anläggning** (*IfcFacility*) på tredje nivån, under projekt och därefter byggplats. På den fjärde nivån finns **anläggningsdel** (*IfcFacilityPart*).

För redovisning av infrastruktur finns i IFC följande typer av anläggningar definierade:

-   Bro (*IfcBridge*)
-   Marin anläggning (*IfcMarineFacility*)
-   Spåranläggning (*IfcRailway*)
-   Väg (*IfcRoad*)

(Det femte området är byggnad (*IfcBuilding*), som inte beskrivs vidare här.)

Egenskaper som sätts på entiteter och objekt på respektive nivå ärvs ner till underliggande nivå. Det innebär att egenskaper som sätts på objekt på anläggningsnivån ärvs ner till anläggningsdelar.

I tabellen nedan visas några enkla exempel.

| **Anläggning**                       | **Exempel på anläggning**             | **Anläggningsdel**                           | **Exempel på anläggningsdel** |
| ------------------------------------ | ------------------------------------- | -------------------------------------------- | ----------------------------- |
| Bro (IfcBridge)                      | Hel eller del av bro                  | Brodel (IfcBridgePart)                       | Broöverbyggnad                |
| Marin anläggning (IfcMarineFacility) | Hel eller del av hamn, färjeläge, pir | Marin anläggningsdel (IfcMarineFacilityPart) | Kajvägg                       |
| Spåranläggning (IfcRailway)          | Spårsträcka, del av bangård           | Spåranläggningsdel (IfcRailwayPart)          | Spåröverbyggnad               |
| Väg (IfcRoad)                        | Vägsträcka                            | Vägdel (IfcRoadPart)                         | Vägöverbyggnad                |

Tabell 1: Exempel på anläggningar och anläggningsdelar.

På nästa nivå av nedbrytning kan **sammansatta byggdelar** (”system”) (*IfcElementAssembly*) användas, till exempel för alla delar i en spårväxel (räler, växeldriv, slipers med mera). Därefter kommer enskilda **komponenter** (*IfcElement*). Några exempel: balk (*IfcBeam*), ballast (*IfcCourse*), räl (*IfcRail*).

## Varför?

Genom att dela upp anläggning i olika områden görs redovisningen tydligare. Dessutom kan utveckling och specialanpassningar för respektive anläggningsområde enklare genomföras vid hantering av IFC-modeller.

## Hur?

Många modelleringsprogram har färdiga inställningar för olika objekttyper så att de får rätt anläggningstillhörighet. Egenskaperna ärvs uppifrån. Ytterligare egenskaper kan läggas på och ärvs då nedåt.

Utöver attributen högre upp i hierarkin kan en anläggning ses på tre sätt. Förutom att representera sig själv som en singulär, fristående anläggning (*Element*) kan den vara en av flera anläggningar i en gruppering (*Complex*), eller vara en del av en större anläggning (*Part*). I IFC-filen framgår detta genom attributet *CompositionType*. Standardvärdet är ”ELEMENT” om inte attributet används.

-   COMPLEX = flera anläggningar
-   ELEMENT = en enskild anläggning
-   PARTIAL = en del av en större anläggning

| **Attribut**                | **Exempel** | **Förklaring**                                                                                  |
| --------------------------- | ----------- | ----------------------------------------------------------------------------------------------- |
| CompositionType<sup>2</sup> |             | Anger anläggningens komplexitet. Om inte CompositionType anges gäller standardvärdet ”ELEMENT”. |

<sup>1</sup> Obligatorisk (värde läggs in av programmet) <br><sup>2</sup> Valfri (värde kan anges av användaren)

*Tabell: Attribut för IfcFacility*

| **Attribut**          | **Exempel** | **Förklaring**                                                                                 |
| --------------------- | ----------- | ---------------------------------------------------------------------------------------------- |
| UsageType<sup>2</sup> |             | Definierar konventionen som används för att dela upp eller bryta ner anläggningen hierarkiskt. |

<sup>1</sup> Obligatorisk (värde läggs in av programmet) <br><sup>2</sup> Valfri (värde kan anges av användaren)

*Tabell: Attribut för IfcFacilityPart*

#### IfcFacility

| **Concept (5)**                     | **Exempel** | **Förklaring**                                          |
| ----------------------------------- | ----------- | ------------------------------------------------------- |
| Body Geometry<sup>2</sup>           |             | Geometrisk representation                               |
| Product Local Placement<sup>2</sup> |             | Placering (i lokalt koordinatsystem)                    |
| Spatial Composition<sup>2</sup>     | Element     | Rumslig sammansättning (Complex, Element eller Partial) |
| Spatial Container<sup>2</sup>       | No          | Rumslig behållare (Fler objekt tillsammans)             |
| Spatial Decomposition<sup>2</sup>   | Yes         | Rumslig nedbrytning ()                                  |

<sup>1</sup> Obligatorisk (värde läggs in av programmet) <br><sup>2</sup> Valfri (värde kan anges av användaren)

*Tabell: Attribut för IfcFacility*

#### IfcFacilityPart

| **Concept (5)**                     | **Exempel** | **Förklaring**                              |
| ----------------------------------- | ----------- | ------------------------------------------- |
| Body Geometry<sup>2</sup>           |             | Geometrisk representation                   |
| Product Local Placement<sup>2</sup> |             | Placering (i lokalt koordinatsystem)        |
| Spatial Container<sup>2</sup>       | No          | Rumslig behållare (Fler objekt tillsammans) |
| Quantity Sets<sup>2</sup>           |             | För mängdning                               |

<sup>1</sup> Obligatorisk (värde läggs in av programmet) <br><sup>2</sup> Valfri (värde kan anges av användaren)

*Tabell: Attribut för IfcFacilityPart*

#### Exempel

![Rumslig nedbrytning](../4.tillampning_ifc/media/hierarki_infra_rail.png)

*Figur: Rumslig nedbrytning av infrastruktur i IFC, så som den presenteras i en BIM-viewer.*
