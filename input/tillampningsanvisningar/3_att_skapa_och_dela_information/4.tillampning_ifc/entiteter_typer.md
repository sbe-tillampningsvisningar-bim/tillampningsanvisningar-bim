# Entiteter och Typer (Entities och PredefinedType)

**Använd korrekta entiteter och bestäm eventuell tillämpning av typer.**

## Vad?

IFC-standaren innehåller en mängd entiteter (*entities*) som representerar fysiska objekt, exempelvis väggar, balkar och rör. Dessa kan beskrivas ytterligare genom att varje entitet även består av ett antal typer via attributet *PredefinedType*.

Attributet *PredefinedType* anges enligt en förutbestämd värdelista för respektive entitet. Alla värdelistor innehåller alltid värdena NOTDEFINED och USERDEFINED.

-   NOTDEFINED innebär att ingen typ är definierad.
-   USERDEFINED innebär att användaren kan definiera en valfri typ.

Nedan visas värdelistan *PredefinedType* för *IfcCovering* (beklädnad).

*Tabell: Ifc:s förbestämda typer av beklädnad.*

![Förbestämda typer av beklädnad](../4.tillampning_ifc/media/exempel_fordeftyperbekladnad.png)

Värdelistan tillämpas genom att ange ett av listans värde med gemener på engelska. För att ange en egendefinierad typ anges värdet USERDEFINED. Den egendefinierade typen anges sedan i attributet *ObjectType* (\#5), se attributen nedan.

*Tabell: Exempel på egenskaper för objektet IfcCovering.*

![Exempel IfcCovering](../4.tillampning_ifc/media/exempel_ifccovering.png)
![Exempel IfcCovering 2](../4.tillampning_ifc/media/exempel_ifccovering2.png)
![Exempel IfcCovering 3](../4.tillampning_ifc/media/exempel_ifccovering3.png)
![Exempel IfcCovering 4](../4.tillampning_ifc/media/exempel_ifccovering4.png)
![Exempel IfcCovering 5](../4.tillampning_ifc/media/exempel_ifccovering5.png)

## Varför?

Att använda korrekt entitet är viktigt i många avseenden. Det kan låta självklart, men det finns flera anledningar till att ”råka modellera något annat”. En orsak kan vara att modelleringsprogrammet inte har en bra funktion för att modellera ett en viss typ av objekt. Modelleraren väljer då aktivt ett annat objekt, som ser bättre ut i den grafiska presentationen. Ett vanligt exempel är att använda bjälklagsplatta (*IfcSlab*) för att modellera markytor. problemet som uppstår är att egenskaper och klassificering blir fel, och exporten till IFC kommer att kunna feltolkas.

#### Använd rätt entitet och objekt

Rätt entitet ska användas bland annat för att:

-   **Mottagande programvara ska tolka objektet korrekt**.  
      
    Många programvaror har funktioner för att beräkna objektens olika typer av mängder (area, löpmeter, volym med mera). Vilka mängder som beräknas skiljer sig beroende på entitet. Om exempelvis en pelare redovisas som en vägg (*IfcWall*) i stället för en pelare (*IfcColumn*) finns det risk att programmet inte kommer att kunna redovisa pelarens tvärsnittsarea.
-   **Korrekt attribut, egenskaper och egenskapsuppsättningar ska redovisas för objektet**.  
      
    För att använda IFC-formatets inbyggda egenskaper och egenskapsuppsättningar är det en förutsättning att korrekt entitet används. För exemplet *IfcWall* och *IfcColumn* redovisas dess egenskaper bland annat i egenskapsuppsättningarna *Pset_WallCommon* och *Pset_ColumnCommon* där ingående egenskaper skiljer sig. Se tabellen nedan.  
      
*Tabell: Egenskaper för IfcWallCommon respektive IfcColumnCommon.*

| **IfcWallCommon**    | **IfcColumnCommon**  |
| -------------------- | -------------------- |
| AcousticRating       | -                    |
| Combustible          | -                    |
| Compartmentation     | -                    |
| ExtendToStructure    | -                    |
| FireRating           | FireRating           |
| IsExternal           | IsExternal           |
| LoadBearing          | LoadBearing          |
| -                    | Roll                 |
| -                    | Slope                |
| Status               | Status               |
| SurfaceSpreadOfFlame | -                    |
| ThermalTransmittance | ThermalTransmittance |

I tabellen ovan visas egenskaper för vägg respektive pelare. Där framgår att flera egenskaper inte kan sättas på det ena eller andra objektet. Det är till exempel inte möjligt att beskriva en väggs luftljudsisoleringsförmåga (*AcousticRating*) eller om den är brandavskiljande (*Compartmentation*) om den modellerats som en pelare. Skulle en vägg felaktigt ha skapats som ett pelarobjekt skulle det bli ett problem vid redovisningen av objektet.

#### Använd typer (PredefinedType).

Att använda typer för entiteter gör att det blir lättare:

-   att sortera informationen i IFC-modellen när den består av fler objekttyper.
-   att ställa krav på informationsinnehåll och följa upp dessa med en mer detaljerad objektindelning.  
      
Om samma krav på informationsinnehåll ställs på exempelvis *IfcCovering*, utan vidare indelning med *PredefinedType*, skulle det innebära att exempelvis undertak och isolering av installationer har samma krav på informationsinnehåll, vilket ofta inte är lämpligt. Detta kan åtgärdas genom att ange undertak som *CEILING* och isolering av installationer som *SLEEVING* vid kravställning av informationsinnehåll.

## Hur?

Vid kravställning av informationsleveranser med IFC-formatet bör det som minimum finnas ett krav som beskriver att objekt ska levereras med korrekt entitet, följt av några beskrivande exempel. Detta krav öppnar dock upp för en del tolkningsfrihet vilket riskerar leveranser med bristande kvalitet. För att minimera denna risk bör beställaren av informationsleveranser mappa entiteterna (och eventuella typer) mot någon form av objekttabell, förslagsvis mot en eller flera tabeller inom CoClass. Se exempel på några olika tabeller för att redovisa undertak nedan.

*Tabell: Exempel på mappning mellan Ifc-entiteter och andra system.*

| **TABELL**          | **KOD** | **BENÄMNING**                          | **ENTITET** | **PREDEFINEDTYPE** |
| ------------------- | ------- | -------------------------------------- | ----------- | ------------------ |
| CoClass KS          | AP      | Undertakskonstruktion                  | IfcCovering | CEILING            |
| CoClass KO          | NCD     | Innertaksbeklädnad                     | IfcCovering | CEILING            |
| CoClass PR          | NSF     | Undertak av förtillverkade komponenter | IfcCovering | CEILING            |
| BSAB Byggdelstabell | 43.E    | Innertak                               | IfcCovering | CEILING            |
| BIMTypeCode         | 351     | Invändiga nedpendlade undertak         | IfcCovering | CEILING            |

För att kunna kravställa objekt på typnivå är det viktigt att säkerställa att de originalprogramvaror som används vid export till IFC-formatet kan leverera objekt på denna nivå.

Det finns ytterligare sätt att dela upp objekt i olika typer, exempelvis via klassificering eller genom egenskaper, men det underlättar om objekten delas upp i typer enligt IFC-formatet. Mer om klassificering och egenskaper kan läsas i övriga avsnitt.
