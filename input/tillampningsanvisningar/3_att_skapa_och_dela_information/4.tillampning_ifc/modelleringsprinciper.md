# Principer för CAD-modellering
**Modellera korrekt för användbar information under hela projekteringen**

## CAD-modellering

Med CAD avses det begreppet Computer Aided Design, som inkluderar 3D-modellering och hantering av information i programvaror.

Vid modellering i CAD är det viktigt att vara medveten om att det är många aktörer som är beroende av informationen. Geometrier, placering, egenskaper och annan information ska kunna delas med andra och användas för olika ändamål. Det är därför viktigt att modelleringen görs så att resultatet blir så riktigt som möjligt och användbart för andra aktörer än bara för den som modellerar. För att en viss aktör ska kunna uppfylla den kravställning som ställts kan en annan aktör behöva genomföra sin modellering på ett sådant sätt att den är användbar för vidare hantering.Det grundläggande upplägget styrs av kraven och det handlar om att förstå och modellera med bra systematik. Här beskrivs några av de områden där hanteringen behöver göras utifrån en bredare syn än de egna behoven.

Vad som ska levereras framgår av kraven för projektet. Kraven för utbyte av information mellan projektörer under projektets gång behöver styras upp och samordnas i projekteringsgruppen i ett tidigt skede och under projektets gång. Detta är grundläggande för det ISO 19650 kallar informationsutbyteskrav (EIR, *Exchange Information Requirements*).

## Skapa modell

När en CAD-modell skapas kommer ett flertal inställningar med. Ofta styrs detta av den mall som används, eller av inställningar i den programvara som används.

Följande delar ska kontrolleras mot leveranskraven när modellen skapas då det får stora konsekvenser längre fram i projektet om de inte är rätt från början. Tänk också på att det ska bli rätt vid export, där resultatet används av andra aktörer.

-   Enhet i modellen.
-   Placering av byggnadsverket i modellen i förhållande till nollpunkten i modellens lokala koordinatsystem (x- och y-koordinat samt z-koordinat och eventuell vridning).
-   Modellens placering i förhållande till verklighetens koordinatsystem, om det inte är samma (x- och y-koordinat samt z-koordinat och eventuell vridning).
-   Kontrollera att det teckensnitt som användas, och som flera parter är beroende av att använda, inte är en specialstil (till exempel för rumstexter).
-   Kontrollera att presentationen av streckade/streckprickade linjer ser rätt ut i slutlig skala (linjer som ska användas av flera parter).

## Modelleringsprinciper

-   **Vid modellering av objekt ska rätt objekttyp väljas.**  
    Genvägar för att visuellt skapa objekt som datamässigt är något annat ska undvikas. Resultatet vid IFC-export blir fel och hantering och automatisering av data i IFC-modellen kompliceras. Exempel på denna typ av fel i modelleringstekniken är när till exempel väggobjekt används för att modellera pelare; när bjälklagsplatta (”slab”) används för att modellera ett tak; eller när en ventilationskanal” används för att modellera ett golvfundament.
-   **Väggar, dörrar och fönster:**
    -   Väggar ska ansluta mot bjälklag.
    -   Väggar ska modelleras så att insida och utsida är rätt definierade på objekten (om modelleringsprogrammet tillhandahåller denna typ av objektegenskap).
    -   Dörrar och fönster ska placeras på rätt sätt i väggarna oavsett om det är en yttervägg eller en innervägg.
    -   Ytterväggar, dörrar och fönster ska tilldelas de egenskaper som möjliggör analyser och beräkningar i tidiga skeden, till exempel transmissionsberäkningar, ljusinsläpp dagsljusberäkningar) enligt de krav som är ställda i projektet.
-   **Utrymmen och våningsplan:**
    -   Utrymmen måste skapats på ett för projektet funktionellt sätt.
    -   Utrymmen ska definieras enligt principen ”golv till tak”, det vill säga stäckas vertikalt från ÖK bjälklag till UK bjälklag på överliggande våningsplan”.
    -   Definitionen för ett våningsplan får inte flyttas utan konsensus i projektet.
-   **Undertak och installationsgolv:**
    -   Undertak ska modelleras på rätt nivå, med rätt tjocklek och på ett sådant sätt att installationer som ligger i och ovan undertaket kan placeras rätt. Även andra typer av objekt som ligger mellan undertak och bjälklag ska kunna visualiseras på rätt sätt utifrån undertakets placering.
    -   Installationsgolv ska modelleras på rätt nivå, med rätt tjocklek och på ett sådant sätt att installationer som ligger i och under installationsgolvet kan placeras korrekt.
    -   Objekt i undertak ska placeras rätt. Detta gäller till exempel infällda luftdon, sensorer, brandvarnare, rökdetektorer och sprinklerhuvuden.
-   **Balkar ska vila på sina upplag, inte sväva lite ovanför i luften.**
-   **Installationsspecifika objekt:**
    -   Systemtillhörighet: Kanaler och rör ska modelleras så att de, som tillhör ett och samma system, är sammanhängande. Om ett system bryts, kommer delarna att CAD-mässigt tillhör olika system.
    -   Utrymmestillhörighet: Objekt som ligger i utrymmesbegränsade delar, till exempel don infällda i väggar, ska modelleras på ett sådant sätt att de får rätt rumstillhörighet. Hur detta ska göras varierar från program till program. I många program kan objektets insättningspunkt läggas inom utrymmets området för att få tillhörigheten. I andra program behöver utrymmestillhörigheten tilldelas manuellt.
    -   Symboler kontra 3D-objekt: För objekt som på ritning presenteras med symbol ska det geometriskt riktiga objektet i modellen vara placerat i rätt position. Symbolens placering får alltså inte påverka objektets läge, till exempel golvbrunn nära vägg.
    -   Eluttag, nätverksuttag, dosor, strömställare, switchar med mera ska placeras rätt både horisontellt och vertikalt.
    -   Armaturer ska modelleras i verklig storlek och placering. Ljusbild kan illustreras med 2D- eller 3D-symbol.
-   **Egenskaper:**
    -   Byggnadsfysikaliska egenskaper:  
        Det finns objekt som innehåller byggnadsfysikalisk information som andra aktörer behöver använda. Denna information behöver vara verifierad och godkänd. Det är därför viktigt att vid leverans tydliggöra vilken information som är verifierad ovh godkänd. Ett exempel är fönster, som har ett U-värd, men egenskapen är kanske ett defaultvärde hos den som modellerar fönstret. Ett annat exempel är hantering av miljöaspekter behöver där egenskaper för de objekt som kravställningen gäller behöver definieras och finnas med vid leverans. Det kan till exempel handla om egenskaper för energimärkning eller klimatpåverkan.
    -   Egenskaper för brand hanteras olika för olika typer av objekt. För ett utrymme är det till exempel omslutande objekt (väggar, dörrar och fönster) som ska ha egenskaper för brandkrav medan själva utrymmet har ett brandkrav.
    -   TypeID enligt BIP är en egenskap som används för littrering av objekt och beteckning av installationsobjekt.
-  **GUID**
    -   CAD-modelleringsprogramm skapar en GUID för varje modellerat objekt. Undvik att radera godkända objekt i stället för att modifiera dem. Ett nytt objekt får automatiskt en ny GUID av programmet, och kan därmed inte härledas i projekt där programspecifik GUID används som identifierare.
    -   I förvaltningsprogrammen används objektspecifika GUID. I vissa projekt ställer beställaren krav på att förvaltningsGUIDen ska finnas med som egenskap på vissa objekt. Denna GUID är inte samma som den GUID som programmet skapar.
    -   Det finns även andra typer av GUID som kan läggas in som egenskap på objekt utifrån ett visst behov.

## Modellering av väggar – ansvarsfördelning mellan arkitekt och konstruktör

Under projekteringen är det viktigt att tydliggöra vem som har ansvar för väggars egenskaper och placering. Ansvaret kan variera under projektets gång och kräver därför kontinuerlig samordning. Vem gör vad från tidigt skede till leverans av relationsmodell?

Olika ansvarsförhållanden kan gälla för olika typer av väggar:

-   ytterväggar
-   fasadkonstruktioner
-   bärande väggar
-   innerväggar
-   partier
-   panelväggar.

#### Ansvarsfördelning i olika skeden

Ansvarsfördelningen kan se ut så här i olika skeden:

-   Tidigt skede, planering och förprojektering:
    -   A modellerar väggar och exporterar förslaget till K
    -   K föreslår stomme och stomsystem.
-   Under projektering:
    -   A använder K:s stomväggar och stomme som underlag för att jämföra med sin egen placering och utbredning, men vid leveranser tas bara A:s väggar med.
    -   K modellerar hela stommen och alla bärande väggar, som också ingår i K:s leverans
-   För granskning:
    -   Både A och K exporterar sina väggar till IFC.
-   Slutleverans:
    -   A exporterar bara sina väggar och K exporterar sina väggar till IFC.När IFC-filerna läggs ihop finns inga dupletter av objekten.

#### Ytterväggar och utfackningsväggar specifikt

Beroende på programvara och modelleringsteknik går det inte att undvika att vissa objekt presenteras av både A och K. Ytterväggar och utfackningsväggar är två exempel. Ljud- och brandegenskaper, till exempel, läggs på av A, medan stomdel modelleras av K:

**Ytterväggar**

A modellerar ytterväggar och yttre begränsningar för byggnaden. Ytterväggen beskrivs som ett objekt, men det har flera skikt. K modellerar stomdelen. Bärande väggkärna blir dubblerad eftersom den ingår i A:s vägg och i K:s stomme. I detta fall är det K:s modell som gäller. De bärande delar som A modellerar ses som ”skuggobjekt”.

När K beskriver ytterväggen behöver varje skikt beskrivas som egna objekt, till exempel:

-   väggkärna av betong med armering (kan ha ingjutningsgods, eldosor med mera)
-   isolering
-   beklädnad av tegel.

**Utfackningsväggar**

Utfackningsväggar som är prefabricerade kan modelleras av både A och K, men ofta ser det ut så här:

A modellerar väggen som en enkel ”låda”, beskriver och betecknar den. Vid detaljprojektering görs en tillverkningsmodell av prefableverantören. Informationen går inte alltid via K utan mått och andra data kan komma direkt från A.
