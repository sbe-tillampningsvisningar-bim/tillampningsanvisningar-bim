# Att skapa informationsutbyteskrav med IDS

## Inledning

En leveransspecifikation för IFC – en *Information Delivery Specification* (IDS) – är ett datorläsbart dokument som definierar utbyteskraven för modellbaserat utbyte som använder IFC. Det specificerar hur objekt, klassificeringar, material, egenskaper och till och med värden och relationer ska levereras. Tekniskt är IDS en fil i XML-format med filändelse .ids. Formatet ägs och förvaltas av buildingSMART International.

IDS kännetecknas av:

-   Strukturerad och standardiserade data: IDS definierar tydligt vilken information som ska levereras och i vilket format (IFC). Detta minskar risken för missförstånd och felaktig dataöverföring.
-   Effektivitet: Genom att använda IDS kan aktörer inom projektet fokusera på att skapa och överföra relevant information i stället för att förhandla om utbyteskrav. Det sparar tid och minskar onödig komplexitet.
-   Automatiserad validering: IDS möjliggör automatiserad validering av den levererade informationen för alla parter. Detta minskar risken för felaktigheter och underlättar kvalitetskontroll.
-   Anpassningsbarhet: IDS kan anpassas för olika projekt och användningsscenarier. Det gör det möjligt att specificera unika krav för varje situation.
-   Juridiskt bindande: IDS kan fungera som ett juridiskt dokument som fastställer ansvar och skyldigheter för informationsutbyte. Det ger en tydlig grund för kontrakt och avtal.
-   Förbättrat samarbete: Genom att använda IDS kan olika aktörer inom byggprocessen kommunicera mer effektivt och samarbeta smidigare. Alla arbetar med samma uppsättning regler och förväntningar.

Eftersom en IDS är en XML-fil kan den tolkas maskinellt, samtidigt som den kan presenteras på ett sätt som är begripligt för en människa i ett läsbart dokument. Eftersom informationen kan tolkas av en programvara så kan den presenteras på olika sätt beroende på vilken mjukvara man använder.

Sammanfattningsvis möjliggör IDS en mer strukturerad, effektiv och pålitlig process för informationsutbyte, vilket är avgörande för att hantera komplexiteten i moderna byggprojekt.

IDS formatet har version 1.0 när dessa anvisningar skrivs. Standarden godkändes sommaren 2024.

#### När använder man en IDS

En IDS används i första skedet för att dokumentera de krav uppdragsgivaren har. Detta blir också en del av kravspecifikationen för uppdragstagaren. Uppdragstagaren kan använda IDS:en för att ställa in sitt modelleringsprogram så att arbetet följer kraven från start. Det kan vara till exempel handla om namngivning av objekt, och vilka egenskapsuppsättningar som ska läggas med i IFC-filen.

Innan leverans kan uppdragstagaren verifiera leveransen maskinellt med IDS:en och få ett svar om något avviker. På samma sätt gör uppdragsgivaren denna kontroll för att se att leveransen innehåller det som efterfrågats.

Detta ger alla parter maskinell kontroll av beställd och producerad information, vilket ger möjlighet att få högre kvalitet i projektet då det minskar risken för mänskliga fel. Fortfarande måste uppdragsgivaren förstå vad den behöver efterfråga.

## Hur man arbetar med IDS

IDS är en öppen standard från buildingSMART International. Det gör det möjligt att använda en IDS i olika delar av processen. Genom att använda lämpliga programvaror för olika ändamål kan IDS:en redovisas i klartext eller tolkas av ett program för att maskinellt lösa en uppgift. Exempel på detta visas sist i detta kapitel. IDS är skapat för att fungera med IFC och bSDD.

IDS kan liknas vid kollisionskontroll av data. Likt kollisionskontroll mellan olika geometrier kan man med en IDS ställa olika krav på data i IFC-modellen och testa detta maskinellt.

För att göra krav maskinläsbara måste språket som används vara formellt beskrivet och strukturerat. Eftersom IDS-formatet är skapat för maskinläsbarhet kan det vara svårt att skriva eller tolka manuellt. Utbudet av programvaror som kan skapa, editera och läsa IDS-formatet ökar och utvecklingen inom detta område är aktiv. Nya program kommer på marknaden och funktionalitet och användargränssnitt blir allt bättre. Programvaror där tillverkare har implementerat funktioner för IDS redovisar buildingSMART International på följande sida: [IDS software implementations - buildingSMART Technical](https://technical.buildingsmart.org/ids-software-implementations/).

#### En IDS uppbyggnad

En IDS har tre delar:

1.  En **beskrivning** av skälen för specifikationen och instruktioner om hur man uppnår det. Den här delen är utformad för att människor ska kunna läsa och förstå varför information efterfrågas. Detta fält används också för att beskriva krav på modellen som helhet.
2.  **Tillämplighet**: Väljer vad av modellen som man avser att specificera. Det finns många olika typer av objekt i IFC-modeller, men varje specifikation gäller endast en delmängd. Delmängden kan identifieras via de tillgängliga aspekterna som entitet (till exempel väggar, fönster), klassificering (till exempel CoClass B10 Ytterväggar) och andra.
3.  **Krav**: vilken information som krävs för delmängden som identifierats i tillämpligheten, såsom nödvändiga egenskaper eller material.

Kortfattat kan man säga att man filtrerar fram objekt med vissa egenskaper som man sedan ger egenskaper till.

Till exempel är specifikationen för "alla väggar måste ha en brandklassningsegenskap" strukturerad så här:

-   Beskrivning: brandklassificeringar för väggar är avgörande för att byggnormerna ska uppfyllas.
-   Tillämplighet: denna specifikation gäller alla väggobjekt.
-   Krav: ovan nämnda väggobjekt ska ha en brandklassningsegenskap.

Tillämplighet och krav beskrivs utifrån sex IFC-aspekter:

-   Attribute
-   Property
-   Entity
-   Classification
-   Material
-   PartOf

En aspekt beskriver sin information med hjälp av fasta parametrar så att datorer kan förstå vilken information som söks.

När en aspekt används för tillämplighet, beskriver den informationen som används för att identifiera de relevanta delarna av modellen.

När en aspekt används för krav, beskriver den de informationsbegränsningar som måste uppfyllas för att uppfylla specifikationen.

| **Aspekttyp**                       | **Parameteraspekt**                       | **Exempel tillämplighet**                                                                                | **Exempel krav**                                                                       |
|-------------------------------------|-------------------------------------------|----------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------|
| **Entity **(Entitet)                | **IFC Class** and **Predefined Type**     | Applies to "IfcWall" with predefined type of "SHEAR"                                                     | Must be an "IfcWall" with a predefined type of "SHEAR"                                 |
| **Attribute **(Attribut)            | **Name** and **Value**                    | Applies to elements with the attribute "Name" having the value "W01"                                     | Must have the attribute "Name" with the value "W01"                                    |
| **Classification **(Klassifikation) | **System** and **Value**                  | Applies to elements classified under "CoClass" as "B10"                                                  | Must have a "CoClass" classification reference of "B10"                                |
| **Property **(Egenskap)             | **Property Set**, **Name**, and **Value** | Applies to elements with a property set of "Pset_WallCommon" with a "LoadBearing" property set to "TRUE" | Must have a "Pset_WallCommon" property set with a "LoadBearing" property set to "TRUE" |
| **Material **(Material)             | **Value**                                 | Applies to "concrete" elements                                                                           | Must have a "concrete" material                                                        |
| **PartOf **(Del av)                 | **Entity** and **Relationship**           | Applies to elements that are "contained in" an "IfcSpace"                                                | Must be "contained in" an "IfcSpace"                                                   |

Tabell som beskriver de olika aspekterna. Hämtad från buildingSMART.

#### Entity

Varje objekt i en IFC-modell har en IFC class, till exempel IfcWall och IfcDoor. Liknande finns för icke geometriska eller hjälpobjekt som IfcProject för projektinformation eller IfcGrid för stomlinjer. Länk till lista av IFC klasser för IFC4x3: [Annex B (informative) Alphabetical listings - Entities - IFC 4.3.2 Documentation (buildingsmart.org)](https://ifc43-docs.standards.buildingsmart.org/IFC/RELEASE/IFC4x3/HTML/annex-b1.html)

Vissa IFC-klasser har också fördefinierade typer (Predefined Type). Dessa ger en närmare beskrivning av entiteten. Det blir lättare för användaren av data om de fördefinierade typerna används men det går att skapa egna beskrivningar (USERDEFINED).

Det är viktigt att specifikationer som tas fram knyts till lämplig IFC klass.

Exempel på tillämpning:

Alla regelväggar: Name=”IFCWALL”, PredefinedType=”ELEMENTEDWALL”

Name="IFCBUILDINGSTOREY"

Alla distributionssystem (varmvatten, el etc.): Name=["IFCDISTRIBUTIONSYSTEM", "IFCDISTRIBUTIONCIRCUIT"]

#### Attribute

Varje entitet i en IFC-modell har en rad standardiserade attribut. Dessa är bestämda och kan inte anpassas eller bytas ut. Några exempel:

-   GlobalId: ett unikt ID för elementet som används för att särskilja unika objekt maskinellt.
-   Name: ett kort namn, kod, nummer eller etikett för att identifiera objektet läsbart för en människa.
-   Description: vanligtvis den längre formen av namnet, skriven för att vara beskrivande och läsbar för människor.
-   Tag: detta är ett ID som kan länka det tillbaka till ursprungs BIM-applikationens ID. Information som inte är kritisk för definitionen av IFC entiteten lagras i en egenskap (Property), inte i ett attribut.

Exempel på tillämpning:

Någon entitet med namnet “Vangstycke”: Name=”Name”, Value=”Vangstycke”

Vilken entitet som helst med ett namn som börjar med “SW” följt av 2 siffror, såsom SW01, SW02, etc.: Name="Name", value="SW[0-9]{2}"

Alla entiteter (vanligtvis en IfcTask) med en status inställd på antingen "NOTSTARTED", "STARTED" eller "COMPLETED": Name="Status", Value=["NOTSTARTED", "STARTED", "COMPLETED"]

#### Classification

Ett klassificeringssystem är en definierad hierarki för att kategorisera element, till exempel med CoClass. Alla objekt i IFC-modellen kan ha en klassificeringsreferens. Ett system specificerat i Classification-aspekten refererar till ett tredje partsystem som ligger utanför IFC-standarden.

Exempel på tillämpning:

Alla klassificerade element: Tomt, inga parametrar.

Alla enheter som klassificeras med CoClass: System="CoClass"

Alla element som är klassificerade med CoClass som börjar med BC: System="CoClass", Value="BC\*"

#### Property

Egenskap (Property) är ett bestämt sätt att knyta data till objekt i en IFC-modell. Objekten kan vara både fysiska (pelare, fönster) eller icke fysiska (material, uppgifter). Egenskaper har ett namn (BaseName), till exempel “Brandklass”. Egenskaper kan grupperas i en egenskapsuppsättning (Property set). Användaren sätter ett värde på egenskapen, till exempel Brandklass=A1.

För att underlätta sömlöst samarbete har buildingSMART International skapat en rad standardiserade egenskapsuppsättningar, som bör användas. Visserligen kan användaren själv skapa egna anpassade egenskapsuppsättningar, men rekommendationen är att undvika detta så långt som möjligt. Det är också vanligt att programvaruleverantörerna bifogar sådana egenskapsuppsättningar anpassade för lokala marknader.

**Alla standardiserade egenskapsuppsättningar börjar med en reserverad sträng, antingen Pset\_ eller Qto_. Dessa är förbjudna att använda i egna egenskapsuppsättningar.**

Quantity är en specialiserad variant av Property där värdet är beräknat utifrån aktuellt objekt, till exempel längd, volym eller vikt. I en IDS kan man specificera värde på både det generella Property och det uträknade Quantity. Länk till en lista av de fördefinierade egenskapsuppsättningarna: [Annex B (informative) Alphabetical listings - Property sets - IFC 4.3.2 Documentation (buildingsmart.org)](https://ifc43-docs.standards.buildingsmart.org/IFC/RELEASE/IFC4x3/HTML/annex-b3.html)

Exempel på tillämpning:

Alla väggelement med ljudklass: Property Set="Pset_WallCommon", Name="AcousticRating"

Alla platsgjutna eller prefabricerade betongelement: Property Set= "Pset_ConcreteElementGeneral", Name="CastingMethod", value=["INSITU", "PRECAST"]

Varje enhet med egen anpassad egenskap med namn “Betongblandning” vald från A, B eller C lagrad i egenskapsuppsättningen “Företaget_betong”: Property Set="Företaget_betong", Name=" Betongblandning ", value=["A", "B", "C"]

#### Material

Alla fysiska objekt har ett material. Det är då kvaliteten på materialet som avses, inte materialet i sig. En tänkbart värde kan vara stålsorten “S235JR” men inte “Stål”. Se även avsnittet om IfcMaterial.

Exempel på tillämpning:

Alla entiteter av trä: Category=”Wood”

Alla delar av stål skall vara S235JR: Value=S235JR”, Category=”Steel”

#### PartOf

Denna aspekt identifierar objekt som är del av ett annat (tillämplighet) eller som kräver att de är del av annat (krav). Det finns många typer av relationer definierade i IFC. Dessa sex relationer kan styras av en IDS: IFCRELAGGREGATES, IFCRELASSIGNSTOGROUP, IFCRELCONTAINEDINSPATIALSTRUCTURE, IFCRELNESTS, IFCRELVOIDSELEMENT, IFCRELFILLSELEMENT.

När en relationstyp inte är specificerad kommer alla sex att beaktas rekursivt. När det är specificerat hanteras bara den specifierade relationen rekursivt.

Exempel på tillämpning:

Varje entitet som är en utfackningsvägg: Relation="IFCRELAGGREGATES", Entity="IFCCURTAINWALL"

Varje entitet som är en del av ett distributionssystem: Relation="IFCRELASSIGNSTOGROUP", Entity="IFCDISTRIBUTIONSYSTEM"

Varje enhet som finns i ett utrymme: Relation="IFCRELCONTAINEDINSPATIALSTRUCTURE", Entity="IFCSPACE"

Man kan kombinera flera aspekter både i tillämpligheten och kravet. Till vissa aspekter kan parametrar knytas i form av en lista av värden, ett intervall eller en sträng. Man kan också ange om något är obligatoriskt, valfritt eller förbjudet.

En IDS-fil kan innehålla flera krav. Dessa krav är oberoende "block" och har ingen hänvisning till andra krav i filen. Detta gör att man kan kopiera och klistra in krav mellan filer.

En IDS förutsätter att IFC-modellen är korrekt skapad. Finns syntaxfel eller om den bryter mot IFC-definitionen kan inte kontrollen mot IDS genomföras. En IFC-modell kan testas med andra tjänster, till exempel buildingSMARTs Validation service, för att se om den är korrekt.

#### Vad en IDS inte kan göra

IDS är begränsat till alfanumerisk information i denna första version, vilket betyder värden på egenskaper, kvantiteter, klassificeringar, material och relationer. Den täcker alltså inte geometriska aspekter, inte heller kontroller som beror av beräknade eller dynamiska värden från modellen. En IDS kontrollerar heller inte värden som refereras till från källor utanför IFC-modellen.

Ytterligare en begränsning är att IDS inte kan användas för att hantera designkrav, eller så kallade "regler". Att till exempel kontrollera ett krav på att alla fönster i ett toalettutrymme ska ha ogenomskinligt glas är inte möjligt inom IDS. Däremot går det att definiera ett krav i IDS på att alla fönster behöver ha en egenskap som definierar vilken typ av glas som finns i fönstret.

#### Hur IDS kan tillämpas i Sverige

I ett projekt kan man ha flera IDS:er som används i kombination. Vissa krav kan då komma från standarder, branschöverenskommelser eller myndigheter och projektspecifika behov.

Det kommer behövas flera typer av kontroller vid mottagande av IFC. Detta illustreras i följande bild:

![Validation](../2.informationsutbyteskrav_openbim/media/validation.png)

*Bild: Exempel på hur IDS:er från olika aktörer kan kombineras i ett projekt.*

1.  En IFC-fil kommer först behöva kontrolleras att den är uppbyggd enligt IFC-standardens schema och regler. Detta kan göras i IFC Validation Service, utvecklad och tillhandahållen av buildingSMART International.
2.  Vidare kommer i Nationella Riktlinjer finnas mallar för hur gemensamma, nationella krav skulle kunna se ut, exempelvis:
    1.  hur modeller ska delas upp (Spatial Structure)
    2.  hur IFC filens metadata (header) ska struktureras
    3.  vilka SI enheter som får användas
    4.  vilka koordinat- och höjdsystem som kan användas
    5.  hur egenskapsuppsättningar som inte är del av IFC ska namnges.
3.  Det kommer också finnas vissa lagkrav på data som ska tillhandahållas, exempelvis genom EU:s digitala produktpass, där utveckling pågår med fokus på hållbarhetsdata under hela livscykeln.
4.  Informationsutbyteskrav kommer också finnas specifikt för en organisation, enskilt projekt, tillämpning, skede och mycket mer.

Vissa av dessa kontroller kommer kunna göras med hjälp av IDS, medan andra som exempelvis buildingSMART Validation service är en tjänst där enskilda filer kontrolleras mot en tjänst.

#### Namngivning av IDS

Benämning av IDS bör följa nedan namnkonvention.

[LANDSKOD]-[URSPRUNG]-[PROJEKT]_[Benämning]

-   [LANDSKOD] – Tvåställig kod enligt ISO 3166-1 och -2. (SE för Sverige)
-   [URSPRUNG] – Identifiering av ursprunget till IDS-filen. Detta anges lämpligtvis med en förkortning i versaler, exempelvis TRV för Trafikverket, VGR för Västra Götalandsregionen, BIP för bipkoder.se, eller BEAst för Byggbranschens elektroniska affärsstandard. Ursprunget kan även skrivas ut utan förkortning men med Pascalnotation, exempelvis OrebroBostader.
-   [PROJEKT] – Denna del är valfri och kan användas för att redovisa projektspecifika IDS-filer, det vill säga sådana som inte är fastställda av ägaren till IDS-filen. Detta kan vara ett projektnummer (exempelvis 11537), en förkortning av projektets namn (exempelvis SGL för Sahlgrenska Life), eller enbart ett valfritt tecken för att markera att innehållet i IDS-filen är projektspecifik.
-   [Benämning] – Denna del är valfri och kan definiera IDS-filens innehåll ytterligare. Benämning anges med Pascalnotation.

## Exempel för några olika IFC-entities

#### Kravställning

| **TILLÄMPNING** | **KRAV**       |            |               |
|-----------------|----------------|------------|---------------|
| **ASPEKT**      | **KRITIERIUM** | **ASPEKT** | **KRITERIUM** |
| Mall            |                |            |               |

#### Byggnad (IfcBuilding)

### Exempel

Byggnad ska numreras med fyra numeriska tecken. Informationen ska placeras på attributet Name.

| **TILLÄMPNING** | **KRAV**           |            |                   |
|-----------------|--------------------|------------|-------------------|
| **ASPEKT**      | **KRITIERIUM**     | **ASPEKT** | **KRITERIUM**     |
| Entitet         | Name = IfcBuilding | Attribute  | Name = \^\\d{4}\$ |

| **TILLÄMPNING** | **KRAV**           |            |                |
|-----------------|--------------------|------------|----------------|
| **ASPEKT**      | **KRITIERIUM**     | **ASPEKT** | **KRITERIUM**  |
| Entitet         | Name = IfcBuilding | Attribute  | Description =  |

#### Montagedelar och element (IfcElementAssembly)

### Exempel

En del måste vara del av ett distributionssystem

| **TILLÄMPNING** | **KRAV**              |            |                       |
|-----------------|-----------------------|------------|-----------------------|
| **ASPEKT**      | **KRITIERIUM**        | **ASPEKT** | **KRITERIUM**         |
|  Entity         | IFCDISTRIBUTIONSYSTEM | Relation   |  IFCRELASSIGNSTOGROUP |

#### Klassifikationssystem (IfcClassification)

### Exempel

Alla delar skall vara kodade med CoClass

| **TILLÄMPNING** | **KRAV**       |                  |                |
|-----------------|----------------|------------------|----------------|
| **ASPEKT**      | **KRITIERIUM** | **ASPEKT**       | **KRITERIUM**  |
| tomt            | tomt           |  Classification  | System=CoClass |

Om inte Aspekt eller kriterie specificeras (är tomt) gäller tillämpningen alla entiteter.

#### Material (IfcMaterial)

### Exempel

Alla pelare skall vara av stål.

| **TILLÄMPNING** | **KRAV**                            |            |                  |
|-----------------|-------------------------------------|------------|------------------|
| **ASPEKT**      | **KRITIERIUM**                      | **ASPEKT** | **KRITERIUM**    |
| Entitet         | Name = IfcColumn Name = IfcMaterial | Attribut   | Category = Steel |

#### Färger på objekt

### Exempel 1:

Alla elstegar ska vara färgade med följande RGB-kod:

| **TILLÄMPNING** | **KRAV**                                          |            |                             |
|-----------------|---------------------------------------------------|------------|-----------------------------|
| **ASPEKT**      | **KRITIERIUM**                                    | **ASPEKT** | **KRITERIUM**               |
| Entitet         | Name = IfcCableCarrierSegment Name = IfcColourRgb | Attribut   | Red = 0 Green = 1  Blue = 0 |

## Exempel på tillämpning, XML-kod

Det finns flera program som kan användas för att skapa IDS. buildingSMART har skapat en sida med en lista över sådana program och vad de kan hantera. Länk till sidan:

[IDS software implementations - buildingSMART Technical](https://technical.buildingsmart.org/ids-software-implementations/)

Nedan visas ett av dessa program där IDS skapas med hjälp av ett läsbart gränssnitt där fält för ”Beskrivning”, ”Tillämplighet” och ”Krav” finns.

![Program för IDS](../2.informationsutbyteskrav_openbim/media/program_ids.png)

Samma exempel visas nedan i ren xml-kod.

![xml-kod](../2.informationsutbyteskrav_openbim/media/xml_bild.jpg)