# Att skapa informationsutbyteskrav (EIR) för IFC

Informationsutbyteskrav för IFC-leveranser bör baseras på buildingSMART-standarden Information Delivery Specification (IDS). Det finns flera sätt att göra detta på och det finns en rad leverantörer som erbjuder verktyg för att skapa IDS. Utmaningen med dessa verktyg är att ge en enkel överblick för både den som skapar kravställningen och för den som ska läsa och arbeta in kraven i sina modeller.

För att underlätta hanteringen redovisas här ett exempel på metodik där kravställningen skapas med hjälp av en Excelmall, som sedan kan redovisas med en Power BI-tavla för mänsklig tolkning. Eftersom Excelmallen följer strukturen i IDS-standarden kan även en eller flera IDS-filer skapas för att automatisera processen med granskning och kontroller av levererade data. En del av denna hantering beskrivs i ”Att skapa informationsutbyteskrav med IDS”.

Förhållandet mellan Excel, Power BI och IDS visas schematiskt i figuren nedan.

![Excel, Power BI, IDS](../2.informationsutbyteskrav_openbim/media/excel_mm.png)

Mallarna för Excel och Power BI innehåller några ofullständiga delar, som vi inte har hunnit att korrigera under projektet. Dessa kommer att förvaltas vidare inom Nationella Riktlinjer och därmed uppdateras och utvecklas.

## Excel-mall för strukturering av krav

Uppbyggnaden av Excel-mallen bygger på struktureringen av IDS samt delar av SS-EN ISO 7817-1:2024 Byggnadsinformationsmodellering – Nivåer för informationsbehov – Del 1: Begrepp och principer. Varje objekt behöver beskrivas utifrån denna struktur med följande aspekter (facets)

-   Attribut (Attribute)
-   Egenskap (Property)
-   Entitet (Entity)
-   Klassifikation (Classification)
-   Material (Material)
-   Del av (part of)

Disciplin har lagts till för att kunna filtrera på disciplintillhörighet. Geometrisk information kan läggas in för att tydliggöra vilken geometrisk informationsnivå objekten ska ha.

Excelfilen är strukturerad i blad och kolumner. Första bladet heter ”Objekt” och är huvudbladet. Där finns alla objekt och aspekter listade. De aspekter som ett objekt förväntas ha är markerat med ett kryss i motsvarande kolumn.

![Excelmall](../2.informationsutbyteskrav_openbim/media/mall.png)

Mallen innehåller några exempel på objekt och kravställd information. Detta är som sagt enbart exempel vilket innebär att användaren själv måste lägga till fler rader med objekt och fler kolumner med kravställd information för att skapa en fullständig kravställning för aktuell informationsleverans.

### OBJEKT och ID

I kolumnen Objekt redovisas alla typer av objekt som ska vara med i kravställningen. Det byggs lämpligen upp med en objekthierarki bestående av flera nivåer. Objekthierarkin kan följa en eller flera klassifikationssystem eller liknande, exempelvis CoClass, men kan till exempel även följa objektstrukturen i den programvara som används vid modellering.

I mallen redovisas en struktur som bygger på BSAB 96 Byggdelstabell och Produktionsresultatstabell samt en egendefinierad struktur för diskreta objekt, det vill säga objekt som inte har någon fysisk representation. Exempel på sådana objekt är våningsplan och installationssystem.

Varje rad i kravställningen bör vara unik och redovisa ett unikt ID. Det görs lämpligen med objektets klassifikationskod enligt nedan.

![Exempel 1 excelmall](../2.informationsutbyteskrav_openbim/media/bild_3.png)

### ENTITET

Här anges objektets motsvarande entitet enligt IFC, exempelvis IfcWall för väggar och IfcAirTerminal för luftdon. Entitet ska väljas enligt IFC4.3.

### DISCIPLIN

Här anges en eller flera discipliner som ansvarar för att modellera och leverera objektet. Detta kan senare användas för filtrering av vilka objekttyper som ska visas i objektlistan per disciplin. Aktuell värdelista anges i bladet DISCIPLIN.

### GEOMETRISK INFORMATION

Dessa kolumner är en del av SS-EN ISO 7817-1:2024 och används för att ställa krav på Detaljeringsgrad, Dimensionalitet, och Utseende. Aktuella värdelistor anges i bladet Geometrisk information.

![Exempel 2 excelmall](../2.informationsutbyteskrav_openbim/media/bild_4.png)

### ATTRIBUTE Facet (Attributaspekt)

Här anges vilka attribut som kravställs per objekt. Dessa krav kan detaljeras ytterligare i bladen ATTRIBUTE och ATTRIBUTE – Definitioner.

![Exempel 3 excelmall](../2.informationsutbyteskrav_openbim/media/bild_5.png)

### CLASSIFICATION Facet (Klassifikationsaspekt

Här anges krav på hur respektive objekt ska klassificeras. Dessa krav detaljeras ytterligare i bladen CLASSIFICATION, CLASSIFICATION Reference, ATTRIBUTE – Definitioner, och ATTRIBUTE.

![Exempel 4 excelmall](../2.informationsutbyteskrav_openbim/media/bild_6.png)

### PROPERTY Facet (Egenskapsaspekt)

Här anges krav på vilka egenskaper som kan redovisas på respektive objekt. Krav på i vilken, eller vilka, egenskapsuppsättningar dessa egenskaper ska redovisas anges vidare i bladet PROPERTYSET. Vidare krav på respektive egenskap anges i bladet PROPERTY.

![Exempel 5 excelmall](../2.informationsutbyteskrav_openbim/media/bild_7.png)

### MATERIAL Facet (Materialaspekt)

Här anges krav på hur material ska redovisas per objekt. Dessa krav detaljeras ytterligare i bladet MATERIAL.

![Exempel 6 excelmall](../2.informationsutbyteskrav_openbim/media/bild_8.png)

### PART OF Facet (Del av-aspekt)

Här anges krav på om, och i så fall hur, respektive objekt ska vara del av andra delar av IFC-strukturen. Detta krav detaljeras ytterligare i bladet PART OF. Exempel på detta är att ett luftdon är en del av ett ventilationssystem, eller att en pelare är en del av ett våningsplan.

![Exempel 7 excelmall](../2.informationsutbyteskrav_openbim/media/bild_9.png)

### DOKUMENTATION

Dessa kolumner är en del av SS-EN ISO 7817-1:2024 och används för att ange krav på ytterligare dokumentation som ska redovisas med respektive objekt.

![Exempel 8 excelmall](../2.informationsutbyteskrav_openbim/media/bild_10.png)

### Bladet HEADER - Definitioner

I detta blad anges krav på beskrivning av IFC-leveransen i sin helhet, exempelvis vilken IFC-version och Model View Definition (MVD) som ska användas.

![Exempel HEADER](../2.informationsutbyteskrav_openbim/media/header.png)

## Redovisning av krav i Power BI

De krav som definieras i Excel-mallen och beskrivits ovan ska läsas in i mallen för Power BI. Denna Power BI-tavla publiceras lämpligen på webben för kommunikation av gällande krav. Nedan följer en beskrivning av den.

Tavlan är uppbyggd med följande delar:

1.  OBJEKT
2.  DISCIPLIN
3.  ENTITET
4.  KLASSIFIKATIONSASPEKT
5.  MATERIALASPEKT
6.  DEL AV-ASPEKT
7.  ATTRIBUTASPEKT
8.  EGENSKAPSASKPEKT & EGENSKAPSUPPSÄTTNINGAR
9.  GEOMETRISK INFORMATION
10. DOKUMENTATION

Se figurerna nedan.

![Power BI-tavla bild 1](../2.informationsutbyteskrav_openbim/media/pbi1.png)

![Power BI-tavla bild 2](../2.informationsutbyteskrav_openbim/media/pbi2.png)

### OBJEKT & DISCIPLIN

För att navigera bland kraven används objekttabellen. Den följer den hierarki som tidigare byggts upp i Excel-mallen. Den går även att filtrera per disciplin och söka på direkta ord. I figuren nedan har disciplinen VS filtrerats fram och objektet PSD Avstängningsventil valts.

![Power BI-tavla bild 3](../2.informationsutbyteskrav_openbim/media/pbi3.png)

När ett objekt markeras i objekttabellen visas dess krav på övriga delar av tavlan.

### ENTITET

Fältet ENTITET anger den entitet som ska representera valt objekt i IFC-formatet. Figuren nedan visar att objektet PSD Avstängningsventil ska representeras som en IfcValve inom IFC-formatet.

![Power BI-tavla bild 4](../2.informationsutbyteskrav_openbim/media/pbi4.png)

### KLASSIFIKATIONSASPEKT

Fältet KLASSIFIKATIONASPEKT anger om/hur valt objekt ska klassificeras. Detaljerade krav för detta visas i fönstret KLASSIFIKATION. Figuren nedan visar att objektet PSB Avstängningsventil ska klassificeras med fyra klassifikationer. Genom att markera (använd Ctrl) någon av de kravställda klassifikationerna visas även de detaljerade kraven för IfcClassifcationReference och IfcClassification nedanför de kravställda klassifikationerna.

![Power BI-tavla bild 5](../2.informationsutbyteskrav_openbim/media/pbi5.png)

### MATERIALASPEKT

Fältet MATERIALASPEKT anger om/hur objektets material ska definieras. I detta exempel finns inga krav på material.

### DEL AV-ASPEKT

Fältet DEL AV-ASPEKT anger om/hur valt objekt ska relatera till övriga objekt i IFC-modellen. I detta exempel finns inga krav på sådana relationer.

### ATTRIBUTASPEKT

Fältet ATTRIBUTASPEKT anger om, och i så fall vilka krav det finns på objektets attribut. Figuren nedan visar att objektet PSB Avstängningsventil har krav på fem attribut.

![Power BI-tavla bild 6](../2.informationsutbyteskrav_openbim/media/pbi6.png)

### EGENSKAPSASPEKT & EGENSKAPSUPPSÄTTNINGAR

Fältet EGENSKAPSASPEKT anger om, och i så fall vilka krav det finns på objektets egenskaper samt i vilket/vilka egenskapsuppsättningar dessa ska redovisas. Figuren nedan visar att objektet PSB Avstängningsventil har krav på egenskapen Status som ska redovisas i två egenskapsuppsättningar.

![Power BI-tavla bild 7](../2.informationsutbyteskrav_openbim/media/pbi7.png)

### GEOMETRISK INFORMATION

Fältet GEOMETRISK INFORMATION anger om, och i så fall vilka krav det finns på objektets geometriska representation. Det anges med de tre fälten Utseende, Dimensionalitet, och Detaljeringsgrad (LOD). Figuren nedan visar dessa krav för objektet PSB Avstängningsventil.

![Power BI-tavla bild 8](../2.informationsutbyteskrav_openbim/media/pbi8.png)

### DOKUMENTATION

Fältet DOKUMENTATION anger om, och i så fall vilka krav det finns på objektets ytterligare dokumentation i form av produktblad, miljödeklaration, och etcetera. Figuren nedan visar att objektet PSB Avstängningsventil har krav på tre typer av ytterligare dokumentation.

![Power BI-tavla bild 9](../2.informationsutbyteskrav_openbim/media/pbi9.png)