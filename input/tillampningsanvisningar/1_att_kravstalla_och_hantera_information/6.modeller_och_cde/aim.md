# Utforma och utveckla en tillgångsinformationsmodell (AIM)

Att kombinera BIM med tillgångsförvaltning är komplext, med utmaningar inom både management och tekniska frågor. Ett speciellt problem är att BIM-data – som tas fram under utformning och produktion – vanligen inte produceras utifrån ett förvaltningsperspektiv. Under projekteringen är det utformning, dimensionering och förberedelser för produktion som styr vilken information som är relevant, inte vad som behövs för drift och underhåll av byggnader och anläggningar.

Siktlinjemetoden syftar till att knyta ihop organisationens informationskrav (OIR), funktionella informationskrav (FIR) och tillgångsinformationskrav (AIR) med de data som berör de fysiska delarna i informationsmodellen som byggs upp under projektering och produktion. Detta kan göras visualiseras genom att med hjälp av UML (*Unified Modelling Language*) skapa relationer mellan funktioner och system.

På det här sättet skapas en tillgångsinformationsmodell med BIM-data som kan utnyttjas av tillgångsförvaltningen, kopplade till kraven från alla nivåer. (Se Bild 1.)

![AIM bild1](../6.modeller_och_cde/media/aim1.png)

![AIM bild2](../6.modeller_och_cde/media/aim2.png)

*Bild 1: Utvecklingen av en tillgångsinformationsmodell från klassificerade BIM-data och det strukturerade arbetet med att ta fram OIR, FIR och AIR. Källa: Line of Sight, Figure 5.*

I bilden illustreras tillgångsinformationsmodellen som en enda “burk” med data. I praktiken består modellen av en disparat samling av informationscontainrar. Det kan vara CAD-modeller, strukturerade filer typ kalkylblad, databaser, ostrukturerade textdokument, bilder och mycket annat som tillsammans avspeglar en byggd tillgång.

För att gå från en projektinformationsmodell (PIM) till en tillgångsinformationsmodell (AIM) enligt ISO 19650 behöver man följa en process med förfining och omvandling, med fokus på de specifika krav och leveranser som behövs under användningsskedet. Här är en detaljerad uppdelning av denna övergång:

## 1. Förstå skillnaden mellan PIM och AIM

-   PIM utvecklas under design- och konstruktionsfaserna i ett byggnadsprojekt (leveransskedet). Den omfattar all information som samlas in, genereras eller uppdateras under dessa faser och omfattar dokument, grafiska modeller och icke-grafiska data.
-   AIM är den långsiktiga informationsmodellen som används för tillgångens drift och underhåll (användningsskedet). Den fokuserar på information som krävs för att stödja förvaltningen av tillgången. AIM kan byggas upp eller uppdateras med den relevanta information som skapas under genomförandet och som ligger i PIM.

## 2. Granska informationskraven

-   Organisationens informationskrav (OIR), som beskriver vilken information som behövs för övergripande strategiska och operativa beslut.
-   Informationsutbyteskrav (EIR), som specificerar vilken information som ska levereras vid varje informationsutbyte.
-   Tillgångsinformationskrav (AIR), som anger vilken information från PIM som måste överföras till AIM, med fokus på långsiktigt underhåll, hållbarhet och driftseffektivitet

## 3. Urval och omvandling av data

-   Filtrera och välj ut data: Alla data i PIM är inte nödvändiga för AIM. Urvalet bör baseras på AIR och bör fokusera på den information som är användbar för tillgångsförvaltning.
-   Transformation: Processen omfattar förfining, formatering och potentiell förbättring av data så att de passar behoven hos de som ska drifta och underhålla byggnader och anläggningar. Det kan till exempel handla om att integrera garantier, underhållsscheman och drifthandböcker i modellen.

## 4. Användning av teknik och programvara

-   Common Data Environment (CDE): ISO 19650 betonar användningen av en CDE för hantering av alla projektdata. Övergången från PIM till AIM kräver en effektiv användning av CDE där data struktureras, lagras och görs tillgängliga enligt projektets definierade faser.
-   Integrationsverktyg: Använd programvaruverktyg som stöder integration av olika dataformat och export av relevanta data till de format som krävs för förvaltningssystemet.

## 5. Validering och överlämnande

-   Kvalitetskontroller: Säkerställ att data uppfyller de standarder som krävs och att all nödvändig information finns med. Valideringsprocesser bör bekräfta att AIM korrekt återspeglar tillgången som den är byggd och att data matchar AIR. AIM kan också kompletteras med andra relationer mellan tillgångarna, till exempel för att beskriva hur funktioner är beroende av varandra.
-   Överlämning: Den slutliga överlämningen innebär att den validerade AIM:en överförs till kunden eller den part som ansvarar för förvaltningen av tillgången, så att de har all nödvändig information för en effektiv förvaltning av tillgången.

## 6. Stöd efter överlämnandet

-   Utbildning och support: Det kan vara avgörande att ge utbildning till tillgångsförvaltarna om hur man använder AIM på ett effektivt sätt.
-   Återkoppling: Etablera mekanismer för löpande återkoppling om AIM:s användbarhet och effektivitet, vilket möjliggör kontinuerlig förbättring av metoderna för hantering av tillgångsinformation.

Att gå från PIM till AIM är inte bara en teknisk uppgift att överföra data, utan en strategisk process som kräver noggrann planering, engagemang från intressenter och löpande förvaltning för att säkerställa att informationen förblir relevant och värdefull under tillgångens hela livscykel.
