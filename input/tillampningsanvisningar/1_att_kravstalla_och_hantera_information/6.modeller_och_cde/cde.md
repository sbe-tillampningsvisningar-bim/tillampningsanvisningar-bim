# Gemensam datamiljö (CDE)

När man pratar om en gemensam datamiljö (CDE) är det lätt att föreställa sig en avancerad teknisk lösning där all information om ett projekt, fastighet eller annan tillgång ligger i ”molnet”, enkelt tillgänglig överallt för alla inblandade och som uppdateras direkt utan filhantering. Ett system som kommer fungera genom hela livscykeln från första tanke, genom projektering och byggnation, under hela driftskedet, tills att dessa material och byggdelar kan demonteras och återbrukas i andra anläggningar. Det är en engagerande målbild som är värd att sträva emot, men hur vi oftast arbetar idag och de flesta tekniska lösningar som finns på marknaden når inte riktigt upp till den visionen ännu med få undantag.

Det är dock inte ett hinder att arbeta som standarden föreskriver. Den teknik som används kan vara ganska enkel och det finns inga faktiska krav på en CDE i ISO 19650-1. I kapitel 12 nämns två aspekter av vad en CDE kan vara:

-   **CDE-lösningen**, handlar om den tekniska lösningen och systemstödet. Den mjukvara och servrar där olika aktörer kan lägga den data som produceras
-   **CDE-arbetsflödet**, som handlar om de processer och rutiner som behövs för att hantera ett arbetsflöde med gemensamma informationsresurser.

Standarden har några viktiga “bör-krav” som gäller de informationscontainrar som lagras. De bör kunna märkas med:

1.  en revideringskod som följer en överenskommen standard, till exempel IEC 82045-1 eller SS 32206 (se nedan)
2.  en statuskod som visar tillåten användning av informationen.

## Versionsbeteckning

Vad gäller revideringskod finns i Sverige en väl etablerad praxis som bygger på SS-EN ISO 7200:2004 och den svenska tillämpningen *SS 32206 Byggdokument – Ändringar*. I SIS Bygghandlingar finns uppdaterade anvisningar, baserat på den kommande versionen av SS 32206.

Revideringskoden kallas här **versionsbeteckning**. Beroende på handlingens status och typ används siffror inom parentes, siffror eller bokstäver för att beteckna olika versioner av en handling. För:

-   icke godkända handlingar, det vill säga alla handlingar som inte har status Förfrågningsunderlag eller status Godkänd, används **siffra inom parentes**
-   alla typer av handlingar godkända som Förfrågningsunderlag används **siffra**
-   programhandlingar, förslagshandlingar, systemhandlingar, relationshandlingar och förvaltningshandlingar med status Godkänd används **siffra**
-   bygghandlingar och tillverkningshandlingar med status Godkänd används **bokstäver**.

## Status

ISO 19650 anger följande statusbeteckningar för en informationscontainer:

-   **Arbete pågår**
-   **Delad**
-   **Godkänd**
-   **Arkiverad**.

Dessutom nämns **egenkontroll**, **granskning** och **godkännande**, när en informationscontainer går från Arbete pågår till Delad eller från Delad till Godkänd

![CDE_granskning och godkännande](../6.modeller_och_cde/media/cde_bild1.png)

*Bild 1: Gransknings- och godkännandeprocessen enligt ISO 19650. Källa: SS-EN ISO 19650-1:2019 (sv), Figur 10.*

I ett svenskt sammanhang är det lämpligt att i stället använda etablerade statusbeteckningar, baserat på SS 32209:2022. Beteckningarna blir då:

-   **Under arbete**
-   **Preliminär**
-   **Godkänd** eller (godkänd som) **Förfrågningsunderlag**
-   **För granskning**, när en informationscontainer går från **Under arbete** till **Preliminär** eller från **Preliminär** till **Godkänd/Förfrågningsunderlag**.
-   **Arkiverad**.

Status **Arkiverad** finns inte i nuvarande version av SS 32209, men bör läggas till. Här finns ytterligare alternativ, till exempel **För egenkontroll** eller **För godkännande** som kan användas internt.

| **Status enligt SS 32209** | **Status enligt ISO 19650** |                           |
|----------------------------|-----------------------------|---------------------------|
| **Kod**                    | **Benämning**               | **Benämning**             |
| UA                         | Under arbete                | Arbete pågår              |
| E-                         | För egenkontroll            | (Egenkontroll)            |
| PR                         | Preliminär                  | Delad                     |
| R-                         | För granskning              | (Granskning, godkännande) |
| G1                         | Godkänd                     | Godkänd                   |
| FU                         | Förfrågningsunderlag        | Godkänd (som FU)          |
| A-                         | Arkiverad                   | Arkiverad                 |

![CDE_informationsutbyte](../6.modeller_och_cde/media/informationsutbyte.png)

*Bild 2: Föreslagna benämningar för gransknings- och godkännandeprocessen.*

## Arbetsflöde och informationsutbyte i en CDE

Informationsutbyten under ett projekt ska utföras enligt den process som beskrivs i ISO 19650 del 2 och 3, avsnitt 5.6 respektive 5.7. Nedan följer en sammanfattning av stegen.

### Under arbete

En informationsleverantör får ett startbesked och samlar befintlig eller påbörjar nya informationscontainrar som då ges statusen Under arbete/Arbete pågår.

### Preliminär

Innan information delas och ges statusen Preliminär/Delad bör informationens kvalitet granskas av **informationsleverantören** enligt bestämda kriterier utifrån informationskraven. Detta kommer kanske främst röra sig om att informationen är strukturerad och i rätt format. Om man i egenkontrollen hittar någon avvikelse som bör åtgärdas behåller man filerna i status Under Arbete. När det inte finns något kvar att åtgärda så fortsätter arbetet men med statusen Preliminär/Delad.

### För granskning

När **informationsleverantören** är klar, men innan informationscontainrarna blir godkända, ska **informationsmottagaren** utföra sina kontroller och granskning. Det bör vara tydligt vad informationsmottagaren kontrollerar. Görs automatiserade kontroller kan det vara en fördel om dessa finns tillgängliga för **informationsleverantören** innan leveransen släpps för granskning. Om det finns anmärkningar på leveransen så återgår status till Under arbete och dessa åtgärdas innan en ny granskning kan göras och status kan ändras till godkänd.

Den här granskningen handlar inte om ”faktainnehållet”, alltså om det som beskrivs är rätt i sak. Inom informationshanteringen är det viktiga att informationen är rätt strukturerad, ligger på rätt ställe, att den går att samläsa med annan information, och att det som levereras fungerar med de system som finns på plats.

Annan typ av granskning där man granskar innehåll och att tekniska krav uppfylls behöver också finnas på plats. Detta kan kombineras med granskningen av informationshanteringens – speciellt då en informationsmottagare kontrollerar en leverans – men ändå skiljas åt i sak.

### Godkänd

Efter en framgångsrik granskning övergår statusen till Godkänd, eller (godkänd som) Förfrågningsunderlag. Över tid kommer informationscontainrar ändra status flertalet gånger vid olika leveranser. Statusen Godkänd är alltså kopplad till specifika leveranser. När en leverans är avklarad och arbete med nya leveranser påbörjas kommer statusen återgå till Under arbete eller Preliminär utifrån vad som är lämpligt.

### Arkiverad

När en informationscontainer får en ny version ska den äldre ej gällande versionen ges status Arkiverad för spårbarhet och framtida referens.

![CDE_livscykeln](../6.modeller_och_cde/media/cde_livscykeln.png)

*Bild 3: Livscykeln för information från leveransskedet in i användningsekedet.*

## Utvärdering av CDE-lösningar och deras funktion

De grundläggande principerna vad gäller ett CDE-arbetsflöde kan uppnås med relativt enkla system, men det finns flera aspekter som kan vara till stöd för att förstå olika systems funktionalitet och styrkor. Dessa kan beskrivas utifrån:

-   dokumenthantering
-   modellhantering/BIM-integration
-   säkerhet
-   livscykelfunktionalitet
-   gränssnitt/integration.

För närvarande finns det många olika systemstöd som hanterar olika aspekter av vad en CDE-lösning kan vara. De flesta CDE-lösningar som finns tillgängliga klarar inte alla aspekter eller övergångar mellan olika skeden. I praktiken behöver därför projekt och företag oftast förlita sig på ett lappverk av olika lösningar.

När man tittar på olika lösningar kan man använda dessa aspekter för att utvärdera hur avancerade olika lösningar är, och förstå vilka lösningar som bäst passar det behov man har inom sin organisation eller i ett projekt. I utvärderingsmodellen nedan är de fem aspekterna indelade i tre nivåer. Dessa nivåer kan användas för att göra en jämförelse mellan olika lösningar. Men varje aspekt kan också förstås beskrivas mer ingående för respektive lösning eller system.

Aspekterna kan också användas åt andra hållet för att beskriva de krav man ställer på den lösning man är ute efter. Många leverantörer kan anpassa sina lösningar utifrån beställarens krav.

Utgångspunkten för val av CDE lösning bör vara informationskraven kopplade till respektive skede och de syften och processer som specificeras i dessa, främst PIR, AIR och EIR.

![CDE_mognad](../6.modeller_och_cde/media/cde_mognad.png)

### Dokumenthantering

Detta är den grundläggande funktionaliteten som behövs för en digital CDE: förmågan att lagra filer och dokument tillsammans med de metadata som behövs. Mer avancerade lösningar bör kunna stödja leveranser och samordning genom CDE-arbetsflödet, med egenkontroller, granskning och godkännande för olika parter och roller i ett projekt.

Nivå 1: Enkel filhantering och delning

Nivå 2: Versionskontroll, arbetsflödeshantering

Nivå 3: Automatiserade arbetsflöden och oföränderliga dataposter sk immutable data

### Modellhantering/BIM-integration

Denna aspekt beskriver hur väl en CDE-lösning kan hantera modellinformation i form av exempelvis IFC-filer. Det kan handla om att det går att lägga samman modeller, om det går att granska 3D-geometrier och alfanumerisk information i modellen, samt om det går att kommentera och fördela arbetet med olika granskningssynpunkter kopplat till modellen. Sådant samarbete kan underlättas genom användning av formatet *BIM Collaboration Format* (BCF) det är en fördel om lösningen kan hantera open bim arbetsflödet.

Nivå 1: Endast lagring av BIM-filer

Nivå 2: IFC- och BCF-integration, gransknings- och kommentarsfunktioner

Nivå 3: Multidisciplinär BIM-modell för samarbete i realtid

### Säkerhet

Säkerhetsaspekten handlar inte bara om risker att obehöriga får tillgång till materialet, vilket är ett grundläggande krav. Säkerhet handlar också om att inom projekt kunna säkerställa att inte fel personer kan – av misstag eller avsiktligen – ändra i information, liksom att kunna dela upp vad olika aktörer får ta del av.

Nivå 1: Ostrukturerade filservrar

Nivå 2: Strukturerade filservrar, rättighetshantering

Nivå 3: Datadelning på objektsnivå, skydd av äganderätt till data

### Livscykelfunktionalitet

Aspekten beskriver hur väl anpassad en lösnings funktionalitet är till respektive skede, de flesta lösningarna har ett ursprung i det ena eller andra skedet om de inte är generella. Med leveransskede avses projektering och produktion.

Nivå 1: Inga speciella stöd för olika skeden

Nivå 2: Stöd för antigen leverans- eller driftskede

Nivå 3: Stöd för leverans- och driftskede

### Gränssnitt/interaktion

Olika lösningar har olika sätt för aktörer att ta del av informationen. Det kan handla om hur lätt det är att komma åt informationen; om det enkelt går att anpassa gränssnitt för olika roller eller scenarier; eller om man kan titta på modellerna i VR/AR för att ge en bättre förståelse för en anläggning innan den byggts eller utan att besöka den fysiskt.

Nivå 1: Endast access via webbläsare eller klient på dator, ej anpassningsbart

Nivå 2: Access via platta/telefon, avancerat webgränssnitt, viss anpassning

Nivå 3: VR/AR stöd, hög grad av anpassning exempelvis via API
