# Projektinformationsmodell (PIM)

## Vad är PIM?
Enligt ISO 19650 är en **projektinformationsmodell** (PIM) den sammanlagda informationsmängd som behövs för att projektera och bygga ett byggnadsverk (en byggnad eller en anläggning). Den kan bestå av olika delmängder av strukturerad eller ostrukturerad information. Exempel på strukturerad information är informationscontainrar som filer för CAD-modeller, tabeller eller databaser. Ostrukturerad information kan vara utskrivna ritningar, bilder eller video. [länk till generellt avsnitt om informationsmodellering]

En **tillgångsinformationsmodell** (AIM) är all information som är relaterad till användningsskedet. Helt naturligt finns relationer mellan AIM och PIM. Delar av innehållet i en PIM bildar underlag till AIM, och motsatt när ett projekt ska genomföras i ett befintligt sammanhang, kommer delar av AIM bli underlag för PIM.

En projektinformationsmodell består alltså vanligen av flera olika informationscontainrar. Idealt är de strukturerade enligt samma principer, så att de kan samverka och sammanställas. Hur detta ska uppnås dokumenteras i **Genomförandeplan för BIM** utifrån det som i ISO 19650 (avsnitt del 1 10.4) kallas ”strategi för informationsfederering” och ”strukturerad uppdelning av informationscontainrar”. Detta görs i syfte att:

-   möjliggöra för olika team att arbeta på olika delar av informationsmodellen samtidigt utan att samordningsproblem uppstår, till exempel rumsliga kollisioner eller funktionella motstridigheter
-   stödja informationssäkerhet, exempelvis där delar av projektet kan vara säkerhetsklassat
-   underlätta informationsöverföring genom att minska storleken på enskilda informationscontainrar och enkelt kunna isolera den information som relevant för ett visst syfte
-   definiera vad som ingår i uppgiftsteamens arbete

Målsättningen med strategin för informationsfederering är att all information enkelt kan samläsas och att information om utrymmen och byggdelar är samma mellan olika team och över tid.

Viktiga punkter att adressera för att informationsfedereringen ska fungera, och som ska dokumenteras i informationskraven och i Genomförandeplan för BIM, är:

-   konsekvent namngivning av informationscontainrar
-   dokumenterade insättningspunkter för CAD-objekt
-   namngivning och numrering av rum, våningsplan, husvolymer, delanläggningar och system
-   system för och dokumentation av gränsdragning mellan olika uppgiftsteam
-   beaktande av leveranser och uppdelning av i olika etapper.

![PIM](../6.modeller_och_cde/media/pim.png)

*Bild 1: Exempel på delmodeller i en PIM.*

## PIM i idéskede och programskede

Tidigt i livscykeln för en byggnad eller anläggnings finns inga geometriska modeller, enbart behov och önskemål från beställaren. Projektinformationsmodellen består inledningsvis huvudsakligen av den tänkta platsens förutsättningar: befintliga byggnader, markförhållanden, vägar, ledningar, vegetation med mera. Här läggs grunden för att fortsätta arbeta med projektet. (Detta benämns ”referensinformation och delade resurser”, och beskrivs i ISO 19650-2, avsnitt 5.2.2. Se vidare i *Upphandling av projekt*.)

Det faktiska arbetet med att utveckla en PIM börjar i och med att leveransskedet drar i gång. Inledningsvis kan det handla om att ta fram en **konceptmodell**. Många varianter av konceptmodeller finns, men gemensamt är att de beskriver en tänkt byggnad eller anläggning i förenklad form, innan en mer detaljerad utformning påbörjats. Det kan vara 3d-volymer över en tänkt byggnad i sin omgivning där man visar våningar och funktioner. Det kan vara 3d-volymer som beskriver byggrätten och andra begränsningar för en fastighet eller stadsdel. Det kan också var funktionsdiagram som visar hur olika funktioner relaterar till varandra, och hur flöden mellan funktionerna är tänkt att fungera, till exempel för ett sjukhus som beskriver avdelningarna och flöden av patienter, besökare, personal och leveranser.

Undan för undan utvecklas så en **kravmodell**, som innehåller den sammanställda kravställningen kopplat till anläggningen. En kravmodell kan utgöras av många olika system och informationsmängder, exempelvis rumsfunktionsprogram, funktionsbeskrivningar av typen AMA-funktion eller andra tekniska beskrivningar. I senare skeden kan också brandkonsulter upprätta 3d-kravmodeller i CAD-verktyg.

Detta blir underlag för att ta fram tekniska lösningar under följande skeden för projektering och produktion. Här finns två typer av krav:

-   informationskrav (OIR, PIR, AIR, EIR) som ställer krav typ och struktur på information
-   tekniska och funktionella krav på byggnaden eller anläggningen.

Kravmodellen ska så långt som möjligt struktureras på samma sätt som resten av projektinformationsmodellen för att möjliggöra analyser och bedömningar av kravuppfyllnad. Här rekommenderas att använda CoClass tabeller, från byggnadsverk och utrymmen ner till byggdelar i form av system och komponenter.

Syften med projektmodellen under idé- och programskede kan vara:

-   simuleringar och utvärderingar av projektets påverkan och förutsättningar, till exempel vindförhållanden, dagsljus, dagvatten, energiproduktion och energianvändning
-   underlag för ekonomisk kalkyl, klimatpåverkan och annat
-   tillstånd och underlag till myndighetsbeslut så som detaljplanering eller bygglov
-   kontakt och samordning mellan olika intressenter, till exempel närliggande fastighetsägare, allmänhet eller tänkta brukare
-   planering och upphandling av projektering och produktion.

### Exempel på kravmodell

För att åskådliggöra hur projektinformationsmodellen växer fram används objektet ”utrymme”. Byggnadsverk av olika slag skapas för att åstadkomma utrymmen som ska möjliggöra olika typer av aktiviteter. Utrymmen och kommer därför finnas representerade från första skedet, vidare i modeller och databaser genom hela byggnadens livscykel.

I idé- och programskedet finns rummen med som ett formulerat behov i den tänkta byggnaden eller anläggningen. Det kan börja med ett lokalprogram där det specificeras vilka rum och funktioner som ska ingå i byggnaden, vilken area som krävs, och vilka samband utrymmena har till varandra och till omgivningen.

Nästa steg blir att konkretisera kraven i ett rumsfunktionsprogram (RFP). Här samlas kraven baserat på de funktioner som ska uppnås: tekniska krav på exempelvis temperatur eller luftomsättning; krav på utformning exempelvis dagsljus, insyn, ytskikt med mera; inredning och utrustning som ska finnas.

Alla dessa krav samlas som egenskaper på utrymmesobjektet i en kravmodell. Arbetet underlättas om man utgår från en begränsad mängd ”typrum” som ska täcka in de flesta sorters utrymmen som ska finns i den planerade byggnaden. Med exempelvis CoClass som utgångspunkt kan man skapa en struktur för dessa för att koppla kravmodellen.

![PIM bild 2](../6.modeller_och_cde/media/pim2.png)

*Bild 2: Exempel på krav på ett utrymme med användning av CoClass-struktur och egenskaper.*

Under planeringsskedet eller tidigt i projekteringsskedet kommer lokalprogrammet omsättas i en CAD-modell där byggnaden och alla utrymmen finns representerade som ”rumsobjekt” med egenskaper som kopplar dem till sitt ”typrum” för att kunna hitta kravställningen i rumsfunktionsprogrammet.

## PIM i projekterings- och produktionsskede

Projektinformationsmodellens andra huvudsyfte är att möjliggöra projektering och produktion av en anläggning. Den består av flera delmängder, eller informationscontainrar som de benämns i BIM-sammanhang. Nedan visas några exempel på vad som kan ingå.

-   **Arkitekt-, konstruktions- och installationsmodeller**. Dessa är CAD-modeller som tillsammans beskriver anläggningen som olika system. Beroende på vilken federeringsstrategi som tagits fram kan de delas upp i ytterligare.
-   **Simuleringsmodeller** som skapas för att utreda byggnadsverkets prestanda utifrån den information som finns tillgänglig under olika delar av processen. Det kan röra sig om energianvändning, dagsljus, person eller logistikflöden, brandförlopp med mera.
-   **Fabrikationsmodeller**. Under produktionen kan underleverantörer för exempelvis fasader eller hissar skapa sina egna mer detaljerade modeller som behövs för att producera och installera dessa byggdelar.
-   **Federerad samordningsmodell**. Alla delmodeller ska kunna läsas ihop för att granska och lösa de samordningsfrågor som uppstår under projekteringen. I produktionsskedet används en sammanslagen modell för att planera och samordna de olika yrkeskategorierna på arbetsplatsen.

Syften med projektmodellen under detta skede kan vara:

-   simuleringar och utvärdering av byggnadsverkets prestanda
-   kalkylunderlag
-   mängdförteckningar för upphandling
-   handlingar för produktion.

### Exempel

Nu finns all utrymmen i den tänkta byggnaden på plats i en CAD-modell och en databas och representeras av rumsobjekt. Rummens ID används för att koppla ihop data från olika miljöer. Dessa utgörs lämpligen av utrymmets klass enligt CoClass tillsammans med rumsnumret.

Varje utrymme ska ha en koppling till kravställningen, men nu skapas informationen som handlar om den föreslagna tekniska lösningen. Som exempel specificerar arkitekten ytskiktens material och kulör.

Dörrar och installationer associeras till sina specifika rum, liksom vilken inredning som ska finnas. Allt detta blir också en del i den information som läggs på rumsobjektet i databasen. Beroende på entreprenadform har entreprenören sedan större eller mindre möjlighet att påverka de tekniska detaljerna.

När bygget är klart och det är dags för besiktning kan kravställningen på varje rum användas för att kontrollera att arbetet är utfört så som det var kravställt.

## PIM i användningsskedet

När projektorganisationen gör sin överlämning till förvaltningsorganisationen blir projektinformationsmodellen (PIM) underlag till tillgångsinformationsmodellen (AIM). Detta är PIM:s andra huvudsyfte. Då bör all den information som genererats om de byggdelar som faktiskt installerades i anläggningen vara helt integrerad i PIM så att den förs över till AIM.

När detta görs ska informationen delas upp på ett annat sätt än i tidigare skeden. Information från vissa delar av PIM förs över till de förvaltningssystem som används. Det handlar om de byggdelar som ingår i driften eller förvaltas vidare, exempelvis installationer och utrustning. Andra delar ligger kvar som **relationsmodeller** i väntan på det tillfälle då det initieras ett nytt projekt, kanske i form av en om- eller tillbyggnad. De delar av PIM som inte längre är relevanta övergår helt till arkiverad status.

Relationsmodellerna beskriver byggnadsverket så som det faktiskt blev byggd. I takt med förändringar bör dessa hållas uppdaterade, så att de kan utgöra underlag till en ny PIM vid ombyggnadsprojekt. Målet för framtida system bör vara att relationsmodeller och förvaltningssystem blir helt integrerade.

### Exempel

Rummen behöver nu föras över till AIM och ingå i ett förvaltningssystem. Det görs genom en dataöverföring med hjälp av IFC-filer från CAD-systemen som användes vid projektering och produktion till det system som förvaltningsorganisationen använder. Hur dataöverföring rent praktiskt ska göras mellan PIM och AIM behöver fastställas i tillgångsinformationskraven (AIR).