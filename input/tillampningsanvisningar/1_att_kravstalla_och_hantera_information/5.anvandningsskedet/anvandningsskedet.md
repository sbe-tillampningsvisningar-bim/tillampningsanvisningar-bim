
# Användningsskedet

Drift och underhåll av befintliga tillgångar pågår under det SS-EN ISO 19650 kallar **användningsskedet**. Det här beskrivs i detalj i SS-EN ISO 19650 Del 3 *Användningsskede*.

Figur 4 i Del 3 bryter ner informationshantering i användningsskedet i åtta aktiviteter. Detta dokument beskriver aktiviteterna som följer efter att drift och underhåll av en tillgång blivit upphandlad och uppdragsbekräftelse färdigställd, det vill säga aktiviteterna 5–8:

![Figure 4 förklarad](../5.anvandningsskedet/media/figure4_forklarad.png)

*Bild 2: SS-EN ISO 19650-3, Figur 4, kompletterad med förklarande text.*

I figuren visar siffrorna enskilda aktiviteter, medan bokstäverna betecknar grupper av aktiviteter. Den vänstra gröna cirkeln visar tillgångsinformationsmodellen innan leveransskedet, och den högra orange cirkeln visar hur den fyllts på med information från drift och underhåll.

|    | **Aktiviteter**                                                                                                                                                                                                                                                                                                                       |
|----|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 1  | bedömning av behov Uppdragsgivarens behov dokumenteras i *organisationens informationskrav* (OIR), *tillgångsförvaltningens informationskrav* (AIR) och *projektets informationskrav* (PIR). Här ingår också att utse egen informationsansvarig för projektet, och att beskriva förutsättningar och krav för det digitala samarbetet. |
| 2  | anbudsförfrågan  Här ingår OIR, PIR, AIR och EIR.                                                                                                                                                                                                                                                                                     |
| 3  | svar på anbudsförfrågan Potentiella huvudansvariga uppdragstagare tar fram anbud. Här ingår att ta fram en genomförandeplan för BIM som visar hur man tänkt uppfylla informationskraven.                                                                                                                                              |
| 4  | uppdragsbekräftelse Tas fram av den huvudansvariga uppdragstagaren av efter accept av anbud.                                                                                                                                                                                                                                          |
| 5  | uppdragsstart Den huvudansvariga uppdragstagaren aktiverar deltagare och andra resurser.                                                                                                                                                                                                                                              |
| 6  | gemensam produktion av information Information från drift och underhåll produceras, interngranskas och delas i en AIM.                                                                                                                                                                                                                |
| 7  | godkännande av informationsmodell (AIM) Uppdragsgivaren granskar och godkänner informationen.                                                                                                                                                                                                                                         |
| 8  | AIM-aggregering Godkänd ny information läggs till befintlig.                                                                                                                                                                                                                                                                          |
|    | **Aktivitetsgrupper**                                                                                                                                                                                                                                                                                                                 |
| A  | samtliga aktiviteter under användningsskedet                                                                                                                                                                                                                                                                                          |
| B  | aktiviteter under varje drifts- och underhållsuppdrag *före* en kritisk händelse                                                                                                                                                                                                                                                      |
| C  | aktiviteter under varje drifts- och underhållsuppdrag *efter* en kritisk händelse eller leveransfas enligt ISO 19650-2                                                                                                                                                                                                                |
| D  | aktiviteter som när en tillgång förvärvas                                                                                                                                                                                                                                                                                             |
| E  | aktiviteter som genomförs under upphandlingsskedet av uppdraget                                                                                                                                                                                                                                                                       |
| F  | aktiviteter som genomförs under informationsplaneringsskedet (för varje beställning)                                                                                                                                                                                                                                                  |
| G  | aktiviteter som genomförs under informationsproduktionsskedet (för varje beställning)                                                                                                                                                                                                                                                 |
|    | **Beslutspunkter, frågor och beslut**                                                                                                                                                                                                                                                                                                 |
| H  | typ av kritisk händelse som ger information                                                                                                                                                                                                                                                                                           |
| K  | fortsättning av arbetet med beställningen                                                                                                                                                                                                                                                                                             |
| L  | fortsättning av arbetet med denna process för informationshantering                                                                                                                                                                                                                                                                   |
| M  | via en beställning gjord före en kritisk händelse                                                                                                                                                                                                                                                                                     |
| N  | mottagen från en annan uppdragsgivare/tillgångsägare                                                                                                                                                                                                                                                                                  |
| P  | via en beställning gjord efter en kritisk händelse eller leveransfas enligt ISO 19650-2                                                                                                                                                                                                                                               |
| Q1 | ja – huvudansvarig uppdragstagare väntar på nästa kritiska händelse                                                                                                                                                                                                                                                                   |
| Q2 | ja – huvudansvarig uppdragstagare fortsätter till nästa milstolpe för informationsleverans                                                                                                                                                                                                                                            |
| Q3 | ja – huvudansvarig uppdragstagare har fortsatt ansvar för tillgången                                                                                                                                                                                                                                                                  |
| R  | nej                                                                                                                                                                                                                                                                                                                                   |

*Tabell 2: Förklaring av Bild 2.*

En sammanfattning av de tre ”spåren” B, C och D i Bild 2 kan se ut så här:

- B: Ordinarie drift och underhåll.
- C: Särskilt upphandlat projekt under användningsskedet eller i leveransskedet.
- D: Förvärv av en tillgång.


Alla spåren mynnar ut i steg 8, då information förs in i tillgångsinformationsmodellen: från drift och underhåll, från projektet respektive om förvärvet.

Om man väljer spår B eller C beror huvudsakligen på två faktorer:

-   vilket behov som ska tillfredsställas
-   om det ligger inom ramen för redan upphandlad drift och underhåll.

Om svaret på fråga två – som benämns H i Bild 2 – är ja, då väljer man spår B. Om svaret är nej, följer man i stället spår C. Som exempel är rimligen byte av filter i ett ventilationsaggregat en ordinarie åtgärd, medan att byta va-stammar i ett bostadshus är en åtgärd som behöver handlas upp som ett projekt.

## Beskrivning av processen för användningsskedet

Behovet att drifta och underhålla tillgångar finns mer eller mindre kontinuerligt under deras användning. För att åstadkomma detta tar tillgångsägaren rollen som **uppdragsgivare**, i syfte att – förutom se till att redan byggda tillgångar blir omhändertagna – bli mottagare av information som löpande visar tillståndet för dem.

Anbudsförfrågan behöver alltså beskriva både vad som ska förvaltas och vilken information som samtidigt ska produceras och lagras. För en beskrivning av hur man handlar upp rätt information, klicka på knappen nedan.

Beroende på vald upphandlingsform handlar beställaren upp en eller flera **huvudansvariga uppdragstagare** som ansvarar för helheten. Den huvudansvariga uppdragstagaren utser eller handlar sedan upp de **uppdragstagare** som behövs i projektet.

Huvudansvarig uppdragstagare och uppdragstagare blir alla sändare av information, och bildar tillsammans ett **leveransteam**. I stora sammanhang kan flera sådana team behövas. Hos dessa skapas efter behov **uppgiftsteam** som utför arbetet.

Alla dessa parter bildar tillsammans ett **team för drift och underhåll**. Grupperna utbyter information med varandra kontinuerligt under projektet, och utgör därmed uppdragsgivare respektive uppdragstagare gentemot varandra.

![Organisation](../5.anvandningsskedet/media/org.png)<br>
*Bild 1: Gränssnitten mellan aktörerna i användningsskedet (SS-EN ISO 19650-3, Figure 2).*

|                                                 | **Förklaring**                            | **Exempel 1**                                        | **Exempel 2**                                        |
|-------------------------------------------------|-------------------------------------------|------------------------------------------------------|------------------------------------------------------|
| A                                               | Uppdragsgivare                            | Drift- och underhållsansvarig hos bostadsbolag       | Projektledare hos Trafikverket                       |
| B                                               | Huvudansvarig uppdragstagare              | Drift- och underhållsentreprenör (FM)                | Drift- och underhållsentreprenör                     |
| C                                               | Uppdragstagare                            | Lokalvårdare, servicetekniker                        | Snöröjare, underhållstekniker                        |
| …                                               | Variabel mängd                            |                                                      |                                                      |
| 1                                               | Team för drift och underhåll              | Samtliga                                             | Samtliga                                             |
| 2                                               | Leveransteam                              | Samtliga anlitade av en B                            | Samtliga anlitade av en B                            |
| 3                                               | Uppgiftsteam                              | Samtliga anlitade av en B att utföra en viss uppgift | Samtliga anlitade av en B att utföra en viss uppgift |
| &ensp; ![Pil 1](../5.anvandningsskedet/media/pil1.png) | Informationskrav och informationsleverans |                                                      |                                                      |
|  ![Pil 2](../5.anvandningsskedet/media/pil2.png) | Samordning av information                 |                                                      |                                                      |

*Tabell 1: Förklaring av Bild 2. Observera att B här står för ”huvudansvarig uppdragstagare”.*

Beskrivningen av användningsskedet är i mångt och mycket en beskrivning av hur drift och underhåll har gått till traditionellt. Den stora skillnaden är poängteringen att informationsleveranser alltid ska baseras på krav från den part som vill ha informationen. Det ska vara ett informationsutbyte, där ett krav resulterar i en leverans. Klicka på knappen nedan för att gå direkt till en beskrivning av informationsutbyte.

Teamet för drift och underhåll, under ledning av den huvudansvariga uppdragstagaren, måste kontinuerligt uppfylla de ställda kraven på informationsleveranser.

## Detaljanvisningar
Nedan följer en beskrivning av skedena 5–8 i standardens Figur 4. 

## 5 Uppdragsstart
När upphandlingen är genomförd och uppdragstagare utsedda kan drift- och underhållsuppdraget starta.

**Innehåll:**<br>
[Leveransteam](https://tillampningsanvisningarbim.kravportal.se/anvandningsskedet#leveransteam)<br>
[Informationsteknik](https://tillampningsanvisningarbim.kravportal.se/anvandningsskedet#linformationsteknik)<br>
[Metoder och rutiner](https://tillampningsanvisningarbim.kravportal.se/anvandningsskedet#metoder-och-rutiner)<br>
[Behåll resurser beredda på nyckelhändelse](https://tillampningsanvisningarbim.kravportal.se/anvandningsskedet#behåll-resurser-beredda-på-nyckelhändelse)<br>

#### Leveransteam

*Ansvarig: huvudansvarig uppdragstagare*

Bekräfta att alla i det leveransteam som beskrivits i genomförandeplanen för BIM finns tillgängliga, att de är insatta i uppdragets krav på informationsleveranser, och vid behov genomföra utbildning.    


#### Informationsteknik 

*Ansvarig: huvudansvarig uppdragstagare*

Se till att de tekniska system som finns specificerade i genomförandeplanen för BIM är implementerade, och att informationsutbytet mellan leveransteamen och till uppdragsgivaren fungerar som avsett.


#### Metoder och rutiner

*Ansvarig: huvudansvarig uppdragstagare*

Testa de metoder och rutiner för produktion av tillgångsinformation som beskrivs i genomförandeplanen för BIM. Här ingår exempelvis uppdelning av informationscontainrar och tillgänglighet för gemensamma resurser och underlag.


#### Behåll resurser beredda på nyckelhändelse
*Ansvarig: huvudansvarig uppdragstagare*

Se till att alla mänskliga och tekniska resurser som specificerats i steg 5.5.1–3 fortsätter vara tillgängliga för eventuella nyckelhändelser under uppdraget.

## 6 Gemensam produktion av information
**Innehåll:**<br>
[Referensinformation och delade resurser](https://tillampningsanvisningarbim.kravportal.se/anvandningsskedet#referensinformation-och-delade-resurser)<br>
[Produktion och leverans av information](https://tillampningsanvisningarbim.kravportal.se/anvandningsskedet#produktion-och-leverans-av-information)<br>
[Kvalitetssäkring](https://tillampningsanvisningarbim.kravportal.se/anvandningsskedet#kvalitetssäkring)<br>
[Granskning av leverans](https://tillampningsanvisningarbim.kravportal.se/anvandningsskedet#granskning-av-leverans)<br>
[Leveransteamets granskning av informationsmodell](https://tillampningsanvisningarbim.kravportal.se/anvandningsskedet#leveransteamets-granskning-av-informationsmodell)<br>
[Godkännande av informationsmodell](https://tillampningsanvisningarbim.kravportal.se/anvandningsskedet#godkännande-av-informationsmodell)<br>

#### Referensinformation och delade resurser 
*Ansvarig: uppgiftsteam*

Kontrollera att alla har tillgång till den information de behöver i den delade arbetsmiljön. Om inte, ska den huvudansvariga uppdragstagaren meddelas, inklusive eventuell påverkan detta kan ha.

#### Produktion och leverans av information
*Ansvarig: uppgiftsteam*

Producera och leverera  information baserat på delplan för informationsleveranser. Informationen ska följa alla föreskrifter för uppdraget avseende informationsstandard, metoder och rutiner, nivå av informationsbehov, informationsstruktur och annat.

#### Kvalitetssäkring
*Ansvarig: uppgiftsteam*

Kvalitetssäkra varje informationscontainer avseende uppdragets informationsstandard innan den lämnas för intern granskning.

#### Granskning av leverans
*Ansvarig: uppgiftsteam*

Innan en informationscontainer delas för andras användning i uppdragets delade datamiljö ska den granskas och godkännas av uppgiftsteamet. Granskningen ska avse den huvudansvariga uppdragstagarens informationskrav, nivån för informationsbehov, och information som behövs för samordning av andra uppgiftsteam. Om leveranser inte godkänns ska orsakerna dokumenteras.


#### Leveransteamets granskning av informationsmodell
*Ansvarig: leveransteamet*

Leveransteamet ska kontinuerligt granska informationsmodellen, och se till att den uppfyller kraven på uppdragets metoder och rutiner, uppdragsgivarens informationskrav och acceptanskriterier, och att huvudplanen för informationsleveranser följs

#### Godkännande av informationsmodell
*Ansvarig: huvudansvarig uppdragstagare*

Innan informationsmodellen levereras till uppdragsgivaren ska den granskas och godkännas av huvudansvarig uppdragstagare. Granskningen ska avse fastställd informationsstandard och metoder och rutiner för produktion av tillgångsinformation. Om leveranser inte godkänns ska orsakerna dokumenteras.

## 7 Leverans av informationsmodell 
**Innehåll:**<br>
[Inlämning av informationsmodell till uppdragsgivare](https://tillampningsanvisningarbim.kravportal.se/anvandningsskedet#inlämning-av-informationsmodell-till-uppdragsgivare)<br>
[Uppdragsgivarens granskning av informationsmodell](https://tillampningsanvisningarbim.kravportal.se/anvandningsskedet#uppdragsgivarens-granskning-av-informationsmodell)<br>


#### Inlämning av informationsmodell till uppdragsgivare
*Ansvarig: uppgiftsteam*

Lämna godkända informationscontainrar till uppdragsgivaren för granskning och godkännande.
Informationsmodell kan också komma från en annan uppdragsgivare/tillgångsägare genom ett förvärv. Även den ska lämnas för granskning och godkännande.


#### Uppdragsgivarens granskning av informationsmodell
*Ansvarig: uppdragsgivare*

Kontrollera att levererade informationscontainrar uppfyller uppdragets metoder och rutiner. Granskningen avser att det är rätt leverabler, och att de uppfyller samtliga informationsutbyteskrav, acceptanskriterier och nivå för informationsbehov.

Icke godkända leveranser görs om och lämnas till ny granskning.

Godkända leveranser blir en slutgiltig leverabel i uppdragets delade datamiljö.


## 8 AIM-aggregering
**Innehåll:**<br>
[Aggregering av godkända informationsmodeller till AIM](https://tillampningsanvisningarbim.kravportal.se/anvandningsskedet#aggregering-av-godkända-informationsmodeller-till-aim)<br>
[Fortsatt underhåll av AIM](https://tillampningsanvisningarbim.kravportal.se/anvandningsskedet#fortsatt-underhåll-av-aim)<br>

#### Aggregering av godkända informationsmodeller till AIM 
*Ansvarig: uppdragsgivare*

För in nya informationsmodeller i tillgångsinformationsmodellen (AIM).


#### Fortsatt underhåll av AIM
*Ansvarig: uppdragsgivare*

Underhåll tillgångsinformationsmodellen så att den fortsätter att uppfylla de krav som ställts. Se *Upphandling av drift och underhåll*. 
