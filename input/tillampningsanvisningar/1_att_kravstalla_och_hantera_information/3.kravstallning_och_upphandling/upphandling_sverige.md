# Upphandling enligt ISO 19650 med svenska standardavtal

## Inledning
Vi kompletterar beskrivningen av upphandlingsskedet med några svenska kommentarer. Syftet med upphandling enligt ISO 19650 är att säkerställa en kontinuitet och kvalitet i hanteringen av uppdragsgivarens tillgångsinformation genom hela livscykeln. Standarden tydliggör delaktiga aktörers roller och ansvar för informationshantering som under åren har utvecklats i svensk praxis. Aktörernas ansvar går också väl att kombinera med standardavtalen enligt ABK, AB och ABT.

<table>
    <tbody>
        <tr>
            <td><b>&#9432;</b></td>
            <td>Observera att denna anvisning är framtagen innan revideringen av standardavtalen AB och ABT slutförts (preliminärt 2025). Anvisningen kommer att uppdateras när de nya standardavtalen träder i kraft.</td>
        </tr>
    </tbody>
</table>

Bilderna nedan visar schematiskt förhållandet mellan uppdragsgivare, huvudansvarig uppdragstagare och uppdragstagare i utförandeentreprenad respektive totalentreprenad där ISO 19650 tillämpas. Samtliga huvudansvariga uppdragstagare har avtal direkt med uppdragsgivaren. Det är däremot viktigt att poängtera att ISO 19650 beskriver förhållanden mellan involverade aktörerna utifrån ett informationshanteringsperspektiv, inte utifrån deras kontraktuella relationer. En huvudansvarig uppdragstagare kan till exempel ha interna uppdragstagare som arbetar i ett uppgiftsteam, och en uppdragsgivare kan ha interna huvudansvariga uppdragstagare.

#### 5.1 Utförandeentreprenad

![Upphandling AB](../3.kravstallning_och_upphandling/media/upphandling_bild1.png)

*Bild 1: Alternativa relationer mellan uppdragsgivare och uppdragstagare i en utförandeentreprenad där ISO 19650 tillämpas. Beroende på upphandlingsform kan uppdragsgivaren välja att teckna avtal med en huvudansvarig uppdragstagare eller med flera.*

Upphandling av projektering och produktion kan göras med en enda huvudansvarig uppdragstagare, som i svenska projekt brukar kallas generalkonsult respektive generalentreprenör. Alternativet är att göra en delad upphandling och teckna avtal med flera huvudansvariga uppdragstagare. I så fall måste uppdragsgivaren själv ansvara för informationssamordningen mellan de huvudansvariga uppdragstagarna. Detta kan göras genom att anlita en intern eller extern BIM-samordnare som koordinerar leveransteamen och rapporterar till uppdragsgivarens informationsansvariga.

Observera att standarden låter både projektering och bygg- och anläggningsentreprenaden vara del av ett och samma projektteam. Det betonar vikten av tätt samarbete under hela leveransfasen.

#### 5.2 Totalentreprenad

![Upphandling ABT](../3.kravstallning_och_upphandling/media/upphandling_bild2.png)

*Bild 2: Relationen mellan uppdragsgivare och uppdragstagare i en totalentreprenad där ISO 19650 tillämpas.*

I en totalentreprenad ansvarar totalentreprenören i egenskap av huvudansvarig uppdragstagare för BIM-samordningen mellan uppgiftsteamen.

#### 5.3 Rangordning mellan handlingar

De flesta bygg- och anläggningsentreprenader som genomförs som utförandeentreprenad använder AB 04 för att reglera parternas ansvar och skyldigheter. I kapitel 1 § 3 anges rangordningen mellan kontraktshandlingarna, som används i de fall motstridiga uppgifter upptäcks.

I denna rangordningsregel nämns inte CAD-modeller. Om entreprenören tillåts använda dessa direkt som underlag för kalkyl eller produktion måste eventuella hanteringen av avvikelser i förhållande till ritningar klargöras. Fyra strategier av kombinationen CAD-modell och ritningar kan tillämpas:

1.  All information finns i CAD-modellen.
2.  CAD-modellen innehåller all information men uppdragsgivaren tillhandahåller ritningar för information, till exempel i syfte att ge anbudsgivare en överblick över entreprenaden.
3.  CAD-modellen kompletteras med ritningar som redovisar exempelvis detaljer och uppställningar.
4.  All information finns i ritningar, dels generade ur modell, dels ritningar som redovisar exempelvis detaljer och uppställningar.

I avsnitten nedan ges förslag på justerad rangordning som används i de fyra varianterna. Texten förs in i Administrativa Föreskrifter, AFC.11.

Rangordningsregeln är en fast bestämmelse. Som anges i kommentaren till AB 04 kapitel 1 § 3 ska en ändring av denna i kontraktet vara tydlig för att gälla före AB 04. Följande gäller för att ändringen ska anses tydlig:

-   Ändringen beskrivs under rätt kod och rubrik i de Administrativa Föreskrifterna.
-   Samtliga ändringar sammanställs i de Administrativa Föreskrifterna under kod AFC.111. I sammanställningen ska det anges under vilken kod och rubrik som ändringen finns.

Om detta uppfylls gäller den ändrade bestämmelsen före bestämmelse i AB 04.

#### 5.3.1 All information i CAD-modellen

Här finns all gällande information i godkända CAD-modeller. Användare kan för eget eller annans bruk vid behov ta ut ritningar, men dessa utgör inte formella handlingar.

**Kontraktshandlingar**

Entreprenören åtar sig att utföra kontraktsarbetena åt beställaren i enlighet med nedanstående kontraktshandlingar.

Med ändring av AB 04 kap 1 § 3 gäller följande.

Vid motstridiga uppgifter i kontraktshandlingarna gäller de i följande ordning, om inte omständigheterna uppenbarligen föranleder annat.

1.  kontrakt
2.  ändringar i AB 04 som är upptagna i sammanställning i administrativa föreskrifter
3.  AB 04
4.  beställning
5.  anbud
6.  särskilda mät- och ersättningsregler
7.  à-prislista eller prissatt mängdförteckning
8.  kompletterande föreskrifter för entreprenaden lämnade före anbudets avgivande
9.  administrativa föreskrifter
10. BIM-föreskrifter
11. ej prissatta mängdförteckningar
12. beskrivningar
13. leveransspecifikationer
14. CAD-modeller
15. övriga handlingar.

#### 5.3.2 CAD-modell med ritningar för information

Här finns all gällande information i en kombination av godkända CAD-modeller och ritningar med status FÖR INFORMATION. Användare kan för eget eller annans bruk generera ritningar från CAD-modellen, men dessa utgör inte formella handlingar.

**Kontraktshandlingar**

Entreprenören åtar sig att utföra kontraktsarbetena åt beställaren i enlighet med nedanstående kontraktshandlingar.

Med ändring av AB 04 kap 1 § 3 gäller följande.

Vid motstridiga uppgifter i kontraktshandlingarna gäller de i följande ordning, om inte omständigheterna uppenbarligen föranleder annat.

1.  kontrakt
2.  ändringar i AB 04 som är upptagna i sammanställning i administrativa föreskrifter
3.  AB 04
4.  beställning
5.  anbud
6.  särskilda mät- och ersättningsregler
7.  à-prislista eller prissatt mängdförteckning
8.  kompletterande föreskrifter för entreprenaden lämnade före anbudets avgivande
9.  administrativa föreskrifter
10. BIM-föreskrifter
11. ej prissatta mängdförteckningar
12. beskrivningar
13. leveransspecifikationer
14. CAD-modeller
15. ritningar
16. övriga handlingar.

#### 5.3.3 CAD-modell med kompletterande ritningar

Här finns all gällande information i godkända CAD-modeller, kompletterat med godkända detaljritningar och typritningar över delar som i CAD-modeller redovisas förenklat. Användare kan för eget eller annans bruk vid behov ta ut ytterligare ritningar från CAD-modell, men dessa utgör inte formella handlingar.

**Kontraktshandlingar**

Entreprenören åtar sig att utföra kontraktsarbetena åt beställaren i enlighet med nedanstående kontraktshandlingar.

Med ändring av AB 04 kap 1 § 3 gäller följande.

Vid motstridiga uppgifter i kontraktshandlingarna gäller de i följande ordning, om inte omständigheterna uppenbarligen föranleder annat.

1.  kontrakt
2.  ändringar i AB 04 som är upptagna i sammanställning i administrativa föreskrifter
3.  AB 04
4.  beställning
5.  anbud
6.  särskilda mät- och ersättningsregler
7.  à-prislista eller prissatt mängdförteckning
8.  kompletterande föreskrifter för entreprenaden lämnade före anbudets avgivande
9.  administrativa föreskrifter
10. BIM-föreskrifter
11. ej prissatta mängdförteckningar
12. beskrivningar
13. ritningar
14. leveransspecifikationer
15. CAD-modeller
16. övriga handlingar.

#### 5.3.4 Enbart ritningar

Här finns all gällande information i godkända ritningar som är genererade ut godkända CAD-modeller, vid behov kompletterat med detaljritningar och typritningar över delar som i CAD-modeller redovisas förenklat. Användare kan för eget eller annans bruk vid behov ta ut ytterligare ritningar från CAD-modell, men dessa utgör inte formella handlingar.

**Kontraktshandlingar**

Entreprenören åtar sig att utföra kontraktsarbetena åt beställaren i enlighet med nedanstående kontraktshandlingar.

Med ändring av AB 04 kap 1 § 3 gäller följande.

Vid motstridiga uppgifter i kontraktshandlingarna gäller de i följande ordning, om inte omständigheterna uppenbarligen föranleder annat.

1.  kontrakt
2.  ändringar i AB 04 som är upptagna i sammanställning i administrativa föreskrifter
3.  AB 04
4.  beställning
5.  anbud
6.  särskilda mät- och ersättningsregler
7.  à-prislista eller prissatt mängdförteckning
8.  kompletterande föreskrifter för entreprenaden lämnade före anbudets avgivande
9.  administrativa föreskrifter
10. BIM-föreskrifter
11. ej prissatta mängdförteckningar
12. beskrivningar
13. ritningar
14. leveransspecifikationer
15. övriga handlingar.

#### 5.3.5 Mängdförteckningar

Under anbudsskedet är mängder på ingående material och utrustning av stor betydelse. Uppdragsgivaren kan låta anbudsgivare hämta mängder själva från CAD-modeller, alternativt ha mängdförteckningar som en del i anbudsunderlaget.

Följande text kan införas i Administrativa Föreskrifter:

”Vissa mängder redovisas i mängdförteckningar. Övriga mängder hämtas vid behov från CAD-modell.”
