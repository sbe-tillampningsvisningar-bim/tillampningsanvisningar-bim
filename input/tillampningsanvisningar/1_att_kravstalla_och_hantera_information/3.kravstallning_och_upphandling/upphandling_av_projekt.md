# Upphandling av projekt

## Inledning 

Den här sammanfattningen av upphandling av bygg- och anläggningsprojekt bygger på beskrivningen i Del 2 av ISO 19650. En viktig inledande kommentar: vi gör här en viss nyansering av den svenska översättningen av standarden. I Del 1 av standarden finns följande översättning av det engelska begreppet *appointment*:

> **beställning**  
> överenskommen beskrivning av tillhandahållande av information beträffande arbeten, varor eller tjänster
> 
> <em>Anmärkning till termpost 1: Denna term används oavsett om det finns någon formell beställning eller inte mellan parterna.</em> 
>

Den engelska termen är dock dubbeltydig, och kan också avse det uppdrag som baseras på beställningen. Det återspeglas i den svenska översättningen av aktörerna: **uppdragsgivare** (*appointing party*), **huvudansvarig uppdragstagare** (*lead appointed party*) och **uppdragstagare** (*appointed party*). Här använder vi därför primärt termen **uppdrag**.

Det handlar alltså om alltså upphandling av informationshanteringen i uppdrag i form av bygg- och anläggningsprojekt. Upphandling av uppdrag som avser information om drifts- och underhållsarbete beskrivs i dokumentet *Upphandling av drift och underhåll.*

Anvisningarna i dokumentet förutsätter att kraven på vilken information som ska levereras redan är fastställda i form av organisationens informationskrav (OIR), tillgångsinformationskrav (AIR) och informationsutbyteskrav (EIR). 


I Del 2 av ISO 19650 finns en figur som bryter ner uppdrag i åtta skeden. Detta dokument beskriver upphandlingsprocessen, som omfattar skede 1–4:

![Process steg 1-4](../3.kravstallning_och_upphandling/media/process_1-4.png)

*Bild 1: Informationshanteringsprocess under leveransskedet av tillgångar. Källa: SS-EN ISO 19650-2:2019 (sv).*

I figuren visar siffrorna delprocesser, medan bokstäverna betecknar grupper av delprocesser. Den vänstra cirkeln visar tillgångsinformationsmodellen innan leveransskedet, och den feta högra cirkeln visar hur den fyllts på med information från projektet.

1.  **Skede 1** är omfattande. Här fastställs alla krav: *organisationens informationskrav* (OIR), *tillgångsförvaltningens informationskrav* (AIR), *projektets informationskrav* (PIR) och *informationsutbyteskrav* (EIR). Här ingår också att utse egen informationsansvarig för projektet, och att beskriva förutsättningar och krav för det digitala samarbetet.
2.  **Skede 2** handlar om att ta fram anbudsförfrågan. Här ingår en lång rad av aktiviteter som beskrivs nedan.
3.  **Skede 3** är det skede då potentiella huvudansvariga uppdragstagare tar fram sina anbud.
4.  **Skede 4** drar i gång när en huvudansvarig uppdragstagare – eller flera om det är en delad upphandling – har blivit antagna. De måste då bekräfta att de faktiskt kan utföra allt de påstått i anbudet.

Skede 5–8 beskrivs i detalj i anvisningarna om *Leveransskedet*, *Genomförandeplan för BIM* och *Ansvarsmatris*.

Bokstäverna i figuren är mindre relevanta i det här sammanhanget, men kan ändå behöva förklaras:

-   A visar återkoppling från ett genomfört projekt tillbaka till upphandlingen av nästa.
-   B visar allt som händer i ett visst projekt.
-   C är avgränsat till ett visst uppdrag i projektet.
-   D är upphandlingen av uppdraget.
-   E är planering av uppdraget.
-   F är det faktiska uppdraget.

Nedan följer en sammanfattande beskrivning av innehållet i Del 2. 

En beskrivning av hur projekt som bedrivs enligt ISO 19650 passar in i de svenska standardavtalen ABK, AB och ABT finns på sidan *Upphandling enligt ISO 19650 med svenska standardavtal*.


## 1. Uppdragsgivarens bedömning av behov
**Innehåll:**  
[1.1 Informationsansvariga](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_projekt/#11-informationsansvariga)  
[1.2 Beslutspunkter och projektinformationskrav (PIR)](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_projekt/#12-beslutspunkter-och-projektinformationskrav-pir)  
[1.3 Milstolpar för informationsleverans](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_projekt/#13-milstolpar-för-informationsleverans)  
[1.4 Informationsstandard ](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_projekt/#14-informationsstandard)  
[1.5 Metoder och rutiner för informationsproduktion](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_projekt/#15-metoder-och-rutiner-för-informationsproduktion)  
[1.6 Referensinformation och delade resurser](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_projekt/#16-referensinformation-och-delade-resurser)  
[1.7 Delad datamiljö (CDE)](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_projekt/#17-delad-datamiljö-cde)  
[1.8 Informationsföreskrifter](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_projekt/#18-informationsföreskrifter)


Det första steget handlar om att uppdragsgivaren ska fastställa direktiv och beskriva förutsättningar för det kommande uppdraget. Allt detta ska ingå i anbudsförfrågan i ett dokument benämnt **BIM-föreskrifter**. Syftet att säkerställa att informationshanteringen blir så effektiv som möjligt. Anbudsgivare ska svara på kraven genom att bland annat formulera en anbudsversion av en **genomförandeplan för BIM** (*BIM Execution Plan*, BEP).

#### 1.1 Informationsansvariga 

*Ansvarig: uppdragsgivaren*  
*Förs in i BIM-föreskrifterna, avsnitt 1.1*

Uppdragsgivaren ska utse en eller flera personer inom sin organisation som ska ansvara för informationshanteringen i uppdraget. Detta är en viktig roll, som ytterst syftar till att försäkra sig om att den information som levereras uppfyller de krav som organisationen behöver, och som används i drift och underhåll. För att hitta rätt person för uppdraget, behöver man analysera vilka arbetsuppgifter som ska utföras, vilka befogenheter som krävs, och vilken kompetens som personen eller personerna behöver.

Uppdraget som informationsansvarig kan ges till tredje part, inklusive den presumtive huvudansvariga uppdragstagaren.

#### 1.2 Beslutspunkter och projektinformationskrav (PIR)

*Ansvarig: uppdragsgivaren*  
*Förs in i BIM-föreskrifter, avsnitt 2.3.*

Under projekt planeras eller uppstår ofta ett antal **kritiska beslutspunkter**. Det kan handla om alternativa planlösningar, val av stomkonstruktion för en byggnad, behov av förstärkningsåtgärder för mark, typ av luftbehandlingssystem, förväntade kostnader för produktion och drift, och mycket annat. Kritiska beslutspunkter uppstår också vid övergången mellan skedena i projekteringen: programhandling, systemhandling och bygghandling, alternativt från vägplan eller järnvägsplan till bygghandling, eventuellt med mellansteget systemhandling.

Vid varje sådan här kritisk beslutspunkt behövs information. Vilka konkreta beslut behöver fattas? Vilken information behövs då? Kraven på PIR ska baseras på de som gäller för organisationen (OIR) och för drift och underhåll (AIR).

#### 1.3 Milstolpar för informationsleverans 

*Ansvarig: uppdragsgivaren*  
*Förs in i BIM-föreskrifter, avsnitt 2.3.2*

Uppdragets tidsplan och milstolpar är i hög grad beroende av entreprenadform. I utförandeentreprenader följer byggprojekt ofta en serie skeden i form av programhandling, systemhandling, bygglovshandling och bygghandling. Varje sådant skede kan i sin tur underindelas, för byggnad till exempel för information om planlösning, stomme, inredning, installationer och så vidare. I väg- och järnvägsprojekt används liknande skedesindelning i form av förstudier, åtgärdsvalsstudier, vägplan respektive järnvägsplan, i vissa fall systemhandling, och till slut bygghandling.

Uppdragsgivaren ska fastställa uppdragets milstolpar för informationsleverans i enlighet med projektplanen. Vilka är de, och vilken information behövs för att kunna fatta beslut inför nästa skede?

#### 1.4 Informationsstandard  

*Ansvarig: uppdragsgivaren*  
*Förs in i BIM-föreskrifter, avsnitt 3*

Det digitala informationsflödet in till och ut från uppdraget måste vara användbart för uppdragsgivaren. Detta får inte påverkas av de metoder som uppdragstagare använder under uppdragets gång. Uppdragsgivaren måste därför beskriva eventuella specifika informationsstandarder som uppdragsgivarens organisation behöver för att kunna använda den på ett bra sätt.

Informationsstandarden kan avse bland annat filformat, informationsstruktur, klassifikationssystem och användning av metadata.

#### 1.5 Metoder och rutiner för informationsproduktion 

*Ansvarig: uppdragsgivaren*  
*Förs in i BIM-föreskrifter, avsnitt 4*

Det praktiska genomförandet av informationsflödet in till och ut från uppdraget behöver också säkerställas. Uppdragsgivaren behöver därför fastställa eventuella specifika metoder och rutiner för informationsproduktion som ska användas. Det kan handla om hur befintlig tillgångsinformation ska samlas in och levereras; hur ny information ska genereras, levereras, granskas och godkännas; hur den ska lagras på ett säkert sätt.

#### 1.6 Referensinformation och delade resurser

*Ansvarig: uppdragsgivaren*  
*Förs in i BIM-föreskrifter, avsnitt 4.1*

Uppdragsgivaren behöver dela med sig av referensinformation, befintlig tillgångsinformation, mallar och objektbibliotek och andra resurser med de presumtiva huvudansvariga uppdragstagarna under anbudsförfarandet eller uppdraget. Fastställ vad som kan behövas i projektet. Använd öppna informationsstandarder så långt som möjligt.

#### 1.7 Delad datamiljö (CDE)

*Ansvarig: uppdragsgivaren*  
*Förs in i BIM-föreskrifter, avsnitt 5*

En fungerande teknisk lösning och gemensamma rutiner för delning av information är centrala för att uppdraget ska fungera som avsett. Alla ska kunna leverera och hämta den information de behöver i sin roll. Uppdragsgivaren ska därför bestämma vilken delad datamiljö (CDE) som ska användas i uppdraget.

Alternativt innehåller anbudsunderlaget endast kraven, och det överlåts åt den huvudansvariga uppdragstagaren att välja CDE. Idealt finns den delade datamiljön på plats under anbudsskedet, och används redan då i kommunikationen mellan parterna.

Kraven på en CDE beskrivs detaljerat i standarden. Det handlar till exempel om hur filnamn ska konstrueras; att filer får metadata som beskriver status, version och klass; datum för versioner; kontrollerad åtkomst.

#### 1.8 Informationsföreskrifter

*Ansvarig: uppdragsgivaren*  
*Förs in i BIM-föreskrifter, avsnitt 6*

Den information som hanteras i ett projekt har ett värde för alla parter, vilket i hög grad påverkar deras affärsmässiga relationer. För att minska risken för tvister ska uppdragsgivaren ta fram informationsföreskrifter som ska införlivas i alla beställningar.

Informationsföreskrifterna ska vara en kontraktshandling i avtalet mellan alla parter som ingår i uppdraget: mellan uppdragsgivare och konsult, underkonsult, entreprenör, underentreprenör eller leverantör.

Informationsföreskrifterna ska innehålla bland annat ansvar, rättigheter och skyldigheter kring hur parterna hanterar information; krav kopplade till GDPR; immateriella rättigheter; vidareutnyttjande av information efter avslutat uppdrag, eller om uppdraget hävs eller avbeställs

Läs mer i standarden, och i malldokumentet *BIM-föreskrifter för projekt*.

<table>
    <tbody>
        <tr>
            <td><b>&#9432;</b></td>
            <td>
                <p>Ansvaret för lämnade uppgifter regleras normalt genom att ABK 09 eller ABT 06 används i avtalet mellan parterna.</p>
                <p>“Beställarens godkännande befriar inte konsulten från ansvar för uppgifter, undersökningsmaterial eller tekniska lösningar” (ABK 09 kap 2 § 4).</p>
                <p>“För riktigheten av uppgifter, undersökningsmaterial och tekniska lösningar ansvarar den part som tillhandhållit dem” (ABT 06 kap 1 § 6).</p>
                <p>Enligt ABK 09 kap 2 § 1 förväntas dock konsulten under vissa omständigheter kontrollera uppgifter från beställaren.</p>
            </td>
        </tr>
    </tbody>
</table>

## 2. Anbudsförfrågan

**Innehåll:**  
[2.1 Informationsutbyteskrav (EIR)](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_projekt/#21-informationsutbyteskrav-eir)  
[2.2 Nivå av informationsbehov](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_projekt/#22-nivå-av-informationsbehov)  
[2.3 Acceptanskriterier](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_projekt/#23-acceptanskriterier)  
[2.4 Referensinformation och delade resurser](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_projekt/#24-referensinformation-och-delade-resurser)  
[2.5 Tidpunkter för informationsleveranser](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_projekt/#25-tidpunkter-för-informationsleveranser)  
[2.6 Krav på anbudssvar och kriterier för utvärdering](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_projekt/#26-krav-på-anbudssvar-och-kriterier-för-utvärdering)  
[2.7 Sammanställ anbudsförfrågan](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_projekt/#27-sammanställ-anbudsförfrågan)  

När behovsbedömningen är klar är det dags för uppdragsgivaren att leta efter hjälp att genomföra det planerade projektet. Den anbudsförfrågan som beskrivs i detta avsnitt är en del av det totala förfrågningsunderlaget för ett sådant. Förfrågningsunderlaget innehåller vanligen dels administrativa föreskrifter, dels någon typ av beskrivning av krav på funktioner och/eller tekniska lösningar. Andra dokument kan också ingå i förfrågningsunderlaget.

För att säkerställa att samtliga uppdrag inom projektet bedrivs enligt ISO 19650 ska också kraven på informationshantering beskrivas. Detta görs genom att teckna ner de fastställda direktiven och förutsättningarna som fastställts i det första steget. Sammanfattningsvis handlar det om:

-   vilken information som ska levereras och in vilken form
-   hur detaljerad informationen ska vara
-   acceptanskriterier för informationskraven
-   vilken stödjande dokumentation som finns tillgänglig
-   tidpunkter för informationsleverans
-   hur anbuden ska utformas och hur de kommer att utvärderas.

####  2.1 Informationsutbyteskrav (EIR)

*Ansvarig: uppdragsgivaren*  
*Förs in i BIM-föreskrifter, avsnitt 2.4*

Det är viktigt att alla uppdragstagare får en tydlig bild av vad som förväntas av dem vad gäller leverans av information. Det handlar då om att på ett begripligt sätt sammanfatta organisationens informationskrav (OIR) och tillgångsinformationskraven (AIR) till övergripande projektinformationskrav (PIR). Dessa ska sedan brytas ner till ett antal informationsutbyteskrav (EIR): en för varje leverantör av information.

Läs mer om kravställning på sidan *Kravkartan*. EIR beskrivs i mer detalj på sidan *Att skapa EIR*.

#### 2.2 Nivå av informationsbehov

*Ansvarig: uppdragsgivaren*  
*Förs in i BIM-föreskrifter, avsnitt 2.4*

Det är viktigt att den information som ska levereras är ”lagom” detaljerad: tillräckligt, men inte för mycket. Kraven ska anpassas till bland annat entreprenadform, skede och typ av byggdel.

Riktlinjer för detta finns i SS-EN 17412-1:2020 *Byggnadsinformationsmodellering – Nivåer för informationsbehov – Begrepp och principer.* Svenska anvisningar till leveransspecifikationer enligt standarden finns på Nationella riktlinjer.

Se även *BEAst Modelleringskrav – Tidiga skeden 1.1* för vägledning för detaljering av byggdelar.

#### 2.3 Acceptanskriterier

*Ansvarig: uppdragsgivaren*  
*Förs in i BIM-föreskrifter, avsnitt 2.4*

En av kärnorna i ISO 19650 är att uppdragsgivare ska granska och godkänna alla leveranser som görs in i projektinformationsmodellen. Det handlar då inte primärt om de tekniska lösningar som beskrivs i CAD-filer och andra typer av dokument, utan att leveransen följer uppdragets informationsstandard, metoder och rutiner. Är informationen lagom detaljerad? Används rätt klassifikation? Är filer korrekt refererade till varandra?

#### 2.4 Referensinformation och delade resurser

*Ansvarig: uppdragsgivaren*  
*Förs in i BIM-föreskrifter, avsnitt 4.1*

Tillgängliggör den referensinformation och de stödjande resurser som fastställdes i punkt 1.6. Detta görs idealt i det kommande uppdragets delade datamiljö.

#### 2.5 Tidpunkter för informationsleveranser

*Ansvarig: uppdragsgivaren*  
*Förs in i BIM-föreskrifter, avsnitt 2.4*

Ta fram en tidplan som visar när informationsleveranser ska göras i förhållande till projektets milstolpar och kritiska beslutspunkter. Här är det viktigt att ta höjd för uppdragsgivarens behov av tid för kvalitetssäkring, granskning och godkännande av informationen som ska levereras.

#### 2.6 Krav på anbudssvar och kriterier för utvärdering

*Ansvarig: uppdragsgivaren*  
*Förs in i BIM-föreskrifter, avsnitt 7 och 8. Kraven och kriterierna flyttas sedan till Administrativa Föreskrifter för uppdraget.*

Krav på anbudssvar och utvärderingskriterier beskrivs normalt i Administrativa Föreskrifter baserade på AMA AF. Vid tillämpning av ISO 19650 kan kriterier för informationsleveranser tillkomma utöver övriga utvärderingskriterier. Kriterierna kan till exempel vara kompetens inom informationshantering eller anbudsversion av genomförandeplan för BIM.

Kriterier för utvärdering kan vara till exempel leveransteamets anbudsversion av genomförandeplan för BIM; kompetensen hos erbjuden personal; uppgiven kapacitet och förmåga; föreslagen planen för projektstart; riskbedömningen.

#### 2.7 Sammanställ anbudsförfrågan

*Ansvarig: uppdragsgivaren*

Avslutningsvis sammanställs de uppgifter som ingår i anbudsförfrågan i ett dokument benämnt **BIM-föreskrifter**. Dessa omfattar sammanfattningsvis:

-   namn på uppdragsgivarens informationsansvarig
-   uppdragsgivarens informationsutbyteskrav
-   nivå av informationsbehov
-   relevant referensinformation och delade resurser
-   beskrivning av delad datamiljö
-   krav på anbudssvar och utvärderingskriterier
-   uppdragets milstolpar för informationsleverans
-   uppdragets informationsstandard
-   uppdragets metoder och rutiner för informationsproduktion.

Skede 2 avslutas i och med att anbudsförfrågan skickas ut eller görs tillgänglig.

## 3. Ta fram anbud

**Innehåll:**  
[3.1 Informationsansvariga](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_projekt/#31-informationsansvariga)  
[3.2 Anbudsversion av genomförandeplan för BIM (BEP)](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_projekt/#32-anbudsversion-av-genomförandeplan-för-bim-bep)  
[3.3 Uppgiftsteamens förmåga och kapacitet](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_projekt/#33-uppgiftsteamens-förmåga-och-kapacitet)  
[3.4 Leveransteamets förmåga och kapacitet](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_projekt/#34-leveransteamets-förmåga-och-kapacitet)  
[3.5 Plan för projektstart ](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_projekt/#35-plan-för-projektstart)  
[3.6 Riskanalys](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_projekt/#36-riskanalys)  
[3.7 Sammanställ anbudet](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_projekt/#37-sammanställ-anbudet)  

Så blir det dags för alla potentiella huvudansvariga uppdragstagare att svara på anbudsförfrågan.

#### 3.1 Informationsansvariga

*Ansvarig: potentiell huvudansvarig uppdragstagare*

Den potentiella huvudansvariga uppdragstagaren utser vem eller vilka som har kompentens och erfarenhet att ansvara för samordningen gentemot uppdragsgivaren och övriga uppdragstagare avseende informationshantering. Har man inte kompetensen i den egna organisationen kan arbetsuppgiften delegeras till en annan uppdragstagare.

Övriga uppdragstagare utser dessutom egna interna ansvariga för informationshantering.

Observera att det kan ingå i uppdraget att den huvudansvariga uppdragstagaren ska ansvara helt eller delvis för arbetet med informationshantering för uppdragsgivarens räkning. Detta uppdrag kan vara skilt från allt konkret projekteringsarbete, och alltså enbart handla om att leda och samordna informationsarbetet.

#### 3.2 Anbudsversion av genomförandeplan för BIM (BEP)

*Ansvarig: potentiell huvudansvarig uppdragstagare*

Den viktigaste delen i anbudet är att den potentiella huvudansvariga uppdragstagaren tar fram en anbudsversion av en **genomförandeplan för BIM**. I det engelska originalet kallas detta *BIM Execution Plan*, med akronymen BEP.

Detta blir ett direkt svar på BIM-föreskrifterna. Genomförandeplanen ska beskriva leveransteamets organisation och medlemmar, och hur de ska arbeta ihop genom att ta fram en ansvarsmatris (”gränsdragningslista”). Här ska också finnas eventuella förslag på justeringar av BIM-föreskrifterna, och förslag på till exempel programvaror, maskinvara och IT-infrastruktur, såvida inte detta föreskrivits av uppdragsgivaren.

I Sverige tas genomförandeplaner för BIM i regel fram med delar av eller helt enligt den struktur som beskrevs från Bygghandlingar 90 Del 8. Denna är nu ersatt med Nationella Riktlinjer.

#### 3.3 Uppgiftsteamens förmåga och kapacitet

*Ansvarig: potentiell huvudansvarig uppdragstagare*

Alla som är tänkta att medverka i projektet måste ha kvalifikationer för att uppfylla alla ställda krav, och de måste vara tillgängliga. De kan också behöva datorstöd, inklusive support, service och kanske utbildning, som måste säkerställas.

#### 3.4 Leveransteamets förmåga och kapcitet

*Ansvarig: potentiell huvudansvarig uppdragstagare*

Den huvudansvariga uppdragstagaren stämmer av och dokumenterar att alla uppgiftsteam kan leverera enligt anbudsförfrågan.

#### 3.5 Plan för projektstart

*Ansvarig: potentiell huvudansvarig uppdragstagare*

Så kommer ännu en stor och viktig arbetsuppgift för den potentielle huvudansvarig uppdragstagaren: att ta fram förslag till en plan för projektstart. Planens syfte är att när projektet startar testa allt som ingår i genomförandeplan för BIM och att säkerställa att alla medverkande kan utföra sina arbetsuppgifter. Här ingår även kompetensutveckling och introduktion för nya medarbetare.

Här rekommenderas att planen för projektstart inte blir ett separat dokument, utan att innehållet ingår i genomförandeplanen för BIM. 

#### 3.6 Riskanalys

*Ansvarig: potentiell huvudansvarig uppdragstagare*

Om uppdragsgivaren ställer höga krav – speciellt om de är ovana för leveransteamet – finns det risk att dessa kanske inte går att uppfylla till hundra procent. En del i anbudet ska därför vara att beskriva risker vad gäller informationsleveranserna, och hur dessa ska hanteras. Riskanalysen kan slås ihop med andra riskanalyser som görs för uppdraget, som har att göra med tider, kostnader och andra parametrar.

#### 3.7 Sammanställ anbudet

*Ansvarig: potentiell huvudansvarig uppdragstagare*

Avslutningsvis sammanställer den potentiella huvudansvariga uppdragstagaren sitt anbud. Här ingår alltså:

-   anbudsversionen av genomförandeplan för BIM
-   beskrivning av leveransteamets förmåga och kapacitet
-   plan för projektstart
-   riskanalys för leveranser av information.

Detta blir en del av det totala anbudet, som förstås också svarar på kraven om vad projektet konkret handlar om: att förvalta byggd tillgång av något slag. Skede 3 avslutas i och med att anbuden når uppdragsgivaren som gör sin utvärdering, och antar ett av anbuden. (Utvärderingen, accepten och medföljande beställning nämns dock inte i standarden.)

## 4. Uppdragsbeskrivning

**Innehåll:**  
[4.1 Uppdaterad genomförandeplan för BIM (BEP)](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_projekt/#41-uppdaterad-genomförandeplan-för-bim-bep)  
[4.2 Detaljerad ansvarsmatris](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_projekt/#42-detaljerad-ansvarsmatris)  
[4.3 Uppdragstagarnas informationsleveranskrav (EIR)](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_projekt/#43-uppdragstagarnas-informationsleveranskrav-eir)  
[4.4 Delplan(er) för informationsleveranser (TIDP)](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_projekt/#44-delplaner-för-informationsleveranser-tidp)  
[4.5 Huvudplan för informationsleveranser (MIDP)](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_projekt/#45-huvudplan-för-informationsleveranser-midp)  
[4.6 Huvudansvariga uppdragstagarens uppdragsbeskrivning](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_projekt/#46-huvudansvariga-uppdragstagarens-uppdragsbeskrivning)  
[4.7 Uppdragstagares uppdragsbeskrivning](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_projekt/#47-uppdragstagares-uppdragsbeskrivning)

Nu är en huvudansvarig uppdragstagare utsedd, och kanske ett antal andra uppdragstagare. Innan projektet drar i gång på allvar behöver beskrivningen av hur informationshanteringen ska gå till ses över.

Skede 4 i Bild 1 beskrivs i det engelska originalet som *appointment*. Den svenska översättningen blev *beställning.* Rubriken på avsnittet som beskriver skedet (5.4) blev *Informationshanteringsprocess – beställning*. Alla aktiviteter som beskrivs handlar dock om sådant den huvudansvariga uppdragstagaren ska göra.

En mer korrekt översättning skulle alltså vara *uppdragsbeskrivning*. Antagandet är att beställningen redan är gjord i och med att uppdragsgivaren antagit anbudet. Nu är det dags att bekräfta och ytterligare beskriva vad som ska göras avseende informationshanteringen i projektet. Det som skrevs i anbudet behöver konkretiseras, förtydligas, kompletteras, kanske i viss mån revideras. Det kan ha kommit kompletterande förfrågningsunderlag som behöver tas hänsyn till. Personer som utlovats i anbudet kanske har slutat. Nya programvaror kanske ger nya möjligheter.

Allt sådant behöver gås igenom, så att uppdragsbeskrivningen ger en stabil och trovärdig grund för den stundande projektstarten.

#### 4.1 Uppdaterad genomförandeplan för BIM (BEP)

*Ansvarig: huvudansvarig uppdragstagare*

Den huvudansvariga uppdragstagaren bekräftar att förslaget till genomförandeplan för BIM uppfyller uppdragsgivarens krav, inklusive sådant som eventuellt tillkommit under upphandlingen. Bekräftelsen görs i överenskommelse med övriga uppdragstagare i leveransteamet. Alla faktauppgifter om medarbetare, ansvar, metoder, rutiner med mera ses över och uppdateras efter behov.

#### 4.2 Detaljerad ansvarsmatris

*Ansvarig: huvudansvarig uppdragstagare*

Den huvudansvariga uppdragstagaren gör den övergripande ansvarsmatrisen för informationsleveranser (”gränsdragningslistan”) mer detaljerad, så att den innehåller uppgifter om vilken typ av information som ska produceras, tidplan för informationsutbyten och mellan vilka parter, samt vilket uppgiftsteam som ansvarar för att producera informationen.

#### 4.3 Uppdragstagarnas informationsleveranskrav (EIR)

*Ansvarig: huvudansvarig uppdragstagare*

Den huvudansvariga uppdragstagaren ska sammanställa uppdragsgivarens och eventuellt egna krav på informationsleveranser från och mellan alla uppdragstagare. Kraven inkluderar typ av information, detaljeringsgrad, kvalitetssäkring och tidpunkt för leverans.

Detta görs genom att upprätta leveransspecifikationer. Mallar för leveransspecifikation finns i Nationella Riktlinjer. Se också *Att upprätta EIR*.

#### 4.4 Delplan(er) för informationsleveranser (TIDP)

*Ansvarig: uppdragstagare*

Varje uppgiftsteam ska upprätta en detaljerad plan för informationsleveranser, baserad på krav från uppdragsgivaren och den huvudansvariga uppdragstagaren. Varje informationscontainer beskrivs där i detalj, inklusive exempelvis informationsnivå, produktionstid (inklusive kvalitetssäkring) och ansvarig part.

#### 4.5 Huvudplan för informationsleveranser (MIDP)

*Ansvarig: huvudansvarig uppdragstagare*

Baserat på delplanerna ska den huvudansvariga uppdragstagaren sammanställa en huvudplan för informationsleveranser. Utöver uppgiftsteamens samordning och eventuella risker avseende leveranser, ska uppdragsgivarens behov av tid för att granska och godkänna informationen beaktas.

#### 4.6 Huvudansvariga uppdragstagarens uppdragsbeskrivning

*Ansvarig: uppdragsgivare*

När uppdragsbeskrivningen är sammanställd ska uppdragsgivaren kontrollera att ställda krav på informationsleveranser från den huvudansvariga uppdragstagaren uppfylls av uppdragets informationsstandard, BIM-föreskrifter, genomförandeplan för BIM och huvudplan för informationsleveranser (MIDP). 

#### 4.7 Uppdragstagares uppdragsbeskrivning

*Ansvarig: huvudansvarig uppdragstagare*

Den huvudansvariga uppdragstagaren ska kontrollera att ställda krav på informationsleveranser från varje uppdragstagare uppfylls av uppdragets informationsstandard, informationsprotokoll, genomförandeplan för BIM och delplan för informationsleveranser (TIDP).

I och med den uppdaterade uppdragsbeskrivningen kan upphandlingen sägas vara avslutad. Nästa skede blir att faktiskt dra i gång projektet. Läs mer om det i avsnittet om Leveransskedet.
