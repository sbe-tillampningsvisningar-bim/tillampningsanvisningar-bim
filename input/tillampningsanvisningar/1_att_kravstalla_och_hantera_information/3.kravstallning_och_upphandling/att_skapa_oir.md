
# Att skapa organisationens informationskrav (OIR)

Byggnadsinformationsmodellering enligt ISO 19650 bygger på en enkel princip: *All tillgångs- och projektinformation som ska tillhandahållas under tillgångens livscykel ska specificeras av uppdragsgivaren genom uppsättningar av informationskrav* (SS-EN ISO 196501:2019, 6.3.1).

Att genomföra detta i praktiken är dock inte lika enkelt. Traditionellt har kraven huvudsakligen handlat om det som fysiskt ska byggas, med mycket liten betoning på vilken information som ska följa med in i användning och förvaltning.

På denna sida sammanfattas en metod för att skapa informationskrav för en organisation (OIR). En längre beskrivning finns i dokumentet "Mer om OIR". Metoden består av fyra steg:

1.  Identifiera organisationens mål.
2.  Identifiera kritiska framgångsfaktorer för att nå målen.
3.  Formulera klartextfrågor som konkretiserar framgångsfaktorerna.
4.  Ställ krav på vilken information som behövs för att svara på klartextfrågorna .

![Line of Sight figure 3](../3.kravstallning_och_upphandling/media/los_3.png)

*Bild 1: Utveckling av organisationens informationskrav som är i linje med organisationens mål. Källa: Line of Sight, Figure 3.*

I beskrivningen nedan används ett fiktivt allmännyttigt bostadsbolag som exempel.

## Steg 1: Identifiera organisationens mål


Enligt SS-EN ISO 19650-1 ska OIR ställa krav på vilken information som behövs för att svara på eller informera om strategiska mål på överordnad nivå hos uppdragsgivaren. Syftet kan vara att stödja till exempel:

-   strategisk verksamhet
-   strategisk tillgångsförvaltning
-   portföljplanering
-   regleringsskyldigheter
-   beslutsfattande.

Målen är givetvis olika för olika typer av organisationer, men oavsett typ kan de inordnas i sex kategorier (utan rangordning):

-   finansiella
-   miljö och klimat
-   verksamhet och drift
-   kunder
-   hälsa och säkerhet
-   anseende.

#### Exempel på organisationens mål

1.  Minska energianvändningen i samtliga byggnader i beståndet.
2.  Öka kundnöjdheten.

## Steg 2: Identifiera kritiska framgångsfaktorer


**Kritiska framgångsfaktorer** (KFF) är de omständigheter eller resultat som krävs för att uppnå organisationens mål. Som en riktlinje kan en uppsättning av fyra till sex KFF vara lämpliga för varje mål. Dessa kan handla om:

-   minskade driftskostnader
-   mindre avhjälpande och mer förebyggande underhåll
-   öka andelen förnybar energi
-   snabbare svar på kundfrågor.

Vilken information krävs sedan för att visa hur det går med framgångsfaktorerna? Det kan handla om att:

-   mäta om organisationens mål uppnås
-   möta intressenternas behov (anställda, slutanvändare, ägare)
-   uppfylla krav från regelverk (kontroller, besiktningar, inspektioner)
-   utveckla policyer (inklusive kvalitetskontroll)
-   stödja affärsledningen (rapportering, revisioner, upphandlingar, analyser).

#### Exempel på kritiska framgångsfaktorer enligt tidigare mål

1.  
    1.  Genomföra åtgärder som minskar energianvändningen i befintliga byggnader.
    2.  Höga krav på energianvändning i nya projekt.
2.  
    1.  Hög kvalitet på inköpta maskiner som kunder använder.
    2.  Tydlig information till användare.

## Steg 3: Identifiera frågor i klarspråk


**Klarspråksfrågor** (KSF) används som ett verktyg för att utveckla frågor som belyser de kritiska framgångsfaktorerna. Fyra till sex frågor bör tas fram för varje KFF.

#### Exempel på frågor i klarspråk enligt tidigare framgångsfaktorer

1.  
    1.  Vilken energianvändning per uppvärmd area har varje byggnad?
    2.  Vilka krav på energianvändning kan vi ställa på nya projekt som ger högsta nytta under byggnadens hela liv? Lönar det sig att isolera mer? Hur påverkas ytan som går att hyra ut och därmed bidra till intäkterna?
2.  
    1.  Vilka fabrikat och modeller av maskiner har vi mest problem med?
    2.  Är användarna nöjda med informationen? Finns de på alla relevanta språk? Kan vi informera på andra sätt?

## Steg 4: Besvara klarspråksfrågor

Svaren på klarspråksfrågorna leder på det här sättet fram till organisationens informationskrav. Det viktiga i detta steg är att svaret ska specificera vilken information som krävs för att kunna följa upp framgångsfaktorerna och därmed även organisationens övergripande mål.

#### Exempel på information som behövs för att svara på tidigare klarspråksfrågor

1.  
    1.  Hur stor uppvärmd yta har varje byggnad? Hur mycket energi använder byggnaden?
    2.  Kalkyler för beräknad kostnad-nyttoanalys för olika typer av tekniska lösningar.
2.  
    1.  Felrapporter uppdelade per fabrikat och modell.
    2.  Användarenkäter. Intervjuer med fastighetsskötare. Platsbesök och spontana samtal med användare.

