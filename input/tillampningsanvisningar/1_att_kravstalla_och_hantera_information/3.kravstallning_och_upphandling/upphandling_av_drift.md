# Upphandling av drift och underhåll

## Inledning

Den här sammanfattningen av upphandling av bygg- och anläggningsprojekt bygger på beskrivningen i Del 3 av ISO 19650. En viktig inledande kommentar: vi gör här en viss nyansering av den svenska översättningen av standarden. I Del 1 av standarden finns följande översättning av det engelska begreppet *appointment*:

<div style="border: 1px solid black; padding: 10px;">

**beställning**  
överenskommen beskrivning av tillhandahållande av information beträffande arbeten, varor eller tjänster<br><em>Anmärkning till termpost 1: Denna term används oavsett om det finns någon formell beställning eller inte mellan parterna.</em> 
</div>
<br>

Den engelska termen är dock dubbeltydig, och kan också avse det uppdrag som baseras på beställningen. Det återspeglas i den svenska översättningen av aktörerna: **uppdragsgivare** (*appointing party*), **huvudansvarig uppdragstagare** (*lead appointed party*) och **uppdragstagare** (*appointed party*). Här använder vi därför primärt termen **uppdrag**.

Det handlar alltså om alltså upphandling av informationshanteringen i uppdrag i form av drift- och underhållsåtgärder. Upphandling av uppdrag som avser projekt beskrivs i dokumentet *Upphandling av projekt .*

Anvisningarna i dokumentet förutsätter att kraven på vilken information som ska levereras redan är fastställda i form av organisationens informationskrav (OIR), tillgångsinformationskrav (AIR) och informationsutbyteskrav (EIR).

Figur 4 i Del 3 bryter ner uppdrag i åtta skeden. Detta dokument beskriver upphandlingsprocessen, som omfattar skede 1–4:

![Processen i del 3](../3.kravstallning_och_upphandling/media/process_del3.png)

*Bild 1: Informationshanteringsprocess för att stödja förvaltning av tillgångar. Källa: SS-EN ISO 19650-3:2020, Figure 4.*

![legend](../3.kravstallning_och_upphandling/media/legend.png)

I figuren visar siffrorna delprocesser, medan bokstäverna betecknar grupper av delprocesser. Den vänstra cirkeln visar tillgångsinformationsmodellen innan det nya uppdraget, och den feta högra cirkeln visar hur den fyllts på med information från drift och underhåll.

1.  Det första skedet är omfattande. Här fastställs alla krav: *organisationens informationskrav* (OIR), *tillgångsförvaltningens informationskrav* (AIR), *projektets informationskrav* (PIR) och *informationsutbyteskrav* (EIR). Här ingår också att utse egen informationsansvarig för projektet, och att beskriva förutsättningar och krav för det digitala samarbetet.
2.  Skede 2 handlar om att ta fram anbudsförfrågan. Här ingår en lång rad av aktiviteter som beskrivs nedan.
3.  I skede 3 tar potentiella huvudansvarig uppdragstagare fram sina anbud.
4.  Skede 4 drar i gång när en huvudansvarig uppdragstagare – eller flera om det är en delad upphandling – har blivit antagna. De måste då bekräfta att de faktiskt kan utföra allt de påstått i anbudet.

Figuren innehåller två så kallade ”utlösande händelser”. Definitionen i SS-EN ISO 19650-1:2019 lyder:

<div style="border: 1px solid black; padding: 10px;">

**utlösande händelse**  
planerad eller oplanerad händelse som förändrar en tillgång eller dess status under dess livscykel, och som resulterar i informationsutbyte.<br>  
<em>Anmärkning till termpost 1: Under leveransskedet återspeglar utlösande händelser normalt sluten på projektskeden.</em> 
</div>
<br>

Alltså: i projekt är utlösande händelser till exempel när systemhandlingsskedet övergår till bygghandlingsskedet. Under tillgångsförvaltningen handlar det i stället om exempelvis:

-   något som utförs under pågående uppdrag, till exempel att filter byts i ett aggregat och att detta dokumenteras, eller
-   planerat utbyte av installationer (efter beslut P) som handlas upp som ett eget projekt.

En sammanfattning av de tre ”spåren” B, C och D i Bild 2 kan se ut så här:

-   B: Ordinarie drift och underhåll, där ett antal utlösande händelser dokumenteras.
-   C: Särskilt upphandlat projekt under användningsskedet, som dokumenteras i sin helhet. (Detta är identiskt med projektprocessen i leveransskedet.)
-   D: Förvärv av en tillgång, som också dokumenteras i sin helhet.

Alla spåren mynnar ut i steg 8, då information förs in i tillgångsinformationsmodellen: från drift och underhåll, från projektet respektive om förvärvet.

Om man väljer spår B eller C beror huvudsakligen på två faktorer:

-   vilket behov som ska tillfredsställas
-   om det ligger inom ramen för redan upphandlad drift och underhåll.

Om svaret på fråga två – som benämns H i Bild 2 – är ja, då väljer man spår B. Om svaret är nej, följer man i stället spår C. Som exempel är rimligen byte av filter i ett ventilationsaggregat en ordinarie åtgärd, medan att byta va-stammar i ett bostadshus är en åtgärd som behöver handlas upp som ett projekt.

Nedan följer en sammanfattande beskrivning av innehållet i Del 3. 

## 1. Uppdragsgivarens bedömning av behov
**Innehåll:**<br>
[1.1 Informationsansvariga](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_drift_och_underhall/#11-informationsansvariga)<br>
[1.2 Organisationens informationskrav (OIR)](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_drift_och_underhall/#12-fastställ-organisationens-informationskrav)<br>
[1.3 Tillgångar som ska förvaltas](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_drift_och_underhall/#13-tillgångar-som-ska-förvaltas)<br>
[1.4 Tillgångsinformationskrav (AIR)](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_drift_och_underhall/#14-tillgångsinformationskrav-air)<br>
[1.5 Förutsägbara utlösande händelser](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_drift_och_underhall/#15-förutsägbara-utlösande-händelser)<br>
[1.6 Tillgångsförvaltningens informationsstandard](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_drift_och_underhall/#16-tillgångsförvaltningens-informationsstandard)<br>
[1.7 Metoder och rutiner](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_drift_och_underhall/#17-metoder-och-rutiner)<br>
[1.8 Referensinformation och delade resurser](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_drift_och_underhall/#18-referensinformation-och-delade-resurser)<br>
[1.9 Delad datamiljö (CDE)](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_drift_och_underhall/#19-delad-datamiljö-cde)<br>
[1.10 Koppling till förvaltningssystem](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_drift_och_underhall/#110-koppling-till-förvaltningssystem)<br>
[1.11 Tillgångsinformationsmodell (AIM)](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_drift_och_underhall/#111-tillgångsinformationsmodell-aim)<br>
[1.12 Upprätthållande av AIM](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_drift_och_underhall/#112-upprätthållande-av-aim)<br>
[1.13 Tillgångsförvaltningens informationsföreskrifter](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_drift_och_underhall/#113-tillgångsförvaltningens-informationsföreskrifter)<br>

Det första steget handlar om att uppdragsgivaren ska fastställa direktiv och beskriva förutsättningar för det kommande uppdraget. Allt detta ska ingå i anbudsförfrågan i ett dokument benämnt **BIM-föreskrifter**. Syftet att säkerställa att informationshanteringen blir så effektiv som möjligt. Anbudsgivare ska svara på kraven genom att bland annat formulera en anbudsversion av en **genomförandeplan för BIM** (*BIM Execution Plan, BEP*).

#### 1.1 Informationsansvariga

*Ansvarig: uppdragsgivaren*  
*Förs in i BIM-föreskrifterna, avsnitt 1.1*

Uppdragsgivaren ska utse en eller flera personer inom sin organisation som ska ansvara för informationshanteringen i uppdraget. Detta är en viktig roll, som ytterst syftar till att försäkra sig om att den information som levereras uppfyller de krav som organisationen behöver, och som används i drift och underhåll. För att hitta rätt person för uppdraget, behöver man analysera vilka arbetsuppgifter som ska utföras, vilka befogenheter som krävs, och vilken kompetens som personen eller personerna behöver.

Uppdraget som informationsansvarig kan ges till tredje part, inklusive den presumtive uppdragstagaren.

#### 1.2 Organisationens informationskrav (OIR)

*Ansvarig: uppdragsgivaren*  
*Förs in i BIM-föreskrifter, avsnitt 2.1.*

Uppdragsgivaren ska redovisa befintliga informationskrav som organisationen har för sin ledning och för sitt system för tillgångsförvaltning.

*Exempel på aktiviteter som ställer krav på information finns i SS-EN ISO 19650-3:2020, Bilaga A.2.*

*För mer information, se sidorna Att skapa organisationens informationskrav (OIR) och Mer om organisationens informationskrav (OIR).*

#### 1.3 Tillgångar som ska förvaltas

*Ansvarig: uppdragsgivaren*  
*Förs in i BIM-föreskrifter, avsnitt 2.3.2.*

Uppdragsgivaren ska identifiera de tillgångar som ska förvaltas, inklusive sådana som är planerade att bygga eller köpa. Beakta då, ur ett kostnads- och nyttoperspektiv:

-   äganderätten till tillgångarna och den information som är kopplad till dem
-   eventuell struktur för uppdelning av tillgångar
-   varje tillgångs inverkan på affärsverksamhetens
-   hur kritisk varje tillgång är för viktiga affärsverksamheter
-   varje tillgångs skick.

#### 1.4 Tillgångsinformationskrav (AIR)

*Ansvarig: uppdragsgivaren*   
*Förs in i BIM-föreskrifter, avsnitt 2.2.1*

För varje identifierad tillgång ska uppdragsgivaren fastställa de tillgångsinformationskrav som behövs för att möta organisationens informationskrav. Behov från både interna och externa intressenter ska beaktas. Lagring av information ska sedan göras i en tillgångsinformationsmodell (AIM). Beakta då:

-   informationens syfte i tillgångsförvaltningen
-   vem som äger tillgångarna och vilken information som finns om dem
-   information som behövs om både grupper av och enskilda tillgångar
-   hur eventuellt system för tillgångsförvaltning enligt ISO 5500 påverkar och påverkas.

*Exempel på information som kan behöva lagras i en AIM finns i SS-EN ISO 19650-3:2020, Bilaga A.4.*

*För mer information, se sidan Att skapa tillgångsinformationskrav (AIR)*

#### 1.5 Förutsägbara utlösande händelser

*Ansvarig: uppdragsgivaren*   
*Förs in i BIM-föreskrifter, avsnitt 2.2.2*

Uppdragsgivaren ska identifiera och dokumentera förutsägbara utlösande händelser under driften när ny eller uppdaterad information uppstår eller krävs. Exempel:

-   mätningar och utvärderingar
-   undersökningar, besiktningar
-   planerat underhåll
-   nya föreskrifter
-   nytt ägande.

Fler exempel på nyckelhändelser finns i SS-EN ISO 19650-3:2020, Bilaga A.3.

#### 1.6 Tillgångsförvaltningens informationsstandard

*Ansvarig: uppdragsgivaren*

Det digitala informationsflödet in till och ut från drift och underhåll måste vara användbart för uppdragsgivaren. Detta får inte påverkas av de metoder som uppdragstagare använder under uppdragets gång. Uppdragsgivaren ska därför i anbudsunderlaget fastställa eventuella specifika informationsstandarder som uppdragsgivarens organisation behöver inom ramen för tillgångsförvaltningens informationsstandard. Beakta då:

-   informationsutbytet mellan alla parter
-   metoder för att strukturera och klassificera information
-   metoden för fastställande av informationsbehov
-   hur informationen ska användas under framtida projekt, till exempel om- och tillbyggnad
-   hur informationen ska användas i drift och underhåll.

Informationsstandarden kan avse bland annat filformat, informationsstruktur, klassifikationssystem och användning av metadata.

#### 1.7 Metoder och rutiner

*Ansvarig: uppdragsgivaren*

Det praktiska genomförandet av informationsflödet in till och ut från tillgångsförvaltningen behöver också säkerställas. I det syftet ska uppdragsgivaren fastställa eventuella specifika metoder och rutiner för informationsproduktion som organisationen behöver inom ramen för tillgångsförvaltningens metoder och rutiner för informationsproduktion. Beakta då:

-   hur insamling och leverans av befintlig tillgångsinformation ska gå till
-   generering, granskning eller godkännande av ny information, till exempel krävda eller tillåtna programvaror och arbetsflöde för granskning
-   säkerhet eller distribution av information, till exempel med användning av metadata och en gemensam datamiljö
-   hur leverans av information till uppdragsgivaren ska gå till.

#### 1.8 Referensinformation och delade resurser 

*Ansvarig: uppdragsgivaren*

Uppdragsgivaren ska fastställa den referensinformation och de övriga resurser som de avser att dela med de presumtiva huvudansvariga utförarna under anbudsförfarandet eller uppdraget. Sträva att använda öppna informationsstandarder. Beakta då:

-   befintlig tillgångsinformation från olika källor, till exempel inventeringar och undersökningar, grundkartor, ritningar och CAD-filer från tidigare projekt eller skeden
-   delade resurser i form av mallar och objektbibliotek, och katalogobjekt definierade i nationella och regionala standarder.

#### 1.9 Delad datamiljö (CDE)

*Ansvarig: uppdragsgivaren*

En fungerande teknisk lösning och gemensamma rutiner för delning av information är centrala för att tillgångsförvaltningen ska fungera som avsett. Alla ska kunna leverera och hämta den information de behöver i sin roll. Uppdragsgivaren ska därför fastställa vilken delad datamiljö (CDE) som ska användas.

Alternativt innehåller anbudsunderlaget endast kraven, och det överlåts åt den huvudansvariga uppdragstagaren att välja CDE. Idealt finns den delade datamiljön på plats under anbudsskedet, och används redan då i kommunikationen mellan parterna.

Kraven är att denna ska möjliggöra:

-   att varje informationscontainer får ett unikt ID bestående av avgränsade fält
-   att varje fält får värde från en överenskommen standard
-   att varje informationscontainer märks med status (lämplighet), version och klassificering (i enlighet med det ramverk som definieras i ISO 12006-2)
-   att informationscontainrar kan övergå från ett tillstånd till ett annat
-   att användarnamn och datum registreras vid revidering eller ändring av tillstånd för informationscontainrar
-   att åtkomst kontrolleras på nivån informationscontainer.

För namngivning av informationscontainrar (ritningsfiler, modellfiler och andra dokument), se SIS Bygghandlingar och Nationella Riktlinjer.

#### 1.10 Koppling till förvaltningssystem

*Ansvarig: uppdragsgivaren*

Om existerande förvaltningssystem ska användas för att lagra tillgångsinformation ska uppdragsgivaren skapa eller anpassa processer för kvalitetsgranskning och säkerhet, så att de uppfyller kraven i ISO 19650-3.

#### 1.11 Tillgångsinformationsmodell (AIM) 

*Ansvarig: uppdragsgivaren*

Uppdragsgivaren ska se över och vid behov anpassa sin tillgångsinformationsmodell (AIM) så att den kan lagra all information som krävs enligt AIR. AIM ska vara en federerad informationsmodell som kan inkludera information från flera uppdragstagare. Beakta då:

-   lämpligheten i befintliga informationssystem
-   möjligheten att integrera eller länka information från en projektinformationsmodell under pågående projekt
-   legala krav gällande informationslagring, speciellt personuppgifter
-   möjligheten i AIM att skilja på information som används för analyser och rapporter från information som hålls uppdaterad av leveransteamen
-   behovet av säkerhetsanpassningar enligt ISO 19650-5.

#### 1.12 Upprätthållande av AIM

*Ansvarig: uppdragsgivaren*

Uppdragsgivaren ska fastställa eller följa befintliga lämpliga processer för att upprätthålla AIM så så länge som är lämpligt. Detta inkluderar processer för:

-   fördela uppgifter för produktion, underhåll, bevarande, överföring, tillgång till, försäkran om och arkivering av information
-   överföra underhållskrav för AIM och/eller äganderätten till AIM när det sker en förändring av tillgångens ägare eller den utnämnande parten
-   fastställa innehåll, innebörd, format och medium för representation, lagring, överföring och hämtning av information
-   upprätta versionskontroll, kontrollera integritet och verifiera information mot AIR och uppdatera AIM när så är lämpligt
-   förbättra informationens kvalitet för att stödja organisationens mål
-   arkivera eller kassera föråldrad, otillförlitlig eller oönskad information
-   arkivera AIM, delvis eller i sin helhet, vid avveckling/livslängdens slut
-   säkerställa att AIM uppfyller de krav på säkerhet som beskrivs i ISO 19650-5
-   lagra information på ett sådant sätt att den är säker, lätt att återfinna och använda förebyggande underhåll för att skydda den från fysisk och teknisk försämring eller föråldring
-   upprätthålla den överenskomna nivån av anpassning mellan innehållet i AIM och den fysiska tillgångens tillstånd.

#### 1.13 Tillgångsförvaltningens informationsföreskrifter 

*Ansvarig: uppdragsgivaren*

Den information som hanteras i tillgångsförvaltningen har ett värde för alla parter, vilket i hög grad påverkar deras affärsmässiga relationer. För att minska risken för tvister kopplade till produktion och användning av information, ska uppdragsgivaren fastställa uppdragets informationsföreskrifter, inklusive eventuella tillhörande licensavtal, som ska införlivas i alla beställningar.

Informationsföreskrifterna är således en kontraktshandling som ska vara med i avtalet mellan alla parter som ingår i uppdraget, oavsett om avtal ingås mellan uppdragsgivare och konsult, underkonsult, entreprenör, underentreprenör eller leverantör. Beakta då:

-   särskilda skyldigheter för uppdragsgivaren, presumtiva huvudansvariga uppdragstagare och presumtiva uppdragstagare avseende hantering eller framställning av information, inklusive användning av uppdragets delade datamiljö
-   krav kopplade till GDPR
-   eventuella garantier eller skyldigheter som är förknippade med projektinformationsmodellen
-   immateriella rättigheter till information, både befintlig och sådan som skapas i uppdraget
-   användning av befintlig tillgångsinformation
-   användning av delade resurser
-   användning av information under uppdraget, inklusive eventuella associerade licensvillkor
-   vidareutnyttjande av information efter avslutat uppdrag, eller om uppdraget hävs eller avbeställs.

## 2. Anbudsförfrågan 
**Innehåll:**<br>
[2.1 Aktiviteter som ska dokumenteras](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_drift_och_underhall/#21-aktiviteter-som-ska-dokumenteras)<br>
[2.2 Uppdragsgivarens informationsutbyteskrav (EIR)](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_drift_och_underhall/#22-uppdragsgivarens-informationsutbyteskrav-eir)<br>
[2.3 Stödjande information](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_drift_och_underhall/#23-stödjande-information)<br>
[2.4 Krav på anbudssvar och kriterier för utvärdering](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_drift_och_underhall/#24-krav-på-anbudssvar-och-kriterier-för-utvärdering)<br>
[2.5 Sammanställ anbudsförfrågan](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_drift_och_underhall/#25-sammanställ-anbudsförfrågan)<br>


När behovsbedömningen är klar är det dags för uppdragsgivaren att leta efter hjälp att genomföra det drift och underhåll av tillgångarna. Den anbudsförfrågan som beskrivs i detta avsnitt är en del av det totala förfrågningsunderlaget för ett sådant. Förfrågningsunderlaget innehåller vanligen dels administrativa föreskrifter, dels någon typ av beskrivning av krav på vilka funktioner som ska driftas och underhållas. Andra dokument kan också ingå i förfrågningsunderlaget.

För att säkerställa att samtliga uppdrag som rör tillgångsförvaltningen bedrivs enligt ISO 19650 ska också kraven på informationshantering beskrivas. Detta görs genom att teckna ner de fastställda direktiven och förutsättningarna som fastställts i det första steget. Sammanfattningsvis handlar det om:

-   vilka aktiviteter som ska dokumenteras och hur det ska gå till
-   acceptanskriterier för informationskraven
-   vilken stödjande dokumentation som finns tillgänglig
-   hur anbuden ska utformas och hur de kommer att utvärderas.

#### 2.1 Aktiviteter som ska dokumenteras 

*Ansvarig: uppdragsgivaren*  
*Förs in i BIM-föreskrifter, avsnitt*

Uppdragsgivaren ska utse en huvudansvarig uppdragstagare för varje utlösande händelse under driftsfasen. Besluten här avgör om informationshanteringen följer spår M, N eller P i Bild 1.

#### 2.2 Uppdragsgivarens informationsutbyteskrav (EIR) 

*Ansvarig: uppdragsgivaren*  
*Förs in i BIM-föreskrifter, avsnitt*

Uppdragsgivaren ska fastställa sina informationsutbyteskrav för varje uppdrag och utlösande händelse så att tilltänkta huvudansvariga uppdragstagare får en tydlig bild av alla informationsleveranser som ska göras.

Beakta då:

-   organisationens informationskrav (OIR)
-   tillgångsinformationskrav (AIR)
-   projektinformationskrav (EIR)
-   krav på säkerhet enligt ISO 19650-5
-   vilken information som behövs för varje utlösande händelse
-   hur ofta informationsleveranser ska göras
-   acceptanskriterier for informationen avseende informationsstandard, hur informationen produceras, och hur delad information får och ska användas.

#### 2.3 Stödjande information

*Ansvarig: uppdragsgivaren*  
*Förs in i BIM-föreskrifter, avsnitt*

Den huvudansvariga uppdragstagaren kan behöva stödjande information för att förstå informationskraven och acceptanskriterierna. Beakta då:

-   befintlig tillgångsinformation
-   information från tidigare uppdrag
-   statusen för informationen 

Den stödjande informationen ska sammanställas och göras tillgänglig för anbudsgivare, till exempel i det kommande uppdragets delade datamiljö.

#### 2.4 Krav på anbudssvar och kriterier för utvärdering

*Ansvarig: uppdragsgivaren*  
*Förs in i BIM-föreskrifter, avsnitt 7 och 8. Kraven och kriterierna flyttas sedan till Administrativa Föreskrifter för uppdraget.*

Krav på anbudssvar och utvärderingskriterier beskrivs normalt i Administrativa Föreskrifter baserade på AMA AF. Vid tillämpning av ISO 19650 kan kriterier för informationsleveranser tillkomma utöver övriga utvärderingskriterier. Kriterierna kan till exempel vara kompetens inom informationshantering eller anbudsversion av genomförandeplan för BIM.

Kriterier för utvärdering kan vara:

-   informationskravens komplexitet
-   leveransteamets genomförandeplan BIM (anbudsversion) och hur den är anpassad till informationsstandarden, metoderna för informationsproduktion, informationsföreskrifterna och EIR
-   kompetensen hos de presumtiva aktörer som utför informationshanteringsfunktionen för leveransteamets räkning
-   den presumtiva huvudansvariga uppdragstagarens bedömning av leveransteamets kapacitet och förmåga
-   leveransteamets föreslagna plan för projektstart
-   leveransteamets bedömning av risk vid informationsleverans.

#### 2.5 Sammanställ anbudsförfrågan

*Ansvarig: uppdragsgivaren*

Avslutningsvis sammanställs de uppgifter som ingår i anbudsförfrågan i ett dokument benämnt **BIM-föreskrifter**.

Dessa föreskrifter omfattar:

-   uppdragsgivarens informationsutbyteskrav (EIR)
-   relevant referensinformation och delade resurser
-   krav på anbudssvar och utvärderingskriterier
-   uppdragets informationsstandard
-   uppdragets metoder och rutiner för informationsproduktion
-   uppdragets informationsföreskrifter.

Skede 2 avslutas i och med att anbudsförfrågan skickas ut eller görs tillgänglig.

## 3. Ta fram anbud 
**Innehåll:**<br>
[3.1 Informationsansvariga](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_drift_och_underhall/#31-informationsansvariga)<br>
[3.2 Anbudsversion av genomförandeplan för BIM](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_drift_och_underhall/#32-anbudsversion-av-genomförandeplan-för-bim)<br>
[3.3 Uppgiftsteamens förmåga och kapacitet](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_drift_och_underhall/#33-uppgiftsteamens-förmåga-och-kapacitet)<br>
[3.4 Leveransteamets förmåga och kapacitet](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_drift_och_underhall/#34-leveransteamets-förmåga-och-kapacitet)<br>
[3.5 Förslag till plan för uppdragsstart](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_drift_och_underhall/#35-förslag-till-plan-för-uppdragsstart)<br>
[3.6 Riskanalys](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_drift_och_underhall/#36-riskanalys)<br>
[3.7 Sammanställ anbudet](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_drift_och_underhall/#37-sammanställ-anbudet)<br>

Så blir det dags för alla potentiella huvudansvariga uppdragstagare att svara på anbudsförfrågan.

#### 3.1 Informationsansvariga

*Ansvarig: potentiell huvudansvarig uppdragstagare*

Den potentiella huvudansvariga uppdragstagaren utser vem eller vilka som har kompentens och erfarenhet att ansvara för samordningen gentemot uppdragsgivaren och övriga uppdragstagare avseende informationshantering. Har man inte kompetensen i den egna organisationen kan arbetsuppgiften delegeras till en annan uppdragstagare.

Övriga uppdragstagare utser dessutom egna interna ansvariga för informationshantering.

Observera att det kan ingå i uppdraget att den huvudansvariga uppdragstagaren ska ansvara helt eller delvis för arbetet med informationshantering för uppdragsgivarens räkning. Detta uppdrag kan vara skilt från allt konkret projekteringsarbete, och alltså enbart handla om att leda och samordna informationsarbetet.

#### 3.2 Anbudsversion av genomförandeplan för BIM 

*Ansvarig: potentiell huvudansvarig uppdragstagare*

Den viktigaste delen i anbudet är att den potentiella huvudansvariga uppdragstagaren tar fram en anbudsversion av en **genomförandeplan för BIM**. I det engelska originalet kallas detta *BIM Execution Plan*, med akronymen BEP.

Detta blir ett direkt svar på BIM-föreskrifterna. Genomförandeplanen ska bland annat beskriva nödvändiga resurser, vilka leveranser som ska göras, vilken information som behöver delas med andra och hur dessa ska engageras i uppdraget vad gäller informationsleveranser.

(Ett förslag på *genomförandeplan för BIM lämplig för förvaltningsuppdrag kommer att publiceras här.)*

#### 3.3 Uppgiftsteamens förmåga och kapacitet

*Ansvarig: potentiell huvudansvarig uppdragstagare*

Alla som är tänkta att medverka i projektet måste ha kvalifikationer för att uppfylla alla ställda krav, och de måste vara tillgängliga. De kan också behöva datorstöd, inklusive support, service oc kanske utbildning, som måste säkerställas.

#### 3.4 Leveransteamets förmåga och kapacitet

*Ansvarig: potentiell huvudansvarig uppdragstagare*

Den huvudansvariga uppdragstagaren stämmer av och dokumenterar att alla uppgiftsteam kan leverera enligt anbudsförfrågan.

#### 3.5 Förslag till plan för uppdragsstart 

*Ansvarig: potentiell huvudansvarig uppdragstagare*

Så kommer ännu en stor och viktig arbetsuppgift för den potentielle huvudansvarig uppdragstagaren: att ta fram en plan för uppdragsstart. Planens syfte är att vid projektstart testa allt som ingår i genomförandeplan för BIM och att säkerställa att alla medverkande kan utföra sina arbetsuppgifter. Här ingår även kompetensutveckling och introduktion för nya medarbetare.

(Ett förslag på *plan för uppdragsstart lämplig för förvaltningsuppdrag kommer att publiceras här.)*

#### 3.6 Riskanalys

*Ansvarig: potentiell huvudansvarig uppdragstagare*

Om uppdragsgivaren ställer höga krav – speciellt om de är ovana för leveransteamet – finns det risk att dessa kanske inte går att uppfylla till hundra procent. En del i anbudet ska därför vara att beskriva risker vad gäller informationsleveranserna, och hur dessa ska hanteras. Riskanalysen kan slås ihop med andra riskanalyser som görs för uppdraget, som har att göra med tider, kostnader och andra parametrar.

#### 3.7 Sammanställ anbudet

Avslutningsvis sammanställer den huvudansvarig uppdragstagaren sitt anbud. Här ingår alltså:

-   anbudsversionen av genomförandeplan för BIM
-   beskrivning av leveransteamets förmåga och kapacitet
-   plan för projektstart
-   riskanalys för leveranser av information.

Detta blir en del av det totala anbudet, som förstås också svarar på kraven om vad projektet konkret handlar om: att skapa en ny byggd tillgång av något slag. Skede 3 avslutas i och med att anbuden når uppdragsgivaren som gör sin utvärdering, och antar ett av anbuden. (Utvärderingen, accepten och medföljande beställning nämns dock inte i standarden. Mer om det i nästa avsnitt.)

## 4. Uppdragsbekräftelse
**Innehåll:**<br>
[4.1 Uppdaterad genomförandeplan för BIM (BEP)](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_drift_och_underhall/#41-uppdaterad-genomförandeplan-för-bim-bep)<br>
[4.2 Detaljerad ansvarsmatris](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_drift_och_underhall/#42-detaljerad-ansvarsmatris)<br>
[4.3 Uppdragstagarnas informationsleveranskrav (EIR)](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_drift_och_underhall/#43-uppdragstagarnas-informationsleveranskrav-eir)<br>
[4.4 Delplan(er) för informationsleveranser (TIDP)](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_drift_och_underhall/#44-delplaner-för-informationsleveranser-tidp)<br>
[4.5 Huvudplan för informationsleveranser (MIDP)](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_drift_och_underhall/#45-huvudplan-för-informationsleveranser-midp)<br>
[4.6 Huvudansvariga uppdragstagarens uppdragsbeskrivning](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_drift_och_underhall/#46-huvudansvariga-uppdragstagarens-uppdragsbeskrivning)<br>
[4.7 Uppdragstagares uppdragsbeskrivning](https://tillampningsanvisningarbim.kravportal.se/upphandling_av_drift_och_underhall/#47-uppdragstagares-uppdragsbeskrivning)<br>

Nu är en huvudansvarig uppdragstagare utsedd, och kanske ett antal andra uppdragstagare. Innan uppdraget drar i gång på allvar behöver beskrivningen av hur informationshanteringen ska gå till ses över.

Skede 4 i Bild 1 beskrivs i det engelska originalet som *appointment*. Den svenska översättningen blev *beställning.* Rubriken på avsnittet som beskriver skedet (5.4) blev *Informationshanteringsprocess – beställning*. Alla aktiviteter som beskrivs handlar dock om sådant den huvudansvariga uppdragstagaren ska göra.

En mer korrekt översättning skulle alltså vara *uppdragsbeskrivning*. Antagandet är att beställningen redan är gjord i och med att uppdragsgivaren antagit anbudet. Nu är det dags att bekräfta och ytterligare beskriva vad som ska göras avseende informationshanteringen i projektet. Det som skrevs i anbudet behöver konkretiseras, förtydligas, kompletteras, kanske i viss mån revideras. Det kan ha kommit kompletterande förfrågningsunderlag som behöver tas hänsyn till. Personer som utlovats i anbudet kanske har slutat. Nya programvaror kanske ger nya möjligheter.

Allt sådant behöver gås igenom, så att uppdragsbeskrivningen ger en stabil och trovärdig grund för den stundande uppdragsstarten.

#### 4.1 Uppdaterad genomförandeplan för BIM (BEP)

Den huvudansvariga uppdragstagaren bekräftar att förslaget till genomförandeplan för BIM uppfyller uppdragsgivarens krav, inklusive sådant som eventuellt tillkommit under upphandlingen. Bekräftelsen görs i överenskommelse med övriga uppdragstagare i leveransteamet. Alla faktauppgifter om medarbetare, ansvar, metoder, rutiner med mera ses över och uppdateras efter behov.

#### 4.2 Detaljerad ansvarsmatris 

*Ansvarig: huvudansvarig uppdragstagare*

Den huvudansvariga uppdragstagaren gör den övergripande ansvarsmatrisen (”gränsdragningslistan”) mer detaljerad, så att den innehåller uppgifter om vilken typ av information som ska produceras, tidsplan för informationsutbyten och mellan vilka parter, samt vilken uppgiftsgrupp som ansvarar för att producera informationen.

#### 4.3 Uppdragstagarnas informationsleveranskrav (EIR)

*Ansvarig: huvudansvarig uppdragstagare*

Den huvudansvariga uppdragstagaren ska sammanställa uppdragsgivarens och eventuellt egna krav på informationsleveranser från och mellan alla uppdragstagare. Kraven inkluderar typ av information, detaljeringsgrad, kvalitetssäkring och tidpunkt för leverans.

Detta görs genom att upprätta leveransspecifikationer. Mallar för leveransspecifikation finns i Nationella Riktlinjer. Se också Att upprätta EIR.

#### 4.4 Delplan(er) för informationsleveranser (TIDP)

*Ansvarig: uppdragstagare*

Varje uppgiftsteam ska upprätta en detaljerad plan för informationsleveranser, baserad på krav från uppdragsgivaren och den huvudansvariga uppdragstagaren. Varje informationscontainer beskrivs där i detalj, inklusive exempelvis informationsnivå, produktionstid (inklusive kvalitetssäkring) och ansvarig part.


#### 4.5 Huvudplan för informationsleveranser (MIDP)

*Ansvarig: huvudansvarig uppdragstagare*

Baserat på delplanerna ska den huvudansvariga uppdragstagaren sammanställa en huvudplan för informationsleveranser. Utöver uppgiftsteamens samordning och eventuella risker avseende leveranser, ska uppdragsgivarens behov av tid för att granska och godkänna informationen beaktas.


#### 4.6 Huvudansvariga uppdragstagarens uppdragsbeskrivning

*Ansvarig: uppdragsgivare*

När uppdragsbeskrivningen är sammanställd ska uppdragsgivaren kontrollera att ställda krav på informationsleveranser från den huvudansvariga uppdragstagaren uppfylls av uppdragets informationsstandard, BIM-föreskrifter, genomförandeplan för BIM och huvudplan för informationsleveranser (MIDP). 

#### 4.7 Uppdragstagares uppdragsbeskrivning 

*Ansvarig: huvudansvarig uppdragstagare*

Den huvudansvariga uppdragstagaren ska kontrollera att ställda krav på informationsleveranser uppfylls av uppdragets informationsstandard, informationsprotokoll, genomförandeplan för BIM och delplan för informationsleveranser (TIDP).

I och med den uppdaterade uppdragsbeskrivningen kan upphandlingen sägas vara avslutad. Nästa skede blir att faktiskt dra i gång den drift och det underhåll som uppdraget omfattar.
