# BIM-föreskrifter för projekt

*Denna sida utgör en mall för disposition av BIM-föreskrifter för projekt. Den har så långt som relevant samma indelning i rubriker som genomförandeplan för BIM.*

*Anvisningar och kommentarer visas i kursiverad stil, medan övrig text är förslag på innehåll.*

*Mallen innehåller avsnitt baserade på SS-EN ISO 19650-2:2019. Innehållet ska minskas eller ökas beroende på behovet i det aktuella projektet.*

*Mallen förutsätter att anbudsinbjudan innehåller Administrativa Föreskrifter (AF) enligt AMA. Där ska framgå en översiktlig projektbeskrivning samt utformning av anbud, tider och annan formalia. Viss text i denna mall ska flyttas till AF.*

*Under samtliga rubriker nedan finns hänvisning till numrerat avsnitt på sidan Upphandling av projekt enligt ISO 19650.*

Dessa BIM-föreskrifter ingår i förfrågningsunderlaget för ett projekt. Syftet är att anbudsgivare ska få en klar bild över de krav som ställs avseende informationshantering i uppdraget. Anbudsgivaren svarar på BIM-föreskrifterna med en anbudsversion av en genomförandeplan för BIM (BEP).

[1 Allmänt om informationshantering](#allmänt-om-informationshantering)

[1.1 Informationsansvariga](#informationsansvariga)

[2 Projektinformationskrav](#projektinformationskrav)

[2.1 Projektets informationskrav (PIR)](#projektets-informationskrav-pir)

[2.2 Organisationens informationskrav (OIR)](#_Toc164662134)

[2.3 Tillgångsförvaltningens informationskrav (AIR)](#_Toc164662135)

[2.4 Informationsutbyteskrav (EIR)](#_Toc164662136)

[3 Projektets informationsstandard](#projektets-informationsstandard)

[3.1 Klassifikation och nedbrytningsstruktur](#klassifikation-och-nedbrytningsstruktur)

[3.2 Referensbeteckningar](#referensbeteckningar)

[3.3 Typbeteckningar](#typbeteckningar)

[3.4 Märkning av informationscontainrar](#_Toc164662141)

[3.5 Utförande av CAD-objekt](#utförande-av-cad-objekt)

[4 Metoder och rutiner för informationsproduktion](#metoder-och-rutiner-för-informationsproduktion)

[4.1 Referensinformation och delade resurser](#referensinformation-och-delade-resurser)

[4.2 Generering av information](#generering-av-information)

[4.3 Granskningsflöde](#_Toc164662146)

[4.4 Leveransformat](#leveransformat)

[5 Delad datamiljö (CDE)](#_Toc164662148)

[6 Informationsföreskrifter](#informationsföreskrifter)

[7 Utvärderingskriterier](#utvärderingskriterier)

[8 Instruktioner för inlämning av anbud](#instruktioner-för-inlämning-av-anbud)

[Bilaga 1: Informationsflödet i projekt](#_Toc164662152)

## Allmänt om informationshantering

Målet med projektets informationshantering är ett obrutet digitalt informationsflöde som så långt som möjligt är baserat på objekt med egenskaper, lagrade i en gemensam databas. Granskning och godkännande ska också så långt som möjligt göras på sådana data. Ritningar ska vara baserade på CAD-modeller med objekt som är länkade till databasen.

Administrativa dokument, tekniska beskrivningar och andra typer av dokument avsedda för direkt mänsklig läsning skapas och distribueras hanteras dock som vanliga kontorsdokument (pdf, docx, xlsx med flera filformat).

Informationsflödet i projektet sammanfattas i Bilaga 1: Informationsflödet i projekt.

#### Informationsansvariga

Informationsansvarig hos uppdragsgivaren är NN, nn@xx.se, telefon 000-00 00 00.

## Projektinformationskrav

Mallar till leveransspecifikationer finns på Nationella Riktlinjer (se länk).

Digital information som berör projektet ska hanteras i enlighet med SS-EN ISO 19650-1:2019, SS-EN ISO 19650-2:2019 och SS-EN ISO 19650-4:2022.

Det övergripande syftet är att informationen ska stödja de beslut som behöver fattas under projektet för att säkerställa produktion enligt ställda krav. Informationen ska sedan kunna användas för organisationens ledning samt för drift och underhåll.

Projektspecifika krav anges i avsnitt 2, 3, 4, 5 och 6.

## Organisationens informationskrav (OIR)

Se tillämpningsanvisningarna *Att skapa organisationens informationskrav (OIR)* och *Mer om organisationens informationskrav (OIR).*

Beställaren betraktar de byggda tillgångarna som en viktig del i genomförandet av sina mål. Den information som produceras i projektet ska, förutom att leda till ett lyckat projekt i sig, kunna användas i organisationens övergripande verksamhetsplanering.

Följande information ska ingå i slutleveransen (exempel):

| **Nr** | **Rubrik**                            | **Beskrivning**                  |
|--------|---------------------------------------|----------------------------------|
| **1**  | **Regelefterlevnad**                  |                                  |
| 1.1    | Brandsäkerhetscertifikat              |                                  |
| 1.2    | Dokumentation för tillgänglighetskrav | Ska utföras av oberoende expert. |
| 1.3    | Miljöpåverkansbedömningar             | Ska utföras av oberoende expert. |
| 1.4    | Obligatorisk ventilationskontroll     | Ska utföras av oberoende expert. |
| **2**  | **Prestandakrav**                     |                                  |
| 2.1    | Energieffektivitet                    | Kraven i XXX ska uppfyllas.      |
| 2.2    | Koldioxidavtrycksmål                  | Kraven i XXX ska uppfyllas.      |
| 2.3    | Komfortparametrar för brukare         | Krav på sensordata               |
| **3**  | **Utrymmeshantering**                 |                                  |
| 3.1    | Klassificering av utrymmen            | Enligt CoClass.                  |
| 3.2    | Areor                                 | Enligt SS 21054:2020.            |
| 3.3    | Inredning och utrustning              | Rumsvis redovisning.             |
| **4**  | **Driftskostnader och budgetering**   |                                  |
| 4.1    | Livscykelkostnader                    | …                                |
| 4.2    | Driftsbudget                          |                                  |
| 4.3    | Prognoser                             |                                  |
| **5**  | **Risk, hälsa, säkerhet**             |                                  |
| 5.1    | Arbetsmiljö                           | Med incidentrapportering.        |
| 5.2    | Riskregister                          |                                  |
| 5.3    | Nödutrymningsplaner                   |                                  |
| 5.4    | Försäkringsdokument                   |                                  |
| 5.5    | Säkerhetsdatablad                     |                                  |
| 5.6    | Larm och övervakningssystem           |                                  |
| 5.7    | Nödutrymningsplaner                   |                                  |
| 5.8    | Låssystem                             |                                  |
| **6**  | **Miljö och hållbarhet**              |                                  |
| 6.1    | Avfallshantering                      |                                  |
| 6.2    | Bedömning om biologisk mångfald       |                                  |
| 6.3    | Materialredovisning                   |                                  |
| **7**  | **Intressenter**                      |                                  |
| 7.1    | Instruktioner till brukare            |                                  |
| 7.2    | Kontaktlistor                         |                                  |

## Tillgångsförvaltningens informationskrav (AIR)

För att ge underlag till löpande tillgångsförvaltning i form av drift, underhåll, ombyggnader och tillbyggnader ska följande information ingå i slutleveransen (exempel):

| **Nr** | **Rubrik**                                | **Beskrivning**             |
|--------|-------------------------------------------|-----------------------------|
| **1**  | **Tekniska specifikationer**              |                             |
| 1.1    | Specifikationer av system och komponenter |                             |
| 1.2    | Underhållsscheman                         |                             |
| 1.3    | Driftshandböcker                          | Inklusive felsökningsguider |
| 1.4    | Märkning av tillgångar                    |                             |
| **2**  | **Garantier**                             |                             |
| 2.1    | Omfattning                                |                             |
| 2.2    | Förfaranden för att göra anspråk          |                             |
| **3**  | **Relationsdokumentation**                |                             |
| 3.1    | Ritningar                                 |                             |
| 3.2    | Tekniska beskrivningar                    |                             |
| 3.3    | CAD-modeller                              |                             |
| **4**  | **Utrymmeshantering**                     |                             |
| 4.1    | Klassificering av utrymmen                |                             |
| 4.2    | Areor                                     |                             |
| 4.3    | Inredning och utrustning                  |                             |
| **5**  | **Risk, hälsa, säkerhet**                 |                             |
| 5.1    | Arbetsmiljö                               |                             |
| 5.2    | Riskregister                              |                             |
| 5.3    | Nödutrymningsplaner                       |                             |
| 5.4    | Försäkringsdokument                       |                             |
| 5.5    | Säkerhetsdatablad                         |                             |
| 5.6    | Larm och övervakningssystem               |                             |
| 5.7    | Nödutrymningsplaner                       |                             |
| 5.8    | Låssystem                                 |                             |
| **6**  | **Miljö och hållbarhet**                  |                             |
| 6.1    | Avfallshantering                          |                             |
| 6.2    | Bedömning om biologisk mångfald           |                             |
| 6.3    | Materialredovisning                       |                             |
| **7**  | **Intressenter**                          |                             |
| 7.1    | Instruktioner till brukare                |                             |
| 7.2    | Kontaktlistor för intressenter            |                             |

## Projektets informationskrav (PIR)

*Se sidan Att skapa projektinformationskrav (PIR)*.

#### Omfattning

Projektet ska leverera information som är relevant för drift och underhåll, efterlevnad av interna regler och yttre reglering, och för riskhantering.

#### Milstolpar för informationsleverans

*Se avsnitt 1.3.*

Följande milstolpar för informationsleveranser finns i projektet (exempel):

| **Beslutspunkt**   | **Syfte/Aktörer/Innehåll**                                                                                                                                                      |
|--------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Programhandlingar  | Första kalkylunderlag inför beslut om genomförande.                                                                                                                             |
|                    | Arkitekt, landskapsarkitekt.                                                                                                                                                    |
|                    | Textbeskrivning av planerat utförande.                                                                                                                                          |
| Systemhandlingar   | Visualisering; simuleringar; arearedovisning; detaljerad kalkyl; beräkning av klimatpåverkan; val av tekniska systemlösningar. Kan ingå i anbudsförfrågan för totalentreprenad. |
|                    | Samtliga projektörer.                                                                                                                                                           |
|                    | CAD-modeller, ritningar, översiktliga tekniska beskrivningar.                                                                                                                   |
| Bygglovshandlingar | För beviljande av kommunen.                                                                                                                                                     |
|                    | Samtliga projektörer.                                                                                                                                                           |
|                    | Ritningar, översiktliga tekniska beskrivningar.                                                                                                                                 |
| Bygghandlingar     | Val av detaljlösningar. Kan ingå i anbudsförfrågan för utförandeentreprenad.                                                                                                    |
|                    | Samtliga projektörer.                                                                                                                                                           |
|                    | CAD-modeller, ritningar, detaljerade tekniska beskrivningar enligt AMA.                                                                                                         |

## Informationsutbyteskrav (EIR)

**Uppdragsgivarens informationsutbyteskrav ska utgöra en sammanställning av OIR, AIR och PIR. Resultatet blir konkreta leveransspecifikationer för alla uppdragstagare. Se Att skapa EIR, och Att skapa informationsutbyteskrav (EIR) med IFC.*

*Mallar för leveransspecifikation finns i Nationella Riktlinjer.*

Uppdragsgivarens informationsutbyteskrav framgår av följande bifogade leveransspecifikationer:

-   Leveransspecifikation A
-   Leveransspecifikation B
-   ...

## Projektets informationsstandard

*Se avsnitt 1.4.*

Projektet ska följa de standarder som beskrivs i detta avsnitt.

## Klassifikation och nedbrytningsstruktur

*Beskriv hur klassifikation av objekt ska tillämpas, som komplement till den typning av objekt som ges av CAD-program och IFC. Frågor att besvara:*

*Vilka objekt ska klassificeras, och på vilken nivå? Här ska framför allt tillgångsinformationskraven (AIR) vara styrande, och i viss mån organisationens informationskrav (OIR). Även entreprenören kan ha önskemål. Vilka objekt – utrymmen, system och komponenter – behöver kunna spåras för produktion, drift och underhåll?*

*Vilket eller vilka klassifikationssystem ska användas? Rekommendationen är en kombination av CoClass för grundläggande klass och BIP för att beteckna projektspecifika typer.*

Samtliga objekt ska i CAD-modeller vara klassificerade enligt CoClass. Klassifikationen ska följa med vid export till IFC.

Byggdelar ska beskrivas i sin nedbrytningsstruktur enligt CoClass tabeller för funktionellt system, konstruktivt system och komponent. Kod för typ ska inte användas i nedbrytningsstruktur, endast kod för klass.

Omfattande exempel finns i AMA Funktion 22, kapitel 3.

## Referensbeteckningar

*Ska objekt kunna spåras på individnivå med användning av formella referensbeteckningar? Beskriv i så fall hur detta ska göras, och vilka objekt som omfattas.*

Referensbeteckningar ska omfatta nivåerna funktionellt system, konstruktivt system och komponent, med aktuell byggnad som toppnod.

Följande objekt ska i CAD-modeller ges referensbeteckningar enligt CoClass:

| **Objekt**              | **Aspekt**   |             |                  |         |
|-------------------------|--------------|-------------|------------------|---------|
|                         | **Funktion** | **Produkt** | **Lokalisering** | **Typ** |
| Byggnad                 |              |             |                  |         |
| Fönster                 |              | X           | X                | X       |
| Dörrar                  |              | X           | X                | X       |
| …                       |              |             |                  |         |
| Luftbehandling          |              |             |                  |         |
| Luftbehandlingsaggregat | X            |             | X                | X       |
| ...                     |              |             |                  |         |

Referensbeteckningen ska användas i samtliga informationskällor som beskriver samma objekt.

Detaljerade instruktioner finns i anvisningen *RDS-manual för CoClass.*

## Typbeteckningar

*Ange metod för typbeteckningar för objekt, till exempel en kombination av CoClass generiska typer och projektspecifika typer baserade på BIP.*

Typbeteckningar enligt avsnitt 3.2 ska utgöras av BIP-koder. Projektspecifika nummer fastställs i samråd mellan parterna.

## Informationsfederering

En informationscontainer är en namngiven uppsättning information, till exempel en fil i en lagringsstruktur.

#### Lagringsstruktur

Huvudansvarig uppdragstagare ansvarar för utformning av struktur för lagring av informationscontainrar. Denna ska vara indelad:

-   först efter projektskede
-   sedan efter uppdragstagare (teknisk disciplin).

Lagringsstrukturen ska godkännas av uppdragsgivaren.

#### Filnamn

*Beskriv hur informationscontainrar ska namnges. Anvisningar för filbenämningar finns i Nationella Riktlinjer.*

Modellfiler och ritningsfiler ska namnges med följande sex delar, åtskilda med bindestreck:

-   Ansvarig part – Innehåll – Redovisning – Nummer – Informationsbeteckning – Granskningsstatus

| **Del**                | **Hänvisning**                                                            |
|------------------------|---------------------------------------------------------------------------|
| Ansvarig part          | SIS Bygghandlingar                                                        |
| Innehåll               | BSAB 96                                                                   |
| Redovisning            | SS 32271:2016                                                             |
| Nummer                 | Överenskoms i projektet                                                   |
| Informationsbeteckning | Enl. Informationsbeteckning och  -beskrivning i Nationella Riktlinjer |
| Granskningsstatus      | Enligt tabell nedan                                                       |

Exempel på informationsbeteckning:

| **Beslutspunkt**     | **Informationsbeteckning** |
|----------------------|----------------------------|
| Systemhandlingar     | 2K30                       |
| Bygglovshandlingar   | 2K12                       |
| Förfrågningsunderlag | 3P30                       |
| Bygghandlingar       | 4P70                       |
| Relationshandlingar  | 5P80                       |

Exempel på kod för granskningsstatus:

| **Granskningsstatus** | **Kod** |
|-----------------------|---------|
| För information       | FI      |
| Preliminär            | PR      |
| Förfrågningsunderlag  | FU      |
| För granskning        | GR      |
| Godkänd               | G1      |

Exempel 1: **A-40-V-150001-4P70-G1.ifc** för att beteckna *Arkitekt \> Sammansatt redovisning \> Volymmodell \> Byggnad 15 \> Plan 00 \> Löpnummer 01 \> Bygghandling \> Godkänd*

Övriga dokument namnges med fyra eller sex delar, åtskilda med bindestreck:

-   Ansvarig part – Innehåll – Nummer – Beskrivning, eller
-   Ansvarig part – Innehåll – Nummer – Beskrivning – Informationsbeteckning – Granskningsstatus

| **Del**                                         | **Hänvisning**                                                            |
|-------------------------------------------------|---------------------------------------------------------------------------|
| Ansvarig part                                   | SIS Bygghandlingar                                                        |
| Innehåll                                        | BSAB 96                                                                   |
| Nummer                                          | Överenskoms i projektet                                                   |
| Beskrivning                                     | Användarspecifik                                                          |
| **För förfrågningsunderlag och bygghandlingar** |                                                                           |
| Informationsbeteckning                          | Enl. Informationsbeteckning och -beskrivning i Nationella Riktlinjer |
| Granskningsstatus                               | Enligt tabell ovan                                                        |

Exempel 1: **P-1C03-18-Protokoll B möte.pdf** för att beteckna *Projektgemensamt \> Mötesprotokoll \> Nummer 18 \> Protokoll B-möte*.

Exempel 2: **L-2D02-1-TB anläggning-3P30-GR** för att beteckna *Landskapsarkitekt \> Teknisk beskrivning \> Teknisk beskrivning för anläggning \> Förfrågningsunderlag \> För granskning*.

## Utförande av CAD-objekt

Objekt i CAD-filer ska ha dimensionalitet, utseende och detaljeringsgrad enligt anvisningar i leveransspecifikationer. Dessa följer *Informationsbeteckning och -beskrivning enligt Nationella Riktlinjer.*

Exempel: **3DY-1-LOD3** för att beskriva *3D ytobjekt \> Symboliskt utseende \> Objektets alla utvändigt synliga delar*.

## Metoder och rutiner för informationsproduktion

*Se avsnitt 1.5 och 2.3.*

Huvudansvarig uppdragstagare ska säkerställa för att samtliga personer i leveransteamet kan arbeta enligt metoder och rutiner som beskrivs i detta avsnitt. En beskrivning av leveransteamets mobilisering ska ingå i genomförandeplan för BIM (BEP).

Under projektering och produktion lagras information i en projektinformationsmodell (PIM) enligt kraven i avsnitt 2.4 (EIR) i en gemensam datamiljö enligt avsnitt 5.

I samband med överlämning överförs information enligt kraven i avsnitt 2.2 och 2.3 till en tillgångsinformationsmodell (AIM).

## Referensinformation och delade resurser

*Se avsnitt 1.6, 2.4.*

Följande information finns lagrad i projektinformationsmodellen (PIM):

-   Mallar för ritningsdefinitionsfiler.
-   Grundkarta över kvarteret.
-   Ritningar över befintliga byggnader.
-   …

## Generering av information

Följande gäller för generering av information:

-   Information som är under arbete får lagras i den delade datamiljön (se avsnitt 5) eller hos uppdragstagaren.
-   CAD-data genereras med valfri programvara som är certifierat för IFC-export.
-   Alfanumeriska data genereras med valfri programvara som kan exportera till PDF/A.
-   Redovisningsteknik på ritningar ska, utöver eventuella speciella anvisningar från beställaren, vara i enlighet med SIS Bygghandlingar. Eventuella anpassningar av handlingarna inför produktion ska göras i samråd med utsedd entreprenör och vara godkända av den part som ersätter arbetet ekonomiskt.
-   Helplansritningar enligt BEAst ska tas fram i de fall det underlättar överskådligheten.

## Leveransformat

Följande format ska användas vid leverans till projektinformationsmodell (PIM) och tillgångsinformationsmodell (AIM):

| **Filtyp**      | **Format**                  |
|-----------------|-----------------------------|
| Modellfiler     | Ifc 4.3 samt originalformat |
| Ritningsfiler   | Pdf                         |
| Övriga dokument | Pdf samt originalformat     |

## Granskningsflöde

Följande gäller för granskning av leveranser:

-   Ansvarig hos varje uppdragstagare ska granska leveranser.
-   Huvudansvarig uppdragstagare ska granska leveranser från övriga uppdragstagare.
-   Uppdragsgivare ska granska leveranser från huvudansvarig uppdragstagare.
-   Granskning ska göras enligt *BEAst Effektivare Granskning*

# Delad datamiljö (CDE)

*Se avsnitt 1.7.*

Projektets delade datamiljö ska uppfylla följande krav för varje informationscontainer:

-   lämplig nivå av IT-säkerhet för att minska risken för intrång och förlust av data
-   möjlighet att ange filnamn enligt avsnitt 3.4.2
-   versionshantering i två nivåer: 0.1–0.99, 1.0–1.99, 2.0–2.99 och så vidare
-   användarnamn och datum registreras vid revidering eller ändring av tillstånd
-   åtkomststyrning i fyra nivåer för varje enskild informationscontainer, och kopplad till personlig inloggning i systemet:
    -   Ingen
    -   Läsa
    -   Ändra
    -   Fullständig (skapa, ändra, radera).
-   angivande av granskningsstatus i följande steg:
    -   För information (FI)
    -   Preliminär (PR)
    -   För granskning (GR)
    -   Förfrågningsunderlag (FU)
    -   Godkänd (G1).

Kod för granskningsstatus ska användas i handlingsförteckningar och i filnamn enligt avsnitt 3.4.2.

## Informationsföreskrifter

*Se avsnitt 1.8.*

Avseende rätt till uppdragsresultatet, beskriv eventuella avsteg från:

-   AB 04 kapitel 1 § 14
-   ABT 06 kapitel 1 §§ 13–14.
-   ABK 09 kapitel 7 §§ 1–12

Med undantag för ABK 09 kapitel 7 § 1 är dessa fasta bestämmelser.

För att en ändring av fast bestämmelse ska gälla före bestämmelse i ABK 09, AB 04 eller ABT 06 ska ändringen vara tydlig. Följande gäller för att ändringen ska anses tydlig:

-   Ändringen beskrivs under rätt kod och rubrik i de Administrativa Föreskrifterna.
-   Samtliga ändringar sammanställs i de Administrativa Föreskrifterna under kod AFC.111. I sammanställningen ska det anges under vilken kod och rubrik som ändringen finns.

Om CAD-modeller ska ingå i kontraktshandlingarna behöver deras plats i rangordningen fastställas. I punktlistan nedan förutsätts att så inte är fallet. Se avsnitt 6.3.

Om relevant, ange vad som gäller för hantering av information för säkerhetsklassade byggnadsverk.

Följande gäller för användning av projektinformationsmodellen (PIM) och tillgångsinformationsmodellen (AIM):

-   Byggproduktionen ska baseras på utdrag från CAD-modeller i form av ritningsfiler.
-   Direkt användning av CAD-modeller får användas enbart för orientering och för beräkning av mängder.
-   Filer får endast användas för det syfte som framgår av informationsbeteckning enligt avsnitt 3.4.2.
-   Information får hanteras enbart baserat på åtkomsträttigheter enligt avsnitt 5.
-   Informationen i projektinformationsmodellen ägs av den som skapat den.
-   Informationen i tillgångsinformationsmodellen ägs av uppdragsgivaren.

Med tillägg till ABK 09 kap 7 § 1 gäller följande:

Uppdragsgivarens nyttjanderätt innefattar att upplåta en begränsad rätt att nyttja uppdragsresultatet till annan uppdragstagare om detta är nödvändigt för att uppfylla de åtaganden denna uppdragstagare har i projektet. Detta gäller oavsett entreprenadform.

Se **AFB.3 Anbudsgivning**.

## Utvärderingskriterier

*Se avsnitt 2.6.*

Som en del i utvärderingen av inkomna anbud bedöms sannolikheten att det föreslagna leveransteamet klarar att uppfylla ställda krav avseende informationshantering enligt ISO 19650. Följande viktade kriterier används (exempel):

| **Kriterium**                                                    | **Vikt %** |
|------------------------------------------------------------------|------------|
| Informationsansvarig hos potentiell huvudansvarig uppdragstagare | 5          |
| Leveransteamet kompetens och erfarenhet                          | 25         |
| Anbudsversion av genomförandeplan för BIM (BEP)                  | 50         |
| Förslag till mobiliseringsplan                                   | 15         |
| Riskanalys                                                       | 5          |
| Totalt                                                           | 100        |

Text förs in i **AFB.53 Prövning av anbud**. Alternativt hänvisas från AF till dessa BIM-föreskrifter.

## Instruktioner för inlämning av anbud

Se avsnitt 2.6.

I anbudet ska följande ingå:

-   Namn och kontaktuppgifter på informationsansvarig hos huvudansvarig uppdragstagare och hos eventuella anlitade underleverantörer (uppdragstagare).
-   Anbudsversion av genomförandeplan för BIM (BEP). Denna ska visa hur samtliga krav i dessa BIM-föreskrifter ska uppfyllas.
-   Sammanfattande bedömning av leveransteamets förmåga och kapacitet enligt SS-EN ISO 19650-2:2019, avsnitt 5.3.4.
-   Förteckning över deltagare i leveransteamet, med angivande av organisation, personnamn och kontaktuppgifter för samtliga uppgiftsteam.
-   Förslag till mobiliseringsplan enligt SS-EN ISO 19650-2:2019, avsnitt 5.3.5.
-   Riskanalys enligt SS-EN ISO 19650-2:2019, avsnitt 5.3.6.

Text förs in i **AFB.3 Anbudsgivning**. Alternativt hänvisas från AF till dessa BIM-föreskrifter.

## Bilaga 1: Informationsflödet i projekt

![](media/de66c3c90978ec917435036919b03686.emf)

1.  Utgångspunkten är *tillgångsägarens mål*.
2.  Under programskedet ska uppdragsgivaren formulera *organisationens krav på information* (*OIR*) som ska komma ut från projektet. Informationen ska stödja de övergripande målen.
3.  Den förvaltande organisationen ska också fastställa sina *krav på tillgångsinformation* (*AIR*), så att resultatet kan skötas så bra som möjligt.
4.  För att säkerställa leveransen av OIR och AIR förs de in i *projektets informationskrav* (*PIR*). I PIR fastställs också milstolpar och vilken information som bör tas fram och levereras till dessa inom projektet, till exempel godkännande, investeringsbeslut, byggstart, olika upphandlingsprocesser. Här blir de riktlinjer för uppdragstagarne, som kompletterar behovet av informationsöverföring mellan teknikområdena.
5.  PIR blir ytterligare konkretiserat i form av *informationsutbyteskrav* (*EIR*) för varje enskilt uppdrag som ingår i projektet.
6.  Nästa steg är att ta fram projektanpassade *BIM-föreskrifter*. Förutom EIR innehåller den projektets milstolpar, en beskrivning av projektets informationsstandard, metoder och rutiner för informationsproduktion, uppgifter om referensinformation, anvisningar om *delad datamiljö* (*CDE*), samt informationsföreskrifter som beskriver rättigheter och skyldigheter som rör informationen.
7.  Referensinformation i form av beskrivningar av befintliga tillgångar, geodata, detaljplaner och annat görs tillgängligt för projektet.
8.  Den huvudansvariga uppdragstagaren svarar på BIM-föreskrifterna genom att ta fram en *genomförandeplan för BIM* (*BEP*). Här ingår en gränsdragningslista mellan deluppdragen, planer för informationsleveranser för varje deluppdrag (*TIDP*), och en huvudplan för informationsleveranser (*MIDP*) som sammanställer dessa.
9.  Under projekteringens gång förs information undan för undan in till den gemensamma *projektinformationsmodellen* (*PIM*), som lagras i den delade datamiljön. Varje leverans baseras på ett specifikt krav, och en kontroll görs av mottagaren innan den blir godkänd för användning.
10. Dokument i form av beskrivningar, ritningar, CAD-modeller och annat publiceras för användning i byggproduktionen.
11. När projektet är färdigställt överlämnas en *tillgångsinformationsmodell* (*AIM*). Information som enbart berörde produktionen kan då vara arkiverad, och information som är relevant för drift och underhåll ska vara tillförd.
12. Dokument i form av drifts- och underhållsinstruktioner, produktblad, garantibevis och annat kan hämtas från AIM.
13. Under användningen kan information delges tillgångsägaren, baserat på de ursprungliga kraven i OIR.