# Mer om organisationens informationskrav (OIR)

## Inledning
Byggnadsinformationsmodellering enligt ISO 19650 bygger på en enkel princip: *All tillgångs- och projektinformation som ska tillhandahållas under tillgångens livscykel ska specificeras av uppdragsgivaren genom uppsättningar av informationskrav* (SS-EN ISO 196501:2019, 6.3.1).

Att genomföra detta i praktiken är dock inte lika enkelt. Traditionellt har kraven har huvudsakligen handlat om det som fysiskt ska byggas, med mycket liten betoning på vilken information som ska följa med in i användning och förvaltning.

Uppdragsgivaren för ett projekt behöver alltså lära sig formulera krav inte bara på vad som ska byggas, utan också om *vilken information som ska följa med i leveransen*. Projektet kan handla om nybyggnad, om- eller tillbyggnad, drift och underhåll, eller om en större åtgärd som inte ingår i det normala underhållet. Dessa krav ska utgöra input i informationshanteringen, och output ska vara informationsleveranser som uppfyller kraven.

Detta dokument beskriver en metod för att ta fram informationskraven för en organisation, och hur dessa kan realiseras genom den information som produceras under förvaltningen av tillgångarna.<sup>1</sup> Beskrivningen av metoden kompletteras med anvisningar i ISO 19650. Använda begrepp förklaras i avsnittet Definitioner.

<sup>1</sup> Metoden beskrivs i rapporten *Line of Sight: an Asset Management Methodology to Support Organisational Objectives – Optimising asset management to realise business success*, framtagen av Cambridge Centre for Smart Infrastructure & Construction, CSIC.

En kortare beskrivning av hur man tar fram OIR finns på sidan ***”Att skapa OIR”***.

## Sammanfattning

Metoden för att fastställa organisationens informationskrav kan sammanfattas i följande steg:

1.  Identifiera **organisationens mål**.
2.  Identifiera de **kritiska framgångsfaktorer** som behöver uppfyllas för att nå organisationens mål.  
    *Exempel:* för att nå målet att minska energianvändningen behöver man genomföra mätbara effektiviseringar som regelbundet följs upp. Energianvändningen för nya tillgångar behöver dokumenteras.
3.  Konkretisera framgångsfaktorerna ytterligare genom att ställa **klartextfrågor**.  
    *Exempel:* Vilka effektiviseringsåtgärder har genomförts? Hur han energianvändningen varit för de senaste perioderna? Hur redovisas energianvändningen för nya tillgångar?
4.  Svaren på klartextfrågorna visar vilken information som organisationen behöver.  
    *Exempel:* Sammanställning av effektiviseringsåtgärder för varje tillgång. Dokumentation av energianvändning för alla tillgångar.

## Tillgångar

En organisation har olika typer av tillgångar, till exempel fysiska, personella och immateriella. I detta sammanhang handlar det om *de fysiska objekt som förser organisationen med de funktioner som behövs för verksamheten*: konkret byggnadsverken med sina utrymmen, inredning och utrustning.

Med hjälp av CoClass kan alla dessa struktureras, klassificeras och beskrivas. Frågan är på vilken detaljeringsnivå de behöver beskrivas, och hur informationen ska hållas både tillgänglig och aktuell. Målet är att så långt som möjligt upprätthålla tillgångarnas funktion och därmed deras värde. Hur ska ledningen för en organisation nå det målet?

## Organisationens mål och behov

För att nå en effektiv tillgångsförvaltning är enligt ISO 19650 behöver ägaren gå igenom tre steg:

1.  Baserat på organisationens mål och dess behov av information, formulera **organisationens informationskrav** (OIR).
2.  Dessa krav ska i nästa steg konkretiseras i form av **tillgångsinformationskrav** (AIR).
3.  Data om drift och underhåll ska sedan lagras i en **tillgångsinformationsmodell** (AIM).

Den här översättningen – från organisationens övergripande och strategiska mål till förvaltningens konkreta behov av aktuell information – är inte enkel att göra. Hur ska den gå till? (Se Bild 1.)

![OIR bild 1](../3.kravstallning_och_upphandling/media/oir1.png)

*Bild 1: Gapet mellan organisationens informationskrav (OIR) och tillgångsinformationskrav (AIR) som behöver överbryggas. Källa: Line of Sight, Figure 1.*

Översättningen kan förenklas genom att som ett mellansteg formulera **funktionella informationskrav** (FIR). Dessa ska förtydliga vilka funktioner som organisationen behöver få levererade av sina fysiska tillgångar. Metoden kallas ”siktlinjemetoden”, baserat på att den syftar till att skapa en begriplig ”röd tråd” genom informationskedjan. Metoden kan beskrivas enligt Bild 2.

![OIR bild 2](../3.kravstallning_och_upphandling/media/oir2.png)

*Bild 2: Siktlinjemetoden och hur den överbryggar gapet mellan organisationens mål och tillgångsinformationsmodellen. Källa: Line of Sight, Figure 2.*

Det första av de tre stegen i metoden beskrivs i mer detalj i följande avsnitt. Steg två (AIR) och tre (AIM) beskrivs i andra anvisningar.

## Vad är OIR?

Organisationens informationskrav (OIR) avser de informationsbehov som en organisation har när det gäller hanteringen av information under livscykeln för en tillgång. Informationen ska användas för att stödja organisationens strategiska mål, operativa processer och efterlevnad av olika typer av regelverk.

OIR inkluderar följande element:

1.  **Informationshanteringsstrategi**: OIR definierar organisationens strategi och mål för informationshantering. Det fastställer organisationens tillvägagångssätt för att fånga, lagra, organisera och använda information för att möta sina affärsbehov.
2.  **Informationspolicy och standarder**: OIR etablerar de policyer och standarder som styr informationshanteringen inom organisationen. Det inkluderar riktlinjer för namngivning av information, hantering av metadata, informationssäkerhet och kvalitetssäkring.
3.  **Informationsbehov och användning**: OIR identifierar organisationens specifika informationsbehov under tillgångens eller projektets livscykel. Det specificerar vilken typ av information som krävs, detaljnivån och det avsedda användningsområdet för informationen för olika ändamål, såsom beslutsfattande, regelverksmässig efterlevnad och rapportering.
4.  **Informationsstyrning**: OIR definierar roller, ansvar och processer för att styra information inom organisationen. Det inkluderar att tilldela ansvar för informationshantering, etablera arbetsflöden för informationsframställning och godkännande samt säkerställa efterlevnad av relevanta regler och standarder.
5.  **Hantering av informationslivscykeln**: OIR beskriver organisationens tillvägagångssätt för att hantera information under dess livscykel. Det inkluderar krav på informationssamling, lagring, bevarande, arkivering och eventuell bortskaffande eller överföring till andra parter.
6.  **Informationsutbyte och samarbete**: OIR specificerar organisationens krav på informationsutbyte och samarbete med externa intressenter, såsom kunder, leverantörer och samarbetspartners. Det kan inkludera riktlinjer för datautbytesformat, protokoll och säkerhetsåtgärder för att säkerställa smidig och säker informationsdelning.
7.  **Infrastruktur för information och kommunikation**: OIR tar upp organisationens behov av infrastruktur för informations- och kommunikationsteknik för att stödja effektiv informationshantering. Det kan inkludera krav på hårdvara, programvara, nätverkssystem och datalagringskapacitet.

Med en tydlig och genomarbetad OIR som grund för all informationshantering underlättas informationsdelning, beslutsfattande och samarbete, både internt och externt.

## Upprätta organisationens informationskrav (OIR)

![OIR bild 3](../3.kravstallning_och_upphandling/media/oir3.png)

*Bild 3: Från organisationens mål till dess informationskrav.*

Enligt SS-EN ISO 19650-1 ska OIR ställa krav på vilken information som behövs för att svara på eller informera om strategiska mål på överordnad nivå hos uppdragsgivaren. Syftet kan vara att stödja till exempel:

-   strategisk verksamhet
-   strategisk tillgångsförvaltning
-   portföljplanering
-   regleringsskyldigheter
-   beslutsfattande.

Målen är givetvis olika för olika typer av organisationer, men oavsett typ kan de inordnas i sex kategorier (utan rangordning):

-   finansiella
-   miljö och klimat
-   verksamhet och drift
-   kunder
-   hälsa och säkerhet
-   anseende.

En nyckel till framgångsrik tillgångsförvaltning är att utveckla mål och planer för organisationens hela verksamhet som sammanfaller med mål som fokuserar på de fysiska tillgångarna.<sup>2</sup> Detta kan göras genom att först identifiera **kritiska framgångsfaktorer**, och därefter ställa enkla **klarspråksfrågor** till organisationen som handlar om att konkretisera faktorerna. Frågorna ska peka på funktioner som går att kontrollera, och som kan uttryckas i organisationens informationskrav. Se Bild 3.

<sup>2</sup> Detta täcks in av standardserien SS-ISO 55000 *Ledningssystem för tillgångar*.

![Line of sight](../3.kravstallning_och_upphandling/media/los_3.png)

*Bild 4: Utveckling av organisationens informationskrav som är i linje med organisationens mål. Källa: Line of Sight, Figure 3.*

#### Kritiska framgångsfaktorer

**Kritiska framgångsfaktorer** (KFF) är de omständigheter eller resultat som krävs för att uppnå organisationens mål. Som en riktlinje kan en uppsättning av fyra till sex KFF vara lämpliga för varje mål. Dessa kan handla om:

-   minskade driftskostnader
-   mindre avhjälpande och mer förebyggande underhåll
-   öka andelen förnybar energi
-   snabbare svar på kundfrågor.

Vilken information krävs sedan för att visa hur det går med framgångsfaktorerna? Det kan handla om att:

-   mäta om organisationens mål uppnås
-   möta intressenternas behov (anställda, slutanvändare, ägare)
-   uppfylla krav från regelverk (kontroller, besiktningar, inspektioner)
-   utveckla policyer (inklusive kvalitetskontroll)
-   stödja affärsledningen (rapportering, revisioner, upphandlingar, analyser).

#### Klarspråksfrågor

**Klarspråksfrågor** (KSF) används som ett verktyg för att utveckla frågor som belyser de kritiska framgångsfaktorerna. Fyra till sex frågor bör tas fram för varje KFF. Exempel:

-   Hur stor är den totala driftskostnaden?
-   Hur stor andel av underhållet är avhjälpande?
-   Hur stor andel av energianvändningen kommer från förnybara källor?
-   Vad är medeltiden för svar till kund?

#### Organisationens informationskrav (OIR)

Svaren på klarspråksfrågorna leder på det här sättet fram till organisationens informationskrav. Exemplet ovan kan kräva leverans av följande information:

-   Kostnader för alla typer av driftåtgärder (förbrukningsmaterial, energi, vatten, driftpersonal med mera).
-   Klassifikation och kontinuerlig dokumentation av underhållsåtgärder.
-   Statistik för energianvändning och dess källor.
-   Kundenkäter.

## Alternativ metod

Det finns många sätt att gå från mål till krav på information i en OIR. En alternativ metod är att använda följande fem steg, baserat på malldokumentet *"Organisational Information Requirements"* (CDBB, University of Cambridge):

1.  **Definiera dina mål**: Definiera din organisations mål/syfte för att fånga, underhålla och använda information relaterad till tillgångar. Se till att du inkluderar bredare affärsbehov, och inte bara projektanskaffning, om det är tillämpligt.
2.  **Identifiera intressenter**: Det första steget i kravinsamling är att tilldela roller i ditt projekt. Detta är när du identifierar dina projektintressenter. En intressent är någon som investerat i projektet, oavsett om de är interna eller externa partners.
3.  **Skapa ett informationskravsschema**: Denna tabell kan innehålla information om vilken typ av information som krävs, vem som kommer att använda den, när den behövs och varför den behövs.
4.  **Identifiera interna och externa organisatoriska grupper**: Denna sektion kan användas för att ge en beskrivning av organisationens interna avdelningar, vilket ger en kontext för varför de kan behöva information.
5.  **Styrning**: Definiera vem som kommer att vara ansvarig för att se till att informationskraven uppfylls och hur detta kommer att övervakas.

## Definitioner

**tillgång**  
sak, enhet eller entitet med potentiellt eller faktiskt värde för en organisation

Anm. 1 till termpost: Värdet kan antingen vara materiellt, immateriellt, finansiellt eller icke-finansiellt, och omfattar hänsyn till risker och förpliktelser. Värdet kan vara positivt eller negativt i olika stadier under tillgångens livstid.

Anm. 2 till termpost: Fysiska tillgångar avser vanligen utrustning, inventarier och egendom som ägs av organisationen. Fysiska tillgångar är motsatsen till immateriella tillgångar, som är icke-fysiska tillgångar som hyreskontrakt, varumärken, digitala tillgångar, nyttjanderätter, licenser, immaterialrätter, anseende eller avtal.

Anm. 3 till termpost: En gruppering av tillgångar som kallas ett tillgångssystem kan även betraktas som en tillgång.

[Källa: SS-ISO 55000:2014]

**informationskrav**  
specifikation för vad, när, hur och för vem information ska produceras

[Källa: SS-EN ISO 19650-1:20XX, 3.3.2]

**organisationens informationskrav - OIR**  
informationskrav relaterade till organisationens mål

[Källa: SS-EN ISO 19650-1:20XX, 3.3.3]

**funktionella informationskrav - FIR**  
*informationskrav* beträffande en tillgångs funktionella output

[Källa: Line of Sight]

**tillgångsförvaltningens informationskrav - AIR**  
informationskrav relaterade till driften av en tillgång

[Källa: SS-EN ISO 19650-1:20XX, 3.3.4]

**informationsmodell**  
uppsättning av strukturerade och ostrukturerade informationscontainrar

[Källa: SS-EN ISO 19650-1:20XX, 3.3.8]

**tillgångsinformationsmodell - AIM**  
*informationsmodell* relaterad till driftsskedet

[Källa: SS-EN ISO 19650-1:20XX, 3.3.9]