# Informationsutbyte

Under alla typer av projekt – både under leveransskedet och under användningsskedet – hanteras och utväxlas en stor mängd information av olika slag. Två viktiga principer i ISO 19650 är att:

-   verksamhetens behov ska ligga till grund för kraven på information
-   utbytet av information ska baseras på kraven från uppdragsgivaren.

Nyckeln för ett framgångsrikt projekt är tydliga informationskrav, utifrån det specifika behovet. Vilket beslut ska stödjas av den specifika begäran om information? Varje sådant **informationsutbyte** ska bestå av:

-   ett informationskrav
-   produktion av information
-   en leverans
-   en granskning
-   efter eventuellt underkännande görs en omarbetning
-   ett godkännande.

![Informationsutbyte](../3.kravstallning_och_upphandling/media/informationsutbyte1.png)

*Bild: Stegen i ett informationsutbyte. Uppdragsgivaren står för informationskrav och granskning; uppdragstagaren står för produktion, leverans och eventuell omarbetning.*

Observera att relationen uppdragsgivare–uppdragstagare finns i alla relationer mellan alla parter:

-   projektets uppdragsgivare och huvudansvarig uppdragstagare (generalkonsult, generalentreprenör, totalentreprenör)
-   huvudansvarig uppdragstagare och dennes uppdragstagare (underkonsulter eller underentreprenörer)
-   uppdragstagare och uppdragstagare (till exempel mellan underkonsulter).