# Tillgångsförvaltning och hierarkisk kravställning

## Vad är tillgångsförvaltning?

**Förvaltning** är ett övergripande begrepp som kan sägas omfatta ledning, administration och skötsel av något. Oavsett vilken organisation man talar om – ett bostadsföretag, en entreprenör, en anläggningsägare med många flera – så behöver den förvaltas på olika nivåer.

Primärt handlar det om att styra själva organisationen, dess verksamhet och att kommunicera med sina kunder eller andra typer av användare. Här är målen på en övergripande nivå, och handlar om vilka produkter eller tjänster som organisationen ska tillhandahålla.

## Vad är tillgångar?

På en nästa nivå behöver organisationens tillgångar hanteras. Dessa kan vara personella (”human resources”), finansiella och mycket annat, som på olika sätt bidrar till organisationens olika verksamheter. Så finns de fysiska tillgångar som organisationen använder i sin verksamhet: byggnader, anläggningar, installationer, inredningar och utrustning.

En standard som övergripande beskriver förvaltning av tillgångar är SS-ISO 55000:2014 *Ledningssystem för tillgångar – Översikt, principer och terminologi*. Här finns definitionen av **tillgång** som används också i SS-EN ISO 19650:

**tillgång**  
sak, enhet eller entitet som har potentiellt eller faktiskt värde för en organisation

Syftet med SS-ISO 55000 är att ägaren ska öka värdeskapandet från sina tillgångar och uppnå organisationens mål. En viktig del i detta är att ha användbar information om tillgångarna.

Det är här SS-EN ISO 19650 tar vid, för att täcka en del av informationsbehovet. Den ger principerna för hur information om **byggda tillgångar** ska hanteras under drift, underhåll och i projekt som skapar nya tillgångar. I Del 2 finns nedanstående bild som illustrerar de lager av förvaltning som en organisation behöver ha:

![Lager av förvaltning i en organisation. ](../3.kravstallning_och_upphandling/media/del2_figur1.png)

*Bild 1: Lager av förvaltning i en organisation. SS-EN ISO 19650 omfattar det innersta lagret informationshantering. ISO 55000 handlar om tillgångsförvaltning, medan ISO 21500 ger vägledning för projekthantering. ISO 9001 täcker ledningssystem för kvalitet. Källa: SS-EN ISO 19650-2, Figur 1. (Termen DRIFTSSKEDE är här felöversatt; det ska vara ANVÄNDNINGSSKEDE.)*

<table>
   <tbody>
      <tr>
         <td><b>AIM</b></td>
         <td>tillgångsinformationsmodell</td>
      </tr>
      <tr>
         <td><b>PIM</b></td>
         <td>projektinformationsmodell</td>
      </tr>
      <tr>
         <td><b>A</b></td>
         <td>start på leveransskedet – överföring av relevant information från AIM till PIM</td>
      </tr>
      <tr>
         <td><b>B</b></td>
         <td>progressiv utveckling av designmodellen till den virtuella konstruktionsmodellen</td>
      </tr>
      <tr>
         <td><b>C</b></td>
         <td>slut på leveransskedet – överföring av relevant information från PIM till AIM</td>
      </tr>
   </tbody>
</table>



*Tabell 1: Förklaring av Bild 1.*

## Hierarkisk kravställning av projekt

Som input till ett projekt som ska skapa eller komplettera en tillgång behöver uppdragsgivaren fastställa kraven på den information som ska levereras till tillgångsinformationsmodellen (AIM). Detta ska göras enligt ett antal steg som visas i Bild 2.

![Process](../3.kravstallning_och_upphandling/media/process1.png)

*Bild 2: I denna figur har ”specificerar” betydelsen ”fastställer innehåll, struktur och metodik”. Källa: SS-EN ISO 19650-1, Figur 2.*

Organisationens informationskrav (OIR) är helheten av den information som tillgångsägaren behöver, där informationen om den byggda miljön är en ibland liten del. Oavsett hur stora tillgångarna är så ger OIR det kompletta underlaget för AIR. Här ställs kraven på den information som behövs för att drifta och underhålla tillgångarna.

I samband med att ett projekt startas ska krav från OIR och AIR ”extraheras” till de operativa nivåerna projektets informationskrav (PIR) och informationsutbyteskrav (EIR). Hur omfattande dessa blir är i hög grad beroende av projektets art. För en enkel kundanpassning som består av att några väggar ska rivas kräver självklart mycket mindre information än nyproduktion av ett helt hus. Ett nytt slitlager på en väg kräver mindre information än om det är en ny väg som ska byggas.

EIR och AIR specificerar i sin tur vilken information som ska lagras i PIM respektive AIM.
