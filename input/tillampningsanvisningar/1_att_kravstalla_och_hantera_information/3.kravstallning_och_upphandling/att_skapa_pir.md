# Att skapa projektets informationskrav (PIR)

## Inledning

Projektinformationskraven (PIR) skapas utifrån de krav som finns beskrivna i organisationens informationskrav (OIR) och tillgångsinformationskraven (AIR). PIR bidrar i sin tur till de mer detaljerade informationsutbyteskraven (EIR).

Organisationer med många projekt kan med fördel arbeta fram PIR på en generell nivå –en “PIR-mall” – kanske tillsammans med en generell projektprocess som grund för de projekt man senare genomför. När beslut tas att starta ett projekt kompletteras mallen med projektets specifika informationskrav, baserat på de aktuella OIR och AIR. Detta görs innan upphandling av konsulter eller entreprenörer genomförs.

PIR tas fram av uppdragsgivaren eller dess representant, och ska godkännas av uppdragsgivaren, och eventuellt av beställaren om denna är en annan organisation eller avdelning än uppdragsgivaren.

När PIR tas fram ska följande beaktas:

-   projektets omfattning
-   det avsedda ändamål för vilket informationen kommer att användas av beställaren
-   projektplanen för arbetet
-   den avsedda upphandlingsformen
-   antalet kritiska beslutspunkter under hela projektet, samt vilka beslut som behöver tas
-   frågeställningar som behöver besvaras av beställare för att fatta välgrundade beslut.

Det finns många olika ändamål, syften och behov av att utbyta information i ett projekt. För en beställare kan det bland annat handla om att:

-   ta fram underlag för kalkyl
-   styrning eller måluppfyllelse för miljö och klimat
-   information till kunder eller marknadsavdelning
-   hälsa och säkerhet.

Nedan beskrivs en metod i fyra steg för att ta fram en projektspecifik PIR.

## Steg 1: Identifiera organisationens krav och projektets omfattning

I ett första steg identifieras de aspekter från OIR och AIR som är överförbara till PIR, exempelvis olika nyckelindikatorer (KPI, Key Performance Indicators), hållbarhetsaspekter, funktionella krav eller information som tas fram i under leveransskedet och sedan nyttjas i användningsskedet.

Projektets omfattning kan dokumenteras genom att svara på frågorna:

-   Vad innebär projektet?
-   Vilken omfattning i tid och arbete innebär ett projekt?
-   Vad är det förväntade resultatet?
-   Vad är motiven bakom projektet?
-   Finns det en affärsplan?
-   Vilka mål eller resultat ska uppnås?
-   Vilken upphandlings- och entreprenadform som används i projektet?
-   Vilka viktiga aktörer berörs av projektet?

Upphandlings- och entreprenadform är en viktig aspekt som bidrar till informationskravet genom omfattningen och detaljering av leveranser vid olika tidpunkter.

Viktiga aktörer som berörs av projektet och deras behov av information kommer påverka kravet, exempelvis:

-   funktioner inom beställarens organisation, exempelvis projektavdelning eller uthyrning
-   projekterande konsulter och annan nödvändig expertis
-   entreprenörer
-   leverantörer av material eller utrustning till projektet
-   myndigheter, exempelvis kommuner.

## Steg 2: Definiera informationsleveransernas ändamål och användning

I steg två identifieras hur informationen ska användas i projektet. Det kan handla om att styra och besluta kring kostnader, ge information till berörda myndigheter, eller ta fram och styra hållbarhetsaspekter som energianvändning och klimatpåverkan.

Exempel på ändamål:

-   kalkylunderlag, där leveransen är areauppgifter för exempelvis BTA, BRA, LOA
-   simuleringar, där leveransen exempelvis kan vara resultatet av en dagsljussimulering för att uppnå krav
-   visualisering, där leveransen kan vara marknadsföringsmaterial
-   uppgifter och tillstånd från myndigheter, exempelvis bygglov.

## Steg 3: Upprätta projektplan och kritiska beslutspunkter

Steg tre handlar om att upprätta en plan över projektets skeden och när viktiga aktörer engageras, till exempel projekterande konsulter och entreprenörer.

Här ingår att identifiera beställarens kritiska beslutspunkter under projektet och vilken information som krävs vid varje beslutspunkt.

Identifiera också andra projektintressenters behov av information, exempelvis myndigheter eller brukare. Vid vilka tidpunkter behöver dessa projektintressenter information under projektprocessen?

Kritiska beslutspunkter kan handla om:

-   inriktningsbeslut
-   investeringsbeslut
-   inflyttning och överlämning till förvaltning.

I bilden nedan visas några exempel på kritiska beslutspunkter och leveranser i ett projekt.

![Projektinformation](../3.kravstallning_och_upphandling/media/pir.png)

*Bild 1: Exempel på kritiska beslutspunkter och andra leveranser*

## Steg 4: Definiera leveranser och input till EIR

Det fjärde och sista steget handlar om att utifrån kritiska beslutspunkter och planerad överföring av information mellan aktörerna skapa en förteckning över de leveranser som ska genomföras i projektet.

Förteckningen ska visa vilken typ av information och hur den bör se ut. Utgå gärna ifrån nivå av informationsbehov och nyckelfrågor exempelvis:

-   Syfte: varför behövs informationen?
-   Beslutspunkt: när behövs informationen utifrån projektplanen?
-   Aktörer: vem levererar, vem bidrar och vem tar emot informationen?
-   Objekt: vad ska levereras? i vilket format, och vad för slags innehåll?

Projektplanen och leveransförteckningen fungerar som underlag när informationsutbyteskrav (EIR) upprättas.