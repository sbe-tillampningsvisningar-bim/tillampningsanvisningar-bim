# Upprätta tillgångsinformationskrav (AIR)

För att kunna upprätta krav på tillgångsinformation måste man först identifiera vilka tillgångar som behöver förses med information. Vilka “informationsobjekt” som behöver kravställas beror på vilken strategi och informationsbehov organisationen har. ISO 19650 innehåller en systematisk metod för att göra detta för de byggda tillgångarna som organisationen behöver för sin verksamhet. Avgränsningen är alltså byggnader och anläggningar som förser verksamheten med utrymmen och installationer.

Ett mer konkret sätt att se detta på är att informationen ska stödja verksamhetens olika arbetsprocesser. Om informationen efterfrågas av exempelvis en kalkylator, underhållsorganisationen eller verksamhetsplaneringen, har dessa har olika sätt att relatera till de aktuella objekten.

Vilka processer som behöver stödjas och vilka behov de har bestäms av organisationens strategier, som baseras på vad man vill uppnå i samverkan med den aktuella digitala mognaden. Behovet av information skiljer sig internt mellan organisationens olika processer, och externt i jämförelse med andra organisationer.

Några exempel:

-   En organisation vars strategi är att jobba med **avhjälpande underhåll** behöver i princip bara veta vilka underhållsobjekt som finns i anläggningen och var de finns.
-   En organisation vars strategi är att jobba med **planerat underhåll** behöver ytterligare information om underhållsplaner för att förlänga driften, samt leverantörers skötselrutiner för att säkra garantitider och så vidare.
-   Siktar man mot ett **prediktivt underhåll** behöver ytterligare driftsinformation, information från sensorer och så vidare.

Allt detta sammantaget behöver avspegla sig i tillgångsinformationskraven: vilken information behövs för vilka tillgångar?

En verksamhet kan beskrivas på många sätt. En metod är att först göra en strukturerad beskrivning av de **funktioner** som verksamheten syftar till: att ge vård, att förse människor med bostäder och så vidare. Beskrivningen visar steg för steg hur funktionerna blir allt mer specialiserade. I Bild 1 visas till vänster de funktioner som behövs för att driva en flygplats.

I nästa steg kan man beskriva de **tekniska lösningar** som behövs för att realisera funktionerna. Även här görs beskrivningen stegvis, till exempel från en hel byggnad, dess utrymmen, och den inredning och utrustning som verksamheten behöver.

Objekten som beskrivs på dessa två sätt har sedan inbördes relationer. En funktion kan ha relationer till underfunktioner, och det finns relationer mellan funktionerna och de tekniska lösningar som realiserar dem. I Bild 1 visas hur funktionen vattenreservoar som behövs i färskvattenförsörjningen realiseras med en teknisk lösning i form av just en vattenreservoar, lokaliserad i ett pumphus. Denna innehåller sedan delar i form av UV-filter, elcentral och mycket annat.

![Exempel CoClass flygplats](../3.kravstallning_och_upphandling/media/flygplats.png)

*Bild 1: Exempelbild på funktioner och tekniska lösningar för en flygplats.*

De tekniska lösningarna kan först beskrivas som generiska produkter. När de väl realiseras består de av artiklar och individer – även kallade förekomster – som utgör själva anläggningen och som kan bytas ut. Artiklar och individer med en egen livscykel måste kunna separeras från objekt som följer anläggningens hela livscykel. Det är också en fråga om informationseffektivitet. En byggnad eller anläggning kan ha flera individer där man använder samma artikel, och man vill då kunna peka på en unik källa till den artikeln.

På denna sida sammanfattas en metod för att upprätta tillgångsinformationskrav (AIR). Metoden består av fem steg:

1.  Identifiera organisationens processer och deras perspektiv.
2.  Identifiera vilka tillgångar som behövs för att tillgodose processernas behov.
3.  Formulera klartextfrågor som konkretiserar tillgångarna och relationerna mellan dem.
4.  Identifiera relationer mellan tillgångarna.
5.  Tilldela tillgångarna sina krav utifrån OIR.

För att kunna upprätta AIR kräv först att organisationens informationskrav (OIR), funktionella informationskrav (FIR) och tekniska informationskrav (TIR) är framtagna. Dessa krav ger underlaget för AIR.

![Kravkedja](../3.kravstallning_och_upphandling/media/steg_krav.png)

*Bild 2 - Schematisk bild över processen att identifiera informationsobjekt i relation till kraven i ISO 19650. (Obs. FIR och TIR är kompletterade enligt metoden Line of Sight – se Att skapa OIR)*

## Steg 1: Identifiera organisationens processer och deras perspektiv

Att identifiera organisationens processer och deras perspektiv görs utifrån strategierna och utförs enklast genom workshops eller intervjuer. Nedanstående punkter bör fastställas innan steg 2 startas.

1.  Identifiera de roller som har behov av information som behöver kravställas.

    *Exempel: ska informationen stödja underhållet, kärnverksamhetens drift eller ekonomiuppföljning så behöver dessa roller representeras.*

    1.  Identifiera dessa rollers arbetsprocesser som informationen ska stödja.

        *Exempel: Felanmälansprocess, arbetsprocesserna för de olika teknikområdena.*

    2.  Identifiera vilken typ av perspektiv som respektive roll betraktar anläggningen: är det funktion eller teknisk lösning man är intresserad av? Vanligtvis finns flera perspektiv inom varje process, exempelvis den faktiska verksamhet som ska bedrivas respektive de tekniska lösningar som behövs (verksamhetsfunktion respektive infrastrukturfunktion).

        *Exempel: verksamheten behöver en funktion för transporter från en sida ett hinder till den andra. En teknisk lösning kan vara en bro eller en tunnel med tillhörande system.*

    3.  Definiera och namnsätt de olika perspektiven. Detta är viktigt för att sedan kunna kommunicera vilket perspektiv man menar: verksamhetens funktioner, de tekniska lösningarna, eller de inköpta artiklarna .

        *Exempel: en pump kan ses utifrån funktionen att skapa ett vätskeflöde, som ett fysiskt objekt som behöver underhållas, eller som en artikel vars garantitid har gått ut.*

    4.  Förankra de olika perspektiven väl så att det finns ett tydligt ramverk att peka på när man börjar identifiera tillgångarna.

## Steg 2: Identifiera tillgångar

De tillgångar som man har behov att kravställa information om identifieras med fördel i workshops med olika roller representerade. Det går då att fånga de olika sätt man ser på sina tillgångar. Värdet av att göra det i workshop med olika roller är att det skapar insyn och förståelse för varandras behov och arbete. Individuella intervjuer är annars ett alternativ. Lämpligen utses en samordnande och ledande person som planerar och driver frågan.

-   Förbered väl med att ta fram olika exempel (stort och smått).
-   Ta fram underlag att diskutera kring, som ritningar, modeller, processcheman.
-   Gå igenom exemplen för att identifiera tillgångarna: vad behöver man information om?

*Exempel flygplats: titta på en översiktsbild och börja dela in det man ser. Landningsbana för att kunna landa och lyfta flygplan, terminal för att hantera passagerare och så vidare. Zooma in i var del och identifiera beståndsdelarna.*

## Steg 3: Formulera klartextfrågor som konkretiserar behovet

När tillgångarna är identifierade behöver de konkretiseras och kopplas till respektive perspektiv. Att konkretisera tillgångarna menas här primärt att identifiera om det är en funktion eller en teknisk lösning. En landningsbana är något fysiskt men också en funktion. Finns det behov av att beskriva dem ur båda perspektiven? Även detta är frågeställningar som fördelaktigast hanteras i workshop med flera discipliner, för diskussionen men också för att skapa förståelse för varandras behov.

-   Förbered klarspråksfrågor för att konkretisera tillgångarnas funktioner, tekniska lösningar, relationer, systemtillhörigheter och så vidare.   
    *Exempelvis en dörr kan ses som en produkt som sitter i ett väggsystem, men det kan också röra sig om en funktion för utrymmning som är relaterad till brandskyddet.*
-   Förbered med liknelser och enkla exempel som kan exemplifiera för att vidga deltagarnas perspektiv och förståelse för olika perspektiv.   
    *Exempelvis en bil behöver en styrfunktion, där den tekniska lösningen är en ratt, styrstag med mera.*
-   Genomför workshop och diskutera respektive rolls behov av att objektifiera anläggningen utifrån ett informationsperspektiv.

Exempel på klarspråksfrågor:

-   Är det funktioner eller produkter?
-   I vilken process har vi behov av information om tillgången?
-   Vilka objekt samverkar? Kan man se dem som ett system?
-   Vilka andra system relaterar objektet till?
-   Vilka behov har den här funktionen? (i bemärkelsen andra funktioner)

Det finns inget ett-till-ett-förhållande mellan funktion och teknisk lösning. Det kan vara en funktion som inte går att köpa som en produkt, utan som realiseras av flera olika produkter.

|          | Funktion | Teknisk lösning |
|----------|----------|-----------------|
|          |          |                 |
| Drivning | **X**    |                 |
| Drivaxel |          | **X**           |
| Motor    |          | **X**           |

*Tabell 1: Ett exempel på funktionen drivning som realiseras av två objekt.*

## Steg 4: Tilldela tillgångarna sina krav utifrån OIR

När tillgångarna väl är identifierade ska relevanta informationskrav knytas till dem. Informationskraven formulerade i OIR ger svar på vilken information som organisationen behöver om sin anläggning.

## Steg 5: Dokumentera informationskraven i EIR

Sista steget är att dokumentera detta med tydlig beskrivning hur informationen ska levereras. Hur ska den beskrivas i 3D-modeller, ritningar, dokument och så vidare, och hur refererar man mellan de olika formaten? Hur ska de som tar fram dokumentationen koppla ihop information om ett och samma objekt som beskrivs på flera ställen i?

Detta samlas i det ISO 19650 kallar informationsutbyteskrav, EIR.

![AIR](../3.kravstallning_och_upphandling/media/air.png)
