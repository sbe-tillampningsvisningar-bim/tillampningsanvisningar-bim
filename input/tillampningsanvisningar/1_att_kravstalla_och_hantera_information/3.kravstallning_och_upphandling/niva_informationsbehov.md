# Nivå av informationsbehov och informationskrav

För alla informationsutbyten behöver uppdragsgivaren fastställa lämplig **nivå för informationsbehov** (*level of information need, som ofta förkortas till LOIN*). Det vill säga vad är syftet med informationen, vad ska beskrivas, och hur detaljerat?

Att förstå, utveckla och kommunicera vilken sorts information och innehåll i leveranser av byggnads- och tillgångsinformation underlättas av att använda ett ramverk. Det ramverk som det som beskrivs i ISO 7817-1:2024 (Nivåer för informationsbehov – Del 1: Begrepp och principer, tidigare SS-EN 17412-1:2020) är användbart för detta syfte.

![LOIN Nationella Riktlinjer](../3.kravstallning_och_upphandling/media/nr_loin.png)

*Bild 1: Informell översättning av ramverk enl Nationella riktlinjer*

En informationsleverans svarar mot olika behov, om behoven kan förtydligas ökar möjlighet att kravställningens kvalitet och därmed även kvalitet och effektivitet i arbete med en leverans också blir bättre. Förenklat kan man se att det i ramverket finns ett antal frågor som skall besvaras för att kunna ange ett informationsbehov och slutligen en leverans: **varför**, **när**, **vem**, **vad** och **hur**.

I ISO 19650 utgår man ifrån beställarens strategiska och affärsmässiga ställningstagande i Organisationens informationskrav (OIR), som skall svara på den övergripande frågan **varför** informationen behövs. Projektinformationskraven (PIR) och Tillgångsinformationskrav (AIR) besvarar **vad** som skall levereras och **när,** på en generell nivå i projekt eller i förvaltningsprocesser.

När man kommit fram vad som skall levereras kan man i detalj specificera informationsinnehållets nivå i leveransen. Detta görs i Informationsutbyteskravet (EIR) som beskriver i detalj **hur** innehållet skall se ut, vilken typ av information. Tillsammans med Genomförandeplanen BIM som besvarar exakt **när** med sina leveransplaner.

Första stegen är att utgå ifrån förutsättningarna för leveransen och att reflektera över syfte, tidpunkt, vem som levererar och vad eller vilka objekt som skall levereras

-   Syfte: Varför behövs informationen?
-   Milstolpar: När behövs informationen utifrån projektplanen?
-   Aktörer: Vem levererar, vem bidrar och vem tar emot informationen?
-   Objekt: Vad ska levereras, i vilket format och vad för slags innehåll?

Idealet är att svaret på dessa frågor är tydliga för beställare och leverantör inför att man påbörjar arbetet med varje leverans och delleverans. Och att man systematiskt arbetar med att ta fram förutsättningarna genom att dokumentera sina informationskrav det vill säga OIR, PIR och AIR. I detta sammanhang kan nämnas att ”objekt” kan specificeras på olika sätt men när man talar om byggnads eller installationsdelar kan man använda sig av klassifikation exempelvis CoClass för att beskriva behoven på en generell nivå.

För att kunna detaljera nivån på information dela ramverket upp informationen i tre informationstyper med olika egenskaper att beskriva innehållet i informationstyperna.

-   **Geometrisk information**, som avser CAD/BIM-modellers sätt att beskriva objektens utseende och uppbyggnad
-   **Alfanumerisk information**, som handlar om beskrivningar av objekts egenskaper med hjälp av text och siffervärden
-   **Dokumentation**, till exempel produktdatablad, foton m.m.

Informationsbehov och informationskrav behöver vara välstrukturerade och med konsekvent namngivning av:

-   Objekt, genom klasser, typer och element
-   Attribut och egenskaper hos objekten (alfanumerisk information)
-   Informationscontainer (filer eller databasinnehåll)

Detta går att uppnå genom att exempelvis använda CoClass och/eller BIP-koder.

![Nivå av informationsbehov](../3.kravstallning_och_upphandling/media/loin.png)