# Att skapa informationsutbyteskrav (EIR)

## Inledning

Ett informationsutbyte består av ett krav, en leverans och en kontroll som utförs av mottagaren.

Informationsutbyteskrav (EIR) specificerar vilken information som ska levereras vid varje informationsutbyte. De upprättas för att uppfylla bestämda syften och vid bestämda tidpunkter, både i projekt (leveransskedet) och under drift och underhåll (användningsskedet). EIR ska definieras så detaljerat att de kan ingå i avtalet mellan parterna.

EIR bygger till stor del projektinformationskraven (PIR) och tillgångsinformationskraven (AIR). Man kan säga att EIR konkretiserar och detaljerar dessa mer övergripande krav.

EIR skapas och samordnas av uppdragsgivare och huvudansvarig uppdragstagare, men de kan också upprättas för informationsutbyte mellan uppdragsgivare och övriga uppdragstagare och mellan uppdragstagare.

EIR är relevant vid all typ av informationsutbyte oavsett om leveransen utgörs av CAD-modeller eller om informationen finns i någon annan form.

En EIR bör beskriva:

-   vilken information som behöver levereras för ett specifikt uppdrag
-   nivå och typ av information för varje krav på information
-   vilka kriterier som ska uppfyllas för att uppfylla varje informationskrav
-   vilka informationsunderlag som behöver vara på plats innan ett informationskrav kan uppnås
-   tidpunkter för leveranser och milstolpar

För att skapa en EIR kan man utgå ifrån dessa steg:

## Steg 1: Identifiera syften

Första steget är att ta in de informationskrav som redan finns formulerade i PIR och AIR. Utifrån dessa kan man få fram en del av olika syften som finns för informationsutbyten inom projektet. Uppdragstagarna kommer också ha egna syften vid informationsleveranser, ofta relaterat till utformning och projektering av en byggd tillgång. Dessa krav finns inte nödvändigtvis i PIR eller AIR.

Uppdragsgivarens syften kan vara

-   märkning av tillgångar (från AIR)
-   planerat underhåll (från AIR)
-   avstämning av driftskostnad (från AIR)
-   avstämning av projektkostnad mot budget (från PIR).

Den huvudansvarige uppdragstagarens syften kan vara

-   rumslig samordning
-   produktionsplanering
-   kalkyl
-   klimatdeklaration
-   energiberäkningar.

## Steg 2: Beskriv leveranser och leveranskedjor

För att ta fram EIR bör man förteckna alla leveranser i uppdraget. Identifiera även relationer mellan leveranser och delleveranser, liksom annat informationsutbyte mellan aktörer som behövs för att kunna färdigställa varje leverans.

Ett exempel på en leveranskedja kan vara ursprungsinformation i form av en strukturerad CAD-modell skapas (syfte A) från vilken information levereras till en annan uppdragstagare för att göra en simulering (syfte B) vars resultat skall vara underlag för ett beslut (syfte C).

När man skapar en förteckning av leveranserna utgå gärna ifrån nivå av informationsbehov (se nästa avsnitt) och nyckelfrågor så som **syfte** (varför?), **beslutspunkt** (när?), **aktör** (vem?) och **objekt** (vad?) (se även Steg 3).

![Informationsutbyte](../3.kravstallning_och_upphandling/media/eir.png)

*Bild 1: Exempel på informationsutbyte mellan tre aktörer.*

## Steg 3: Beskriv nivå av informationsbehov

En viktig del i EIR är att för varje leverans beskriva nivå av informationsbehov. Detta beskrivs utifrån tre aspekter:

-   **geometri**, som avser CAD-modellers sätt att beskriva objektens utseende och uppbyggnad
-   **alfanumerisk information**, som handlar om beskrivningar av objekts egenskaper med hjälp av text och siffervärden
-   kompletterande **dokumentation**, till exempel produktdatablad.

Informationen behöver vara välstrukturerad och med konsekvent namngivning, exempelvis genom CoClass och/eller BIP-koder av:

-   objekt (typer och element)
-   attribut och egenskaper hos objekten (alfanumerisk information)
-   informationscontainer (filer eller databasinnehåll).

Oavsett vad som levereras är en konsekvent struktur och namngivning viktig, exempelvis för att undvika oklarheter eller dubbletter av information hos filer eller attribut/egenskaper. Detta gäller både informationsutbyteskraven och själva leveransen.

Läs mer i dokumentet "Nivå av informationsbehov".

## Steg 4: Dokumentera informationsutbyteskravet

Informationsutbyteskravet ska dokumenteras strukturerat för att underlätta leveransplanering och genomförande samt att möjliggöra automatisk verifikation. Att använda tabeller eller en databas kommer underlätta detta.

-   Använd en databas eller tabeller för dokumentation om det inte finns specialiserat systemstöd.
-   Använd konsekvent namngivning och klassifikation av objekt och attribut.
-   Inkludera inte information som finns på annan plats.
-   Dokumentera relevanta krav. Undvik att beskriva grundläggande tillvägagångsätt, där man istället kan hänvisa till standarder eller vanlig branschpraxis.
-   Gör en bruttolista med krav där det går att filtrera ut till respektive syfte och aktör.
