# Standardiserad informationshantering i byggande och förvaltning

Tillförlitlig och lättillgänglig information är av stor betydelse för ägare av byggda tillgångar. Ju mer man vet om sitt innehav, desto lättare blir det blir det att få ut maximal nytta av dem.

Alltmer information som rör byggande och förvaltning hanteras digitalt. Målet med det har länge varit ett obrutet informationsflöde. Överlämning av information från en aktör till en annan ska inte behöva omtolkas, eller ens hanteras manuellt.

Digital informationshantering av byggda tillgångar benämns *byggnadsinformationsmodellering* (*BIM*). Den formella definitionen i ISO 19650 lyder:

**byggnadsinformationsmodellering (BIM)**  
*användande av en delad digital representation av en byggd tillgång för att underlätta processer för utformning, produktion, drift och underhåll i syfte att utgöra ett pålitligt underlag för beslut*

Här behövs några förtydliganden:

-   Med **delad** menas inte nödvändigtvis att alla lagrar data på samma ställe, utan att alla parter ska kunna hitta den information de behöver.
-   Med **digital representation** menas alla former av data, till exempel kartdata; rapporter och undersökningar; beräkningar; geometrier och annat i CAD-filer; förteckningar; tekniska beskrivningar.
-   Med **byggd tillgång** avses alla typer av byggnadsverk: hus, vägar, spårvägar, parker och så vidare.
-   Begreppet täcker in **hela livscykeln**, inte bara utformning och byggande. Eftersom drift och användning är betydligt mer omfattande – i både tid och pengar – är informationen som rör det skedet viktig att hålla i huvudet också i de tidigare skedena.

BIM är alltså inte enbart ”3D-CAD”, utan en metodik baserad på systematisk hantering av digital information. För att detta ska fungera praktiskt förutsätts bland annat:

-   att alla använder samma digitala språk för att identifiera och beskriva de objekt som hanteras
-   att informationen struktureras på ett överenskommet sätt
-   att öppna format används för överföring av information
-   att kommunikationen mellan aktörerna baseras på tydliga krav
-   att alla leveranser granskas av mottagaren innan de godkänns.

I dag finns alla dessa förutsättningar på plats:

-   Klassifikationssystemet **CoClass**, som är baserat på internationell standard, har klasser och egenskaper för allt inom byggd miljö, såväl fysiska som icke-fysiska företeelser. CoClass kan användas för:
-   att skapa informationsmodeller som beskriver hur sammansatta objekt är uppbyggda, och deras inbördes relationer
-   att ställa funktionella och/eller tekniska krav på alla nivåer av komplexitet
-   att ge alla relevanta objekt en unik identitet
-   att beskriva objektens funktion, konstruktion, läge och typ.
-   För överföring av geometriska data från CAD-modeller finns filformatet *IFC*, som också är internationell standard. Informationen i en IFC-fil kan inkludera CoClass.
-   Hur digital information ska hanteras finns beskrivet i **ISO 19650**, som är den grundläggande BIM-standarden.


Bilden nedan visar schematiskt hur informationen flödar i ett projekt som tillämpar ISO 19650.

![Standardiserad informationshantering](../1.introduktion/media/standardiserad_hantering1.png)

1.  Utgångspunkten är *tillgångsägarens mål*. Det kan till exempel vara ett politiskt mål att skapa goda kommunikationer i en stad. En utredning kommer fram till att ett stomnät av spårvägar ska byggas.
2.  Under programskedet ska uppdragsgivaren formulera *organisationens krav på information* (**OIR**) som ska komma ut från projektet. Informationen ska stödja de övergripande målen. För spårvägen kan det handla om tider för färdigställande och kostnader för produktionen, och därefter om användningsgrad, driftskostnader, kundnöjdhet och mycket annat.
3.  Den förvaltande organisationen ska också fastställa sina *krav på tillgångsinformation* (**AIR**), så att resultatet kan skötas så bra som möjligt. Exempel på informationskrav är drift- och underhållsinstruktioner, scheman över signalanläggningar, CAD-modeller som beskriver anläggningen.
4.  För att säkerställa leveransen av OIR och AIR förs de in i *projektets informationskrav* (**PIR**). Här blir de riktlinjer för de anlitade konsulterna, som kompletterar behovet av informationsöverföring mellan teknikområdena.
5.  PIR blir ytterligare konkretiserat i form av *informationsutbyteskrav* (**EIR**) för varje enskilt uppdrag som ingår i projektet.
6.  Nästa steg är att ta fram en projektanpassad *BIM-manual*. Förutom EIR innehåller den projektets milstolpar, en beskrivning av projektets informationsstandard, metoder och rutiner för informationsproduktion, uppgifter om referensinformation, anvisningar om *delad datamiljö* (**CDE**), samt informationsföreskrifter som beskriver rättigheter och skyldigheter som rör informationen.
7.  Referensinformation i form av beskrivningar av befintliga tillgångar, geodata, detaljplaner och annat görs tillgängligt för projektet.
8.  Den huvudansvariga uppdragstagaren svarar på BIM-manualen genom att ta fram en *genomförandeplan för BIM* (**BEP**). Här ingår en gränsdragningslista mellan deluppdragen, planer för informationsleveranser för varje deluppdrag (**TIDP**), och en huvudplan för informationsleveranser (**MIDP**) som sammanställer dessa.
9.  Under projekteringens gång förs information undan för undan in till den gemensamma *projektinformationsmodellen* (***PIM**), som lagras i den delade datamiljön. Varje leverans baseras på ett specifikt krav, och en kontroll görs av mottagaren innan den blir godkänd för användning. Objekt klassificeras och struktureras enligt CoClass, och leveranser av CAD-modeller görs i form av IFC-filer.
10. Dokument i form av beskrivningar, ritningar, CAD-modeller och annat publiceras för användning i byggproduktionen.
11. När projektet är färdigställt överlämnas en *tillgångsinformationsmodell* (**AIM**). Information som enbart berörde produktionen kan då vara bortrensad, och information som är relevant för drift och underhåll ska vara tillförd.
12. Dokument i form av drifts- och underhållsinstruktioner, produktblad, garantibevis och annat kan hämtas från AIM.
13. Under användningen kan information delges tillgångsägaren, baserat på de ursprungliga kraven i OIR.

Den här samlade bilden kan verka överväldigande, men skiljer sig faktiskt inte praktiskt så mycket från det arbetssätt som oftast används vid större byggprojekt. Den stora skillnaden handlar inte så mycket om de enskilda aktörernas arbetssätt, utan om uppdragsgivarens styrning av processen.

Alla informationsleveranser ska:

-   vara baserade på krav från uppdragsgivaren/mottagaren, inte på vad uppdragstagaren/avsändaren ”tror” är rätt, eller ”hur man brukar göra”
-   kontrolleras av uppdragsgivaren/mottagaren innan de godkänns för användning.

Hur detta konkret ska gå till ska fastställas i dialog mellan uppdragsgivaren och den huvudansvariga uppdragstagaren.

-   Uppdragsgivaren beskriver alla sina krav i projektanpassade *BIM-föreskrifter*.
-   Det gemensamma arbetssättet dokumenteras i en *genomförandeplan för BIM* (**BEP**). Den blir grunden för allt informationsutbyte mellan parterna, från start till mål.

Om projekt genomförs som visas ovan så förbättras kommunikationen i projektet, antalet missförstånd minimeras, och effektiviteten ökar. Den stora vinsten finns dock i användningsskedet i och med att rätt information finns på plats redan från början.
