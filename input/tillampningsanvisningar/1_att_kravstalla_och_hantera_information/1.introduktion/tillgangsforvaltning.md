# Tillgångsförvaltning


Detta dokument beskriver tillgångar utifrån tre internationella standarder:

-   SS-EN ISO 19650-1:2019 *Organisering och digitalisering av information för byggd miljö, innefattande byggnadsinformationsmodellering (BIM) –Informationshantering genom byggnadsinformationsmodellering – Del 1: Begrepp och principer*
-   ISO 19650-3:2020 *Strukturering av information om byggd miljö – Informationshantering genom byggnadsinformationsmodellering – Del 3: Användningsskede*
-   SS-ISO 55000:2014 *Ledningssystem för tillgångar – Översikt, principer och terminologi*

Den grundläggande definitionen av **tillgång** finns i ISO 55000:

> **tillgång**  
> sak, enhet eller entitet med potentiellt eller faktiskt värde för en organisation
> 
> Anm. 1 till termpost: Värdet kan antingen vara materiellt, immateriellt, finansiellt eller icke-finansiellt, och omfattar hänsyn till risker och förpliktelser. Värdet kan vara positivt eller negativt i olika stadier under tillgångens livstid.
> 
> Anm. 2 till termpost: Fysiska tillgångar avser vanligen utrustning, inventarier och egendom som ägs av organisationen. Fysiska tillgångar är motsatsen till immateriella tillgångar, som är icke-fysiska tillgångar som hyreskontrakt, varumärken, digitala tillgångar, nyttjanderätter, licenser, immaterialrätter, anseende eller avtal.
> 
> Anm. 3 till termpost: En gruppering av tillgångar som kallas ett tillgångssystem kan även betraktas som en tillgång.

I ISO 55000 finns också definitionen av hur tillgångar förvaltas:

> **förvaltning av tillgångar**  
> *en: asset management*  
> koordinerade åtgärder i en organisation för realisering av värde från tillgångar
> 
> Anm. 1 till termpost: Realisering av värde omfattar normalt en avvägning mellan kostnader, risker, möjligheter och nyttan med prestanda.
> 
> Anm. 2 till termpost: Åtgärder kan även avse tillämpningen av delarna i ledningssystemet för tillgångar.
> 
> Anm. 3 till termpost: Termen ”åtgärd” har en bred betydelse och kan till exempel omfatta metod, planering, planer och dessas införande.

Förvaltning av tillgångar – som vi här benämner **tillgångsförvaltning** – är ett strategiskt systematiskt förhållningssätt för organisationer att maximera värdet av alla sina tillgångar. Syftet är att säkerställa att tillgångar uppfyller de nödvändiga funktionerna på ett effektivt och hållbart sätt genom hela dess livscykel. Det inkluderar utveckling, koordinering och styrning av aktiviteter som rör tillgångarnas förvärv, användning och underhåll, samt eventuell avveckling.

ISO 55000 uppmuntrar till inrättandet av en strategisk plan och ett ledningssystem för förvaltning av tillgångar som balanserar kostnader, möjligheter och risker för att nå organisationens övergripande mål.

Tillgångar är alltså ett vitt begrepp, som omfattar både materiella och immateriella värden. ISO 19650 behandlar information om en avgränsad del av allt detta. I Del 3 finns följande definition, citerad från SS-EN ISO 41011:2024 *Facility Management – Termer och definitioner* (här i egen svensk översättning):

> **anläggningsförvaltning**  
> **fastighetsförvaltning  
> anläggnings- och fastighetsförvaltning**  
> *en: facility management*  
> organisatorisk funktion som integrerar människor, plats och process inom den byggda miljön i syfte att förbättra människors livskvalitet och produktiviteten i kärnverksamheten.

Den engelska termen ”facility” är mångtydig och svåröversatt, men avser i det här fallet de ”byggda tillgångar” som behövs för organisationens verksamhet: byggnader med sina installationer och anläggningar i form av vägar, järnvägar, broar och så vidare. På något ställe också benämns detta ”infrastrukturtillgångar”. I ISO 55000 motsvarar detta ”utrustning, inventarier och egendom”.

Lämpliga svenska översättningar blir då termen **fastighetsförvaltning** för byggnader respektive **anläggningsförvaltning** för andra typer av byggnadsverk. Som samlingsterm kan **anläggnings- och fastighetsförvaltning** användas.

ISO 19650 hanterar alltså inte immateriella värden, tjänster eller andra typer av tillgångar som är viktiga för organisationen. Men syftet är att “BIM enligt ISO 19650” ska ge bidra till det större perspektivet tillgångsförvaltning. Här behövs information om de fysiska tillgångarna, som tillsammans med övrig information kopplad till kärnverksamheten skapar värde och ger underlag för analyser och beslut. Även det omvända gäller: processerna i det större perspektivet ställer krav på de fysiska tillgångarnas omfattning, prestanda, kostnader för drift och underhåll, miljöpåverkan och mycket annat.

Information som stödjer tillgångsförvaltning enligt ISO 55000 ska också stödja de processer som strategin för tillgångsförvaltning innefattar. Den strategiska planen för förvaltning av tillgångar (*Strategic Asset Management Plan*, SAMP) ska ange hur organisationens mål ska omvandlas till mål för förvaltning av tillgångar. Man kan alltså säga att organisationens informationskrav enligt ISO 19650 (OIR) är den del av SAMP som behandlar de byggda tillgångarna.

![ISO 55000 enl. TCG](../1.introduktion/media/iso55000_tcg.png)

*Bild 1: TCG:s tolkning av ISO 55000, baserad på Institute of Asset Management (www.theaim.com) från 2014.*

**Anläggnings- och fastighetsförvaltning** är alltså en ägares långsiktiga drift och underhåll av sina byggda tillgångar i syfte tillgängliggöra dem för den överordnade kärnverksamheten. Det omfattar inte kärnverksamheten inom exempelvis processindustri, där den produktion som bedrivs ibland också benämns “drift”.

Utöver den tekniska förvaltningen av de byggda tillgångarna omfattas också administrativa och ekonomiska aspekter. Detta pågår under det som SS-EN ISO 19650 benämner **användningsskedet**. Det är då som tillgången används, driftas och underhålls.

I båda standarderna betonas att syftet med tillgångsförvaltningen är att bidra till organisationens mål. För att uppnå det krävs information på alla nivåer – från övergripande till detaljerad – som försörjer ledningssystemet för tillgångar med rätt information som underlag för beslutsfattande.

## Hur skapas tillgångar?

Tillgångsförvaltningen för ett specifikt byggnadsverk kan startas vid förvärv av en redan byggd tillgång. Alternativt startar den som ett resultat av att någon låter bygga något, till exempel ett företag som investerar i en ny byggnad för huvudkontoret. De senare benämns **leveransskedet,** då tillgången utformas, produceras och överlämnas.

Leveransskedet bedrivs som ett eller flera **projekt**, där en uppdragsgivare avtalar med en eller flera uppdragstagare i form av konsulter och/eller entreprenörer beroende på upphandlingsform. Det här beskrivs i SS-EN ISO 19650-2.

Detsamma gäller under användningsskedet. Även här har vi uppdragsgivare och uppdragstagare. Primärt handlar det om att en förvaltare – intern eller extern – är utsedd att sköta drift och underhåll. Ibland händer dock saker under användningsskedet som gör att man skapar ett projekt, till exempel för en större underhållsåtgärd eller en om- eller tillbyggnad. Ett sådant projekt ligger utanför ordinarie drift och underhåll; det blir i all praktisk mening identiskt med ett nybyggnadsprojekt. Det här beskrivs i SS-EN ISO 19650-3.

SS-EN ISO 19650 gör mycket liten skillnad mellan användningsskedet och leveransskedet. Vikten av att fastställa informationsbehovet är lika viktigt i båda. Användningsskedet är också tidsmässigt långt större än leveransskedet. För stora tillgångsägare kan man kan rentav se ett projekt i form av nybyggnad eller tillbyggnad som en störning i förvaltningen.

![Tillgångsförvaltning process](../1.introduktion/media/tillgangsforvaltning.png)

*Bild 2: Tillgångsförvaltning, där ett projekt kan vara inledningen, och därefter uppstå flera gånger under användningsskedet.*

## Varför är strukturerad informationshantering viktigt?

Relevant och välstrukturerad information är en viktig framgångsfaktor i förvaltningen. Ju mer effektivt förvaltningen bedrivs, desto större nytta ger tillgångarna. Vilken information som behöver finnas tillgänglig under användningsskedet ska kravställas på två nivåer:

-   Den första är vad organisationens ledning behöver veta om sina tillgångar: kapacitet, kvalitet, driftskostnader med mera. Detta betecknas som **organisationens informationskrav** (OIR).
-   Nästa nivå handlar om vad förvaltningsorganisationen praktiskt behöver veta för att på bästa sätt driva och underhålla tillgångarna. Detta betecknas **tillgångsinformationskrav** (AIR).

Båda dessa innehåller viktig input när ett projekt behöver inledas respektive när drift och underhåll behöver handlas upp. För varje projekt eller åtgärd görs en bedömning av vilken ny information som ska produceras.

## Hur påverkar informationskraven genomförandet av projekt?

Under tillgångsförvaltningen uppdateras informationen om tillgångarna löpande, till exempel i form av dokumentation av underhållsåtgärder, uppföljning av driftdata, reparationer och utbyte av komponenter.

När ett projekt drar i gång – oavsett om det handlar om nyproduktion eller under användningsskedet av en befintlig tillgång – ska informationen flöda enligt Bild 2.

![Livscykeln och ISO 19650](../1.introduktion/media/livscykeln_och_iso_19650_20240115.png)

*Bild 3: Informationsflödet in och ut ur projekt. Med ”specificerar” avses här ”fastställer innehåll, struktur och metodik”. Skedena 0–4 är hämtade från beskrivningen av livscykeln i Nationella Riktlinjer. Källa: SS-EN ISO 19650-1, Figur 2, modifierad och kompletterad.*

Om en utredning kommit fram till att ett projekt ska genomföras ska kraven fastställas under programskedet. Både utredningen och programmet bidrar då till OIR, som i sin tur ger underlaget för AIR. Båda dessa ger sedan underlag för **projektinformationskraven** (PIR) och **informationsleveranskraven** (EIR). EIR specificerar hur **projektinformationsmodellen** (PIM) ska utföras, och på motsvarande sätt specificerar AIR hur **tillgångsinformationsmodellen** (AIM) ska fyllas på med information från projektet.

## Hur gör man som tillgångsägare?

Det är alltså viktigt att tillgångsägaren ger direktiv till de projekt som ska genomföras, så att den producerade informationen blir maximalt nyttig under användningsskedet. Här kan du läsa mer om:

- [Att skapa organisationens informationskrav (OIR)](https://tillampningsanvisningarbim.kravportal.se/tillampningsanvisningar_bim/att-skapa-oir/) 
- [Att skapa tillgångsinformationskrav (AIR)](https://tillampningsanvisningarbim.kravportal.se/tillampningsanvisningar_bim/att-skapa-air/)
- [Leveransskedet](https://tillampningsanvisningarbim.kravportal.se/tillampningsanvisningar_bim/leveransskedet/)
- [Användningsskedet](https://tillampningsanvisningarbim.kravportal.se/tillampningsanvisningar_bim/anvandningsskedet/)
