# Informationsmodellering och informationsstrukturering

## Inledning

Effektiv tillgångsförvaltning är beroende av korrekt och tillgänglig information. Ofta handlar det om stora mängder data som behöver sammanställas för att göras enkelt sökbar och lätt att förstå. Två grundstenar i detta är **informationsmodellering** och **informationsstrukturering**.

**Informationsmodellering** handlar om att skapa ett schema för hur informationen ska organiseras, och hur olika delar av information hänger ihop med varandra. Ett exempel är klassifikationsstandarden ISO 12006-2, som innehåller ett schema som visar hur byggd miljö kan beskrivas med hjälp av ett antal begrepp och deras relationer:

![Informationsmodell](../1.introduktion/media/informationsmodell.png)

*Bild 1: Informationsmodell som beskriver byggd miljö. Fet linje visar relationen ”typ av”. Källa: SS-EN ISO 12006-2:2015, Figure 1.*

**Informationsstrukturering** är steget där man tar schemat och börjar fylla i den med faktisk information. Det handlar om att organisera och sortera informationen så att den blir lätt att hitta och använda. Sorteringen anpassas till vilken typ av information det handlar om, och kan göras på olika sätt. Som exempel kan byggdelar sorteras – det vill säga klassificeras – baserat på exempelvis funktion, form, läge, material, teknikområde eller vilken typ av byggnadsverk de ingår.

Strukturering innebär att data organiseras på ett fastställt sätt som gör det möjligt att söka eller göra urval på ett enhetligt sätt. Strukturen kan vara hierarkisk, till exempel genom att byggdelar beskrivs som system, eller genom att data i filer är märkta (”taggade”) så att man kan söka efter specifika typer av information. Ett textdokument utan rubriker är ostrukturerat, medan en xml-fil har taggar som beskriver varje enskilt avsnitt i dokumentet

Informationsmodellering och informationsstrukturering är alltså två steg i samma process. Först skapar man en övergripande plan med informationsmodellering. Sedan använder man informationsstrukturering för att fylla i planen med konkret information.

#### Lite om modeller

Vad är då en **modell**? En bra definition finns i Rikstermbanken: ”förenklad representation av verkliga eller tänkta företeelser” (TNC).

Representationen kan ta sig många former. Den kan vara en fysisk modell i valfri skala, från 1:1 och neråt beroende på hur stora företeelser som ska representeras. Detta är en ofta överlägsen metod för mänsklig tolkning, där man enkelt ser hur stora företeelserna är, och hur de förhåller sig till varandra och kanske till sin omgivning.

Närbesläktad med denna är en virtuell fysisk modell – en VR-modell (*virtual reality*) – där betraktaren med hjälp av bildskärm vid datorn eller i VR-glasögon som ger en ”omslukande” upplevelse kan röra sig runt i exempelvis en byggnad. Grunden för detta är en 3-dimensionell CAD-modell, som försetts med texturer, ljussättning, skuggor och så vidare.

![Exempel VR-modell](../1.introduktion/media/vr_exempel.png)

*Bild 2: Exempel på VR-modell. Källa: https://sketchfab.com.*

Nästa steg i förenkling är en CAD-modell, som i 2D eller 3D beskriver geometriska objekt. Objekten kan också förses med alfanumerisk information som beskriver andra egenskaper, och med länkar som kopplar dem till andra datakällor.

![Exempel 3D CAD-modell](../1.introduktion/media/3d_cad.png)

*Bild 3: Enkel 3D CAD-modell.*

En modell kan också vara en ritning: en grafisk representation av en fysisk företeelse. Det kan vara allt ifrån en skiss på en servett till en detaljerad och skalriktig ritning med linjer, symboler, färger och andra inslag.

Vidare kan en modell vara alfanumerisk, alltså bestå enbart av text och siffror. Frasen ”liten faluröd stuga med vita knutar” kan sägas vara en modell som i ord beskriver det lilla huset på landet. Mer utvecklat kan texten utgöra ett program för en byggnad, som mer eller mindre detaljerat beskriver målet för ett byggprojekt: hur stora ytor behövs, vilken utrustning, hur ska det se ut, och så vidare.

En sådan ostrukturerad textform är enkel att åstadkomma och användbar i tidiga skeden, men kräver en hel del tolkning för att bli underlag för detaljutformning, produktion, drift och underhåll.

Mer strukturerad alfanumerisk information finns i beräkningsmodeller som görs i kalkylprogram, till exempel för att göra mängdberäkningar inför inköp eller klimatdeklaration. En sådan fil kan alltså sägas vara en informationsmodell.

## Informationsmodellering

Informationsmodellering är alltså en strukturerad metod för att definiera och analysera data och dess relationer inom ett visst område. En definition finns i SS-EN ISO 10209:2022<sup>1</sup> (i egen översättning):


**informationsmodell**  
\<metadata\> konceptuell modell som beskriver en specifik organisation av data som möjliggör kommunikation för en viss tillämpning

<sup>1</sup> SS-EN ISO 10209:2022 *Teknisk produktdokumentation – Termer och definitioner – Termer för tekniska ritningar, produktdefinitioner och dokumentation*.

#### Komponenter i informationsmodellering

Informationsmodellering bygger på följande delar:

1.  **Dataenheter**, som är de primära objekten som information samlas in om. I en byggkontext kan dataenheter inkludera byggnader, utrymme, material eller utrustning.
2.  **Attribut**, som ger detaljer om dataenheterna. För ett utrymme kan attributen inkludera dess höjd, golvbeläggning och inredning.
3.  **Relationer** som beskriver hur dataenheter är relaterade till varandra. Exempel: ett körfält är en del av en vägöverbyggnad, och en balk stödjer ett bjälklag.
4.  **Begränsningar** som lägger restriktioner på värdena av attribut eller relationer för att säkerställa dataintegritet. Detta kan innebära att antalet våningar i en byggnad måste vara ett positivt heltal.
5.  **Schema** som ger den övergripande strukturen av informationsmodellen genom att definierar hur enheter, attribut och relationer är organiserade och sammankopplade.

Ett schema kan bestå av en informationsstruktur. Mer om detta längre ner.

#### Metoder för informationsmodellering

Informationsmodellen kan utvecklas steg för steg:

1.  **Konceptuell modellering**, som innebär att på hög nivå definiera abstrakta representationer av informationen, med fokus på de viktigaste enheterna och deras relationer utan att fördjupa sig i detaljerade attribut eller databasspecifikationer.
2.  **Logisk modellering**, där den konceptuella modellen förfinas till en mer detaljerad modell, inklusive specifika attribut och typerna av data (till exempel text, heltal, datum) som varje attribut kommer att hålla.
3.  **Fysisk modellering**, som översätter den logiska modellen till en fysisk struktur som kan implementeras i ett specifikt databashanteringssystem. Det inkluderar definitionen av tabeller, primära och främmande nycklar samt index.

Benämningen ”fysisk” ska tas med en nypa salt; snarare handlar det om den praktiska implementeringen av modelleringen i en databas.

#### Tillämpningar inom byggindustrin

Inom byggindustrin är informationsmodellering avgörande för effektiv hantering och genomförande av byggprojekt. Det stödjer olika aktiviteter, inklusive:

-   Projektplanering och projekthantering, i syfte att visualisera projektet med avseende på resurser, tidsplaner och beroenden, vilket möjliggör effektivare planering och hantering.
-   Samordning av utformning, för att underlätta samarbetet mellan arkitekter, ingenjörer och entreprenörer, och säkerställer att designen är sammanhängande och att konflikter löses innan byggandet börjar.
-   Simulering och analys, till exempel klimatdeklaration, energiprestanda, strukturanalys, för att optimera utformningen och driften av byggprojektet.
-   Byggledning, för logistik, schemaläggning och övervakning av framsteg för att säkerställa att projektet håller sig på rätt spår.
-   Fastighetsförvaltning: efter leverans ger informationsmodeller en detaljerad representation av byggnaden för underhåll, renovering och driftseffektivitet.

#### Byggnadsinformationsmodellering (BIM)

Byggnadsinformationsmodellering (BIM) är en specifik tillämpning av informationsmodellering inom bygg, med fokus på den digitala representationen av fysiska och funktionella egenskaper hos platser. BIM sträcker sig bortom ritningar och omfattar rumsliga relationer, ljusanalys, geografisk information samt kvantiteter och egenskaper hos byggdelar. Det underlättar samverkan kring digital information, vilket förbättrar samarbetet mellan alla intressenter i ett byggprojekt.

#### Slutsats

Informationsmodellering fungerar som en grundläggande metod för modern datahantering och analys, och erbjuder en strukturerad ram för att organisera, förstå och effektivt använda data. Inom byggsektorn har informationsmodellering genom BIM revolutionerat hur projekt planeras, genomförs och hanteras, vilket leder till ökad effektivitet, minskade kostnader och förbättrade resultat.

## Informationsstrukturering

Informationsstrukturering handlar om att organisera och arrangera data på ett systematiskt sätt så att den blir lättillgänglig, begriplig och därmed användbar. Alla beståndsdelar i företeelsen som ska beskrivas behöver identifieras, och relationerna mellan delarna behöver identifieras.

Det krävs alltså två grundläggande steg i informationsstrukturering:

1.  **Klassificera** alla delar: helheten, de eventuella system den består av, och de komponenter som ingår i systemen. Här handlar det om en kategorisering som är relevant för behovet. Resultatet blir en **typ-av-struktur**.
2.  Gör en eller flera **nedbrytningar** av helheten som beskriver relationer mellan helhet, system och delar. Här är syftet att visa olika typer av hierarkier mellan beståndsdelarna: en **del-av-struktur**.

Resultatet blir en beskrivning av företeelsen där varje del är klassificerad, och där relationerna mellan delarna framgår.

Hur detta bör gå till i byggande och förvaltning beskrivs i SS-EN ISO 12006-2:2020 *Strukturering av information om byggnadsverk – Del 2: Ramverk för klassificering (av information)*. Både svenska, andra nationella och internationella klassifikationssystem har denna som grund: BSAB 96, CoClass, Uniclass, Omniclass, IEC/ISO 81346-12 och andra.

I ISO 12006-2 används termerna ”klassifikation” (typ-av) respektive ”kompositionell strukturering” (del-av) för att beskriva de två stegen i informationsstrukturering. Parallellt används termerna ”klassifikationshierarki” respektive ”kompositionshierarki” för att beteckna samma sak.

(Standarden är när detta skrivs under omarbetning, och mycket talar för att man i stället kommer att använda termerna ”klassifikationsstruktur” respektive ”nedbrytningsstruktur”. Det sistnämnda är ett mer flexibelt begrepp, som beskrivs mer i nästa avsnitt.)

![Klassifikationshierarki](../1.introduktion/media/hierarki.png)

*Bild 4: Illustration av en klassifikationshierarki och en kompositionshierarki. Källa: SS-EN ISO 12006-2:2020, Figure 2.*

Många alternativa sätt att beskriva grunderna i informationsstrukturering går till, men förhoppningsvis är det begripligt. En enkel sammanfattning lyder: **vilka delar finns, och hur hänger de ihop?**

#### Kedja av modelltyper

Ett annat sätt att beskriva behovet av att informationshanteringen i en verksamhet görs på ett systematiskt sätt visas i Bild 2.

![Kedjan av modelltyper](../1.introduktion/media/kedja.png)

*Bild 5: Kedjan av modelltyper.*

1.  En gemensam **begreppsmodell** är utgångspunkten. Den säkerställer att alla har samma syn på vad som menas när någon använder ett visst begrepp. Det är här ett tydligt och överenskommet klassifikationssystem har sin roll.
2.  **Informationsmodellen** beskriver sedan innehållet i beskrivningen av de aktuella företeelserna.
3.  Baserat på denna kan data sedan lagras i en **datamodell**, som har en tydlig informationsstruktur.

En begreppsmodell kan vara mer eller mindre avancerad. I sin enklaste form är det en ordlista, men den kan också beskrivas i form av en ontologi som beskriver relationen mellan begreppen.


#### Klassifikation

I grunden är klassifikation inte komplicerat. Att klassificera innebär att sortera in objekt i grupper, baserat på en eller flera egenskaper som är relevanta för sammanhanget. En sådan samling av objekt kan kallas klass eller typ eller grupp; det spelar mindre roll. I CoClass används ”klass” för grupper av objekt som primärt förenas av att de har samma funktion. ”Typ” används för att i ett nästa steg beskriva objekt som har ytterligare någon egenskap, vanligen att de har en viss teknisk lösning.

Att åstadkomma ett ”perfekt” klassifikationssystem är inte lätt att göra. De flesta system som har med byggande och förvaltning att göra är enkelt uppbyggda, och förtecknar grupper av objekt som är relevanta i form av system, komponenter, produktionsresultat, produkter och annat.

En mer strikt syn på klassifikation finns i IEC/ISO 81346 del 2 och 12, som CoClass tabeller för byggdelar är baserade på. De är unika på det sättet att alla klasser är tydligt definierade, och att de flesta är baserade på objektets funktion. Det här ger en stor fördel när man vill beskriva en företeelse över hela dess livscykel. Så länge behovet av en viss funktion finns, kan objektet som uppfyller funktionen identifieras. Den tekniska lösningen i form av en viss produkt eller konstruktion kan variera över tiden, men den klassificerade funktionen består.

Men oavsett vilket eller vilka klassifikationssystem man använder är det grundläggande att objekten i en informationsstruktur blir identifierade på ett tydligt och användbart sätt.


#### Nedbrytning

Så: när man väl bestämt vilket klassifikationssystem som ska användas blir nästa steg att beskriva hur helheten ska brytas ner i sina beståndsdelar.

I sin enklaste form innebär nedbrytning av ett objekt att man beskriver hur delarna sitter ihop rent fysiskt i form av en kompositionshierarki. Men det finns andra sätt att bryta ner en komplex företeelse.

En viktig standard som beskriver metoder för nedbytning är SS-EN IEC 81346-1, utg 2:2023 *Struktureringsprinciper och referensbeteckningar – Del 1: Grundläggande regler*. Här kallas det för **aspekter**, som kan användas för att på olika sätt beskriva objekt:

-   **Funktionsaspekten**: vad ett objekt är avsett att göra, eller vad det i verkligheten gör. Den visar de funktionsmässiga relationerna mellan objektets komponenter. Resultatet är en **funktionell nedbrytning**.
-   **Produktaspekten**: med vilka medel objektet gör vad det är avsett att göra. Den visar de konstruktionsmässiga relationerna mellan objektets komponenter, alltså dess sammansättning eller komposition. Resultatet är en **konstruktiv nedbrytning**.
-   **Lokaliseringsaspekten**: avsedd eller verklig lokalisering och vid behov placering av objektet. Den visar de rumsliga relationerna mellan objektets komponenter. Resultatet är en **rumslig nedbrytning**.
-   **Typaspekten**: objektets tillhörighet till en definierad typ, där ”typ” definieras som ”uppsättning objekt med en definierad uppsättning gemensamma kännetecken”. Den används inte för nedbrytning utan för beskrivning av de enskilda delarna.

Typaspekten är alltså inte relevant för nedbrytning av en komplex företeelse, men de övriga är det i hög grad.

-   **Funktionsaspekten** är grundläggande för att visa vilka system som behövs för att åstadkomma ett byggresultat, och för att beskriva hur systemens delar samverkar. Exempel:  
    ![Funktionell nedbrytning](../1.introduktion/media/funktionell_nedbrytning.png)

*Bild 6: Förenklat exempel på funktionell nedbrytning av system i en vårdbyggnad.*

-   **Produktaspekten** används för att visa hur funktionerna realiseras med hjälp av fysiska system och komponenter. Vissa av dessa kan ha flera funktioner. Exempel:  
    ![Produktaspekten](../1.introduktion/media/produktaspekten.png)

*Bild 7: Förenklat exempel på konstruktiv nedbrytning av ett luftbehandlingsaggregat.*

-   **Lokaliseringsaspekten** kan användas för att beskriva de rumsliga sambanden i en byggnad, till exempel för att bryta ner en sjukhusbyggnad i vårdavdelningar med deras olika typer av utrymmen. Exempel:  
    ![Lokaliseringsaspekten](../1.introduktion/media/lokaliseringsaspekten.png)

*Bild 8: Förenklat exempel på rumslig nedbrytning av en vårdbyggnad.*

#### Vertikala och horisontella relationer

En nedbrytning av en företeelse kan sägas vara vertikal: man ”borrar sig ner” djupare i strukturen för att visa allt finare detaljer. Man kan kalla detta för **vertikala relationer**. Metoden är användbar för att beskriva relationen mellan helhet och delar, till exempel för någon som ska dimensionera, bygga, drifta eller underhålla ett byggnadsverk.

Dessa detaljer blir mindre viktiga för någon som sitter i verksamhetsledningen för en större organisation. Här behöver man i stället överblick över samtliga tillgångar på en aggregerad nivå, och hur dessa hänger ihop. Detta kan kallas **horisontella relationer**.

Sådana relationer beskriver till exempel hur det svenska vägnätet är uppbyggt, eller hur delarna i vårdkedjan av länkar i form av primärvård, privat sjukvård, sjukhusvård och specialistvård hänger ihop. På detaljnivå handlar de horisontella relationerna exempelvis om hur en operationsavdelning består av ett antal sammanlänkade funktionella utrymmen: operationssalar, förråd, desinfektionsutrymme, personalutrymmen med flera.

Alla dessa realiseras av tekniska lösningar som beskrivs och bryts ned utifrån vertikala relationer när man betraktar dem utifrån ett bygg- eller underhållsperspektiv.

**Det är viktigt att komma ihåg att klassifikationssystem för bygg hanterar enbart de vertikala relationerna.** För övergripande verksamhetsstyrning behövs andra metoder, som ligger utanför ämnet för både detta dokument liksom för BIM. Relevanta standarder inom detta område är exempelvis till exempel ISO 55000 *Ledningssystem för tillgångar* och ISO 9001 *Ledningssystem för kvalitet*.

#### Kompletterande informationsstrukturering

Utöver klassifikation och nedbrytning kan ytterligare nivåer av beskrivning läggas till en informationsstruktur. Man kan bland annat:

-   göra en **sekvensering**, där man till exempel beskriver kronologisk ordning mellan händelser som påverkar företeelsen
-   lägga på **metadata** som på kompletterande sätt beskriver objekten, till exempel status och ägare
-   göra **korslänkar** mellan objekten som visar andra relationer än del-av, till exempel ”försörjer-försörjd” av för att visa att ett objekt på något sätt är försörjt av ett annat.

Alla dessa exempel kan utgöra viktiga delar i BIM, men kräver att data kan lagras i relationsdatabaser.

#### Slutsats

Strukturering är grundläggande för effektiv hantering och kommunikation av information. Genom att organisera information på ett strukturerat sätt kan organisationer förbättra tillgängligheten, användbarheten och värdet av sina data. Detta är avgörande i den digitala tidsåldern där stora mängder information genereras och konsumeras. Korrekt strukturerad information kan leda till förbättrat beslutsfattande, förbättrat lärande och mer effektiva processer.

## Sammanfattande slutsatser

Informationsmodellering och informationsstrukturering arbetar tillsammans för att effektivt organisera och representera data och information. Att förstå sambandet mellan dessa två koncept är avgörande för att utveckla robusta informationssystem där komplexa data behöver organiseras och användas effektivt.

#### Informationsmodellering

Informationsmodellering innebär att skapa abstrakta representationer av ett systems data och relationerna mellan olika delar av denna data. Det är en mer holistisk ansats som omfattar förståelsen för de typer av data som är inblandade, hur dessa dataelement interagerar med varandra, och hur de kan organiseras för att stödja systemets krav. Informationsmodeller används ofta för att konceptualisera och planera strukturen på databaser, databashanteringssystem och för den övergripande arkitekturen av informationssystem.

Byggnadsinformationsmodellering (BIM) en typ av informationsmodellering som ger en digital representation av de fysiska och funktionella egenskaperna hos en anläggning. BIM omfattar inte bara de geometriska eller spatiala elementen utan också relaterade data som rör utformning, byggande och driftsfasen i byggnadens livscykel.

#### Informationsstrukturering

Informationsstrukturering å andra sidan fokuserar på att organisera och arrangera data inom ramen för den informationsmodell som tillhandahålls. Det innebär att skapa kategorier, definiera hierarkier, fastställa metadata och bestämma relationerna mellan olika informationselement för att säkerställa att den är tillgänglig, förståelig och användbar.

I den digitala representationen av en byggnad inom ett BIM-system skulle informationsstrukturering innebära hur data om olika byggdelar (som väggar, fönster, installationer) organiseras, kategoriseras och relateras inom databasen. Detta inkluderar strukturering av egenskaper (som material, dimensioner, kostnader), relationer (hur komponenter passar ihop eller interagerar) och annan metadata som förbättrar användbarheten av informationsmodellen.

#### Relationen mellan informationsmodellering och informationsstrukturering

Relationen mellan informationsmodellering och informationsstrukturering kan ses som en progression från det konceptuella till det praktiska. **Informationsmodellering** börjar med en bred vy, som fastställer vilken information som behövs och hur den konceptuellt relaterar inom systemet. Det lägger grunden genom att definiera omfånget och gränserna för den information som ska hanteras.

När modellen är etablerad kommer **informationsstrukturering** in i bilden för att detaljerat beskriva hur denna information organiseras, lagras och nås inom ramen för modellen. Det innebär att tillämpa modellen på verkliga data och säkerställa att informationen presenteras på ett sätt som är logiskt, effektivt och i linje med användarens behov.

I grund och botten tillhandahåller informationsmodellering ritningen, och informationsstrukturering bygger infrastrukturen inom den ritningen för att göra informationssystemet operativt och effektivt. Båda är avgörande för att skapa informationssystem som är robusta, skalbara och kapabla att stödja komplexa data och processer, särskilt inom områden som kräver noggrann organisation och tillgänglighet av data, såsom utformning, byggande och förvaltning.