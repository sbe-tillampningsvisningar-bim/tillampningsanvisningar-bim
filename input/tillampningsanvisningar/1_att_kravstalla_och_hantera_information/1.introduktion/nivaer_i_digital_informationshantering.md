# Nivåer i digital informationshantering - digitala mognadsgrader

## Inledning

För ägare och brukare är tillräcklig och aktuell information om byggda tillgångar av stor betydelse. Ju mer man vet, desto klokare beslut går det att fatta. Informationen måste också vara lätt tillgänglig och enkel att hitta i. Det är här digitaliseringen har sin största nytta. Att leta i en väl strukturerad och uppdaterad databas går betydligt enklare än att orientera sig i ett källararkiv med pärmar.

Detta dokument beskriver fem nivåer vad gäller digital informationshantering, eller, om man så vill, fem grader av ”digital mognad”. Målet för en organisation bör vara att hitta rätt nivå för sina behov. En liten organisation, där mycket kunskap om den egna verksamheten bygger på individers personliga kunskap och erfarenhet, har mindre nytta av en fullständig digitalisering än den stora verksamheten där ansvaret kan vara mer delat mellan många individer.

De fem nivåer som beskrivs är självklart inte absoluta, utan en glidande skala från ingen till fullständig digitalisering.

Två begrepp som används i dokumentet kan behöva förklaras:

-   **Objektorientering** innebär i det här sammanhanget att alla objekt som är av intresse har en identifikation som gör att det går att hitta relevant information om dem. Data om objekten kan lagras i olika källor, men går att hitta tack vare en referensbeteckning som ger dem en entydig identifiering. Läs om referensbeteckningar här.
-   **Strukturering** innebär att data organiseras på ett fastställt sätt som gör det möjligt att söka eller göra urval på ett enhetligt sätt. Strukturen kan vara hierarkisk, till exempel genom att byggdelar beskrivs som system, eller genom att data i filer är märkta (”taggade”) så att man kan söka efter specifika typer av information. Ett textdokument utan rubriker är ostrukturerat, medan en xml-fil har taggar som beskriver varje enskilt avsnitt i dokumentet.


## Mognadsnivåer enligt ISO 19650

ISO 19650 syftar till att hjälpa uppdragsgivare som vill genomföra projekt och förvaltning på ett strukturerat sätt, så långt som lämpligt genom digital informationshantering. I SS-EN ISO 19650-1:2019 beskrivs en sekvens av mognadsnivåer som bygger på kombinationer av standarder, teknik och metoder för informationshantering. Ju mer avancerad kombination som används, desto större potentiell nytta har verksamheten av den information som hanteras.

![Nivåer i digital informationshantering](../1.introduktion/media/nivaer_iso19650.png)

*Bild 1: Ett perspektiv på mognadsnivåer för analog och digital informationshantering. Källa: SS-EN ISO 19650-1:2019, Figur 1. (I den svenska översättningen av standarden används termen ”skede” i figuren, medan texten använder ”mognadsnivå”.)*

Av figuren framgår att:

-   på alla nivåer används både strukturerade och ostrukturerade data
-   på nivå 1 och 2 används gemensam lagring av digitala informationscontainrar (filer), medan nivå 3 använder lagring också i databaser
-   det som särskiljer nivå 1 och 2 är användningen av ”samverkande informationsmodeller”, till exempel i form av CAD-filer som länkar till varandra
-   i nivå 3 sker denna samverkan också på objekt-nivå.

*Mognadsnivå 2 identifieras också som ”BIM enligt ISO 19650-serien”. Här används en blandning av manuella och automatiserade informationshanteringsprocesser för att generera en samverkande informationsmodell. Informationsmodellen omfattar alla informationscontainrar som levereras av uppgiftsteam i förhållande till en tillgång eller ett projekt. (SS-EN ISO 19650-1:2019, sid 7)*

Nedan beskrivs en alternativ nivåindelning av den digitala informationshanteringen, i fem nivåer:

1.  Analog – utan styrning (ad hoc)
2.  Analog och digital – hanterad
3.  Digital – standardiserad
4.  Digital – standardiserad och kontrollerad
5.  Digital – optimerad och innovativ

## Nivå 0: Analog – utan styrning (ad hoc)

Nivå 0 karaktäriseras av i stort sett fullständig brist på styrning av informationshanteringen. Huvudsakligen används analog information på papper. Viss information finns som ostrukturerade digitala dokument, till exempel ritningar i PDF-format. Informationslagringen görs främst på individnivå, baserat på vad vilken information den personen behöver.

### Kännetecken

-   **Processer**: Vanligen ostrukturerade och reaktiva; informationshanteringen är inkonsekvent och icke-standardiserad. Ingen digital ärendehantering.
-   **Teknologi**: Begränsad användning av digitala verktyg; beroende av manuella processer och pappersbaserade system.
-   **Data**: Ofullständiga, inaktuella och ostrukturerade data; betydande datasilos existerar.
-   **Styrning**: Brist på formella policyer och procedurer för datahantering; minimal dokumentation och arkivering.

### Effekter

-   Hög risk för dataförlust och fel.
-   Ineffektiv och tidskrävande informationssökning.
-   Dåligt beslutsfattande på grund av svåråtkomliga och opålitliga data.

### Leveransskedet

Under leveransskedet råder hög frihetsgrad för uppdragstagare att välja verktyg, men alla leveranser sker i form av utskrifter, eventuellt tillsammans med ostrukturerade digitala dokument.

### Användningsskedet

Under användningsskedet samlas information huvudsakligen i pappersform. Ingen ajourhållning av ritningar eller annan dokumentation. Driftdata sparas baserat på individuella behov. Underhåll är huvudsakligen oförutsett.

## Nivå 1: Analog och digital – hanterad

Nivå 1 karaktäriseras av en delvis objektorienterad och strukturerad information.

### Kännetecken

-   **Processer**: Grundläggande processer är definierade och upprepbara; viss standardisering börjar ta form. Digital manuell ärendehantering.
-   **Teknologi**: Användning av grundläggande digitala verktyg, såsom CAD, kalkylblad och enkla databaser.
-   **Data**: Förbättrade datainsamlings- och lagringspraxis; vissa ansträngningar för att standardisera dataformat.
-   **Styrning**: Inledande etablering av datahanteringspolicyer; grundläggande dokumentationspraxis är på plats.

### Effekter

-   Minskad risk för dataförlust och fel jämfört med nivå 0.
-   Mer konsekvent informationshantering, men fortfarande risk för ineffektivitet.
-   Bättre dataåtkomst för beslutsfattande.

### Leveransskedet

Under leveransskedet råder frihet att välja verktyg, men kommunikation mellan parterna görs i överenskomna format. Slutleverans görs med en blandning av utskrifter och filer.

##3 Användningsskedet

Under användningsskedet används huvudsakligen utskrifter som underlag. Driftdata sparas digitalt i en enkel typ av ärendehantering. Underhåll är planerat.

## Nivå 2: Digital – standardiserad

Nivå 2 karaktäriseras av helt objektorienterad och strukturerad information, som ajourhålls och kontrolleras vid behov. All information om ett objekt kan spåras genom konsekvent användning av referensbeteckningar för märkning av objekt och dess dokumentation.

### Kännetecken

-   **Processer**: Väl definierade och dokumenterade processer; organisationstäckande standarder är etablerade. Digital manuell ärendehantering.
-   **Teknologi**: Användning av integrerade mjukvarusystem, till exempel BIM-verktyg.
-   **Data**: Strukturerade och standardiserade dataformat; bättre datakvalitet och integration över system.
-   **Styrning**: Formella policyer och procedurer efterlevs; omfattande dokumentations- och arkiveringshantering.

### Effekter

-   Förbättrad datatillförlitlighet och tillgänglighet.
-   Strömlinjeformade informationshanteringsprocesser.
-   Mer exakt och snabb information för beslutsfattande.

### Leveransskedet

Under leveransskedet används ISO 19650 fullt ut, med huvudsakligen filbaserad CDE, gemensam klassifikation och struktur, kravställda leveranser, överenskomna leveransformat och så vidare.

### Användningsskedet

Under användningsskedet används en blandning av utskrifter och visning på bildskärm som underlag. Driftdata sparas digitalt i ett förvaltningssystem. Underhåll är planerat.

## Nivå 3: Digital – standardiserad och kontrollerad

Nivå 3 karaktäriseras av helt objektorienterad och strukturerad information som ajourhålls och kontrolleras kontinuerligt.

### Kännetecken

-   **Processer**: Processer är inte bara standardiserade utan också mätta och kontrollerade; kontinuerliga förbättringsinitiativ är på plats. Digital automatiserad ärendehantering.
-   **Teknologi**: Avancerad användning av teknologi, inklusive prediktiv analys och datavisualiseringsverktyg.
-   **Data**: Högkvalitativ, konsekvent och omfattande data; omfattande användning av datastandarder och digital samverkan.
-   **Styrning**: Robust styrningsramverk med regelbundna revisioner och prestationsmätningar; stark tonvikt på efterlevnad och ansvarsskyldighet.

### Effekter

-   Hög nivå av dataintegritet och tillförlitlighet.
-   Proaktiv hantering av informationstillgångar.
-   Datadrivet beslutsfattande med prediktiva insikter.

### Leveransskedet

Under leveransskedet används ISO 19650 fullt ut, med huvudsakligen databas-baserad CDE, gemensam klassifikation och struktur, kravställda leveranser, överenskomna leveransformat och så vidare.

### Användningsskedet

Under användningsskedet används en blandning av utskrifter och visning på bildskärm som underlag. Driftdata sparas digitalt i ett förvaltningssystem. Underhåll är planerat, baserat på data som beskriver tillgångarnas aktuella status.

## Nivå 4: Digital – optimerad och innovativ

Nivå 4 karaktäriseras av digitala tvillingar: digitala modeller som – baserat på realtidsdata om yttre och inre förhållanden – interagerar med sin fysiska motsvarighet i syfte att optimera nyttan.

### Kännetecken

-   **Processer**: Fullt optimerade och kontinuerligt förbättrade processer; adaptiva och innovativa tillvägagångssätt för informationshantering. Digital automatiserad ärendehantering.
-   **Teknologi**: Integration av banbrytande teknologi, såsom artificiell intelligens (AI), maskininlärning (ML) och sakernas internet (IoT) för realtidsdatainsamling och analys.
-   **Data**: Sömlös dataintegration över alla system; realtidsdata av hög kvalitet tillgänglig för olika intressenter.
-   **Styrning**: Exemplarisk styrningspraxis med kontinuerlig övervakning och förbättring; stark anpassning till strategiska mål.

### Effekter

-   Exceptionell datanoggrannhet, tillgänglighet och användbarhet.
-   Realtidsinsikter och prediktiv analys som driver strategiska beslut.
-   Smidig och responsiv informationshantering som anpassar sig till förändrade behov och teknologier.

### Leveransskedet

Under leveransskedet används ISO 19650 fullt ut, med databas-baserad CDE, gemensam klassifikation och struktur, kravställda leveranser, överenskomna leveransformat och så vidare. AI används för behovsanalys och för att ta fram och utvärdera lösningar.

### Användningsskedet

Under användningsskedet används data från AIM direkt i digitala verktyg. Tillgångarnas tillstånd kontrolleras och regleras i hög grad automatiskt. Driftdata och underhållsåtgärder sparas automatiskt.

## Att välja nivå

Dagens svenska praxis i *projekt* kan sägas ligga på mognadsnivå 2 enligt indelningen i ISO 19650-1. I *förvaltning* är bilden mer splittrad mellan nivå 1 och 2. Nivå 3 är förmodligen ovanligt i något skede.

Att gå från nivå 1 till 2 handlar huvudsakligen om att förvaltningen kräver och använder digitala data snarare än utskrifter. Här finns det stora vinster att göra i tillgångsförvaltningen.

Att gå till nivå 3 är ett större steg. Här är alla steg kontrollerade och effektiviteten mäts kontinuerligt. För kritiska verksamheter som kärnkraftsproduktion och järnvägsdrift borde det vara är en självklarhet, men för drift och underhåll av byggnader, vägar, va-ledningar och andra enklare typer av byggnadsverk är nivå 3 möjligen inte ekonomiskt försvarbart att sträva mot.

Nivå 4 bygger på digitala tvillingar som styr sina fysiska motsvarigheter. En fullständigt genomförd nivå 4 torde vara orealistisk för alla typer av tillgångar. Snarare kommer vissa tillgångsägare använda nivå 4 för sina kritiska tillgångar, och använda en kombination av nivå 2 och 3 för övriga.

## Sammanfattning (tabell)

Kännetecknen för de fem nivåerna sammanfattas i Tabell 1:
| **Nivå** |                                                                                                                           |
|----------|---------------------------------------------------------------------------------------------------------------------------|
|          | **Ajourhållning**                                                                                                         |
| 1        | Det som inkommer uppdateras inte alltid                                                                                   |
| 2        | Bara det som inkommer uppdateras                                                                                          |
| 3        | Alla ändringar kan inkomma och bli uppdaterade                                                                            |
| 4        | Alla ändringar inkommer och blir uppdaterade                                                                              |
| 5        | Alla ändringar automatiskt i realtid                                                                                      |
|          | **Processer**                                                                                                             |
| 1        | Ostrukturerade och reaktiva; inkonsekvent och icke-standardiserad informationshantering; ingen digital ärendehantering    |
| 2        | Definierade och upprepbara; viss standardisering; digital manuell ärendehantering                                         |
| 3        | Väl definierade och dokumenterade; organisationstäckande standarder; digital manuell ärendehantering                      |
| 4        | Standardiserade, mätta och kontrollerade; kontinuerliga förbättringar; digital automatiserad ärendehantering              |
| 5        | Optimerade; kontinuerliga förbättringar; adaptivt och innovativt tillvägagångssätt; digital automatiserad ärendehantering |
|          | **Teknologi**                                                                                                             |
| 1        | Manuell och analog; begränsad användning av digitala verktyg                                                              |
| 2        | Grundläggande digitala verktyg                                                                                            |
| 3        | Integrerade mjukvarusystem                                                                                                |
| 4        | Avancerad; prediktiv analys och datavisualisering                                                                         |
| 5        | Banbrytande teknologi: artificiell intelligens (AI), maskininlärning (ML), sakernas internet (IoT)                        |
|          | **Data**                                                                                                                  |
| 1        | Ofullständiga, inaktuella och ostrukturerade; datasilos                                                                   |
| 2        | Förbättrad insamling och lagring; viss standardisering                                                                    |
| 3        | Strukturerade och standardiserade format; bättre datakvalitet och integration                                             |
| 4        | Högkvalitativ, konsekvent och omfattande; omfattande användning av datastandarder och digital samverkan                   |
| 5        | Sömlös dataintegration; realtidsdata av hög kvalitet                                                                      |
|          | **Styrning**                                                                                                              |
| 1        | Saknas, minimal dokumentation och arkivering                                                                              |
| 2        | Vissa policyer och dokumentationspraxis                                                                                   |
| 3        | Formella policyer och procedurer; omfattande dokumentation och arkivering                                                 |
| 4        | Robust styrning; revisioner och prestationsmätningar; efterlevnad och ansvarsskyldighet                                   |
| 5        | Exemplarisk styrning; kontinuerlig övervakning och förbättring; stark anpassning till strategiska mål                     |