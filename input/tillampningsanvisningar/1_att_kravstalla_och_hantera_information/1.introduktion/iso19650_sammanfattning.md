# SS-EN ISO 19650 - en sammanfattning

## Inledning - standardens fem delar

Detta dokument innehåller en sammanfattning av standardserien SS-EN ISO 19650, och förklarar dess viktigaste begrepp. Vissa engelska termer anges inom parentes för att förklara de akronymer som används.

ISO 19650 syftar till att ge riktlinjer för hur man ska gå till väga för att samordna den kollektiva insats som krävs i ett bygg- eller anläggningsprojekt för att åstad­komma information som är maximalt användbar i alla skeden, och för alla inblandade. För närvarande består serien av fem delar, som beskrivs i denna sammanfattning:

1. Begrepp och principer<br>
2. Informationsleverans vid överlämning av tillgångar (leveransskede)<br>
3. Användningsskede<br>
4. Informationsutbyte<br>
5. Principer och krav för ett säkerhetsmedvetet tillvägagångssätt

För varje steg i byggprocessen, och för varje typ av leverans, finns i de fem delarna detaljerade förklaringar och vissa anvisningar. Utöver detta finns en europeisk vägledning för implementering av Del 1 och Del 2 i form av SS-CEN/TR 17439:2020. Denna håller för närvarande på att uppdateras – med svensk medverkan – så att den täcker alla fem delar.


## ISO 19650-1: Begrepp och principer 

I Del 1 finns den grundläggande definitionen om vad BIM innebär:

***byggnadsinformationsmodellering (BIM)***  
*användande av en delad digital representation av en byggd tillgång för att underlätta processer för utformning, produktion, drift och underhåll i syfte att utgöra ett pålitligt underlag för beslut*

Här behövs tre förtydliganden:

-   Med **delad** menas inte nödvändigtvis att alla lagrar data på samma ställe, utan att alla parter ska kunna hitta den information de behöver.
-   Med **digital representation** menas all form av data och information, till exempel kartdata; rapporter och undersökningar; beräkningar; geometrier och annat i CAD-filer; förteckningar; tekniska beskrivningar.
-   Begreppet täcker in **hela livscykeln**, inte bara utformning och byggande. Eftersom drift och användning är betydligt mer omfattande – i både tid och pengar – är informationen som rör det skedet viktig att ta hänsyn till också i de tidigare skedena.

**Anläggnings- och fastighetsförvaltning** är en ägares långsiktiga drift och underhåll av sina byggda tillgångar i syfte att få maximalt värde från dem. Detta kan startas som ett resultat av ett **projekt** genom att någon låter bygga något, till exempel ett företag som investerar i en ny byggnad för huvudkontoret. Som en del i anläggnings- och fastighetsförvaltningen kan nya projekt tillkomma för att skapa nya byggnader och anläggningar, tillbyggnader eller ombyggnader, som illustreras i Bild 1. Stora tillgångsägare som Trafikverket har kontinuerligt många projekt i gång vid sidan av den ordinarie verksamheten.
![Anläggnings- och fastighetsförvaltning](../1.introduktion/media/bild1.png)
*Bild 1: Anläggnings- och fastighetsförvaltning, där ett projekt kan vara inledningen, och därefter uppstå flera gånger under användningsskedet. Med ”tillgångsförvaltning” avses förvaltning av de byggda tillgångarna.*

## ISO 19650-2: Leveransskedet 

Del 2 av ISO 19650 omfattar det som benämns **leveransskedet**. I det skedet sker en större förändring av tillgångarna i form av nybyggnad, ombyggnad eller tillbyggnad. Även insatser under driftskedet – som beskrivs i ISO 19650-3 – kan bli så omfattande att en separat upphandling av ett projekt kan behövas.

När ett behov av ett projekt har uppstått tar tillgångsägaren rollen som **uppdragsgivare**, i syfte att bli mottagare av information. Beroende på vald upphandlingsform anlitar uppdragsgivaren en eller flera **huvudansvariga uppdragstagare** som ansvarar för helheten. Detta kan vara i form av en extern ”generalkonsult”, alternativt en intern eller extern projektledare eller projekteringsledare.

Den huvudansvariga uppdragstagaren utser eller handlar sedan upp de **uppdragstagare** som behövs, eventuellt i samråd med uppdragsgivaren. Det förekommer också att uppdragsgivaren själv tar på sig uppgiften att tillsätta all den expertis som behövs. 

Den huvudansvariga uppdragstagaren och övriga uppdragstagare blir alla leverantörer av information, och bildar tillsammans ett **leveransteam**. I större projekt kan flera sådana leveransteam behövas. Hos dessa skapas efter behov **uppgiftsteam** som utför arbetet. (För en djupare beskrivning av upphandlingsformer, se [Upphandling av projekt](https://tillampningsanvisningarbim.kravportal.se/tillampningsanvisningar_bim/upphandling_projekt/) och [Upphandling av drift och underhåll](https://tillampningsanvisningarbim.kravportal.se/tillampningsanvisningar_bim/upphandling_drift_underhall/) Se speciellt avsnitt 5 *Upphandling enligt ISO 19650 i Sverige*).).

Alla dessa parter bildar tillsammans ett **projektteam**. Teamen utbyter information med varandra kontinuerligt under projektet, och utgör därmed uppdragsgivare respektive uppdragstagare gentemot varandra.

![Organisationsbild](../1.introduktion/media/del2_figur2.png)

*I Bild 2 och i Tabell 1 visas schematiskt aktörerna och hur de samverkar med varandra.*

|                                                 | **Förklaring**                            | **Exempel 1**                                                                                | **Exempel 2**                                                                             |
|-------------------------------------------------|-------------------------------------------|----------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------|
| A                                                | Uppdragsgivare                            | Projektledare hos bostadsbolag                                                               | Projektledare inom anläggningsverksamhet                                                      |
| B                                                | Huvudansvarig uppdragstagare              | Företag för arkitektur respektive byggnadskonstruktion                                       | Totalentreprenör                                                                          |
| C                                               | Uppdragstagare                            | Arkitekt, inredningsarkitekt, fasadkonstruktör respektive stålkonstruktör, betongkonstruktör | Trafikkonsult, vägprojektör, landskapsarkitekt, schaktentreprenör, beläggningsentreprenör |
| …                                               | (fler uppdragstagare)                     |                                                                                              |                                                                                           |
| 1                                                | Projektteam                               | Samtliga                                                                                     | Samtliga                                                                                  |
| 2                                                | Leveransteam                              | Samtliga anlitade av B                                                                       | Samtliga anlitade av B                                                                    |
| 3                                                | Uppgiftsteam                              | Samtliga anlitade av B att utföra en viss uppgift                                            | Samtliga anlitade av B att utföra en viss uppgift                                         |
| ![pil](../1.introduktion/media/pil1_.png)  | Informationskrav och informationsleverans |                                                                                              |                                                                                           |
| ![pil](../1.introduktion/media/pil2_.png) | Informationskoordinering                  |                                                                                              |                                                                                           |

*Tabell 1: Förklaring av Bild 2.*

Under projektet hanteras och utväxlas en stor mängd information av olika slag. Två viktiga principer i ISO 19650 är att:

-   verksamhetens behov ska ligga till grund för kraven på information
-   utbytet av information ska baseras på kraven från beställaren av information.

Nyckeln för ett framgångsrikt projekt är tydliga informationskrav, utifrån det specifika behovet. Vilket beslut ska stödjas av den specifika begäran om information? Ett sådant **informationsutbyte** består av:

-   ett informationskrav
-   produktion av information
-   en leverans
-   en granskning
-   efter eventuellt underkännande görs en omarbetning
-   ett godkännande

![Godkännandeprocess](../1.introduktion/media/godkannande.png)

*Bild 3: Stegen i ett informationsutbyte. Uppdragsgivaren står för informationskrav och granskning; uppdragstagaren står för produktion, leverans och eventuell omarbetning.*

Observera att relationen uppdragsgivare–uppdragstagare finns också i leveranser mellan uppdragstagare, och mellan utförare och huvudutförare. Det finns en uppdragsgivare för hela projektet, och därefter uppdragsgivare för varje leverans från en aktör till en annan.

Mångfalden av informationskrav kan beskrivas på olika nivåer – eller faser – under projektet:

-   **Organisationens informationskrav** (*organizational information requirements,* *OIR*): vilken information behövs för att uppnå organisationens mål? Det kan till exempel handla om strategiska beslut om affärer och fysiska tillgångar av olika slag. Vad behöver organisationen veta om kvaliteten och kapaciteten hos dess befintliga och tillkommande tillgångar och deras värde för verksamhetens mål, som underlag för strategiska beslut av ledningen?
-   **Tillgångsinformationskrav** (*asset information requirements,* *AIR*): vad behövs för drift och underhåll? Vilken information behöver levereras för att möta organisationens krav? Hur ska informationen levereras och lagras?
-   **Projektinformationskrav** (*project information requirements,* *PIR*): vilken information behöver utbytas mellan parterna under planering, projektering och produktion? Hur ska informationen formateras, struktureras, levereras och lagras?
-   **Informationsleveranskrav** (*exchange information requirements,* *EIR*): vilken information ingår i det specifika uppdraget? Hur ska informationen levereras?

![Informationskrav](../1.introduktion/media/informationskrav.png)

*Bild 4: Relationer mellan informationskraven i ett projekt och hur de specificerar innehållet i informationsmodellerna. Synpunkter och krav från användningen används som input till varje nytt projekt.*

För alla dessa nivåer behöver beställaren av information fastställa **nivå för informationsbehov**. Vad är syftet med informationen, vad ska beskrivas, och hur detaljerat? För leverantören blir det en viktig uppgift att sortera bort data som inte är relevant i den aktuella leveransen.

Informationsutbytet sker både under tillgångens **leveransskedet** – det vill säga design och produktion – och under **användningsskedet**, när den väl har levererats.

Leveransgruppen, under ledning av huvudutföraren, måste svara på samtliga informationskrav genom att formulera en **genomförandeplan för BIM** (*BIM Execution Plan, BEP*). Denna syftar till att beskriva informationshanteringen som utförs av leveransgruppen och dess arbetsgrupper. En del av detta bör vara en **huvudplan för informationsleveraner** (*Master Information Delivery Plan, MIDP*), som anger nyckelpunkterna för leveranser.

Summan av den information som skapas i projektet kan sägas utgöra en **projektinformationsmodell** (*project information model, PIM*). I samband med leverans överförs relevanta delar av denna till en **tillgångsinformationsmodell** (*asset information model, AIM*). I eventuellt kommande projekt kan en arkiverad PIM utgöra indata.

De individuella informationsleveranserna består av utdrag ur projektinformationsmodellen i form av **informationscontainers**, typiskt sett i form av datorfiler som innehåller CAD-data, textbeskrivningar, tabeller, scheman eller någon annan form av presentation.

För att informationen ska vara tillgänglig för alla krävs en överenskommen metod för hantering av de informationsmängder som levereras genom en **gemensam datamiljö** (*Common Data Environment, CDE*), som kan bestå av en eller flera platser för datahantering. Observera att detta inte per automatik innebär åtkomst till informationens källor, till exempel CAD-modeller. Processen för datahantering ska styras av regler för datamognad (se nedan), versionskontroll, ändringshantering, behörigheter, spårning, ansvarsskyldighet och säkerhet. För att denna ofta komplexa miljö ska fungera effektivt för alla inblandade måste en **strategi för informationsfederering** finnas på plats. Målet för denna är att strukturera och hantera informationen på ett sätt som är väl anpassat till de specifika behoven.

Frågor om datasäkerhet och behörigheter måste beaktas vid val av teknisk lösning för den gemensamma datamiljön. Detsamma gäller vilka typer av informationsmängder – i dagligt tal vilka filformat – som ska användas, och hur dessa ska organiseras i mappstrukturer eller med hjälp av metadata i dokumenthanteringssystem.

Data som publiceras behöver beskrivas med hjälp av metadata eller på annat sätt hur färdigställd den är. Detta kan göras med hjälp av metadata eller på annat sätt. Mognaden beskrivs i fyra steg:

1.  Under arbete, endast synligt för arbetsgruppens medlemmar
2.  Delad, används i samarbete av utsedda parter i leveransteamet och av den ledande utsedda parten
3.  Publicerad, som används av utförare för efterföljande projektaktiviteter
4.  Arkiverad.

En tolkning av detta som följer svensk standard SS 32209 för hantering av dokument lyder så här:

1.  Under arbete
2.  Preliminär
3.  För granskning
4.  Godkänd, alternativt Förfrågningsunderlag (vilket definieras som ”godkänt för användning i förfrågningsunderlag”)
5.  Arkiverad.

Innan data publiceras som godkänd måste den vara granskad och godkänd enligt överenskomna krav på informationsleveranser som är dokumenterade i genomförandeplanen; först av huvudutföraren och slutligen av beställaren (se Bild 3). Efter steg 2 används därför status ”för granskning” som ett tredje steg.

Projektinformationsmodellen används av entreprenören under produktionen, och uppdateras kontinuerligt när behov uppstår. Modellen används direkt eller via utskrivna ritningar och andra dokument.

När produktionen är klar ska både det fysiska resultatet och informationsmodellen överlämnas till beställaren. I båda fallen genomförs ”slutstädning”. I modellens fall handlar det om att först arkivera produktinformationsmodellen, därefter att rensa bort data som inte är relevant för användningsskedet. Resultatet är en tillgångsinformationsmodell: en AIM.

## ISO 19650-3: Användningsskedet 

Efter leveransen börjar den nya tillgången att användas, driftas och underhållas. Även här finns parterna uppdragsgivare, uppdragstagare och uppdragstagare.

I Del 3 används samma figur som i Del 1, men med annan benämning på grupp 1 (Bild 4):

![roller](../1.introduktion/media/del2_figur2.png)

|                                                 | **Förklaring**                            | **Exempel 1**                                                                                | **Exempel 2**                                                                             |
|-------------------------------------------------|-------------------------------------------|----------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------|
| A                                                | Uppdragsgivare                            | Förvaltningschef hos bostadsbolag                                                               | Distriktsansvarig inom anläggningsverksamhet                                                         |
| B                                                | Huvudansvarig uppdragstagare              | FM-företag                                      | Drift- och underhållsentreprenör                                                                         |
| C                                               | Uppdragstagare                            | Lokalvårdare, servicetekniker | Snöröjare, anläggningsentreprenör, servicetekniker  |
| …                                               | (fler uppdragstagare)                     |                                                                                              |                                                                                           |
| 1                                                | Drift- och underhållsgrupp                               | Samtliga                                                                                     | Samtliga                                                                                  |
| 2                                                | Leveransteam                              | Samtliga anlitade av B                                                                       | Samtliga anlitade av B                                                                    |
| 3                                                | Uppgiftsteam                              | Samtliga anlitade av B att utföra en viss uppgift                                            | Samtliga anlitade av B att utföra en viss uppgift                                         |
| ![pil](../1.introduktion/media/pil1_.png)  | Informationskrav och informationsleverans |                                                                                              |                                                                                           |
| ![pil](../1.introduktion/media/pil2_.png) | Informationskoordinering                  |                                                                                              |                                                                                           |

## ISO 19650-4: Informationsutbyte 

I Del 4 av ISO 19650 detaljeras beskrivningen av hur informationsutbyten ska gå till. Här används några nya termer jämfört med tidigare, mer översiktliga delar (här i egen översättning). Man pratar om tre roller avseende informationscontainrar:

-   tillhandahållare
-   mottagare
-   granskare.

Informationsutbytet beskriv som en process enligt Bild 1:

![informationsutbytesprocessen](../1.introduktion/media/informationsutbyte.png)
*Bild 1: Informationsutbytesprocessen. Källa: SS-EN ISO 19650-4:2022, Figure 1.*

Processen förklaras så här:

1 Skapa material med status **Under arbete**.

2 Lagra som status **Delad**, alternativt **Preliminär** eller **För granskning**.

3 Lagra som status **Publicerad**, alternativt **Godkänd**.

4 Genomför justeringar.

A Beslut A: godkänn för delning (a1) eller kräv justering (a0) eller avlägsnande från delat tillstånd (a2).

B Beslut B: godkänd för publicering (b1) eller kräv justering (b0).

Beslut a2 och b1 innebär alltså en genväg, där en informationscontainer går direkt från **Under arbete** till **Publicerad/Godkänd**.

Hur allt detta ska gå till i praktiken ska testas innan projektet går in i skarpt läge.

Vilka kriterier som ska användas för godkännande måste vara överenskomna och kända. De sammanfattas under följande rubriker:

1.  Gemensamma datamiljön: kontrollera namn, status och andra metadata, liksom säkerhetskrav (avsnitt 7.1).
2.  Leveranstider och dataformat (avsnitt 7.2).
3.  Kontinuitet i förhållande till andra informationscontainrar, tidigare versioner och annat (avsnitt 7.3).
4.  Kommunicerbarhet: datumformat, enheter, teckenuppsättningar och liknande (avsnitt 7.4).
5.  Konsekvens avseende avsaknad av dubbletter, överlappningar och luckor; attribut och egenskaper; länkar till annan dokumentation; noggrannhet avseende lokalisering, egenskapsvärden, klassifikation och annan textinformation (avsnitt 7.6).
6.  Fullständighet i förhållande till EIR och andra förväntningar (avsnitt 7.6).
7.  Övriga kriterier som bedöms viktiga, till exempel avseende funktion, kapacitet, tekniska standarder, kommersiella och legala krav, hälsa och trygghet.

Man ska också följa eventuella krav på säkerhet som ställs, baserat på råden i ISO 19650-5. Innan justeringar av icke godkänd information ska genomförs ska potentiella problem och risker identifieras och tas om hand.

Det går att undanta informationscontainrar från granskningsprocessen. Detta avser framför allt information som inte kommer att förändras, till exempel certifikat och underlagsmaterial som kartor, fotografier och video.

## ISO 19650-5 Principer och krav för ett säkerhetsmedvetet tillvägagångssätt 

Del 5 av ISO 19650 har inte ingått i projektet. Som rubriken visar handlar det om hur säkerhetsfrågor ska beaktas i känsliga projekt.

Nedan visas en AI-översatt översättning av introduktionen till standarden.

**Introduktion**

Den byggda miljön befinner sig i en period av snabb utveckling. Det förväntas att införandet av byggnadsinformationsmodellering (BIM) och den ökande användningen av digital teknik vid utformning, konstruktion, tillverkning, drift och förvaltning av tillgångar eller produkter samt tillhandahållande av tjänster inom den byggda miljön kommer att ha en omvälvande effekt på de berörda parterna.

Det är troligt att initiativ eller projekt som utvecklar nya tillgångar eller lösningar, eller modifierar eller förvaltar befintliga, måste bli mer samarbetsinriktade för att öka effektiviteten. Ett sådant samarbete kräver mer transparenta och öppna arbetssätt och, i så stor utsträckning som möjligt, lämplig delning och användning av digital information.

Den kombinerade fysiska och digitala byggda miljön kommer att behöva leverera framtida skattemässiga, finansiella, funktionella, hållbarhets- och tillväxtmål. Detta kommer att påverka upphandlings-, leverans- och driftsprocesserna, bland annat genom ett ökat samarbete mellan olika discipliner och sektorer. Det kommer också att leda till en ökad användning av digitala verktyg och ökad tillgång till information.

Användningen av datorbaserad teknik stödjer redan nya arbetssätt, t.ex. utvecklingen av fabriksbaserad tillverkning utanför produktionsanläggningen och automatisering på plats. Sofistikerade cyberfysiska system, som använder sensorer (cyber- eller beräkningselementet) för att styra eller påverka fysiska delar av systemet, kan arbeta i realtid för att påverka resultat i den verkliga världen.

Det förväntas att sådana system kommer att användas för att uppnå fördelar som ökad energieffektivitet och bättre hantering av tillgångarnas livscykel genom att samla in realtidsinformation om tillgångarnas användning och skick. De finns redan inom transport, försörjning, infrastruktur, byggnader, tillverkning, hälsovård och försvar, och när de kan interagera som integrerade cyberfysiska miljöer kan de användas för att utveckla smarta samhällen.

Som en konsekvens av den ökande användningen av och beroendet av informations- och kommunikationsteknik finns det ett behov av att ta itu med inneboende sårbarhetsfrågor och därmed de säkerhetsimplikationer som uppstår, oavsett om det gäller byggda miljöer, tillgångar, produkter, tjänster, individer eller samhällen, samt all tillhörande information.

Detta dokument tillhandahåller ett ramverk för att hjälpa organisationer att förstå de viktigaste sårbarhetsfrågorna och vilken typ av kontroller som krävs för att hantera de resulterande säkerhetsriskerna till en nivå som är acceptabel för berörda parter. Syftet är inte att på något sätt underminera samarbete eller de fördelar som BIM, andra samarbetsbaserade arbetsmetoder och digital teknik kan generera.

Termen organisation omfattar inte bara utnämnande parter och utnämnda parter, enligt definitionen i ISO 19650-1, utan även organisationer på efterfrågesidan som inte är direkt involverade i en utnämning.

Informationssäkerhetskraven för en enskild organisation, en organisationsavdelning eller ett system anges i ISO/IEC 27001 men kan inte tillämpas på flera organisationer. BIM och andra digitala arbetsmetoder och tekniker för samarbete innebär i allmänhet att information delas i samarbete mellan ett stort antal oberoende organisationer inom sektorn för byggd miljö. Därför uppmuntrar detta dokument till antagandet av ett säkerhetsmedvetet, riskbaserat tillvägagångssätt som kan tillämpas över, såväl som inom, organisationer. Att tillvägagångssättet är lämpligt och proportionerligt har också den fördelen att åtgärderna inte bör hindra små och medelstora företag från att delta i leveransteamet.

Det säkerhetsmedvetna förhållningssättet kan tillämpas under hela livscykeln för ett initiativ, ett projekt, en tillgång, en produkt eller en tjänst, oavsett om den är planerad eller befintlig, där känslig information erhålls, skapas, bearbetas och/eller lagras.

Figur 1 visar hur detta säkerhetstänkande integreras med andra organisatoriska strategier, policyer, planer och informationskrav för digitalt stödd leverans av projekt samt underhåll och drift av tillgångar med hjälp av BIM.

![Informationssäkerhetsprocess](../1.introduktion/media/del5.png)

*Figur 1: Integrering av det säkerhetsmedvetna arbetssättet i den bredare BIM-processen. Källa: SS-EN ISO 19650-5:2020, Figure 1.*

**Förklaring**

A samordnade och konsekventa strategier och policyer

B samordnade och konsekventa planer

C samordnade och konsekventa informationskrav

D aktiviteter som genomförs under användningsskedet

E aktiviteter som genomförs under leveransskedet (se även ISO 19650-2)

1 organisatoriska planer och mål

2 strategisk plan/policy för förvaltning av tillgångar (se ISO 55000)

3 säkerhetsstrategi

4 andra organisatoriska strategier och policyer

5 plan för förvaltning av tillgångar (se ISO 55000)

6 plan för hantering av säkerhet

7 andra organisatoriska planer

8 krav på tillgångsinformation (AIR)

9 krav på säkerhetsinformation (som utgör en del av säkerhetsstyrningsplanen)

10 organisationens informationskrav (OIR)

11 strategisk affärsplan och strategisk brief

12 tillgångens operativa användning

13 prestandamätning och förbättringsåtgärder

NOTERA

Numreringen i A, B och C innebär ingen inbördes ordning.