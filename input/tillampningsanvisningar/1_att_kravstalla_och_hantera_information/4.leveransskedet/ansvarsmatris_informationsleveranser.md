# Ansvarsmatris för informationsleveranser

En ansvarsmatris för informationsleveranser – också kallad gränsdragningslista – upprättas i två steg:

1. en översiktlig anbudsversion
2. en detaljerad inför projektstart.

Observera att det exempel på ansvarsmatris som visas i ISO 19650-2, Bilaga A, endast ger en överblick över alla aktiviteter för informationshantering i standarden (se länk till motsvarande Ansvarsmatris för informationshantering). Den säger inget om den faktiska produktionen av information i ett projekt.

Mer användbart är att i stället basera matrisen på uppdragsgivarens milstolpar för informationsleverans och visa:

- vilken information som produceras
- när informationsutbyte sker och med vem
- vilket uppgiftsteam som ansvarar.

Nedan visas exempel från en ansvarsmatris för informationsleveranser i projektering, även kallat gränsdragningslista för projektering. Motsvarande ansvarsmatris kan upprättas för att redovisa gränsdragning för informationsleveranser mellan entreprenader.

Allteftersom ett projekt fortskrider och informationen detaljeras, behöver ansvarsmatrisen kompletteras och förfinas. Det är vanligt att den blir mycket omfattande.

![Ansvarsmatris](../4.leveransskedet/media/ansvarsmatris.png)