# Leveransskedet

## Processen för leveransskedet
En ny tillgång i den byggda miljön – en byggnad, en park, en väg, en bro och så vidare – planeras, projekteras, produceras och överlämnas under det SS-EN ISO 19650 kallar **leveransskedet**. Det kan också avse en större åtgärd under användningsskedet av befintliga tillgångar, som alltså inte ingår i ordinarie drift och underhåll: en större renovering, ombyggnad eller liknande.

Det här beskrivs i detalj i SS-EN ISO 19650 Del 2 *Informationsleverans vid överlämning av tillgångar*.

Figur 3 i Del 2 bryter ner informationshantering i leveransskedet i åtta aktiviteter. Detta dokument beskriver aktiviteterna som följer efter att ett projekt blivit upphandlat och uppdragsbekräftelse färdigställd, det vill säga aktiviteterna 5–8:

![Del2 figur3 förklarad](../4.leveransskedet/media/figure2_forklarad.png)

*Bild 1: SS-EN ISO 19650-2, Figur 3, kompletterad med förklarande text.*

I figuren visar siffrorna enskilda aktiviteter, medan bokstäverna betecknar grupper av aktiviteter. Den vänstra gröna cirkeln visar tillgångsinformationsmodellen innan leveransskedet, och den högra orange cirkeln visar hur den fyllts på med information från det genomförda projektet.

|   | **Aktiviteter**                                                                                                                                                                                                                                                                                                                       |
|---|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 1 | bedömning av behov<br> Uppdragsgivarens behov dokumenteras i *organisationens informationskrav* (OIR), *tillgångsförvaltningens informationskrav* (AIR) och *projektets informationskrav* (PIR). Här ingår också att utse egen informationsansvarig för projektet, och att beskriva förutsättningar och krav för det digitala samarbetet. |
| 2 | anbudsförfrågan<br>  Här ingår OIR, PIR, AIR och EIR.                                                                                                                                                                                                                                                                                     |
| 3 | svar på anbudsförfrågan <br>Potentiella huvudansvariga uppdragstagare tar fram anbud. Här ingår att ta fram en genomförandeplan för BIM som visar hur man tänkt uppfylla informationskraven.                                                                                                                                              |
| 4 | uppdragsbekräftelse <br>Tas fram av den huvudansvariga uppdragstagaren av efter accept av anbud.                                                                                                                                                                                                                                          |
| 5 | projektstart <br>Den huvudansvariga uppdragstagaren aktiverar deltagare och andra resurser.                                                                                                                                                                                                                                               |
| 6 | gemensam produktion av information <br> Information från projekteringen produceras, interngranskas och delas undan för undan i en PIM.                                                                                                                                                                                                    |
| 7 | leverans av informationsmodell<br>  Görs efter uppdragsgivarens granskning och godkännande. Informationen rensas och överförs till AIM.                                                                                                                                                                                                   |
| 8 | projektavslut<br>  Slutet på leveransskedet, början på användningsskedet.                                                                                                                                                                                                                                                                |
|   | **Aktivitetsgrupper**                                                                                                                                                                                                                                                                                                                 |
| B | Samtliga aktiviteter i ett projekt.                                                                                                                                                                                                                                                                                                   |
| C | Aktiviteter i ett specifikt (del)uppdrag i ett projekt (en upphandling).                                                                                                                                                                                                                                                              |
| D | Aktiviteter under upphandlingsskedet av uppdraget.                                                                                                                                                                                                                                                                                    |
| E | Aktiviteter under informationsplaneringen av uppdraget.                                                                                                                                                                                                                                                                               |
| F | Aktiviteter under produktionsskedet av uppdraget.                                                                                                                                                                                                                                                                                     |
| A | Den producerade informationsmodellen kan användas vid nästa uppdrag.                                                                                                                                                                                                                                                                  |

*Tabell 1: Förklaring av Bild 1.*

## Organisationen i leveransskedet

När ett behov av ett projekt har uppstått tar tillgångsägaren rollen som **uppdragsgivare**, i syfte att bli mottagare av information. 

Beroende på vald upphandlingsform anlitar uppdragsgivaren en eller flera **huvudansvariga uppdragstagare** som ansvarar för helheten. Detta kan vara i form av en extern ”generalkonsult”, alternativt en intern eller extern projektledare eller projekteringsledare.

Den huvudansvariga uppdragstagaren utser eller handlar sedan upp de **uppdragstagare** som behövs, eventuellt i samråd med uppdragsgivaren. Det förekommer också att uppdragsgivaren själv tar på sig uppgiften att tillsätta all den expertis som behövs.

Den huvudansvariga uppdragstagaren och övriga uppdragstagare blir alla leverantörer av information, och bildar tillsammans ett **leveransteam**. I större projekt kan flera sådana leveransteam behövas. Hos dessa skapas efter behov **uppgiftsteam** som utför arbetet. (För en djupare beskrivning av upphandling, se anvisningar för *”Upphandling av projekt”* och *”Upphandling av drift och underhåll”*. Se även särskilt avsnittet  *”Upphandling enligt ISO 19650 med svenska standardavtal”*).


![Del2 figur2](../4.leveransskedet/media/del2_figur2.png)<br>
*Bild 2: Gränssnitten mellan aktörerna i ett projekt (ISO 19650-2, Figur 2).*

I tabell 2 visas schematiskt aktörerna och hur de samverkar med varandra.

|                                                 | **Förklaring**                            | **Exempel 1**                                                                                | **Exempel 2**                                                                             |
|-------------------------------------------------|-------------------------------------------|----------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------|
| A                                                | Uppdragsgivare                            | Projektledare hos bostadsbolag                                                               | Projektledare hos Trafikverket                                                            |
| B                                               | Huvudansvarig uppdragstagare              | Företag för arkitektur respektive byggnadskonstruktion                                       | Totalentreprenör                                                                          |
| C                                               | Uppdragstagare                            | Arkitekt, inredningsarkitekt, fasadkonstruktör respektive stålkonstruktör, betongkonstruktör | Trafikkonsult, vägprojektör, landskapsarkitekt, schaktentreprenör, beläggningsentreprenör |
| …                                               | Variabel mängd                            |                                                                                              |                                                                                           |
| 1                                               | Projektteam                               | Samtliga                                                                                     | Samtliga                                                                                  |
| 2                                               | Leveransteam                              | Samtliga anlitade av en B                                                                    | Samtliga anlitade av en B                                                               |
| 3                                               | Uppgiftsteam                              | Samtliga anlitade av en B att utföra en viss uppgift                                         | Samtliga anlitade av en B att utföra en viss uppgift                                      |
| &ensp; ![pil1](../4.leveransskedet/media/pil1.png) | Informationskrav och informationsleverans |                                                                                              |                                                                                           |
| &ensp; ![pil2](../4.leveransskedet/media/pil2.png) | Samordning av information                 |                                                                                              |                                                                                           |

*Tabell 2: Förklaring av Bild 2. Observera att B här står för ”huvudansvarig uppdragstagare”, inte ”beställare”.*

## Leveranser i leveransskedet

Beskrivningen av leveransskedet är i mångt och mycket en beskrivning av hur projektering har gått till traditionellt. Den stora skillnaden är poängteringen *att informationsleveranser alltid ska baseras på krav från den part som vill ha informationen*. Det ska vara ett informationsutbyte, där ett krav resulterar i en leverans. 

Leveransteamet, under ledning av den huvudansvariga uppdragstagaren, måste svara på samtliga informationskrav genom att formulera en **genomförandeplan för BIM** (BEP, *BIM Execution Plan*). Denna syftar till att beskriva informationshanteringen som utförs av leveransgruppen och dess arbetsgrupper. En del av detta bör vara en **huvudplan för informationsleveranser**, som anger nyckelpunkterna för leveranser.


Summan av den information som skapas i projektet kan sägas utgöra en **projektinformationsmodell** (PIM).

De individuella informationsleveranserna består av utdrag ur projektinformationsmodellen i form av **informationscontainers**, typiskt sett i form av datorfiler som innehåller CAD-data, textbeskrivningar, tabeller, scheman eller någon annan form av presentation.

## Informationsutbyte i leveransskedet

För att informationen ska vara tillgänglig för alla krävs en överenskommen metod för hantering av de informationsmängder som levereras genom en **gemensam datamiljö** (CDE, *Common Data Environment*), som kan bestå av en eller flera platser för datahantering. Observera att detta inte per automatik innebär åtkomst till informationens källor, till exempel CAD-modeller.

Processen för datahantering ska styras av regler för datamognad (se nedan), versionskontroll, ändringshantering, behörigheter, spårning, ansvarsskyldighet och säkerhet. För att denna ofta komplexa miljö ska fungera effektivt för alla inblandade måste en **federeringsstrategi** finnas på plats. Målet för denna är att strukturera och hantera informationen på ett sätt som är väl anpassat till de specifika behoven.

Frågor om datasäkerhet och behörigheter måste beaktas vid val av teknisk lösning för den gemensamma datamiljön. Detsamma gäller vilka typer av informationsmängder – i dagligt tal vilka filformat – som ska användas, och hur dessa ska organiseras i mappstrukturer eller med hjälp av metadata i dokumenthanteringssystem.

Data som publiceras behöver beskrivas med hjälp av metadata eller på annat sätt hur färdigställd den är. Detta kan göras med hjälp av metadata eller på annat sätt. Mognaden beskrivs i fyra steg:

1.  under arbete, endast synligt för arbetsgruppens medlemmar
2.  delad, används i samarbete av utsedda parter i leveransteamet och av den ledande utsedda parten
3.  publicerad, som används av utförare för efterföljande projektaktiviteter
4.  arkiverad.

En tolkning av detta som följer svensk standard för hantering av dokument lyder så här:

1.  under arbete
2.  preliminär
3.  för granskning
4.  godkänd
5.  arkiverad.

Innan data publiceras som godkänd måste den vara granskad och godkänd enligt överenskomna krav på informationsleveranser som är dokumenterade i genomförandeplanen; först av den huvudansvariga uppdragstagaren och slutligen av uppdragsgivaren. Efter steg 2 används därför status ”för granskning” som ett tredje steg.

Projektinformationsmodellen används av entreprenören under produktionen, och uppdateras kontinuerligt när behov uppstår. Modellen används direkt eller via utskrivna ritningar och andra dokument.

När produktionen är klar ska både det fysiska resultatet och informationsmodellen överlämnas till beställaren. I båda fallen genomförs ”slutstädning”. I modellens fall handlar det om att först arkivera produktinformationsmodellen, därefter att rensa bort data som inte är relevant för användningsskedet. Resultatet är en **tillgångsinformationsmodell**: en AIM (*asset information model*).

I eventuellt kommande projekt kan AIM utgöra indata. Man kan då också behöva använda data också från den arkiverade PIM.

## Detaljanvisningar

Nedan följer en beskrivning av skedena 5–8 i standardens Figur 2. 

## 5 Projektstart
När upphandlingen är genomförd och uppdragstagare utsedda kan projektet starta.

**Innehåll:**<br>
[Leveransteam](https://tillampningsanvisningarbim.kravportal.se/leveransskedet#leveransteam)<br>
[Informationsteknik](https://tillampningsanvisningarbim.kravportal.se/leveransskedet#informationsteknik)<br>
[Metoder och rutiner](https://tillampningsanvisningarbim.kravportal.se/leveransskedet#metoder-och-rutiner)<br>


#### Leveransteam
*Ansvarig: huvudansvarig uppdragstagare*

Bekräfta att alla i det leveransteam som beskrivits i genomförandeplanen för BIM finns tillgängliga, att de är insatta i uppdragets krav på informationsleveranser, och vid behov genomföra utbildning.  

#### Informationsteknik

*Ansvarig: huvudansvarig uppdragstagare*

Se till att de tekniska system som finns specificerade i genomförandeplanen för BIM är implementerade, och att informationsutbytet mellan leveransteamen och till uppdragsgivaren fungerar som avsett.

#### Metoder och rutiner

*Ansvarig: huvudansvarig uppdragstagare*

Testa de metoder och rutiner för informationsproduktion som beskrivs i genomförandeplanen för BIM. Här ingår exempelvis uppdelning av informationscontainrar och tillgänglighet för gemensamma resurser och underlag.

## 6 Gemensam produktion av information
**Innehåll:**<br>
[Referensinformation och delade resurser](https://tillampningsanvisningarbim.kravportal.se/leveransskedet#referensinformation-och-delade-resurser)<br>
[Produktion och leverans av information](https://tillampningsanvisningarbim.kravportal.se/leveransskedet#produktion-och-leverans-av-information)<br>
[Kvalitetssäkring](https://tillampningsanvisningarbim.kravportal.se/leveransskedet#kvalitetssäkring)<br>
[Granskning av leverans ](https://tillampningsanvisningarbim.kravportal.se/leveransskedet#granskning-av-leverans)<br>
[Leveransteamets granskning av informationsmodellen ](https://tillampningsanvisningarbim.kravportal.se/leveransskedet#leveransteamets-granskning-av-informationsmodellen)<br>

#### Referensinformation och delade resurser
*Ansvarig: uppgiftsteam*

Kontrollera att alla har tillgång till den information de behöver i den delade arbetsmiljön. Om inte, ska den huvudansvariga uppdragstagaren meddelas, inklusive eventuell påverkan detta kan ha.

#### Produktion och leverans av information
*Ansvarig: uppgiftsteam*

Producera och leverera  information baserat på delplan för informationsleveranser. Informationen ska följa alla föreskrifter för projektet avseende informationsstandard, metoder och rutiner, nivå av informationsbehov, informationsstruktur och annat.

#### Kvalitetssäkring
*Ansvarig: uppgiftsteam*

Kvalitetssäkra varje informationscontainer avseende projektets informationsstandard innan den lämnas för intern granskning.

#### Granskning av leverans
*Ansvarig: uppgiftsteam*

Innan en informationscontainer delas för andras användning i projektets delade datamiljö ska den granskas och godkännas av uppgiftsteamet. Granskningen ska avse den huvudansvariga uppdragstagarens informationskrav, nivån för informationsbehov, och information som behövs för samordning av andra uppgiftsteam. Om leveranser inte godkänns ska orsakerna dokumenteras.

#### Leveransteamets granskning av informationsmodellen
*Ansvarig: leveransteamet*

Leveransteamet ska kontinuerligt granska informationsmodellen, och se till att den uppfyller kraven på projektets metoder och rutiner, uppdragsgivarens informationskrav och acceptanskriterier, och att huvudplanen för informationsleveranser följs.

## 7 Leverans av informationsmodell
**Innehåll:**<br>
[Inlämning av informationsmodell till huvudansvarig uppdragstagare](https://tillampningsanvisningarbim.kravportal.se/leveransskedet#inlämning-av-informationsmodell-till-huvudansvarig-uppdragstagare)<br>
[Huvudansvarig uppdragstagares granskning av informationsmodellen](https://tillampningsanvisningarbim.kravportal.se/leveransskedet#huvudansvarig-uppdragstagares-granskning-av-informationsmodellen)<br>
[Inlämning av informationsmodell till huvudansvarig uppdragsgivare](https://tillampningsanvisningarbim.kravportal.se/leveransskedet#inlämning-av-informationsmodell-till-huvudansvarig-uppdragsgivare)<br>
[Uppdragsgivarens granskning av informationsmodellen](https://tillampningsanvisningarbim.kravportal.se/leveransskedet#uppdragsgivarens-granskning-av-informationsmodellen)<br>

#### Inlämning av informationsmodell till huvudansvarig uppdragstagare
*Ansvarig: uppgiftsteam*

Lämna godkända informationscontainrar till huvudansvarig uppdragstagare för granskning och godkännande.

#### Huvudansvarig uppdragstagares granskning av informationsmodellen
*Ansvarig: huvudansvarig uppdragstagare*

Kontrollera att levererade informationscontainrar uppfyller projektets metoder och rutiner. Granskningen avser att det är rätt leverabler, och att de uppfyller samtliga informationsutbyteskrav, acceptanskriterier och nivå för informationsbehov.

#### Inlämning av informationsmodell till uppdragsgivare
*Ansvarig: uppgiftsteam*

Lämna godkända informationscontainrar till uppdragsgivaren för granskning och godkännande.

#### Uppdragsgivarens granskning av informationsmodell
*Ansvarig: uppdragsgivare*

Kontrollera att levererade informationscontainrar uppfyller projektets metoder och rutiner. Granskningen avser att det är rätt leverabler, och att de uppfyller samtliga informationsutbyteskrav, acceptanskriterier och nivå för informationsbehov.

Icke godkända leveranser görs om och lämnas till ny granskning.

Godkända leveranser blir en slutgiltig leverabel i projektets delade datamiljö.



## 8 Projektavslut
**Innehåll:**<br>
[Arkivera PIM](https://tillampningsanvisningarbim.kravportal.se/leveransskedet#arkivera-pim)<br>
[Samla in lärdomar](https://tillampningsanvisningarbim.kravportal.se/leveransskedet#samla-in-lärdomar)<br>

#### Arkivera PIM
*Ansvarig: uppdragsgivare*

När projektet avslutats ska uppdragsgivaren arkivera informationen i den delade datamiljön. Beakta då behoven för tillgångsinformationsmodellen, liksom framtida behov av åtkomst och användning, till exempel som underlag för framtida projekt.

#### Samla in lärdomar
*Ansvarig: uppdragsgivare, huvudansvariga uppdragstagare*

Tillsammans med samtliga huvudansvariga uppdragstagare ska uppdragsgivaren samla in lärdomar från projektet för användning i framtida projekt. Detta bör göras kontinuerligt under hela projektet.
