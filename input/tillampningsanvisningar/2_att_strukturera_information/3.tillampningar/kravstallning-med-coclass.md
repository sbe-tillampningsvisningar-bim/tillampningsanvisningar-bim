# Kravställning med CoClass

Eftersom alla klasser i CoClass primärt baseras på objektets funktion passar systemet väl för att ställa krav på något som ska byggas eller byggas om, utan att därmed bestämma hur det ska konstrueras och byggas.

Utgångspunkten för all kravställning är att en verksamhet vill skapa nya eller förbättrade möjligheter till aktiviteter eller tekniska funktioner av olika slag. För att åstadkomma detta behöver man **byggresultat**: ”byggobjekt som är format eller förändrat som ett resultat av en eller flera byggprocesser som använder en eller flera byggresurser”, som det definieras i ISO 12006-2. 

Med hjälp av byggvaror, arbetskraft, hjälpmedel och information skapas fysiska resultat som ingår i nya eller förändrade byggnadsverk. Under byggprocessens gång skapas också en stor mängd information som används i projektering, produktion, drift och underhåll.

CoClass kan användas för att strukturera de krav som ställs i enlighet med processbeskrivningen i ISO 19650. Krav ställs på enskilda objekt, som identifieras genom sin klass i en viss tabell. Relationerna mellan de tabeller som ingår i systemet åstadkommer en **kravhierarki**, där krav som ställs på en hög nivå gäller för de lägre nivåerna, så vida inte annat anges.

I alla tabeller är klassindelningen primärt baserad på funktion, i andra hand på läge eller form (utformning, teknisk lösning). I CoClass finns följande tabeller för byggresultat som kan användas för kravställning:
- byggnadsverkskomplex
- byggnadsverk
- utrymmen
- funktionella system
- konstruktiva system
- komponenter.


## Metod
Kravställning med CoClass går till på följande sätt:

1. Bygg upp en informationsstruktur som visar alla objekt som ska kravställas. Gå uppifrån och ner i CoClass tabeller; med fokus främst på utrymmen.
1. Lägg till egenskaper på objekten som beskriver kraven. Här ligger fokus på två grupper av intressenter:
- de som ska använda utrymmena
- de som ska drifta och underhålla anläggningen eller byggnaden.

Krav ärvs enligt sambandet mellan tabellerna i CoClass:

- Krav som ställs på ett **byggnadsverkskomplex** gäller för samtliga ingående byggnadsverk. Exempel: *om krav ställs på uppvärmning med fjärrvärme, gäller kravet alla byggnader i området.*
- Krav som ställs på ett **byggnadsverk** gäller för samtliga ingående utrymmen och byggdelar. Exempel: *om krav ställs att en byggnad ska ha en viss maximal energianvändning per år, måste alla byggdelar som påverkar detta utformas så att de bidrar till att uppfylla kravet (väggar, bjälklag, yttertak, värmesystem, luftbehandlingssystem).*
- Krav som ställs på ett **utrymmeskomplex** gäller alla utrymmen som ingår i det. Exempel: *krav som ställs på en lägenhet gäller alla dess utrymmen.*
- Krav som ställs på ett **utrymme** gäller samtliga byggdelar som åstadkommer det och som ingår i det. Exempel: *om krav ställs på ljudmiljö i ett utrymme, måste omslutande väggar och bjälklag utformas så att de tillräckligt mycket dämpar ljud utifrån, och att ventilationen inte låter för mycket.*
- Krav på **funktionellt system** gäller för samtliga ingående konstruktiva system och komponenter. Exempel: *krav på bärighet på en vägkropp innebär att överbyggnad och slitlager måste tåla en viss last.*
- Krav på **konstruktivt system** gäller för samtliga ingående komponenter. Exempel: *krav på ett pumpsystem innebär att pumpar och ledningar ska klara en viss kapacitet.*
- Krav på **komponenter** gäller alla förekomster av komponenten i sammanhanget (byggnadsverket, utrymmeskomplexet, utrymmet eller systemet). Exempel: *krav på fönster ljudklass i en viss yttervägg gäller alla fönster i det.*

## Verktyg
För närvarande finns två digitala verktyg från Svensk Byggtjänst för kravställning med CoClass:

- [AMA Funktion](https://byggtjanst.se/ama/amafunktion), som är en webbtjänst för att ta fram ett förfrågningsunderlag för upphandling av totalentreprenad. Verktyget bygger upp en strukturerad databas med objekt och egenskaper. Denna kan skrivas ut till ett pdf-dokument som påminner om tekniska beskrivningar enligt AMA.
- [CoClass Studio](https://byggtjanst.se/tjanster/coclass-studio), där användare kan bygga upp en struktur med objekt och egenskaper. Denna är åtkomligt för andra webbapplikationer genom API. Strukturen kan också exporteras till en strukturerad textfil för fortsatt bearbetning i exempelvis ordbehandlare eller kalkylprogram.

Det går givetvis att använda ordbehandlare direkt genom att använda rubriknivåer som innehåller CoClass-objekt. För varje nivå beskriver man i klartext vilka krav man ställer. På det sättet åstadkoms något som liknar traditionella rumsfunktionsprogram, som är kompletterade med krav på hela byggnadsverket. Resultatet blir en **funktionsbeskrivning.**

Krav på utrymmen som avseravseende ytskikt, installationer och annat ställs då genom att respektive objekt ”stoppas in” i utrymmet. Exempel: om väggar ska målas används *B.RE11.FSZ10 Väggsystem > Inre ytkompletteringar på vägg > Färg*. Om en toalettstol ska finnas, används *G11. JD11.XKB Normalspillvattensystem > Invändigt avloppstransportsystem med självfall > Toalettstol*.
