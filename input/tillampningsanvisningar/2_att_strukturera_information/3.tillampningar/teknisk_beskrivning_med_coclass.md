# Teknisk beskrivning med CoClass och AMA

Tekniska beskrivningar i bygg- och anläggningsprojekt görs nästan uteslutande baserat på AMA-publikationerna från Svensk Byggtjänst. Här finns väl beprövade tekniska lösningar inom alla teknikområden. AMA-böckerna kompletteras med RA (Råd och Anvisningar) som hjälper projektören att ställa krav och redovisa relevanta uppgifter till entreprenören.

AMA innehåller koder och rubriker för två begrepp:

-   **Byggdelar (BD)**, som helt enkelt är delar av byggnadsverk: grundläggning, väggar, ventilationssystem och så vidare. AMA innehåller framför allt kvalitetskrav på byggdelarna, utan att föreskriva hur detta ska uppnås.
-   **Produktionsresultat (PR)**, som är föreskrifter för det arbete och material som behövs för att åstadkomma byggdelarna. För varje byggdel krävs ett eller flera produktionsresultat.

I dagsläget är byggdelarna i AMA helt baserade på klasser enligt BSAB 96. Tabellen för produktionsresultat finns dock både i BSAB 96 och i CoClass. I detta dokument beskrivs hur AMA kan användas baserat helt på CoClass, och hur detta kan kopplas till CAD-modeller.

I de flesta tekniska beskrivningar som görs i dag finns ingen direkt koppling mellan byggdelarna och de produktionsresultat som ska realisera dem. Byggdelarna finns enbart för att ställa generella krav på funktion och/eller utförande, till exempel på väggkonstruktioner eller varmvattensystem.

Man skulle kunna säga att detta leder till att produktionsresultaten inte ”vet” vilka byggdelar som de åstadkommer. Beskrivningsförfattaren måste berätta för läsaren vilka byggdelar som avses. Exempel:

![Bild 1 Teknisk beskrivning](../3.tillampningar/media/tb_1.png)



Det blir lättare att förstå sambandet mellan byggdelarna och produktionsresultaten om ordningen kastas om. I markbeskrivningar kan kraven struktureras först per utrymme, därefter per byggdel som skapar dem:

![Bild 2 Teknisk beskrivning](../3.tillampningar/media/tb_2.png)




Här ser man att produktionsresultatet *DCB.213* ska användas som en del i överbyggnaden för den hårdgjorda ytan A1. (Se också Bild 2.)

En effekt av denna metod är förstås att produktionsresultaten upprepas för varje byggdel som har dem som ”ingrediens”. Åtminstone under en övergångsperiod kan detta upplevas som en nackdel. I en välstrukturerad beskrivning i digital form är det dock inga problem att beräkna totala omfattningen av ett visst produktionsresultat.

En tydlig fördel är dock att man kan använda samma dokument för att först ställa krav, därefter fylla på med de byggdelar och produktionsresultat som behövs. Det kan göras av skilda individer: en som ställer krav, en som tar fram tekniska lösningar. Detta kan göras av projektörens organisation i en upphandling av en utförandeentreprenad, eller av projektör respektive en entreprenör i en totalentreprenad.

## PR-koder i CAD-modeller

I utförandeentreprenader som använder standardkontraktet AB 04 har den tekniska beskrivningen högre rang än ritningar och modeller. En uppgift som förekommer i en beskrivning har alltså företräde framför sådant som redovisas exempelvis i en CAD-fil.

En genomtänkt kombination av CAD-modell och teknisk beskrivning kan i hög grad rationalisera projekteringsarbetet. Ju mer som redovisas i den tekniska beskrivningen, desto mindre behöver CAD-objekten belastas med egenskaper som beskriver hur de ska utföras.

Ett enkelt sätt att göra detta på är att ge CAD-objekten ett attribut som kopplar det till ett eller flera produktionsresultat. Genom att klicka på ett objekt i en programvara kan man då idealt direkt få uppgifterna från AMA om hur byggdelen ska realiseras. Man behöver då inte gå omvägen via en teknisk beskrivning.

![exempel1](../3.tillampningar/media/exempel1.png)

*Bild 1: Exempel på informationsmodell över en skolgård byggd i CoClass Studio. Byggdelen UTA10 Fyllning har egenskapen **PRWR Produktionsresultat** med värdet **CEB.1123**. I AMA finns krav på fyllningen som ska följas. Krav på utförande och material finns här också lagrat i objektet, men skulle alternativt kunna finnas i en teknisk beskrivning.*

Från CAD-modellen kan man också tänka sig ett utdrag i textform som förtecknar alla objekt i form av byggdelar inklusive dess kopplade produktionsresultat. Utdraget kan ligga som underlag för en teknisk beskrivning.

## Teknisk beskrivning med CoClass byggdelar

I dagsläget finns inget stöd för en automatiserad överföring mellan CoClass Studio och AMA Beskrivningsverktyg från Svensk Byggtjänst. Beskrivningsverktyget innehåller dessutom endast byggdelar enligt BSAB 96. Den som vill göra en teknisk beskrivning baserad på CoClass får i stället använda mer manuella metoder. När detta väl är gjort, kan man dock enkelt spara beskrivningen som en mall som utgångspunkt för nästa beskrivning.

Två typer av verktyg kan då användas: textbehandling med exempelvis Microsoft Word eller Google Docs, eller kalkylprogramvara typ Microsoft Excel eller Google Kalkylblad.

## Teknisk beskrivning med textbehandlare

Nedan visas i ett exempel på en textbehandlad beskrivning där CoClass-klassificerade objekt kopplats till AMA. CoClass egenskaper används för koppling till kod och rubrik i AMA, och för att föreskriva de krav som enligt RA ska anges i beskrivningen.

![Bild 3 Teknisk beskrivning](../3.tillampningar/media/tb_3.png)
![Bild 4 Teknisk beskrivning](../3.tillampningar/media/tb_4.png)



## Teknisk beskrivning med kalkylprogram

Nedan visas i ett exempel på en teknisk mängdbeskrivning gjord med Microsoft Excel, som är en kopia av den som visas i Bild 1. Informationsstrukturen visas genom att raderna i kalkylbladet givits olika nivåer. Läs mer om detta i *Skapa en informationsstruktur med CoClass.*

![Teknisk beskrivning](../3.tillampningar/media/teknisk_beskrivning_bild9.png)

*Bild 2: Exempel på teknisk beskrivning gjord i kalkylprogram.*

I exemplet fyller den som gör beskrivningen i värden i de rosa cellerna i kolumn D och E. Anbudsgivare kan sedan fylla i värden för aktiviteter och i kolumn F och G för att ta fram en prissatt mängdförteckning.

De grå cellerna med orange text innehåller beräkningar, där exempelvis volymen fyllningsmaterial är produkten av arean för yta *A1: Rundslingan* (235 m2) och tjockleken på lagret (250 mm), vilket ger 59 m3.

Utrymmet BEE10 Hårdgjord yta (A1:Rundslingan) är här kravställt med hjälp av egenskaperna *CNDN Utformning* och *USBE Dimensioneringsgrund*.

Informationsstrukturen kan åskådliggöras genom att öppna nivå efter nivå.

![Teknisk beskrivning nivå 1](../3.tillampningar/media/teknisk_beskrivning_bild3.png)

*Bild 3: Nivå 1 visar byggnadsverk och utrymmen.*

![Teknisk beskrivning nivå 2](../3.tillampningar/media/teknisk_beskrivning_bild4.png)

*Bild 4: Nivå 2 visar funktionella system i utrymmet. (Här endast i utrymme BEE10.)*

![Teknisk beskrivning nivå 3](../3.tillampningar/media/teknisk_beskrivning_bild5.png)

*Bild 5: Nivå 3 visar konstruktiva system.*

![Teknisk beskrivning nivå 4](../3.tillampningar/media/teknisk_beskrivning_bild6.png)

*Bild 6: Nivå 4 visar komponenter.*

![Teknisk beskrivning nivå 5](../3.tillampningar/media/teknisk_beskrivning_bild7.png)

*Bild 7: Nivå 5 visar egenskapen PRWR Produktionsresultat för alla objekt.*

![Teknisk beskrivning nivå 6](../3.tillampningar/media/teknisk_beskrivning_bild8.png)

*Bild 8: Nivå 6 visar alla egenskaper för alla objekt.*

## Koppling mellan teknisk beskrivning och CAD-modell
Svensk Byggtjänst har i dag ingen tjänst som möjliggör för externa programvaruleverantörer att koppla CAD-modeller direkt till AMA. Att lagra all information som finns i en teknisk beskrivning är inte heller rimligt att lägga in som egenskaper på CAD-objekten.

En mellanväg att gå är att ha en egenskap på CAD-objektet där kod eller koder för produktionsresultat anges. Den som använder CAD-modellen i produktion kan då söka upp koden i en digital AMA för att se vilka föreskrifter som gäller. Vilken hjälp detta skulle ge i praktiken är dock osäkert.

En något mer utvecklad variant är att använda CoClass referensbeteckningar både i CAD och i den tekniska beskrivningen. Användaren kan då se vilka produktionsresultat som ska användas, tillsammans med de projektspecifika krav som finns i beskrivningen.

I bilden nedan har beskrivningen kompletterats med en kolumn som visar en något förenklad referensbeteckning, utan prefix som visar de olika aspekterna. Se *RDS-manual för CoClass*.

Exempel: referensbeteckningen BEE4/A1.CA1.UTA1 ska formellt skrivas ++BEE4/-A1.CA1.UTA1 för att fyllningen i den hårdgjorda ytan.

Om samma beteckning finns på motsvarande CAD-objekt får användaren en komplett bild av kraven som ställs.

![Bild 9](../3.tillampningar/media/teknisk_beskrivning_bild9.png)

*Bild 9: Teknisk beskrivning kompletterad med en kolumn som visar en förenklad referensbeteckning.*
