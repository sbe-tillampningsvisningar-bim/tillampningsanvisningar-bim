# Klimatdeklaration med CoClass

Sedan början av 2022 ska [vissa husbyggnadsprojekt](https://www.boverket.se/sv/klimatdeklaration/vilka-byggnader/ska-deklareras/) lämna in en klimatdeklaration i samband med att slutbesked erhållits från kommunen. Klimatdeklarationen ska göras i en e-tjänst hos den ansvariga myndigheten Boverket. Här samlas alla uppgifter i ett klimatdeklarationsregister, som kommer att bli ett viktigt underlag för kommande gränsvärden.

Deklarationen omfattar i dagsläget endast byggskedet. Klimatpåverkan från drift och underhåll av byggnaden behöver inte deklareras. Ett mer energisnålt hus gynnas alltså inte i deklarationen; snarare tvärtom eftersom det troligen behöver mer isoleringsmaterial. Byggnadens energiklass och ljudklass ska dock anges.

Som underlag för deklarationen behövs två datakällor: mängduppgifter på allt som ingår, och klimatberäkningar för de material som används. Sådana beräkningar finns det flera källor till, bland andra [Boverkets klimatdatabas](https://www.boverket.se/sv/klimatdeklaration/klimatdatabas/); [Byggsektorns miljöberäkningsprogram](https://www.ivl.se/projektwebbar/byggsektorns-miljoberakningsverktyg.html), BM från IVL; [Anavitor](https://www.nordicbim.com/sv/losningar/anavitor-lca); och [One Click LCA](https://www.oneclicklca.com/se/).

Boverket har beslutat att följande byggdelar ska ingå i beräkningarna:

- Bärande delar i grundläggning, inklusive isolering.
- Övriga bärande delar: stomme, bjälklag, trappor, balkonger med mera.
- ”Klimatskärm”: ytterväggar, ytterdörrar, fönster, yttertak.
- Innerväggar, innerdörrar.

![infab](../3.tillampningar/media/infab.svg)

*Illustration: Infab, källa [Boverket](https://www.boverket.se/sv/klimatdeklaration/gor-sa-har/byggdelar-som-ingar/).*

Indelningen är inte baserad på något klassifikationssystem, utan mer grundad i en pragmatisk och produktionsinriktad gruppering, inspirerat av BSAB 83 och dess varianter. Den är därmed inte heller helt enkel att ge underlag för i en CAD-modell, med mindre än att varje byggdel får en egenskap som visar om den tillhör någon av grupperna.

Om däremot alla byggdelar redan är klassificerade med hjälp av CoClass underlättas arbetet med att ta fram beräkningsunderlaget. Det som ska byggas beskrivs med valfri nivå av komplexitet. Vanligen räcker det med att klassificera objekt ner till nivån konstruktivt system, och låta varje system få en typbeteckning som beskriver den. Detaljerna om vad som ingår i varje typ av konstruktion kan lagras på annat ställe, till exempel i ett receptregister.

Om man använt CoClass i CAD-modellen omfattar deklartationen alla objekt som ingår i följande system:

- A20 Grundläggning
- B Väggsystem
- C Bjälklagssystem
- D Yttertakssystem
- S.RE12 Anordningar > Yttre ytkompletteringar på vägg.

Den sista krävs för att få med de yttre ytskikten – färg, puts och annat – som ska beräknas. I CoClass tillhör dörrar och fönster till väggar och yttertak, medan ytskikten hör till det funktionella systemet *S Anordningar*.

Det är dock inte nödvändigt att modellera byggnaden i CAD. Det kan räcka med ritningar så att man kan mäta längd och höjd på de ingående byggdelarna. Sedan kan man använda Svensk Byggtjänsts verktyg CoClass Studio för att bygga en informationsmodell som beskriver byggnaden. Här kan man ta fram underlag för mängdning genom att ange antingen mått på varje enskild byggdel, eller genom en summering per byggdelstyp. Bilden nedan visar ett exempel på hur det kan se ut:

![CoClass studio](../3.tillampningar/media/coclass-studio.png)

*Exempel på en fiktiv kontorsbyggnad i CoClass Studio. Baserat på arkitektritningar har man här fört in total längd och höjd på B10 Ytterväggssystem, och fört in antalet fönster.*

En sådan struktur kan exporteras till en strukturerad textfil (i JSON-format) och kopplas till recept i entreprenörens kalkylverktyg. Receptet innehåller alla komponenter som ingår. Komponenterna, materialen och mängderna som kan sedan föras in i något av de nämnda verktygen för klimatberäkning. En byggdelstyp kan här klimatberäknas baserat på generiska data, eller på faktiska produktdeklarationer (EPD:er).

Strukturen kan också byggas upp i valfritt kalkylprogram, till exempel Excel eller Google Kalkylark. Se exempel i anvisningen *Skapa en informationsstruktur med CoClass*.

Det svenska kravet på klimatdeklaration följer EU:s ambition att alla industrier ska minska sin miljö- och klimatpåverkan. En uppdatering är på gång vad gäller byggproduktförordningen, som kommer att innebära att alla byggprodukter ska redovisas med hjälp av en digital produktdeklaration, baserat på europeisk standard: en produktdatamall.

Flera projekt inom Smart Built Environment har och kommer att utveckla metoder och svenska tillämpningar för detta. Målet är att förenkla arbetet med att ta fram korrekta klimatdeklarationer, baserade på maskinläsbara värden för faktiska produkter i stället för schablonvärden.

Läs mer:

<https://www.boverket.se/sv/klimatdeklaration/> 

<https://coclass.byggtjanst.se/> 

<https://smartbuilt.se/projekt/informationsinfrastruktur/data-templates/> 



