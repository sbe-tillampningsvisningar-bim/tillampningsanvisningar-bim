# Skapa en informationsstruktur med CoClass

För att möjliggöra effektiv förvaltning krävs tillräcklig och tillförlitlig information om de byggda tillgångarna. Om informationen dessutom är väl strukturerad underlättas både användning och uppdatering.

I den här beskrivningen används referensbeteckningar baserade på CoClass. För mer information om referensbeteckningar, se RDS-manual för CoClass.

Med hjälp av CoClass tabeller för byggnadsverk, system och komponenter går det enkelt att skapa sådana informationsstrukturer. Med hjälp av dessa kan man:

-   få en helhetsbild av det byggda
-   identifiera alla delar av intresse
-   visa deras funktion
-   visa hur delarna är uppbyggda
-   visa var de är lokaliserade
-   visa deras inbördes relationer, till exempel hur funktioner i ett system försörjs av andra.

Att identifiera klass för **byggnadsverk** är inte svårt: är det ett AAA Småhus; ett BBH Fyrtorn; en CAA Bro och så vidare. Alla objekt som beskrivs i informationsstrukturen för ett visst byggnadsverk ska ha unika referensbeteckningar, som visar deras klass och löpnummer.

Nästa steg blir att identifiera vilka **funktionella system** som finns i byggnadsverket. För byggnader handlar det om väggsystem, bjälklagssystem, ventilationssystem, elsystem, värmesystem, passagesystem och så vidare. I Bild 1 visas ett förenklat exempel på ett småhus.

![Exempel](../3.tillampningar/media/exempel_funktionella_system.png)

*Bild 1: Exempel på funktionella system i en villa. Källa: CoClass – Informationshantering för byggd miljö, Klas Eckerberg, Svensk Byggtjänst 2019.*

När byggnadsverket delats upp i olika system går det att börja ställa funktionella krav utan att specificera den tekniska lösningen. En sådan tidig kravställning kan användas vid upphandling av projektering eller av totalentreprenader.

Som exempel kan krav ställas på det funktionella luftbehandlingssystemet: att det ska ha en effektivitet enligt VAS 1500, 83 % värmeåtervinning, ljudkrav på don med mera. I detta tidiga skede blir informationsstrukturen enkel:

![informationsstruktur 1](../3.tillampningar/media/struktur_1.png)

Småhuset har alltså ett luftbehandlingssystem, som man kan ställa krav på. Kravstrukturen beskriver inte den tekniska lösningen, utan det är upp till projektören eller entreprenören att ta fram en lösning som uppfyller funktionskraven.

I nästa steg kan det funktionella systemet beskrivas med hjälp av dess delar i form av **konstruktiva system** och **komponenter**. Hur komplex beskrivningen blir beror förstås på hur stort det aktuella byggnadsverket är.

Exempel: i ett flerbostadshus kan varje lägenhet få ett eget luftbehandlingsaggregat som bildar ett eget funktionellt system, med ett enda konstruktivt system för luftdistributionen. Om man i stället väljer ett centralt aggregat så finns det bara ett funktionellt system i huset, men desto fler konstruktiva system för varje våningsplan eller andra zonindelningar. I Bild 2 visas ett exempel på ett lägenhetsaggregat.

![exempel luftbehandlingssystem](../3.tillampningar/media/exempel_luftbehandling.png)

*Bild 2: Exempel på ett luftbehandlingssystem i ett flerbostadshus. Källa: AMA Funktion 22, Svensk Byggtjänst 2022.*

Exempel på det senare:

![Informationsstruktur med CoClass 2](../3.tillampningar/media/struktur_2.png)

Strukturen varierar självklart också mellan olika funktionella system. Elkraftssystemet i ett flerbostadshus har vanligtvis ett enda försörjande elkraftssystem i form av en servis in i byggnaden. Sedan har man ett konstruktivt system per lägenhet i form av en gruppcentral.

Liknande uppdelning finns för avloppssystemet: ett funktionellt system, och sedan ett konstruktivt system för stamledningen på varje våningsplan. På dessa kopplas ingående komponenter i form av golvbrunnar, toalettstolar med mera. Exempel:

![Inormationsstruktur med CoClass 3](../3.tillampningar/media/struktur_3.png)

Observera att elkraftdistributionssystemet här fått ett löpnummer som är samma som för lägenheten. En sådan ”siffra med betydelse” är okej att använda så länge det är dokumenterat.

Som alternativ till att låta löpnumret visa vilken lägenhet strömbrytaren finns i är att använda lokaliseringsaspekten:

![Informationsstruktur med CoClass 4](../3.tillampningar/media/struktur_4.png)


Alternativt kan man starta med lokaliseringsaspekten och visa vilka funktioner som finns i ett visst rum:

![Informationsstruktur med CoClass 5](../3.tillampningar/media/struktur_5.png)

Ett mer komplett exempel på informationsstruktur visas om man klickar på bilden nedan. Denna är byggd i verktyget CoClass Studio (Svensk Byggtjänst).


![Informationsstruktur](../3.tillampningar/media/informationsstruktur.png)

## Strukturera i kalkylblad

Ett enkelt verktyg för att själv bygga informationsstrukturer är att göra det i ett kalkylblad, till exempel med Excel eller Google Kalkylark. Här ka man steg för steg beskriva sin byggnad eller anläggning.

I filen *Exempelstruktur.xlsx* (nedladdningsbar via länk) finns en vårdbyggnad beskriven (samma som finns som Revit-fil och IFC). Den utgör ”toppnoden” **AJB1 Vårdbyggnad**, av typen **AJB10 Sjukhusbyggnad**. Därefter kommer sju nivåer:

1.  Utrymmeskomplexet **XDA Våningsplan**, respektive de **funktionella system** som är gemensamma för våningsplanet.
2.  Utrymmeskomplexet **XBB Vårdavdelning**, respektive de **konstruktiva system** som är gemensamma för våningsplanet.
3.  Utrymmena i varje vårdavdelning (**EAC Korridor** med flera), respektive de **komponenter** som är gemensamma för våningsplanet.
4.  De **funktionella system** som skapar eller utrustar varje utrymme.
5.  De **konstruktiva system** som skapar eller utrustar varje utrymme.
6.  De **komponenter** som skapar eller utrustar varje utrymme.
7.  De **komponenter** som är en del av en annan komponent.

I den övre delen av bladet visas byggnaden utifrån en **funktionsaspekt**. Här är både utrymmen och byggdelar visade som funktioner. Tankegången är så här:

-   Med utgångspunkt från ett lokalprogram, och tillgänglig byggnadsyta, inser arkitekten att det krävs en byggnad i två våningar. En enkel CAD-modell byggs upp som visar samliga rum i byggnaden.
-   Toppnoden **AJB1 Vårdbyggnad** läggs in överst i strukturen.
-   På Nivå 1 läggs våningsplanen in: **XDA10** och **XDA11**, numrerade enligt Lantmäteriets metod när 10 är entréplan.
-   I varje våningsplan läggs utrymmen in: **EAC1006 Korridor**, **CAC1007 Medicinförråd** och så vidare.
-   Utrymmena förses med byggdelar i form av funktionella system, konstruktiva system och komponenter.

![Exempelstruktur](../3.tillampningar/media/exempelstruktur_bild3.png)

*Bild 3: Utdrag från Exempelstruktur.xlsx.*

I de gulfärgade cellerna visas några exempel på associativa relationer mellan objekt: **Försörjer/Försörjs** av respektive **Styr/Styrs av**.

Den nedre delen av kalkylbladet visas samma byggnad men ur **produktaspekten**. Här finns inga utrymmen, endast byggdelar. Dessa är samma som de som visas ur funktionsaspekten. Några pilar visar exempel på denna ”dubbla identitet”.

## Informationsstruktur som förfrågningsunderlag

En sådan här informationsstruktur kan utvecklas vidare genom att varje objekt förses en kolumn som innehåller krav på egenskaper, och ytterligare en eller flera kolumner där en anbudsgivare kan ge sina svar i form av teknisk lösning och kostnad. Så utformad kan strukturen användas som en del i ett förfrågningsunderlag.

Nedan visas ett exempel, där informationsstrukturen visas genom att raderna i kalkylbladet givits olika nivåer. Här har objekten fått CoClass egenskaper. Anbudsgivaren beskriver sina föreslagna tekniska lösningar i de rosa cellerna.

![Exempelstruktur_bild 4](../3.tillampningar/media/exempelstruktur_bild4.png)

*Bild 4: Exempel på informationsstruktur använd som förfrågningsunderlag.*

Jämfört med traditionella förfrågningsunderlag får man i stället för ett dokument från varje teknisk disciplin ett enda dokument som visar samtliga krav. Svaren från anbudsgivare kan därmed enklare jämföras med varandra.
