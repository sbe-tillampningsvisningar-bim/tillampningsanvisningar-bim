# **Kostnadskalkylering med CoClass**

Det traditionella sättet att kostnadskalkylera bygg- och anläggningsprojekt är att mer eller mindre manuellt identifiera objekt på ritningar eller i CAD-modeller, räkna dem, och sedan påföra tidsåtgång, kostnader för arbete och inköp, och på nedersta raden få en slutsumma. I bästa fall finns en mängdförteckning, eller en mängdsatt teknisk beskrivning att utgå ifrån.

Arbetet underlättas i hög grad om det finns färdiga recept på de ingående byggdelarna. Det kan vara enskilda komponenter som fönster och dörrar, men också sammansatta system som olika typer av väggkonstruktioner. Större entreprenörer har väl utvecklade system för detta, med interna metoder för att beskriva, koda och strukturera objekten och deras information.

Kopplingen mellan underlaget i form av en CAD-modell och kalkylprogrammet kan i hög grad automatiseras om samma informationsmodell används av projektören och den som ska kalkylera. Detta åstadkoms bäst med CoClass. Till skillnad från BSAB 96 finns här byggdelar på alla nivåer av sammansättning klassificerade: funktionella system, konstruktiva system och komponenter. Till alla dessa finns också fastställda typer, ofta baserade på svensk praxis i form av AMA.

![Konstruktiva system](../3.tillampningar/media/konstruktiva_system.png)

*Bild 1: Exempel på typer av konstruktiva system i CoClass.*

Om objekt i CAD-modellen är klassificerade enligt CoClass blir det en enkel sak att exportera ut mängder och sedan föra dem vidare till kalkylprogrammet.

![Exempel mängdförteckning](../3.tillampningar/media/mangdforteckning.png)

*Bild 2: Exempel på mängdförteckning för fönster från en CAD-modell. Varje fönster har här en unik referensbeteckning, beräknad utifrån klasserna för objektet, dess förälder, förförälder och löpnummer. Varje fönster har dessuom en typbeteckning enligt BIP-koder och en BSAB 96-kod. Fönstren är också kopplade till utrymmet de finns i (kolumn ccPosLocalization).*

Att utgå från en CAD-modell är dock inget krav. Enkla arkitektskisser som visar rumsindelning, dörrar, fönster och inredning räcker utmärkt som underlag för tidiga kalkyler. Genom att mäta och räkna på ritning kan mängder föras in i en informationsmodell och sedan kompletteras med kostnader för material och arbete.

Informationsmodellen kan byggas upp i CoClass Studio, eller i valfritt kalkylprogram, till exempel Microsoft Excel eller Google Kalkylark. Se exempel i ”Skapa en informationsstruktur med CoClass".

I bilden nedan visas ett fiktivt och förenklat exempel på kostnadsberäkning för en platsgjuten väggstomskiva. I de rosa cellerna fyller kalkylatorn in kända värden, som sedan beräknas i de ljusgrå med orange text. Här används CoClass klasser för byggdelar, försedda med beskrivande egenskaper.

![Exempel kalkyl](../3.tillampningar/media/exempel_kalkyl.png)

*Bild 3: Exempel på kalkyl baserad på CoClass.*