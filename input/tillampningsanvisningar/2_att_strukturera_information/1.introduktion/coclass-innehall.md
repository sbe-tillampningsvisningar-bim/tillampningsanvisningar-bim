# CoClass innehåll

CoClass innehåller en rad tabeller som på olika sätt beskriver den byggda miljön genom att klassificera objekt, och genom att på olika sätt beskriva objekten.

## CoClass grunder

CoClass är en svensk tillämpning och utökning av ett antal internationella standarder. Inga avsteg görs från de principer och metoder som standarderna föreskriver.

### SS-EN ISO 12006-2:2015

Vad gäller den grundläggande synen på klassifikation av den byggda miljön är CoClass en tillämpning av den internationella standarden SS-EN ISO 12006-2 *Strukturering av information om byggnadsverk – Del 2: Ramverk för klassificering (av information).* Här finns förslag på ett antal tabeller, varav de flesta finns i CoClass.

### SS-EN IEC 81346-2:2019 och SS-EN ISO 81346-12

Grunden för indelningen av klasser av utrymmen och byggdelar finns i den internationella standardserien IEC/ISO 81346.

-   I SS-EN IEC 81346-2 *Struktureringsprinciper och referensbeteckningar – Del 2: Klassificering av objekt och koder för klasser* finns klasser för utrymmen och komponenter.
-   I SS-ISO 81346-12:2019 *Struktureringsprinciper och referensbeteckningar – Del 12: Bygg- och installationssystem* finns klasser för funktionella system och för konstruktiva system.

Övriga tabeller är CoClass egna, men bygger på samma princip för att skapa klasser.

## Tabeller för byggresultat

För närvarande finns följande tabeller som beskriver fysiska byggresultat:

-   **Byggnadsverkskomplex**, som är en ”samling av ett eller flera byggnadsverk avsett att betjäna minst en funktion eller brukaraktivitet”.  
    *Exempel: AAA Småhusområde, BFD Campus, CBB Reningsverk, DAA Vägnät, EBG Golfbana.*
-   **Byggnadsverk**, som är en ”självständig enhet i byggd miljö med karakteristisk form och rumslig struktur, avsedd att stödja minst en funktion eller brukaraktivitet”. Det finns två grundläggande varianter av byggnadsverk: **byggnad** (som har tak) och **anläggning** (som inte har tak).  
    *Exempel: AAA Småhus, BFA Pumpstation, CAD Järnväg, DCA Torg.*
-   **Utrymme**, som är en ”avgränsad tredimensionell utsträckning definierad fysiskt eller språkligt”. Man kan skilja på *byggt utrymme* som har fysiska gränser – till exempel ett rum eller en kraftledningsgata – och *aktivitetsutrymme* som är den plats som en aktivitet kräver. CoClass gör ingen skillnad mellan dessa varianter. Ett BBA Kök kan vara ett avskilt rum eller en zon i ett större allrum. CoClass gör heller ingen formell skillnad mellan inomhus och utomhus, även om aktiviteten som utrymmet är avsett för ofta visar detta.  
    *Exempel: AAA Rum, BEA Matsal, CAH Värdeförråd, DAD Apparatrum, EAC Korridor, FBA Gångbana, GBA Åker, HCD Detaljplanegräns, JBA Bruttoarea (BTA).*
-   **Byggdel**, som är en ”del av byggnadsverk med karakteristisk funktion, form eller läge”. Byggdelar indelas i tre nivåer:
-   **Funktionellt system**, som har en ”övergripande inneboende funktion”: att skapa utrymmen och att förse byggnadsverket med installationer och anordningar. På den här nivån beskrivs inga tekniska lösningar. Funktionella system betecknas med en bokstav.  
    *Exempel: B Väggsystem, K Elkraftssystem, S Anordningar.*
-   **Konstruktivt system**, som är en ”sammanhängande teknisk lösning med en viss inneboende funktion”. Ett funktionellt system innehåller ett eller flera konstruktiva system. Konstruktiva system betecknas med två bokstäver. På nivån en bokstav visas en överordnad funktion, på nästa nivå förtydligas funktionen.  
    *Exempel: AD Väggkonstruktion, CA Överbyggnad för väg och plan, JJ Luftdistributionssystem, MA Varningssystem, RB Inredning.*
-   **Komponent**, som är en ”produkt använd som beståndsdel i en sammansatt produkt, system eller anläggning”. En komponent kan bestå av andra komponenter. Komponenter betecknas med tre tecken. Även här innebär varje nivå en mer detaljerad funktion. Den tredje nivån visar dock ibland en typ av teknisk lösning.  
    *Exempel:* *EAA Elektrisk lampa, NCA Markbeläggning, UAA Belysningsarmatur.*

De tre tabellerna för byggdelar kombineras för att beskriva hur olika system är uppbyggda. Ett funktionellt system kan bestå av ett eller flera konstruktiva system, som i sin tur består av en eller flera komponenter. Exempel: *B Väggsystem* byggs upp av en *AD Väggkonstruktion*, som kan innehålla till exempel *ULM Väggkärna* och *NCB Väggbeklädnad.* Genom att kombinera koderna beskrivs systemets uppbyggnad: *B.AD.NCB* visar väggkärnans plats.

![CoClass vägg](../1.introduktion/media/coclass-vagg.png)

*Bild 1: Enkel väggstruktur visad i verktyget CoClass Studio.*

## Klasser och typer

I alla tabeller som innehåller klasser av byggresultat är indelningen primärt baserad på funktion, i andra hand på läge eller form (utformning, teknisk lösning).

Komponenter betecknas som nämnts med tre tecken. Vid användning av klasserna kan behov finnas att stanna på nivå ett eller två. I så fall ska ”jokertecken” i form av frågetecken (?) användas. Om det inte anses lämpligt kan annat tecken användas, till exempel understreck (_).

Exempel: Klass *G?? Genererande objekt* har på nästa nivå *GP? Vätskeflödesgenererande objekt*; i dagligt tal en ”pump”. Mer detaljerat än så behöver funktionen inte beskrivas: något som får en vätska att flöda. (Vilken typ av vätska det handlar om beskrivs när pumpen används i ett system för vatten eller bränsle eller annat.) På nivå tre finns därför klasser för olika konstruktiva varianter av pumpar, till exempel *GPB Dynamisk pump*.

CoClass går dock ytterligare ett steg. För att möjliggöra en mer detaljerad beskrivning finns som komplement en stor mängd fastställda **typer**, baserat på ytterligare egenskaper som särskiljer typen från andra medlemmar av klassen.

Typer används antingen för att specificera funktionen ytterligare, eller för att mer eller mindre detaljerat visa en teknisk lösning. Exempel på typer av komponenter: klass *NCA Markbeläggning* har typer för alla material som beskrivs också med produktionsresultat i AMA. *QQA Fönster* har däremot typer som visar på vilket sätt fönstret öppnas: slagfönster, vridfönster och så vidare.

![Fönstertyper](../1.introduktion/media/fonstertyper.png)

*Bild 2: Typer av fönster i CoClass. QQA10 är en typ av QQA, medan QQA11–QQA16 är typer av QQA10.*

Typer finns också för funktionella och konstruktiva system. Exempel: det konstruktiva systemet *AD Väggkonstruktion* har typer som *AD10 Väggkonstruktion med solid stomme*, som i sin tur har subtyper som *AD11 Väggkonstruktion med solid stomme av platsgjuten betong*.

Det funktionella systemet *F Vatten- och vätskesystem* innehåller potentiellt både dricksvatten och drivmedel. Här finns följaktligen behov att använda till exempel *F20 Tappvattensystem*, med subtyperna *F21 Tappkallvattensystem* och *F22 Tappvarmvattensystem*.

Typer finns också för övriga tabeller (byggnadsverkskomplex, byggnadsverk och utrymmen).

Om det finns ett behov av att lägga till typer utöver de som fastställts i CoClass får användare definiera typer i nummerserien 90–99, alternativt 900–999.

## Övriga tabeller

Utöver tabellerna för byggresultat finns tabeller för:

-   **Egenskaper**, som är indelade i två övergripande kategorier:
-   Materiella egenskaper, som beskriver objektets storlek, kapacitet och mycket annat på ett mätbart och objektivt sätt. Exempel: *ENCL Energiklass, MEWD Bredd, MLML Material.*
-   Administrativa egenskaper, som omfattar bland annat benämningar, adresser, ägande, klassifikation, produktion och dokumentation. Exempel: *ADRP Fastighetsadress, PRAY Aktivitet, DNDP Prestandadeklaration*.
-   **Aktiviteter** som kan kopplas till objektet. Detta kan avse både hur det ska produceras och hur det ska underhållas. Exempel: *AAD Kvalitetsledning, BDA Provtagning, CAD Modellering, EFB Spackling, EUF Räfsning.*
-   **Material och resurser**, som kan användas för att beskriva vad objektet består av. Detta är tänkt främst för klimatdeklaration och andra miljöberäkningar. Tabellen är gemensam med Boverket och IVL. Exempel: *CEFM Skumbetong, GMPB Gipsskiva.*
-   **Produktionsresultat** (”AMA-koder”), som används för att koppla objektet till föreskrifter i tekniska AMA som visar hur objektet ska produceras. Den här tabellen delas med BSAB 96. Exempel: *BFB.2 Fällning av enstaka träd, FSD.22 Väggar av betongblock, PRB.15 Golvbrunnar av plast.*

## Licenser för att använda CoClass

Det finns en avgiftsfri licens kallad **CoClass Bas**, men då får man tillgång enbart till grundtabellerna i ett ”läsläge” på skärmen. Man får då inte se alla de hundratals typerna av klasser.

För att kunna använda CoClass digitalt genom att skapa egna strukturer, och för att se kompletta tabeller, krävs en personlig licens för **CoClass Studio**.

För att komma åt alla tabeller via egna datorapplikationer krävs en företagslicens för **CoClass API**. Då når man också de strukturer man byggt upp i CoClass Studio. Dessa kan sedan delas utan extra kostnad till andra intressenter.

Mer information om kostnader finns hos [Svensk Byggtjänst](https://byggtjanst.se/tjanst/coclass).
