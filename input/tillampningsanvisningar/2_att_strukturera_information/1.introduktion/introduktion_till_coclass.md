# Introduktion till CoClass

CoClass är ett modernt objektorienterat klassifikationssystem för all byggd miljö under hela dess livscykel, och speciellt utformat för digital informationshantering. Systemet utgör en svensk anpassning till och utökning av flera internationella standarder. Det ägs av en grupp företag, organisationer och myndigheter, och förvaltas av Svensk Byggtjänst. [Läs mer om CoClass hos Svensk byggtjänst här](https://byggtjanst.se/tjanst/coclass).

## CoClass genom livscykeln

Bilden nedan visar indelningen av livscykeln. Numreringen används för att förteckna de många användningsområdena för CoClass.

![Livscykeln](../1.introduktion/media/livscykeln.png)

### **0 Utredning**

Under utredningsskedet är det knappast relevant att arbeta objektorienterat. Fokus ligger också primärt på utveckling av den aktuella verksamheten, och i mindre grad på strukturerad kravställning på eventuella byggåtgärder. Övergripande krav på kapacitet på nivån byggnadsverk är aktuella, men dessa behöver inte nödvändigtvis använda CoClass klasser.

### **1 Program**

Under programskedet fastställs kraven på det som ska byggas eller förvaltas. Här kan CoClass användas för att ställa kraven både systematiskt och hierarkiskt. Programhandlingar görs vanligen som enkla textdokument, men en strukturering med hjälp av CoClass kan underlätta både att utarbeta programmet och att ta kraven vidare in i projekteringen. Här används primärt klasser för byggnadsverk och för utrymmen, där överordnade krav ställs på kapacitet, prestanda, miljö- och klimatpåverkan och utformning.

Se vidare [Kravställning med CoClass](https://tillampningsanvisningarbim.kravportal.se/tillampningsanvisningar_bim/kravstallning-med-coclass/).

### **2 Projektering**

Gränsen mellan program och projektering är flytande. Projekteringen inleds ofta med ett systemhandlingsskede, då kraven detaljeras och övergripande tekniska lösningar fastställs. Detta kan redovisas i en **funktionsbeskrivning** baserad på CoClass. En sådan kan göras med hjälp av [AMA Funktion](https://byggtjanst.se/ama/amafunktion), [CoClass Studio](https://byggtjanst.se/tjanster/coclass-studio), eller i ett textdokument. Här används klasser för byggnadsverk, utrymmen och byggdelar.

En funktionsbeskrivning är i sig en informationsmodell som kan användas för olika syften. Den kan ligga till grund för tidiga mängdberäkningar som underlag för kostnadskalkyler och miljö- och klimatberäkningar. Se [Kostnadskalkylering med CoClass](https://tillampningsanvisningarbim.kravportal.se/tillampningsanvisningar_bim/kostnadskalkylering_coclass/) respektive [Klimatdeklaration med CoClass](https://tillampningsanvisningarbim.kravportal.se/tillampningsanvisningar_bim/klimatdeklaration-med-coclass/).

I vidare projekteringen ska kraven sedan omsättas till arkitektonisk utformning och detaljerade tekniska lösningar. En del i detta kan vara att ta fram en **byggdelsindelad teknisk beskrivning**. Byggnaden eller anläggningen beskrivs i en sådan primärt genom de byggdelar som ingår. För varje byggdel anges vilka produktionsresultat som ska åstadkomma det. Exempel: komponenten *NCA Markbeläggning* kan kopplas till produktionsresultatet *DCC.2413 Slitlager kategori B av dränerande asfaltbetong.*

Hela AMA Anläggning och större delen av de övriga tekniska AMA-delarna består av sådana koder, som alltså kan sägas följa CoClass. I AMA Beskrivningsverktyg från Svensk Byggtjänst används i dag byggdelar från BSAB 96. I framtiden kan dessa komma att ersättas med CoClass byggdelar.

Inget hindrar dock att man med hjälp av andra verktyg skapar beskrivningar som helt bygger på CoClass. Referenser till krav på byggdelar i AMA måste då ersättas med klartext i beskrivningen. Läs mer om detta i Teknisk beskrivning med CoClass.

En viktig del i objektorienterat arbete är att kunna spåra individuella objekt. Här kan **referensbeteckningar** användas för att beskriva digitala objekt ur fyra aspekter: vilken funktion det har; hur det sitter ihop med andra objekt; var det finns; vilken typ de tillhör. Detta görs genom att varje led i den sammansatta koden får ett löpnummer som ger det en unik identitet. Se vidare [RDS-manual för CoClass](https://tillampningsanvisningarbim.kravportal.se/tillampningsanvisningar_bim/rds-manual-coclass/).

Exempel: *-J1.JJ2.RNB3/%RNB32/++BAB212* visar *Luftbehandlingssystem nr 1 \> Luftdistributionssystem nr 2 \> Luftdon nr 3 /* av typ *Tilluftsdon /* som sitter i *Mötesrum nr 212.*

Tack vare referensbeteckningar kan man på ett flexibelt sätt:

-   göra detaljerade mängdförteckningar ner på individuell nivå
-   länka data om ett specifikt objekt lagrade på olika ställen
-   spåra enskilda objekt över hela livscykeln: vilka krav ställdes på det; var ska det installeras; hur blev det byggt; hur fungerar det; hur ofta behöver det underhållas; var finns det; när går garantitiden ut.

### **3 Upphandling**

Beroende på upphandlingsform kan entreprenören använda informationsmodellen i förfrågningsunderlaget direkt, eller efter egna kompletteringar. Arbetet med kalkylering underlättas i hög grad om entreprenören använder standardlösningar – ”byggdelsrecept” – som också använder CoClass. På det sättet blir det enkelt att möta beställarens krav med sina tekniska lösningar som innehåller data om kostnad, arbete och tidsåtgång. Läs mer i [Kostnadskalkylering med CoClass](https://tillampningsanvisningarbim.kravportal.se/?page_id=60810&preview=true).

### **4 Produktion**

Också produktionen kan underlättas med en konsekvent användning av CoClass. Om alla relevanta objekt är klassificerade och försedda med referensbeteckningar kan logistik och produktionsplanering anpassas till det aktuella projektet. Mängder kan erhållas från CAD-modeller, och till exempel visa vilka material och mängder som behövs i ett visst utrymme i form av installationer, beklädnader, ytskikt och inredning. Totala mängder för en viss produktionsetapp kan också tas fram.

### **5 Användning**

För underhållsplanering gäller i stället att kraven ska kopplas till de objekt som ska driftas och underhållas. Även här kan CoClass Studio användas för att skapa en informationsstruktur, där objekten kopplas till önskade underhållsåtgärder. Om strukturen redan är skapad i projekteringsskedet – där objekten beskrevs för produktion – blir merarbetet litet att komplettera med aktiviteter för drift och underhåll. Se Bild 1.

![Underhåll i CoClass studio](../1.introduktion/media/underhall_i_coclass_studio.png)

*Bild 1: Enkel struktur i CoClass Studio, där underhållsmålning i ett utrymme kravställs.*

### **6 Avveckling**

Även under avvecklingsskedet kan en välstrukturerad informationsmodell som underlag för mängdförteckningar vara till stor nytta. Objekt som är försedda med egenskaper som beskriver materialinnehåll, deponiklass och annat som är relevant för byggdelarnas omhändertagande underlättar i hög grad planering och genomförande av en rivning.