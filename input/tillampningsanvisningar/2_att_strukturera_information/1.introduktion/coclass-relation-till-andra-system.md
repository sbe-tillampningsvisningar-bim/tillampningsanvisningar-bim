# CoClass relation till andra system

I detta dokument beskrivs relationen mellan CoClass och:

-   ISO 12006-2
-   SfB, BSAB 83 och SBEF
-   BSAB 96
-   IEC/ISO 81346
-   Aff
-   RealEstate Core
-   Boverkets indelning för klimatdeklaration
-   Typbeteckningar (BIP-koder)
-   IFC

Syftet med dokumentet är inte att argumentera för eller emot de olika sätten att identifiera objekt i den byggda miljön, utan att visa deras syfte och hur de kompletterar varandra.

## Systematik för byggd miljö

För att vara praktiskt användbar måste systematik dels vara specifik för ett ämnesområde, dels ha ett tydligt syfte. Systematiken bygger alltid på någon typ av *klassifikation*, det vill säga att man indelar ämnesområdet i avgränsade grupper av objekt.

I sin enklaste form är klassifikationen *enumerativ* – ”uppräknande” – i form av en förteckning över alla objekt av intresse. Ett exempel är byggvaruhandelns system BK 04, som innehåller en enkel men omfattande gruppering av byggvaror och verktyg.

Den andra metoden är att man klassificerar objekt baserat på en eller flera egenskaper och därmed åstadkommer en *facetterad* klassifikation, som alltså definierar klasser utifrån ett visst perspektiv. Ett välkänt exempel är Linnés system för att indela växter baserat på deras metod för fortplantning.

I båda fallen är målet att klassifikationen är komplett: alla objekt av intresse ska vara klassificerade; ett visst objekt får bara finnas i en enda klass; klasser som innehåller ”övrigt” undviks. Detta är dock svårt att undvika i enumerativa system.

## ISO 12006-2

**ISO 12006-2** är den grundläggande standarden för byggklassifikation, som övergripande definierar indelningsgrunderna för de olika klassifikationstabellerna. Den har sina rötter i det gamla svenska SfB-systemet, och används i dag i olika klassifikationssystem över hela världen.

Standarden kom i en första version 2002. Den fastställde en gradvis nedbrytning av byggd miljö i ett antal tabeller, och kom till användning i nationella system som Uniclass, Omniclass, Uniformat och BSAB 96.

Den nuvarande versionen av standarden är från 2015. Jämfört med den tidigare från 2002 finns här ett uttalat systemtänkande, där byggnadsverk byggs upp av byggdelar i form av hierarkiska system. Dessa kan beskrivas som helheter, eller brytas ner till delar av delar ner till den nivå som passar den aktuella situationen. Nedbrytningen görs primärt baserat på objektens funktion, vilket möjliggör flexibla strukturella nedbrytningar som i CoClass.

I introduktionen av 12006-2:2015 beskrivs behovet av gemensam klassifikation för alla parter i bygg- och förvaltningsprocessen. Fokus är tydligt:

-   *att hantera information om de fysiska byggobjekt som utgör beståndsdelar i den byggda miljön.*

Denna avgränsning är viktig att göra. Den byggda miljön skapas för att tillfredsställa en mängd olika behov: boende, rekreation, produktion av tjänster och varor, sjuk- och hälsovård, transporter och mycket annat. *Alla dessa typer av aktiviteter och processer som ingår i skilda verksamheter omfattas inte av standarden, och därmed inte av dess tillämpningar.*

Följande grundförutsättningar gäller:

-   Verksamhetens krav utgör input till bygg- och förvaltningsprocessen.
-   ISO 12006-2 omfattar enbart *den fysiska realiseringen av verksamhetens krav*.

Standarden syftar uttryckligen till att bli ett medel för digital byggnadsinformationsmodellering (BIM), där alla parter använder samma klassifikation av objekten.

Standarden bryter ner beskrivningen av den byggda miljön i ett antal föreslagna tabeller. Grunden är verksamheter som behöver **utrymmen**. Dessa realiseras av **byggresultat** i form av **byggdelar**. Dessa kan aggregeras i **byggnadsverk**, som i sin tur samlas i **byggnadsverkskomplex**. Byggresultat skapas av **resurser** i form av **varor**, **hjälpmedel**, **information** och **aktörer**.

Byggresultaten kan betraktas som ett **arbetsresultat**, som utgör kombinationen av de resurser som används i det specifika fallet. Exempel:

-   En projektör använder sitt kunnande, information om ställda krav, en dator och en programvara för att skapa ett digitalt arbetsresultat i form av en CAD-modell.
-   En entreprenör använder sitt kunnande, information om ställda krav, byggvaror, verktyg och maskiner för att skapa ett fysiskt arbetsresultat. Ett sådant benämns **produktionsresultat**.

Klasser av objekt kan skapas baserat på olika aspekter, till exempel funktion, form och/eller läge. ”Form” ska ses i vid mening, omfattande även konstruktion – det vill säga teknisk lösning – inklusive material. ISO 12006-2 innehåller i sig inga kompletta tabeller med sina klasser, men ger exempel på klasser i samtliga typer av objekt som ingår i schemat.

De fysiska delarna av den byggda miljön används för att skapa och utrusta de utrymmen som en verksamhet kräver. ISO 12006-2 skiljer på:

**utrymme**  
avgränsad tredimensionell utsträckning definierad konkret eller abstrakt

**byggt utrymme**  
*utrymme* definierat av byggd eller naturgiven miljö eller båda, avsett för brukaraktivitet eller utrustning  
Not 1: Ett byggt utrymme är, till exempel, ett rum definierat av golv, tak och väggar, eller en gångstig eller kraftledningsgata definierad av omgivande skog.

**aktivitetsutrymme**  
*utrymme* definierat av den rumsliga utsträckningen av en aktivitet  
Not 1: En rumslig utsträckning av en aktivitet, till exempel ett bord eller en säng, och aktivitetsutrymmet runt dem.

Ett utmärkande drag i 12006-2 är fokuseringen på *systemtänkande*. De föreslagna tabellerna är relaterade till varandra, som en serie av system och sub-system.

**byggsystem**  
interagerande *byggobjekt* organiserade för att uppnå ett eller flera syften

Standarden betonar att alla objekt kan betraktas som **system**. Systemtänkandet syftar till att låta den som utformar och sköter delar av den byggda miljön att tänka helheter snarare än delar, och hur helheterna förhåller sig till varandra. Delarna väljs för att tillsammans med andra delar uppfylla de krav på funktion(er) som ställs på helheten.

Exempel: om krav ställs att ett dagvattensystem ska minimera inflödet till ett kommunalt reningsverk behöver delar i form av buffrande dammar och infiltrationsbäddar ingå i systemet.

**Egenskaper** ska enligt 12006-2 också betraktas som objekt, och ska därför ”hanteras som en speciell klass som genomsyrar resultat, processer och resurser”. Ett digitalt objekt är enbart en platshållare för egenskaper som beskriver ett verkligt eller tänkt objekt i världen. Objektets klass är en av dessa egenskaper.

Egenskaper kan också ha egenskaper, till exempel värdetyp (text, numeriskt med flera), definition, källa, noggrannhet och aktualitet.

## SfB, BSAB 83 och SBEF

ISO 12006-2 var ursprungligen alltså inspirerat av den svenska SfB-system, som presenterades av Svensk Byggtjänst i boken ByggAMA 1950. Den grundläggande klassifikationsprincipen kan sägas vara baserad på när arbeten utförs produktionsskedet: först ställer man i ordning mark och grund, sedan husets stomme, därefter kompletterar man stommen, och så vidare. På den översta nivån är strukturen så här:

| Klass | Benämning                    |
|-------|------------------------------|
| 1     | Terräng och undergrund       |
| 2     | Råbyggnader                  |
| 3     | Kompletteringar              |
| 4     | Ytskikt                      |
| 5     | VVS-anläggningar             |
| 6     | El-och transportanläggningar |
| 7     | Rumskompletteringar          |
| 8     | Inredningar                  |

*Tabell 1: Huvudklasser i SfB-systemet.*

Varje huvudnivå innehåller sedan subklasser, exempelvis:

| 2  | Råbyggnader                    |
|----|--------------------------------|
| 20 | Råbyggnader, tomt              |
| 21 | Ytterväggar                    |
| 22 | Innerväggar                    |
| 23 | Bjälklag                       |
| 24 | Trappor                        |
| 26 | Terrasstak                     |
| 27 | Yttertak                       |
| 28 | Övriga råbyggnader, hus        |
| 29 | Råbyggnader, hus [Summa 21–28] |

*Tabell 2: Exempel på subklasser i SfB-systemet.*

Nästa steg i utvecklingen var SBEF-systemet...

![SBEF-systemet](../1.introduktion/media/sbef.png)

## BSAB 96

BSAB 96 är den senaste versionen av en lång svensk tradition inom byggklassifikation. Dess föregångare SfB-systemet, BSAB 72 och BSAB 83 lever vidare, både internationellt och i Sverige. Dessa svenska system låg till grund för principerna i ISO 12006-2. BSAB 96 är alltså en tillämpning av den första versionen av denna standard, medan tabellerna är unika för systemet. Systemet ägs och förvaltas av Svensk Byggtjänst. För vidare läsning om BSAB 96, se [2].

En viktig skillnad mellan BSAB och dess efterföljare CoClass är grunden för indelning i klasser. CoClass är ett facetterat system som bygger helt på den strikt funktionella synen i IEC/ISO 81346, medan BSAB är enumerativ. Här listas alla objekt av intresse, och för de flesta teknikområden finns också en avslutande klass ”övrigt” eller ”med mera”.

BSAB 96 innehåller följande tabeller:

| **Tabell**                | **Kommentar**                                                                                                    |
|---------------------------|------------------------------------------------------------------------------------------------------------------|
| Infrastrukturella enheter | Motsvarar Byggnadsverkskomplex i CoClass. Tabellen är ej fastställd.                                             |
| Byggnadsverk              | Används inte i några andra tjänster hos Svensk Byggtjänst.                                                       |
| Utrymmen                  | Relativt komplett för anläggning, rudimentär för hus. Används inte i några andra tjänster hos Svensk Byggtjänst. |
| Byggdelar                 | Klassificerar endast system, inte komponenter. Används i AMA.                                                    |
| Produktionsresultat       | Gemensam med CoClass. Används i AMA.                                                                             |

*Tabell 3: Tabeller i BSAB 96.*

BSAB 96 i form av byggdelar och produktionsresultat används i dag främst i AMA, men är också frekvent använt för att klassificera CAD-objekt. Till det är systemet klart underlägset CoClass eftersom byggdelar endast finns på systemnivån. På grund av detta används ofta produktionsresultat för att beskriva objekt i CAD-filer, trots att koderna visar på en koppling till föreskrifter om material och utförande i AMA.

Tabellen för produktionsresultaten har en struktur som är anpassad för syftet att ställa krav på utförande. På högre nivå finns samlande rubriker som exempelvis DC Marköverbyggnader m m. För att hitta ett slitlager på en väg – som i CoClass finns som NCA Markbeläggning – behöver man gå ner ända till DCC.14 Bitumenbundna slitlager kategori A. En kod som neutralt klassificerar objektet i fråga finns helt enkelt inte.

Tabellerna för byggnadsverk och utrymmen används inte i några kända verktyg, men utrymmestabellen har använts i vissa projekt.

Klasserna i BSAB 96 namnges i plural: Vägar på mark, Överbyggnader för väg och plan, Markbeläggningar, vilket visar syftet att föreskriva utförande för typer av objekt.

## IEC/ISO 81346

**IEC/ISO 81346** är en serie standarder som har två syften: *klassifikation* av funktionella objekt och regler för *referensbeteckningar* som identifierar och beskriver objekten utifrån olika aspekter (funktion, sammansättning, lokalisering och typ).

Serien är alltså gemensam för IEC (*International Electrotechnical Commission*) och ISO (*International Organization for Standardization*), det vill säga elektrotekniska respektive ”övriga” standarder. Serien hette från början IEC 61346. Den har sitt ursprung i en tysk klassifikation för industriell produktion, med speciellt fokus på kraftindustri, som utvecklades i slutet av 1960-talet.

De dubbla syftena klassifikation och referensbeteckningar har resulterat i att indelningen av klasser så långt som möjligt baseras på funktion, inte på teknisk lösning. Målet är att varje funktionellt objekt som behövs i en byggnad eller anläggning ska kunna identifieras under hela dess livscykel, från design och produktion till användning och till slut avveckling.

| **Del** | **Innehåll**                                                                                                                                         | **Kommentar**                                                                     |
|---------|------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------|
| Del 1   | Regelverket för klassifikation och referensbeteckningar.                                                                                             | Generell för alla teknikområden.                                                  |
| Del 2   | Klasser för utrymmen och ”klasser av objekt med samma inneboende funktion” &sup1;.                                                                     | Generell för alla teknikområden.                                                  |
| Del 10  | Klasser för funktionella system och tekniska system för kraftverksanläggningar samt generella klasser för byggnadsverkskomplex och byggnadsverk&sup2;. | Används ej inom bygg, men kommer troligen få genomslag i svenska kraftsammanhang. |
| Del 12  | Klasser för funktionella system och konstruktiva&sup3; system för bygg och anläggning.                                                                 |                                                                                   |


*Tabell 4: Tabeller i IEC/ISO 81346.*

Även om det inte uttrycks formellt kan Del 2 och Del 12 av IEC/ISO 81346 sägas vara tillämpningar av ISO 12006-2:2015. Funktionella system, konstruktiva system och komponenter kan alla sägas vara typer av byggdelar, som tillsammans skapar utrymmen och byggnadsverk.

Referensbeteckningar beskrivs på djupet i [RDS-manual för CoClass](https://tillampningsanvisningarbim.kravportal.se/tillampningsanvisningar_bim/rds-manual-coclass/).

---
&sup1; Klasserna för objekt med viss inneboende funktion används i CoClass med benämningen ”komponent” för att visa att de utgör delar av ett system. De kan dock användas för att klassificera komplexa funktioner i form av system eller ”komponentsystem”.

&sup2; Tabellerna för byggnadsverkskomplex och byggnadsverk togs fram av Klas Eckerberg för användning inom CCIC (Construction Classification International Cooperation). De infördes därefter IEC/ISO 81346-10, och är nu under införande i IEC/ISO 81346-2.

&sup3; I 81346-12 benämns tabellen *technical systems*. I CoClass har detta översatts till *konstruktiva system* eftersom de omfattar konstruktiva delar av ett funktionellt system.

## CoClass

CoClass är den svenska tillämpningen av ISO 12006-2 och IEC/ISO 81346, och innehåller fler och utökade tabeller. De tabeller som baseras på IEC/ISO 81346 innehåller vissa tillägg av klasser, mindre justeringar av definitioner, och tillägg av ett stort antal generiska typer.

Syftet med CoClass är att skapa ett gemensamt språk för digital hantering av information för byggd miljö. Systemet ägs av en grupp organisationer, och förvaltas av Svensk Byggtjänst. CoClass innehåller följande tabeller:

| **Tabell**              | **Källa**           |
|-------------------------|---------------------|
| Byggnadsverkskomplex    | CoClass             |
| Byggnadsverk            | CoClass             |
| Utrymmen                | IEC/ISO 81346-2     |
| Funktionella system     | IEC/ISO 81346-12    |
| Konstruktiva system     | IEC/ISO 81346-12    |
| Komponenter             | IEC/ISO 81346-2     |
| Produktionsresultat     | CoClass och BSAB 96 |
| Material och resurser   | IVL och Boverket    |
| Egenskaper              | CoClass             |
| Aktiviteter             | CoClass             |
| Förvaltningsaktiviteter | CoClass             |

*Tabell 5: Tabeller i CoClass. Tabellen för Förvaltningsaktiviteter är ersatt med tabellen för Aktiviteter, men är fortfarande formellt en del av systemet.*

CoClass används i dag i CoClass Studio, i AMA Funktion, och i ökande grad för att klassificera objekt i CAD-modeller och andra tillämpningar för att klassificera och referensbeteckna objekt i produktion och i tillgångsförvaltning.

Klasserna i CoClass namnges i singular: Väg, Överbyggnad för väg och plan, Markbeläggning, vilket visar syftet att identifiera ett specifikt objekt.

Läs mer i [Introduktion till CoClass](https://tillampningsanvisningarbim.kravportal.se/tillampningsanvisningar_bim/introduktion-till-coclass/).

## Aff

Aff, eller Avtal för fastighetsförvaltning, är ett system som bygger på en gemensamt framtagen branschstandard för att underlätta avtal och upphandlingar inom fastighetsförvaltning och fastighetsdrift. Det är utformat för att skapa tydliga och rättvisa avtal mellan beställare och leverantörer, vilket minskar risken för diskussioner och tvister.

Aff används genom ett webbaserat avtalssystem som inkluderar branschregelverket ABFF, Aff-definitioner, och mallar för att skapa kompletta avtal. Systemet är anpassat för både stora och små företag och syftar till att förbättra beställarens förmåga att ställa krav och säkerställa att avtalade tjänster levereras korrekt.

Aff ges ut av den ideella branschföreningen Aff Forum, som består av olika parter inom branschen. I Aff finns definitioner både på tjänster och objekt.

Strukturen i Aff är uppbyggd på tio tjänsteområden betecknade SA till SJ, där SC Fastighetsteknik, SD Utemiljö och SG Säkerhet är vanligast för underhåll. Varje tjänsteområde har numrerade underrubriker i upp till fyra nivåer. Exempel:

-   SC Fastighetsteknik

    SC4 VA-, VVS-, KYL- OCH PROCESSMEDIESYSTEM

    SC4.6 VÄRMESYSTEM I BYGGNAD

SC4.6.3 Värmedistributionsinstallationer

Krav på underhållsåtgärder kan anges i fritext under valfri rubriknivå ovan, beroende på hur detaljerad beskrivningen behöver vara.

En mappning mellan CoClass och Aff finns framtagen, som gör det enkelt att överföra data från projektering till förvaltning.

Se mer om Aff och CoClass i [Relation mellan CoClass och AFF – Nationella Riktlinjer (nationella-riktlinjer.se)](https://www.nationella-riktlinjer.se/innehall/metoder/relation-mellan-coclass-och-aff/) och [Digitalisering – Aff, CoClass, BIM, BIP, hjälp! - Förvaltarforum (forvaltarforum.se)](https://forvaltarforum.se/2019/06/14/digitalisering-aff-coclass-bim-bip-hjalp/).

## RealEstate Core

RealEstate Core (REC) är en öppen och standardiserad informationsmodell som syftar till att förbättra hanteringen av fastighetsdata genom att erbjuda en gemensam struktur och terminologi. Den är speciellt utvecklad för att underlätta digitalisering och integration av olika system inom fastighetssektorn, inklusive byggnader, fastighetssystem och smarta lösningar. REC riktar sig till fastighetsägare, systemleverantörer och utvecklare som vill skapa en sammanhängande och effektiv informationshantering inom fastigheter.

![Real Estate Core](../1.introduktion/media/rec.png)

*Bild 1: Övergripande schema för REC.*

Målet med REC är att knyta ihop data från olika världar:

-   digitala modeller över byggnader
-   byggnadens drift och underhåll
-   sakernas internet (IoT)
-   affärsdata för processer och överenskommelser.

De digitala byggnadsmodellerna kan klassificeras på valfritt sätt, till exempel med CoClass eller BSAB 96. En del objekt finns dock beskrivna i REC, till exempel byggnad, våningsplan, utrymme, system och en rad typer av byggdelar. Vid behov är en mappning mellan systemen enkel att göra.

## Boverkets indelning för klimatdeklaration av byggnader

Sedan början av 2022 ska [vissa husbyggnadsprojekt](https://www.boverket.se/sv/klimatdeklaration/vilka-byggnader/ska-deklareras/) lämna in en klimatdeklaration i samband med att slutbesked erhållits från kommunen. Klimatdeklarationen ska göras i en e-tjänst hos den ansvariga myndigheten Boverket. Här samlas alla uppgifter i ett klimatdeklarationsregister, som kommer att bli ett viktigt underlag för kommande gränsvärden.

Som underlag för deklarationen behövs två datakällor: mängduppgifter på allt som ingår, och klimatberäkningar för de material som används.

Boverket har beslutat att följande byggdelar ska ingå i beräkningarna:

-   Bärande delar i grundläggning, inklusive isolering.
-   Övriga bärande delar: stomme, bjälklag, trappor, balkonger med mera.
-   ”Klimatskärm”: ytterväggar, ytterdörrar, fönster, yttertak.
-   Innerväggar, innerdörrar.

![Illustration av Infab](../1.introduktion/media/infab.svg)

*Bild 2: Illustration av Infab, källa [Boverket](https://www.boverket.se/sv/klimatdeklaration/gor-sa-har/byggdelar-som-ingar/).*

För praktisk tillämpning kan informationsmodeller över byggnaden – till exempel i form av en CAD-modell – som innehåller klassificerade objekt mappas mot Boverkets indelning. Hos Svensk Byggtjänst finns en översättningstabell baserad på CoClass.

Läs mer här: [Klimatdeklaration med CoClass](https://tillampningsanvisningarbim.kravportal.se/granskning-1/klimatdeklaration-med-coclass/)

## Typbeteckningar (BIP-koder)

I takt med att projekteringen blir alltmer detaljerad och närmar sig inköp och produktion så ökar behovet att specificera **typer av objekt**, ända ner till nivån artikel från tillverkare. Sådana typbeteckningar är viktiga som komplement till klasser och generiska typer i CoClass.

Här kan beteckningar enligt BIP vara en bra utgångspunkt. Dessa bygger delvis på svensk standard, delvis på praktiska erfarenheter i projekt. De omfattar både komponenter – benämnda ”produkter” – och system. BIP-koder ägs och förvaltas av föreningen BIM Alliance.

![BIP-koder](../1.introduktion/media/bip-koder.png)

*Bild 3: Exempel på BIP-koder. Typer av tilluftsdon betecknas med TD1-TD9, och ges löpnummer (xx).*

BIP-koderna är mappade mot produktionsresultat i BSAB 96 och CoClass (kolumn BSABwr) och mot byggdelar i BSAB 96 (BSABe). Den senare innehåller många luckor, eftersom byggdelar i BSAB 96 endast finns på systemnivå. En mappning mot CoClass tabell för komponenter finns framtagen, men är ännu inte publicerad.

[BIP-koderna finns här](https://www.bipkoder.se/#/).

## IFC

IFC (Industry Foundation Classes) är ett dataschema för att beskriva byggd miljö, och ett filformat för överföring av data mellan olika CAD-system. Det är framtaget och utvecklas kontinuerligt av buildingSMART International, och delvis antagen som formell standard i form av SS-EN ISO 16739-1:2020.

Syftet med IFC är att standardisera överföringen av data i CAD-modeller, primärt för byggnader men numera också för infrastruktur. Formatet innehåller generiska klasser av objekt, som kan beskrivas i detalj med hjälp av egenskapsuppsättningar (*property sets*) för varje klass.

Förutom att beskriva objektets fysiska och funktionella egenskaper kan de beskrivas med hjälp av en med hjälp av en grupp klassifikationsegenskaper. Man kan också skapa egna egenskapsuppsättning: ett *Custom Property Set*. I ett sådant kan man lagra till exempel klasskoder för CoClass och BSAB, inklusive fullständiga referensbeteckningar om CAD-programmet klarar att skapa sådana.

Alla objekt kan också ha attributen *Name, Description, LongName* och *ObjectType* som ger användaren möjlighet att beskriva dem i detalj.

Följande objekt finns definierade i IFC:

| **Typ av objekt**    | **Kommentar**                                                                                                                                                                                                                |
|----------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Byggnadsverkskomplex | Saknas                                                                                                                                                                                                                       |
| Byggnadsverk         | Finns endast som *IfcBuilding*, *IfcRoad*, *IfcRailway*, *IfcTunnel* och *IfcMarineFacility*.                                                                                                                                |
| Utrymmen             | Finns endast som *IfcSpace*, med beskrivning via *IfcSpaceType*. Förinställa typer finns i egenskapen *IfcSpaceTypeEnum: SPACE*, *PARKING*, *GFA* (*Gross Floor Area*), *INTERNAL*, *EXTERNAL*, *USERDEFINED*, *NOTDEFINED*. |
| Funktionella system  | Finns endast som *IfcSystem* och *IfcBuildingSystem*.                                                                                                                                                                        |
| Konstruktiva system  | Finns några, till exempel *IfcBuildingElementPart*, *IfcCaissonFoundation*, *IfcDeepFoundation*, *IfcPavement*.                                                                                                              |
| Komponenter          | Många, men huvudsakligen generiska typ *IfcBuildingElementPart*, *IfcController*, *IfcCovering*, *IfcPile*, *IfcFooting*.                                                                                                    |

*Tabell 6: Exempel på objekttyper i IFC som är relevanta för anläggning.*

Utöver att beskriva fysiska företeelser kan IFC hantera relationer, prestanda, kostnader, installation, drift och aktörer.

IFC finns i flera utgåvor, där den senaste versionen IFC 4.3 framför allt kompletterades med geometrier, objekt och egenskaper för infrastruktur.

För att möjliggöra samarbete kring CAD-modeller finns en metod att skapa specifika utdrag ur en IFC-fil kallad MVD (*Model View Definition*). Dessa kan användas för koordinering mellan teknikområden (*Coordination view*); för väg- och järnvägsprojekt (*Alignment based Reference View*); för design (*Design transfer View*).

En MVD kan kompletteras med maskinläsbara krav på informationsleveransen: en IdS (*Information Delivery Specification*).

För att utväxla information om ett specifikt ärende eller problem finns formatet BCF (*BIM Collaboration Format*). Det överför en beskrivning av en specifik vy av modellen, och vilka objekt som är relevanta att betrakta.

## Systematik i CAD-programvaror

Alla CAD-program behöver någon metod för att särskilja olika typer av objekt. Den enklaste metoden är att använda unika ”lager” eller ”skikt” som döps efter innehållet. Objekten kan sedan modelleras som linjer i 2D eller 3D, som polygoner i 2D eller 3D, eller som 3-dimensionella objekt i form av ytor eller solida kroppar.

Enligt den internationella standarden för CAD-lager (SS-EN ISO 13567-2:2017) ska lämplig ”nationell tabell” användas för att namnge objekten. Här har man sex tecken på sig att ange kod. Svensk tillämpning av standarden finns i handboken SB 11 från Svensk Byggtjänst. I den senaste utgåvan rekommenderas CoClass tabeller.

Modern objektorienterad modellering utgår dock från att objekten förses med en uppsättning egenskaper. En sådan beskriver CAD-programmets interna klass eller typ: ett bjälklag, ett fönster, en belysningsstolpe, en överbyggnad. Om projektören saknar en objekttyp får man kompromissa och till exempel modellera överbyggnader på mark med objektet bjälklag.

I de fall data behöver delas med andra kan en konvertering krävas. De programvaror som används i dag för 3D-modellering löser detta genom att exportera IFC-filer.&#x2074; Förutom att beskriva geometrierna behöver objekten få korrekt klass. Detta kräver ett ett-till-ett-förhållande mellan CAD-programmet och IFC.

Oavsett om detta är möjligt eller inte ökar säkerheten om CAD-objekten förses med en eller flera kompletterande klassifikationer. Dessa ska sedan föras vidare till eventuell IFC-export. Många svenska CAD-applikationer kan i dag ange BSAB-klass på objekt, och flera har börjat ange också motsvarande CoClass-klass.

Andra egenskaper i CAD-filen som programvaran eller användaren lagt till behöver också föras över till IFC-filen. Detta kräver noggranna inställningar i den exporterande programvaran.

---
&#x2074; För lagerbaserad 2D-CAD används dock ofta AutoCAD:s filformat dwg.

## Jämförelse CoClass–BSAB 96–IFC

CoClass, BSAB 96 och IFC syftar alla till att beskriva byggd miljö, men gör det på olika sätt och med olika syften.

Tabellen nedan visar omfattningen av innehållet i de tre systemen:

| **Typ av objekt**    | **CoClass**            | **BSAB 96**                                  | **IFC**                            |
|----------------------|------------------------|----------------------------------------------|------------------------------------|
| Byggnadsverkskomplex | Komplett               | Ej komplett, ej fastställd                   | Saknas                             |
| Byggnadsverk         | Komplett               | Komplett för anläggning, rudimentärt för hus | Rudimentär                         |
| Utrymmen             | Komplett               | Komplett                                     | Rudimentär                         |
| Byggdelar            | System och komponenter | Endast system                                | Blandat, huvudsakligen komponenter |
| Produktionsresultat  | Komplett               | Komplett                                     | Saknas                             |

*Tabell 7: Innehållet i CoClass, BSAB 96 och IFC.*

Några exempel på objektklasser:

| **CoClass**                           | **BSAB 96**                               | **IFC**                                                        |
|---------------------------------------|-------------------------------------------|----------------------------------------------------------------|
| CAH Väg                               | DCB/B Vägar för biltrafik                 | *IfcRoad*                                                      |
| FA\# Utrymme för vägfordon            | 111 Utrymmen för biltrafik                | *IfcSpacetype: EXTERNAL*; *ObjectType*: Utrymme för biltrafik  |
| Q13 Allmänbelysningssystem för trafik | 63.FCB Belysningssystem vid väg           | *IfcDistributionCircuit; ObjectType*: Belysningssystem vid väg |
| CB Överbyggnad för väg och plan       | 31.B Överbyggnader för väg och plan       | *IfcRoadPart; ObjectType:* Överbyggnad                         |
| NCA Markbeläggning                    | DCC.14 Bitumenbundna slitlager kategori A | *IfcCourse; ObjectType:* Bitumenbundet slitlager               |

*Tabell 8: Jämförelse mellan objektbeskrivningar i CoClass, BSAB 96 och IFC. Objekttyper finns inte fastställda i IFC, utan måste bestämmas av projektören. Exemplen är i detta fall föreslagna av författaren.*

En avgörande skillnad mellan CoClass och BSAB 96 är att det senare inte klarar att beskriva objektens relation till andra objekt. BSAB 96 innehåller också färre klasser, och saknar helt byggdelar på den nivå som i CoClass benämns komponenter. BSAB 96 saknar också möjligheten att identifiera enskilda förekomster av objekt; målet är i stället att beskriva *alla* objekt som ingår i en viss klass.