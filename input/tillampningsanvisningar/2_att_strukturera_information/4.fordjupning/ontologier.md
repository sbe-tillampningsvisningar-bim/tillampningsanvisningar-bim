# Ontologier - gemensam begreppsapparat för standarder

## Inledning

SBE-projektet *Tillämpningsanvisningar BIM* utgår från följande internationella standarder:

-   ISO 12006-2:2020, som är den grundläggande standarden för byggklassifikation.
-   IEC/ISO 81346-serien, som innehåller regler för hur man identifierar objekt och ger klasser för utrymmen och byggdelar. Här ingår den svenska tillämpningen CoClass.
-   ISO 19650-serien, som beskriver olika aspekter av byggnadsinformationsmodellering (BIM).
-   ISO 16739, IFC (*Industry Foundation Classes*), som är ett överföringsformat för geometrier och annan data till och från CAD-system.

Dessa standarder saknar till viss del en gemensam begreppsapparat; det som på fackspråk kan benämnas ”semantisk interoperabilitet”. Genom att skapa en sådan underlättas effektiv samverkan mellan standarderna och ökar möjligheten att tillämpa dem på ett konsekvent sätt.

## Metod

Som dokumentationsmetod används en beskrivningslogik (*Description Logic*, DL) i form av en logisk formalism för ontologier tillsammans med den semantiska webben och dess språk (*Web Ontology Language*, OWL).

Standarderna definierades i form av OWL2, medan OWL användes för att definiera relationer mellan klasser och relationerna i standarderna. Genom att använda en ”*Upper Ontology*” kan klasser och relationer relateras mot denna generella ontologi, och klasser kan relateras till varandra för att specificera att de är samma koncept men med olika benämningar.

### Ontologi

En ontologi är en systematisk och logisk beskrivning av något som existerar, och hur dess delar relaterar till varandra. Ontologier kan användas för att representera olika typer av entiteter, till exempel fysiska objekt, abstrakta begrepp, relationer, händelser och egenskaper. En ontologi kan också användas för att definiera de grundläggande kategorierna och principerna för ett visst kunskapsområde eller vetenskaplig disciplin.

Nedanstående begrepp är centrala i samband med en ontologi:

-   **Ontologi**: en formell och explicit beskrivning av ett kunskapsområde, som består av termer, egenskaper, relationer och restriktioner som gäller för de entiteter som ingår i området.
-   **Klass**: en grupp av entiteter som delar gemensamma egenskaper eller karaktäristika. En klass kan ha subklasser och superklasser, som uttrycker delmängds- och övermängdsrelationer mellan klasser.
-   **Individ**: en specifik entitet som tillhör en eller flera klasser. En individ kan ha olika egenskaper och relationer till andra individer.
-   **Egenskap**: en binär relation mellan två entiteter, som uttrycker något om deras förhållande. Det finns två typer av egenskaper i OWL: objektegenskaper och datatypsegenskaper. Objektegenskaper relaterar individer till andra individer, medan datatypsegenskaper relaterar individer till värden av en viss datatyp, såsom strängar, tal eller datum.
-   **Restriktion**: ett villkor som begränsar de möjliga värdena eller kombinationerna av värden för en egenskap. Restriktioner kan användas för att definiera klasser genom att ange nödvändiga eller tillräckliga villkor för att tillhöra en klass. Restriktioner kan också användas för att ange kardinalitet, det vill säga hur många gånger en egenskap kan förekomma för en given individ.

### OWL

Det finns olika sätt att definiera en ontologi, beroende på syftet och användningsområdet. Ett vanligt sätt är att använda ett logiskt språk, såsom första ordningens logik eller beskrivningslogik, för att uttrycka terminologin och axiomatiken (studier av axiomen, alltså de grundläggande påståendena inom ett område). Ett annat sätt är att använda ett grafiskt notationssystem som OWL för att visualisera ontologins struktur och innehåll. Oavsett vilket sätt man väljer är det viktigt att en ontologi är väldefinierad, konsistent, fullständig och begriplig för de som ska använda den.

OWL har flera användningsområden inom ingenjörsvetenskap, till exempel:

-   Semantisk interoperabilitet, med målet att skapa gemensamma vokabulärer och ontologier som möjliggör utbyte och integration av data mellan olika källor och applikationer. Detta kan vara användbart för att samordna olika ingenjörsverktyg, plattformar, standarder och annat.
-   Kunskapsrepresentation och resonemang, i syfte att modellera olika typer av kunskap om fysiska objekt, processer, system, domäner och annat, och för att dra slutsatser om dem med hjälp av automatiserade slutsatser. Detta kan vara användbart till exempel för att lösa problem, verifiera design och optimera prestanda.
-   Informationsutvinning och sökning, för att komplettera data med semantisk information som gör det möjligt att söka och filtrera data baserat på deras innebörd och kontext. Detta kan vara användbart för att hitta relevant information från stora mängder data, såsom tekniska dokument, rapporter och artiklar.

## Använda standarder och tillhörande ontologier

Nedan beskrivs de standarder som används i tillämpningsanvisningarna. Vissa av dessa har publika OWL-representationer. Tillgängliga sådana har kvalitetssäkrats, medan övriga har tagits fram inom projektet.

### SS-EN ISO 12006-2:2020 Building construction – Organization of information about construction works – Part 2: Framework for classification

Denna standard är basen för klassifikationssystemen inom bygg och anläggningssektorn. Projektet har utvecklat en OWL representation av standarden, som ännu inte är kvalitetssäkrad.

![Ontologi 12006-2 OWL](../4.fordjupning/media/ont1.png)

[Länk till nuvarande 12006-2 OWL (browser)](https://service.tib.eu/webvowl/#iri=https://content.eurostep.com/hubfs/OWL/File1.owl)

### IEC 81346-1:2009 Industrial systems, installations and equipment and industrial products – Structuring principles and reference designations

Standarden finns i en ny version utgiven 2022, som är översatt i form av SS-EN IEC 81346-1, utg 2:2023 *Struktureringsprinciper och referensbeteckningar - Del 1: Grundläggande regler*.

Standarden har en publikt tillgänglig OWL-representation. Denna behöver dock valideras mot den nyligen utgivna versionen av 81346-1.

![Ontologi 81346-1 OWL](../4.fordjupning/media/ont2.png)

[Länk till nuvarande 81346-1 (näst senaste utgåva](https://service.tib.eu/webvowl/#iri=https://content.eurostep.com/hubfs/OWL/File2.owl)

### CoClass

CoClass är den svenska tillämpningen och utökningen av IEC/ISO 81346-1, -2 och -12. Ontologin för CoClass är under arbete och kommer att färdigutvecklas inom ramen för detta projekt.

![Ontologi CoCLass](../4.fordjupning/media/ont3.png)

[Länk till nuvarande CoClass ontologi](https://service.tib.eu/webvowl/#iri=https://content.eurostep.com/hubfs/OWL/File3.rdf)

### ISO 19650-1:2019 Organization and digitization of information about buildings and civil engineering works, including building information modelling (BIM) – Information management using building information modelling

Arbete har gjorts för att öka förståelsen av ISO 19650-1. Detta har resulterat i ett embryo som behöver slutföras och kvalitetssäkras.

![Ontologi ISO 19650](../4.fordjupning/media/ont4.png)

[Länk till initial version av ISO 19650-1 CDE OWL](https://service.tib.eu/webvowl/#iri=https://content.eurostep.com/hubfs/OWL/File4.owx)

### IFC 4.3

Industry Foundation Classes (IFC) är en öppen internationell standard för BIM-data.

Det finns en stor OWL ontologi för IFC 2x3 som kan användas för att förstå klasser och relationer i den senaste utgåvan av IFC (4.3). Denna innehåller nya infrastrukturklasser men basstrukturen i standarden är i stort den samma. Nedan del av IFC2x3 ontologin.

![Ontologi IFC OWL](../4.fordjupning/media/ont5.png)

[Länk till IFC 2x3 OWL](https://service.tib.eu/webvowl/#iri=https://content.eurostep.com/hubfs/OWL/File5.owl)

